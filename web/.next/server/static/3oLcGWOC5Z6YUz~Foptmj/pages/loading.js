module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 102);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ }),
/* 2 */,
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SHAPES = exports.COLORS = void 0;
var COLORS = {
  I: '#3cc7d6',
  O: '#fbb414',
  T: '#b04497',
  J: '#3993d0',
  L: '#ed652f',
  S: '#95c43d',
  Z: '#e84138'
};
exports.COLORS = COLORS;
var SHAPES = {
  I: [[0, 0, 0, 0], [1, 1, 1, 1], [0, 0, 0, 0], [0, 0, 0, 0]],
  O: [[1, 1], [1, 1]],
  T: [[0, 1, 0], [1, 1, 1], [0, 0, 0]],
  J: [[1, 0, 0], [1, 1, 1], [0, 0, 0]],
  L: [[0, 0, 1], [1, 1, 1], [0, 0, 0]],
  S: [[0, 1, 1], [1, 1, 0], [0, 0, 0]],
  Z: [[1, 1, 0], [0, 1, 1], [0, 0, 0]]
};
exports.SHAPES = SHAPES;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.gameReducer = gameReducer;
exports.gameJoinedReducer = gameJoinedReducer;
exports.getBlankGame = getBlankGame;
exports.getBlankPlayer = getBlankPlayer;
exports.getBlankPlayerRound = getBlankPlayerRound;
exports.getNextPlayerTetromino = getNextPlayerTetromino;
exports.stripGameEffects = stripGameEffects;
exports.isPlayer = isPlayer;
exports.getPlayer = getPlayer;
exports.getCurPlayer = getCurPlayer;
exports.getOtherPlayer = getOtherPlayer;
exports.allPlayersReady = allPlayersReady;
exports.addUserToGame = addUserToGame;
exports.updatePlayer = updatePlayer;
exports.getGameActionOffset = getGameActionOffset;

var _lodash = __webpack_require__(7);

var _grid4 = __webpack_require__(17);

var _tetromino = __webpack_require__(3);

var _tetromino2 = __webpack_require__(30);

var _grid5 = __webpack_require__(16);

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function gameReducer(state, action) {
  if (!state) {
    throw new Error("Game action ".concat(action.type, " called on void state"));
  }

  if (action.type === 'JOIN_GAME') {
    var _action$payload = action.payload,
        _actionId = _action$payload.actionId,
        _userId = _action$payload.userId,
        user = _action$payload.user;
    var players = state.players;

    if (players.length > 1) {
      if (isPlayer(state, user)) {
        console.warn("User ".concat(user.id, " tried to join game more than once"));
      } else {
        console.warn("User ".concat(user.id, " tried to join already full game"));
      }

      return state;
    }

    var _players = _slicedToArray(players, 1),
        player1 = _players[0];

    var game = updatePlayer(state, player1.user.id, {
      // Stop player1's game when player2 arrives
      status: 'PENDING',
      // Previous losses are irrelevant to 1vs1 game
      losses: 0
    });
    return bumpActionId(addUserToGame(game, user), _userId, _actionId);
  } // Ensure action consistency


  var _action$payload2 = action.payload,
      actionId = _action$payload2.actionId,
      userId = _action$payload2.userId;
  var offset = getGameActionOffset(state, action);

  if (offset > 0) {
    throw new Error("Refusing detached game action");
  }

  if (offset < 0) {
    console.warn("Past game action ".concat(actionId, " ignored (").concat(offset, "ms delta)"));
    return state;
  }

  var newState = gameJoinedReducer(state, action); // Don't bump player.lastActionId if action left state intact. This allows us
  // to avoid broacasting "noop" actions and minimize network activity
  // FIXME: Sometimes actions were recorded that seemed like "noop" actions when
  // played back from backfill response. Eg. Often an `ENABLE_ACCELERATION`
  // action would be recorded and then when played back it followed a state
  // that had `player.dropAcceleration: true`, which made it noop and entered
  // an infinite loop of crashed backfills. Disabling this optimization until
  // I understand what is going on.

  return newState === state ? newState : bumpActionId(newState, userId, actionId);
}

function gameJoinedReducer(state, action) {
  var userId = action.payload.userId;

  switch (action.type) {
    case 'PLAYER_READY':
      {
        var _getPlayer = getPlayer(state, userId),
            prevStatus = _getPlayer.status;

        var game = updatePlayer(state, userId, {
          status: 'READY'
        }); // Reset game when all players are ready to (re)start

        if (allPlayersReady(game) && ( // This condition allows solo players to pause and resume
        // (by clicking on the "2p insert coin" button)
        game.players.length > 1 || prevStatus !== 'PAUSE')) {
          var id = game.id,
              players = game.players;
          var round = getGameRound(game);
          return _objectSpread({}, game, {
            players: players.map(function (player) {
              return _objectSpread({}, player, getBlankPlayerRound({
                gameId: id,
                round: round
              }));
            }),
            dropFrames: _grid4.DROP_FRAMES_DEFAULT
          });
        }

        return game;
      }

    case 'PLAYER_PAUSE':
      {
        if (state.players.length > 1) {
          throw new Error('Pausing multiplayer game not allowed');
        }

        return updatePlayer(state, userId, {
          status: 'PAUSE'
        });
      }

    case 'DROP':
      {
        var rows = action.payload.rows;
        var player = getPlayer(state, userId);
        var grid = player.grid,
            activeTetromino = player.activeTetromino,
            activeTetrominoGrid = player.activeTetrominoGrid,
            activeTetrominoPosition = player.activeTetrominoPosition,
            dropAcceleration = player.dropAcceleration,
            flashYay = player.flashYay,
            quake = player.quake;

        if (player.status === 'LOST') {
          console.warn("DROP action denied for loosing player");
          return state;
        } // Clear lines generated by a previous `APPEND_PENDING_BLOCKS` action
        // NOTE: Old functionality left for posterity
        // if (hasLines(grid)) {
        //   const { clearedGrid, rowsCleared } = clearLines(grid);
        //   const blocksCleared = getBlocksFromGridRows(grid, rowsCleared);
        //   const newState = updatePlayer(state, userId, {
        //     grid: clearedGrid,
        //     blocksCleared
        //   });
        //
        //   return rewardClearedBlocks(newState, userId);
        // }
        // Drop active Tetromino until it hits something


        var newPosition = {
          x: activeTetrominoPosition.x,
          y: activeTetrominoPosition.y + rows
        }; // New active Tetromino position is available, uneventful path

        if ((0, _grid5.isPositionAvailable)(grid, activeTetrominoGrid, newPosition)) {
          return updatePlayer(state, userId, {
            activeTetrominoPosition: newPosition
          });
        } // Active Tetromino has hit the ground
        // A big frame skip could cause the Tetromino to jump more than one row.
        // We need to ensure it ends up in the bottom-most one in case the jump
        // caused the Tetromino to land


        newPosition = (0, _grid5.getBottomMostPosition)(grid, activeTetrominoGrid, newPosition); // Game over when active Tetromino lands (partially) outside the well.
        // NOTE: This is not ideal because the landed Tetromino, even though it
        // doesn't fit when it lands, could cause one or more lines which
        // after cleared could make room for the entire Tetromino. To implement
        // this we would need to somehow re-apply the part of the active Tetromino
        // that didn't fit upon landing, after the lines have been cleared.

        if (newPosition.y < 0) {
          return _objectSpread({}, state, {
            players: state.players.map(function (player) {
              var newAttrs = // TODO: Only set LOST state. Allow for draw
              player.user.id === userId ? {
                status: 'LOST',
                losses: player.losses + 1
              } : {
                status: 'WON'
              };
              return _objectSpread({}, player, newAttrs);
            })
          });
        } // This is when the active Tetromino hits the bottom of the Well and can
        // no longer be controlled


        var newGrid = (0, _grid5.transferTetrominoToGrid)(player, activeTetrominoGrid, newPosition, _tetromino.COLORS[activeTetromino]);
        var newState = state;

        var _round = getGameRound(newState);

        var drops = player.drops + 1;
        newState = updatePlayer(newState, userId, _objectSpread({
          drops: drops,
          grid: newGrid
        }, getNextPlayerTetromino({
          gameId: state.id,
          round: _round,
          drops: drops
        }), {
          // Clear acceleration after dropping Tetromino. Sometimes the key
          // events would misbehave and acceleration would remain on even after
          // releasing DOWN key
          dropAcceleration: false
        }));

        if (!(0, _grid5.hasLines)(newGrid)) {
          return newState;
        }

        var _clearLines = (0, _grid5.clearLines)(newGrid),
            clearedGrid = _clearLines.clearedGrid,
            rowsCleared = _clearLines.rowsCleared;

        var blocksCleared = (0, _grid5.getBlocksFromGridRows)(newGrid, rowsCleared);
        newState = updatePlayer(newState, userId, {
          grid: clearedGrid,
          blocksCleared: blocksCleared,
          flashYay: altFlashClass(flashYay),
          quake: dropAcceleration ? altQuakeClass(quake, rowsCleared.length) : null
        });
        newState = rewardClearedBlocks(newState, userId); // Transfer blocks from cleared lines to enemy grid 😈
        // We reference the old grid, to get the blocks of the cleared lines
        // *without* the blocks added from the just transfered active Tetromino

        return sendClearedBlocksToEnemy(newState, userId, grid, rowsCleared);
      }

    case 'APPEND_PENDING_BLOCKS':
      {
        var _player = getPlayer(state, userId);

        var _grid = _player.grid,
            blocksPending = _player.blocksPending,
            _activeTetrominoGrid = _player.activeTetrominoGrid,
            _activeTetrominoPosition = _player.activeTetrominoPosition; // XXX: The appended blocks might result in trimming existing blocks, by
        // lifting them higher than the well permits. This is odd because it
        // "trims" some blocks

        var _newGrid = (0, _grid5.appendBlocksToGrid)(_grid, blocksPending); // Push active Tetromino up if necessary


        if ((0, _grid5.isPositionAvailable)(_newGrid, _activeTetrominoGrid, _activeTetrominoPosition)) {
          return updatePlayer(state, userId, {
            grid: _newGrid,
            blocksPending: []
          });
        } // Receiving rows of blocks from enemy might cause the active Tetromino
        // to overlap with the grid, so in some cases it will be pushed up
        // mid-drop to avoid collisions. The next DROP action will instantly
        // transfer active Tetromino to wall grid in these cases


        var _newPosition = (0, _grid5.getBottomMostPosition)(_newGrid, _activeTetrominoGrid, _activeTetrominoPosition);

        return updatePlayer(state, userId, {
          // The next `DROP` event will determine whether the well is full and
          // if the game is over or not
          grid: _newGrid,
          blocksPending: [],
          activeTetrominoPosition: _newPosition
        });
      }

    case 'MOVE_LEFT':
    case 'MOVE_RIGHT':
      {
        var direction = action.type === 'MOVE_LEFT' ? -1 : 1;

        var _player2 = getPlayer(state, userId);

        var _grid2 = _player2.grid,
            _activeTetrominoGrid2 = _player2.activeTetrominoGrid,
            _activeTetrominoPosition2 = _player2.activeTetrominoPosition;

        var _newPosition2 = _objectSpread({}, _activeTetrominoPosition2, {
          x: _activeTetrominoPosition2.x + direction
        }); // Attempting to move the Tetromino outside the Well bounds or over landed
        // Tetrominoes will be ignored


        if (!(0, _grid5.isPositionAvailable)(_grid2, _activeTetrominoGrid2, _newPosition2)) {
          return state;
        }

        return updatePlayer(state, userId, {
          activeTetrominoPosition: _newPosition2
        });
      }

    case 'ROTATE':
      {
        var _player3 = getPlayer(state, userId);

        var _grid3 = _player3.grid,
            _activeTetrominoGrid3 = _player3.activeTetrominoGrid,
            _activeTetrominoPosition3 = _player3.activeTetrominoPosition;

        var _newGrid2 = (0, _grid5.rotate)(_activeTetrominoGrid3); // If the rotation causes the active Tetromino to go outside of the
        // Well bounds, its position will be adjusted to fit inside


        var _newPosition3 = (0, _grid5.fitTetrominoPositionInWellBounds)(_grid3, _newGrid2, _activeTetrominoPosition3); // If the rotation causes a collision with landed Tetrominoes than it won't
        // be applied


        if (!(0, _grid5.isPositionAvailable)(_grid3, _newGrid2, _newPosition3)) {
          return state;
        }

        return updatePlayer(state, userId, {
          activeTetrominoGrid: _newGrid2,
          activeTetrominoPosition: _newPosition3
        });
      }

    case 'ENABLE_ACCELERATION':
      {
        var _player4 = getPlayer(state, userId);

        if (_player4.dropAcceleration) {
          return state;
        }

        return updatePlayer(state, userId, {
          dropAcceleration: true
        });
      }

    case 'DISABLE_ACCELERATION':
      {
        var _player5 = getPlayer(state, userId);

        if (!_player5.dropAcceleration) {
          return state;
        }

        return updatePlayer(state, userId, {
          dropAcceleration: false
        });
      }

    case 'PING':
      {
        var time = action.payload.time;
        return updatePlayer(state, userId, {
          ping: time
        });
      }

    default:
      return state;
  }
}

function getBlankGame(_ref) {
  var id = _ref.id,
      user = _ref.user,
      _ref$dropFrames = _ref.dropFrames,
      dropFrames = _ref$dropFrames === void 0 ? _grid4.DROP_FRAMES_DEFAULT : _ref$dropFrames;
  return {
    id: id,
    players: [getBlankPlayer(id, user)],
    dropFrames: dropFrames
  };
}

function getBlankPlayer(gameId, user) {
  return _objectSpread({
    user: user,
    lastActionId: 0,
    status: 'PENDING',
    losses: 0
  }, getBlankPlayerRound({
    gameId: gameId
  }));
}

function getBlankPlayerRound() {
  var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      gameId = _ref2.gameId,
      _ref2$round = _ref2.round,
      round = _ref2$round === void 0 ? 0 : _ref2$round,
      _ref2$drops = _ref2.drops,
      drops = _ref2$drops === void 0 ? 0 : _ref2$drops;

  return _objectSpread({
    drops: 0,
    score: 0,
    lines: 0,
    grid: (0, _grid5.generateEmptyGrid)(_grid4.WELL_ROWS, _grid4.WELL_COLS),
    blocksCleared: [],
    blocksPending: []
  }, getNextPlayerTetromino({
    gameId: gameId,
    round: round,
    drops: drops
  }), {
    dropAcceleration: false
  }, getBlankPlayerEffects());
}

function getNextPlayerTetromino() {
  var _ref3 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      gameId = _ref3.gameId,
      _ref3$round = _ref3.round,
      round = _ref3$round === void 0 ? 0 : _ref3$round,
      _ref3$drops = _ref3.drops,
      drops = _ref3$drops === void 0 ? 0 : _ref3$drops;

  // Generate random Tetromino sequence per game round
  var roundId = (parseInt(gameId, 16) * (round + 1)).toString(16);
  var activeTetromino = (0, _tetromino2.getNextTetromino)(roundId, drops);
  return {
    activeTetromino: activeTetromino,
    activeTetrominoGrid: _tetromino.SHAPES[activeTetromino],
    activeTetrominoPosition: (0, _tetromino2.getInitialPositionForTetromino)(activeTetromino, _grid4.WELL_COLS),
    nextTetromino: (0, _tetromino2.getNextTetromino)(roundId, drops + 1)
  };
}

function stripGameEffects(game) {
  return _objectSpread({}, game, {
    players: game.players.map(function (player) {
      return _objectSpread({}, player, getBlankPlayerEffects());
    })
  });
}

function isPlayer(game, curUser) {
  if (!curUser) {
    return false;
  } // Flow requires us to store the current user's id aside, as the curUser
  // object may change by the time the some callback is called. Smart!


  var id = curUser.id;
  return game.players.some(function (p) {
    return p.user.id === id;
  });
}

function getPlayer(game, userId) {
  var player = (0, _lodash.find)(game.players, function (p) {
    return p.user.id === userId;
  });

  if (!player) {
    throw new Error("Player with userId ".concat(userId, " does not exist"));
  }

  return player;
}

function getCurPlayer(game, curUser) {
  if (!game.players.length) {
    throw new Error('Games must have at least one player');
  }

  return curUser && isPlayer(game, curUser) ? getPlayer(game, curUser.id) : game.players[0];
}

function getOtherPlayer(game, curPlayer) {
  // NOTE: This only works with max 2 players per game
  return (0, _lodash.find)(game.players, function (p) {
    return p !== curPlayer;
  });
}

function allPlayersReady(game) {
  return game.players.filter(function (p) {
    return p.status === 'READY';
  }).length === game.players.length;
}

function addUserToGame(game, user) {
  var id = game.id,
      players = game.players;
  return _objectSpread({}, game, {
    players: [].concat(_toConsumableArray(players), [getBlankPlayer(id, user)])
  });
}

function updatePlayer(game, userId, attrs) {
  var players = game.players;
  var player = getPlayer(game, userId);
  var playerIndex = players.indexOf(player);
  return _objectSpread({}, game, {
    players: [].concat(_toConsumableArray(players.slice(0, playerIndex)), [_objectSpread({}, player, attrs)], _toConsumableArray(players.slice(playerIndex + 1)))
  });
} // This function can have three responses:
// - 0, which means the action points to game state
// - <0, which means the action is from the past and will be discarded
// - >0, which means the action is detached and backfill is required


function getGameActionOffset(game, action) {
  // There's no previous player action to follow when user just joined
  if (action.type === 'JOIN_GAME') {
    return 0;
  }

  var _action$payload3 = action.payload,
      userId = _action$payload3.userId,
      actionId = _action$payload3.actionId,
      prevActionId = _action$payload3.prevActionId;
  var player = (0, _lodash.find)(game.players, function (p) {
    return p.user.id === userId;
  }); // Sometimes we get actions from players that aren't found in the state
  // snapshot, because it's from a time before they joined

  if (!player) {
    return actionId;
  }

  return prevActionId - player.lastActionId;
}

function rewardClearedBlocks(game, userId) {
  var dropFrames = game.dropFrames;
  var player = getPlayer(game, userId);
  var score = player.score,
      lines = player.lines,
      blocksCleared = player.blocksCleared,
      dropAcceleration = player.dropAcceleration; // TODO: Calculate cells in Tetromino. All current Tetrominoes have 4 cells

  var cells = 4; // Rudimentary scoring logic, no T-Spin and combo bonuses

  var points = dropAcceleration ? cells * 2 : cells;

  if (blocksCleared.length) {
    points += _grid4.LINE_CLEAR_BONUSES[blocksCleared.length - 1] * (lines + 1);
  }

  return _objectSpread({}, updatePlayer(game, userId, {
    score: score + points,
    lines: lines + blocksCleared.length
  }), {
    // Increase speed whenever a line is cleared (fast game)
    dropFrames: blocksCleared.length ? dropFrames - _grid4.DROP_FRAMES_DECREMENT : dropFrames
  });
}

function getGameRound(game) {
  return game.players.reduce(function (acc, next) {
    return acc + next.losses;
  }, 0);
}

function sendClearedBlocksToEnemy(game, userId, unclearedGrid, rowsCleared) {
  var curPlayer = getPlayer(game, userId);
  var enemy = getOtherPlayer(game, curPlayer);

  if (!enemy) {
    return game;
  }

  var flashNay = enemy.flashNay;
  var blocksPending = (0, _grid5.overrideBlockIds)((0, _grid5.getBlocksFromGridRows)(unclearedGrid, rowsCleared), (0, _grid5.getNextCellId)(enemy));
  return updatePlayer(game, enemy.user.id, {
    blocksPending: [].concat(_toConsumableArray(enemy.blocksPending), _toConsumableArray(blocksPending)),
    flashNay: altFlashClass(flashNay)
  });
}

function altFlashClass(flashSuffix) {
  return flashSuffix === 'b' ? 'a' : 'b';
}

var QUAKE_A = ['a1', 'a2', 'a3', 'a4'];
var QUAKE_B = ['b1', 'b2', 'b3', 'b4'];

function altQuakeClass(quakeSuffix, magnitude) {
  if ([1, 2, 3, 4].indexOf(magnitude) === -1) {
    throw new Error("Invalid quake magnitute: ".concat(magnitude));
  }

  var offset = magnitude - 1;
  return quakeSuffix === QUAKE_B[offset] ? QUAKE_A[offset] : QUAKE_B[offset];
}

function getBlankPlayerEffects() {
  return {
    flashYay: null,
    flashNay: null,
    quake: null,
    ping: null
  };
}

function bumpActionId(game, userId, actionId) {
  return updatePlayer(game, userId, {
    lastActionId: actionId
  });
}

/***/ }),
/* 5 */,
/* 6 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addCurUserToState = addCurUserToState;
exports.createUserSession = createUserSession;
exports.getDashboard = getDashboard;
exports.getGame = getGame;
exports.createGame = createGame;
exports.backfillGameActions = backfillGameActions;
exports.getDailyStats = getDailyStats;
exports.getApiUrl = getApiUrl;
exports.Unauthorized = Unauthorized;

var _regenerator = _interopRequireDefault(__webpack_require__(6));

var _isomorphicUnfetch = _interopRequireDefault(__webpack_require__(43));

var _cookie = _interopRequireDefault(__webpack_require__(44));

var _validation = __webpack_require__(45);

var _global = __webpack_require__(12);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// NOTE: This method is strictly called on the server side
function addCurUserToState(_x, _x2) {
  return _addCurUserToState.apply(this, arguments);
}

function _addCurUserToState() {
  _addCurUserToState = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(req, store) {
    var _cookie$parse, sessionId, res, user;

    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _cookie$parse = _cookie.default.parse(req.headers.cookie || ''), sessionId = _cookie$parse.sessionId;

            if (!sessionId) {
              _context.next = 12;
              break;
            }

            _context.prev = 2;
            _context.next = 5;
            return fetchJson('/auth', {
              headers: {
                cookie: "sessionId=".concat(sessionId)
              }
            });

          case 5:
            res = _context.sent;
            user = (0, _validation.getValidUser)(res);
            store.dispatch((0, _global.auth)(user));
            _context.next = 12;
            break;

          case 10:
            _context.prev = 10;
            _context.t0 = _context["catch"](2);

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[2, 10]]);
  }));
  return _addCurUserToState.apply(this, arguments);
}

function createUserSession(_x3) {
  return _createUserSession.apply(this, arguments);
}

function _createUserSession() {
  _createUserSession = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee2(userName) {
    return _regenerator.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            return _context2.abrupt("return", fetchPost('/auth', {
              userName: userName
            }));

          case 1:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));
  return _createUserSession.apply(this, arguments);
}

function getDashboard() {
  return _getDashboard.apply(this, arguments);
}

function _getDashboard() {
  _getDashboard = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee3() {
    return _regenerator.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            return _context3.abrupt("return", fetchJson("/dashboard"));

          case 1:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, this);
  }));
  return _getDashboard.apply(this, arguments);
}

function getGame(_x4) {
  return _getGame.apply(this, arguments);
}

function _getGame() {
  _getGame = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee4(gameId) {
    return _regenerator.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            return _context4.abrupt("return", fetchJson("/game/".concat(gameId)));

          case 1:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, this);
  }));
  return _getGame.apply(this, arguments);
}

function createGame() {
  return _createGame.apply(this, arguments);
}

function _createGame() {
  _createGame = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee5() {
    return _regenerator.default.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            return _context5.abrupt("return", fetchPost('/game'));

          case 1:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, this);
  }));
  return _createGame.apply(this, arguments);
}

function backfillGameActions(_x5) {
  return _backfillGameActions.apply(this, arguments);
}

function _backfillGameActions() {
  _backfillGameActions = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee6(req) {
    return _regenerator.default.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            return _context6.abrupt("return", fetchPost("/backfill", req));

          case 1:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, this);
  }));
  return _backfillGameActions.apply(this, arguments);
}

function getDailyStats() {
  return _getDailyStats.apply(this, arguments);
}

function _getDailyStats() {
  _getDailyStats = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee7() {
    return _regenerator.default.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            return _context7.abrupt("return", fetchJson("/daily-stats"));

          case 1:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, this);
  }));
  return _getDailyStats.apply(this, arguments);
}

function getApiUrl(path) {
  var baseUrl =  true ? getProdBaseUrl() : undefined;
  return path ? "".concat(baseUrl).concat(path) : "".concat(baseUrl, "/");
}

function Unauthorized() {
  this.name = 'Unauthorized';
  this.message = 'Invalid user session';
}

function getProdBaseUrl() {
  // Relative paths allow us to serve the prod app from any proxy address (eg.
  // via ngrok), but server-side requests need to contain the host address
  return typeof window === 'undefined' ? 'http://localhost:3000' : '';
}

function fetchJson(urlPath, options) {
  return (0, _isomorphicUnfetch.default)(getApiUrl(urlPath), options).then(parseResponse);
}

function fetchPost(urlPath) {
  var body = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  return fetchJson(urlPath, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  });
}

function parseResponse(response) {
  if (response.status >= 200 && response.status < 300) {
    return response.json();
  } else {
    if (response.status === 401) {
      throw new Unauthorized();
    } // TODO: Forward server error if response is parsable
    // return response.json().then(
    //   response => {
    //     throw new Error(response.error);
    //   },
    //   () => {
    //     throw new Error('Server error');
    //   }
    // );


    throw new Error('Server error');
  }
}

/***/ }),
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.auth = auth;
exports.unauth = unauth;
exports.loadDashboard = loadDashboard;
exports.addGame = addGame;
exports.removeGame = removeGame;
exports.openGame = openGame;
exports.closeGame = closeGame;
exports.stripGameEffects = stripGameEffects;
exports.startBackfill = startBackfill;
exports.endBackfill = endBackfill;
exports.queueGameAction = queueGameAction;
exports.updateStats = updateStats;

function auth(user) {
  return {
    type: 'AUTH',
    payload: {
      user: user
    }
  };
}

function unauth() {
  return {
    type: 'UNAUTH'
  };
}

function loadDashboard(_ref) {
  var games = _ref.games,
      stats = _ref.stats;
  return {
    type: 'LOAD_DASHBOARD',
    payload: {
      games: games,
      stats: stats
    }
  };
}

function addGame(game) {
  return {
    type: 'ADD_GAME',
    payload: {
      game: game
    }
  };
}

function removeGame(gameId) {
  return {
    type: 'REMOVE_GAME',
    payload: {
      gameId: gameId
    }
  };
}

function openGame(gameId) {
  return {
    type: 'OPEN_GAME',
    payload: {
      gameId: gameId
    }
  };
}

function closeGame() {
  return {
    type: 'CLOSE_GAME'
  };
}

function stripGameEffects() {
  return {
    type: 'STRIP_GAME_EFFECTS'
  };
}

function startBackfill(gameId, backfillId) {
  return {
    type: 'START_BACKFILL',
    payload: {
      gameId: gameId,
      backfillId: backfillId
    }
  };
}

function endBackfill(backfillId) {
  return {
    type: 'END_BACKFILL',
    payload: {
      backfillId: backfillId
    }
  };
}

function queueGameAction(action) {
  return {
    type: 'QUEUE_GAME_ACTION',
    payload: {
      action: action
    }
  };
}

function updateStats(stats) {
  return {
    type: 'UPDATE_STATS',
    payload: {
      stats: stats
    }
  };
}

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.curGameReducer = curGameReducer;
exports.getCurGame = getCurGame;
var initialState = null;

function curGameReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case 'OPEN_GAME':
      {
        var gameId = action.payload.gameId;
        return gameId;
      }

    case 'CLOSE_GAME':
      {
        return null;
      }

    default:
      return state;
  }
}

function getCurGame(_ref) {
  var games = _ref.games,
      curGame = _ref.curGame;

  if (!curGame) {
    throw new Error('Current game is missing from state');
  }

  if (!games[curGame]) {
    throw new Error("Current game points to missing game ".concat(curGame));
  }

  return games[curGame];
}

/***/ }),
/* 14 */,
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.curUserReducer = curUserReducer;
exports.getCurUser = getCurUser;
var initialState = null;

function curUserReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case 'AUTH':
      {
        var user = action.payload.user;
        return user;
      }

    case 'UNAUTH':
      {
        return null;
      }

    default:
      return state;
  }
}

function getCurUser(state) {
  if (!state.curUser) {
    throw new Error('Current user is missing from state');
  }

  return state.curUser;
}

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateEmptyGrid = generateEmptyGrid;
exports.rotate = rotate;
exports.getExactPosition = getExactPosition;
exports.isPositionAvailable = isPositionAvailable;
exports.getBottomMostPosition = getBottomMostPosition;
exports.transferTetrominoToGrid = transferTetrominoToGrid;
exports.hasLines = hasLines;
exports.clearLines = clearLines;
exports.fitTetrominoPositionInWellBounds = fitTetrominoPositionInWellBounds;
exports.getBlocksFromGridRows = getBlocksFromGridRows;
exports.overrideBlockIds = overrideBlockIds;
exports.appendBlocksToGrid = appendBlocksToGrid;
exports.getNextCellId = getNextCellId;

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function generateEmptyGrid(rows, cols) {
  var matrix = [];

  for (var row = 0; row < rows; row++) {
    matrix[row] = [];

    for (var col = 0; col < cols; col++) {
      matrix[row][col] = null;
    }
  }

  return matrix;
}

function rotate(grid) {
  var matrix = [];
  var rows = grid.length;
  var cols = grid[0].length;

  for (var row = 0; row < rows; row++) {
    matrix[row] = [];

    for (var col = 0; col < cols; col++) {
      matrix[row][col] = grid[cols - 1 - col][row];
    }
  }

  return matrix;
}

function getExactPosition(_ref) {
  var x = _ref.x,
      y = _ref.y;
  // The position has floating numbers because of how gravity is incremented
  // with each frame
  return {
    x: Math.floor(x),
    y: Math.floor(y)
  };
}

function isPositionAvailable(grid, tetrominoGrid, position) {
  var rows = grid.length;
  var cols = grid[0].length;
  var tetrominoRows = tetrominoGrid.length;
  var tetrominoCols = tetrominoGrid[0].length;
  var tetrominoPositionInGrid = getExactPosition(position);
  var relativeRow;
  var relativeCol;

  for (var row = 0; row < tetrominoRows; row++) {
    for (var col = 0; col < tetrominoCols; col++) {
      // Ignore blank squares from the Tetromino grid
      if (tetrominoGrid[row][col]) {
        relativeRow = tetrominoPositionInGrid.y + row;
        relativeCol = tetrominoPositionInGrid.x + col; // Ensure Tetromino block is within the horizontal bounds

        if (relativeCol < 0 || relativeCol >= cols) {
          return false;
        } // Check check if Tetromino hit the bottom of the Well


        if (relativeRow >= rows) {
          return false;
        } // Tetrominoes are accepted on top of the Well (it's how they enter)


        if (relativeRow >= 0) {
          // Then if the position is not already taken inside the grid
          if (grid[relativeRow][relativeCol]) {
            return false;
          }
        }
      }
    }
  }

  return true;
}

function getBottomMostPosition(grid, tetrominoGrid, position) {
  // Snap vertical position to grid
  var y = Math.floor(position.y);

  while (!isPositionAvailable(grid, tetrominoGrid, {
    x: position.x,
    y: y
  })) {
    y -= 1;
  }

  return _objectSpread({}, position, {
    y: y
  });
}

function transferTetrominoToGrid(player, tetrominoGrid, position, color) {
  var grid = player.grid;
  var rows = tetrominoGrid.length;
  var cols = tetrominoGrid[0].length;
  var tetrominoPositionInGrid = getExactPosition(position);
  var newGrid = grid.map(function (l) {
    return l.map(function (c) {
      return c;
    });
  });
  var relativeRow;
  var relativeCol;
  var nextCellId = getNextCellId(player);

  for (var row = 0; row < rows; row++) {
    for (var col = 0; col < cols; col++) {
      // Ignore blank squares from the Tetromino grid
      if (tetrominoGrid[row][col]) {
        relativeCol = tetrominoPositionInGrid.x + col;
        relativeRow = tetrominoPositionInGrid.y + row; // When the Well is full the Tetromino will land before it enters the
        // top of the Well, so we need to protect against negative row indexes

        if (newGrid[relativeRow]) {
          newGrid[relativeRow][relativeCol] = [nextCellId++, color];
        }
      }
    }
  }

  return newGrid;
}

var createEmptyLine = function createEmptyLine(cols) {
  return _toConsumableArray(Array(cols)).map(function () {
    return null;
  });
};

var isLine = function isLine(row) {
  return !row.some(function (cell) {
    return cell === null;
  });
};

function hasLines(well) {
  return well.reduce(function (acc, rowBlocks) {
    return acc || isLine(rowBlocks);
  }, false);
}

function clearLines(grid) {
  /**
   * Clear all rows that form a complete line, from one left to right, inside
   * the Well grid. Gravity is applied to fill in the cleared lines with the
   * ones above, thus freeing up the Well for more Tetrominoes to enter.
   */
  var rows = grid.length;
  var cols = grid[0].length;
  var clearedGrid = grid.map(function (l) {
    return l;
  });
  var rowsCleared = [];

  for (var row = rows - 1; row >= 0; row--) {
    if (isLine(clearedGrid[row])) {
      for (var row2 = row; row2 >= 0; row2--) {
        clearedGrid[row2] = row2 > 0 ? clearedGrid[row2 - 1] : createEmptyLine(cols);
      } // Because the grid "falls" with every cleared line, the index of the
      // original row is smaller than the current row index by the number of
      // cleared on this occasion. We `unshift` because the lines are cleared
      // from bottom to top, but will then be applied from top to bottom


      rowsCleared.unshift(row - rowsCleared.length); // Go once more through the same row

      row++;
    }
  }

  return {
    clearedGrid: clearedGrid,
    rowsCleared: rowsCleared
  };
}

function fitTetrominoPositionInWellBounds(grid, tetrominoGrid, _ref2) {
  var x = _ref2.x,
      y = _ref2.y;
  var cols = grid[0].length;
  var tetrominoRows = tetrominoGrid.length;
  var tetrominoCols = tetrominoGrid[0].length;
  var newX = x;
  var relativeCol;

  for (var row = 0; row < tetrominoRows; row++) {
    for (var col = 0; col < tetrominoCols; col++) {
      // Ignore blank squares from the Tetromino grid
      if (tetrominoGrid[row][col]) {
        relativeCol = newX + col; // Wall kick: A Tetromino grid that steps outside of the Well grid will
        // be shifted slightly to slide back inside the Well grid

        if (relativeCol < 0) {
          newX -= relativeCol;
        } else if (relativeCol >= cols) {
          newX -= relativeCol - cols + 1;
        }
      }
    }
  }

  return {
    x: newX,
    y: y
  };
}

function getBlocksFromGridRows(grid, rows) {
  return rows.map(function (rowIndex) {
    return grid[rowIndex].map(function (block) {
      return block;
    });
  });
}

function overrideBlockIds(blocks, fromCellId) {
  var nextCellId = fromCellId;
  return blocks.map(function (blockRow) {
    return blockRow.map(function (block) {
      return block ? [nextCellId++, block[1]] : null;
    });
  });
}

function appendBlocksToGrid(grid, blocks) {
  var rows = grid.length;
  var cols = grid[0].length;
  var newGrid = generateEmptyGrid(rows, cols); // 1. Apply new block rows at the bottom, and collect top "border shape"
  // Hmm, this isn't very good, because 99% enemy blocks will create a line
  // NOTE: Old functionality left for posterity
  // const availRowsPerCol = new Array(cols).fill(rows);

  blocks.forEach(function (rowBlocks, rowIndex) {
    var absRowIndex = rows - blocks.length + rowIndex;
    rowBlocks.forEach(function (block, colIndex) {
      if (block) {
        newGrid[absRowIndex][colIndex] = block; // availRowsPerCol[colIndex] = Math.min(
        //   availRowsPerCol[colIndex],
        //   absRowIndex
        // );
      }
    });
  }); // 2. "Pour" previous blocks over new ones

  for (var colIndex = 0; colIndex < cols; colIndex++) {
    // const rowOffset = rows - availRowsPerCol[colIndex];
    var rowOffset = blocks.length;

    for (var rowIndex = rows - 1; rowIndex >= rowOffset; rowIndex--) {
      newGrid[rowIndex - rowOffset][colIndex] = grid[rowIndex][colIndex];
    }
  }

  return newGrid;
}

function getNextCellId(player) {
  var max = Math.max;
  var grid = player.grid,
      blocksCleared = player.blocksCleared,
      blocksPending = player.blocksPending;
  return max(getMaxCellIdFromGrid(grid), getMaxCellIdFromGrid(blocksCleared), getMaxCellIdFromGrid(blocksPending)) + 1;
}

function getMaxCellIdFromGrid(grid) {
  var max = Math.max;
  return max.apply(void 0, _toConsumableArray(grid.map(function (r) {
    return max.apply(void 0, _toConsumableArray(r.map(function (cell) {
      return cell ? cell[0] : 0;
    })));
  })));
}

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LINE_CLEAR_BONUSES = exports.DROP_FRAMES_ACCELERATED = exports.DROP_FRAMES_DECREMENT = exports.DROP_FRAMES_DEFAULT = exports.WELL_COLS = exports.WELL_ROWS = void 0;
var WELL_ROWS = 20;
exports.WELL_ROWS = WELL_ROWS;
var WELL_COLS = 10;
exports.WELL_COLS = WELL_COLS;
var DROP_FRAMES_DEFAULT = 48; // 1 row in less than a second

exports.DROP_FRAMES_DEFAULT = DROP_FRAMES_DEFAULT;
var DROP_FRAMES_DECREMENT = 1.5;
exports.DROP_FRAMES_DECREMENT = DROP_FRAMES_DECREMENT;
var DROP_FRAMES_ACCELERATED = 1; // 60 rows per second

exports.DROP_FRAMES_ACCELERATED = DROP_FRAMES_ACCELERATED;
var LINE_CLEAR_BONUSES = [100, 300, 500, 800];
exports.LINE_CLEAR_BONUSES = LINE_CLEAR_BONUSES;

/***/ }),
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */
/***/ (function(module, exports) {

module.exports = require("next-redux-wrapper");

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createStore = createStore;

var _redux = __webpack_require__(25);

var _reduxThunk = _interopRequireDefault(__webpack_require__(26));

var _reduxDevtoolsExtension = __webpack_require__(27);

var _jsReady = __webpack_require__(28);

var _curUser = __webpack_require__(15);

var _games = __webpack_require__(29);

var _curGame = __webpack_require__(13);

var _backfills = __webpack_require__(32);

var _stats = __webpack_require__(33);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var rootReducer = (0, _redux.combineReducers)({
  jsReady: _jsReady.jsReadyReducer,
  curUser: _curUser.curUserReducer,
  games: _games.gamesReducer,
  curGame: _curGame.curGameReducer,
  backfills: _backfills.backfillsReducer,
  stats: _stats.statsReducer
});

function createStore(initialState) {
  return (0, _redux.createStore)(rootReducer, initialState, (0, _reduxDevtoolsExtension.composeWithDevTools)((0, _redux.applyMiddleware)(_reduxThunk.default)));
}

/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = require("redux-thunk");

/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = require("redux-devtools-extension");

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.jsReadyReducer = jsReadyReducer;
var initialState = false;

function jsReadyReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case 'JS_LOAD':
      {
        return true;
      }

    default:
      return state;
  }
}

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.gamesReducer = gamesReducer;

var _game = __webpack_require__(4);

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {};

function gamesReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case 'LOAD_DASHBOARD':
      {
        var games = action.payload.games;
        return getEffectlessGames( // NOTE: We don't care about order at the moment
        games.reduce(function (acc, game) {
          return _objectSpread({}, acc, _defineProperty({}, game.id, game));
        }, {}));
      }

    case 'ADD_GAME':
      {
        var game = action.payload.game;
        return _objectSpread({}, state, _defineProperty({}, game.id, (0, _game.stripGameEffects)(game)));
      }

    case 'REMOVE_GAME':
      {
        var gameId = action.payload.gameId;
        return Object.keys(state).reduce(function (acc, gId) {
          return gId !== gameId ? _objectSpread({}, acc, _defineProperty({}, gId, state[gId])) : acc;
        }, {});
      }

    case 'STRIP_GAME_EFFECTS':
      {
        return getEffectlessGames(state);
      }

    case 'JOIN_GAME':
    case 'PLAYER_READY':
    case 'PLAYER_PAUSE':
    case 'DROP':
    case 'MOVE_LEFT':
    case 'MOVE_RIGHT':
    case 'ROTATE':
    case 'ENABLE_ACCELERATION':
    case 'DISABLE_ACCELERATION':
    case 'APPEND_PENDING_BLOCKS':
    case 'PING':
      {
        var _gameId = action.payload.gameId;

        if (!_gameId) {
          throw new Error("Game action ".concat(action.type, " has no gameId"));
        }

        if (!state[_gameId]) {
          throw new Error("Game action ".concat(action.type, " for missing game ").concat(_gameId));
        }

        return _objectSpread({}, state, _defineProperty({}, _gameId, (0, _game.gameReducer)(state[_gameId], action)));
      }

    default:
      return state;
  }
}

function getEffectlessGames(games) {
  return Object.keys(games).reduce(function (acc, gameId) {
    return _objectSpread({}, acc, _defineProperty({}, gameId, (0, _game.stripGameEffects)(games[gameId])));
  }, {});
}

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getNextTetromino = getNextTetromino;
exports.getInitialPositionForTetromino = getInitialPositionForTetromino;

var _crc = __webpack_require__(31);

var _tetromino = __webpack_require__(3);

function getNextTetromino(gameId, nth) {
  // $FlowFixMe
  var tetrominos = Object.keys(_tetromino.SHAPES);
  var randNum = (0, _crc.str)(gameId + nth);
  return tetrominos[Math.abs(randNum) % tetrominos.length];
}

function getInitialPositionForTetromino(tetromino, gridCols) {
  /**
   * Generates positions a Tetromino entering the Well. The I Tetromino
   * occupies columns 4, 5, 6 and 7, the O Tetromino occupies columns 5 and
   * 6, and the remaining 5 Tetrominoes occupy columns 4, 5 and 6. Pieces
   * spawn above the visible playfield (that's why y is -2)
   */
  var grid = _tetromino.SHAPES[tetromino];
  return {
    x: Math.round(gridCols / 2) - Math.round(grid[0].length / 2),
    y: -2
  };
}

/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = require("crc-32");

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.backfillsReducer = backfillsReducer;

var _lodash = __webpack_require__(7);

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {};

function backfillsReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case 'START_BACKFILL':
      {
        var _action$payload = action.payload,
            gameId = _action$payload.gameId,
            backfillId = _action$payload.backfillId;
        return _objectSpread({}, state, _defineProperty({}, gameId, {
          backfillId: backfillId,
          queuedActions: []
        }));
      }

    case 'END_BACKFILL':
      {
        var _backfillId = action.payload.backfillId;

        var _gameId = (0, _lodash.findKey)(state, function (b) {
          return b.backfillId === _backfillId;
        });

        if (!_gameId) {
          return state;
        }

        return (0, _lodash.omit)(state, _gameId);
      }

    case 'QUEUE_GAME_ACTION':
      {
        var queuedAction = action.payload.action;
        var _queuedAction$payload = queuedAction.payload,
            actionId = _queuedAction$payload.actionId,
            _gameId2 = _queuedAction$payload.gameId;
        var backfill = state[_gameId2];

        if (!backfill) {
          console.warn("Trying to queue action ".concat(actionId, " outside backfill"));
          return state;
        }

        var _backfillId2 = backfill.backfillId,
            queuedActions = backfill.queuedActions;
        return _objectSpread({}, state, _defineProperty({}, _gameId2, {
          backfillId: _backfillId2,
          queuedActions: [].concat(_toConsumableArray(queuedActions), [queuedAction])
        }));
      }

    default:
      return state;
  }
}

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.statsReducer = statsReducer;
var initialState = {
  actionAcc: 0,
  actionLeft: 0,
  actionRight: 0,
  actionRotate: 0,
  games: 0,
  lines: 0,
  seconds: 0
};

function statsReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case 'LOAD_DASHBOARD':
    case 'UPDATE_STATS':
      {
        var stats = action.payload.stats;
        return stats;
      }

    default:
      return state;
  }
}

/***/ }),
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),
/* 44 */
/***/ (function(module, exports) {

module.exports = require("cookie");

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getValidUser = getValidUser;

function getValidUser(user) {
  if (!user || typeof user.id !== 'string' || typeof user.name !== 'string') {
    throw new Error("Invalid user: ".concat(String(user)));
  }

  return {
    id: user.id,
    name: user.name
  };
}

/***/ }),
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */,
/* 56 */,
/* 57 */,
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */,
/* 94 */,
/* 95 */,
/* 96 */,
/* 97 */,
/* 98 */,
/* 99 */,
/* 100 */,
/* 101 */,
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(103);


/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(__webpack_require__(6));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _router = _interopRequireDefault(__webpack_require__(51));

var _nextReduxWrapper = _interopRequireDefault(__webpack_require__(23));

var _store = __webpack_require__(24);

var _api = __webpack_require__(8);

var _Loading = _interopRequireDefault(__webpack_require__(104));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// NOTE: I built a Loading component a while ago for Flatris, but later
// realized it wasn't needed since everything was so fast. So I made a page
// to be able to look at it ¯\_(ツ)_/¯
var LoadingPage =
/*#__PURE__*/
function (_Component) {
  _inherits(LoadingPage, _Component);

  function LoadingPage() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, LoadingPage);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(LoadingPage)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleClick", function () {
      _router.default.push('/');
    });

    return _this;
  }

  _createClass(LoadingPage, [{
    key: "render",
    value: function render() {
      return _react.default.createElement("div", {
        onClick: this.handleClick
      }, _react.default.createElement(_Loading.default, null));
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(_ref) {
        var req, store;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                req = _ref.req, store = _ref.store;

                if (!req) {
                  _context.next = 4;
                  break;
                }

                _context.next = 4;
                return (0, _api.addCurUserToState)(req, store);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      }

      return getInitialProps;
    }()
  }]);

  return LoadingPage;
}(_react.Component);

var _default = (0, _nextReduxWrapper.default)(_store.createStore)(LoadingPage);

exports.default = _default;

/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var SHAPES = {
  I: [[0, 0, 0, 0], [1, 1, 1, 1], [0, 0, 0, 0], [0, 0, 0, 0]],
  O: [[1, 1], [1, 1]],
  T: [[0, 1, 0], [1, 1, 1], [0, 0, 0]],
  J: [[1, 0, 0], [1, 1, 1], [0, 0, 0]],
  L: [[0, 0, 1], [1, 1, 1], [0, 0, 0]],
  S: [[0, 1, 1], [1, 1, 0], [0, 0, 0]],
  Z: [[1, 1, 0], [0, 1, 1], [0, 0, 0]]
};
var round = Math.round,
    random = Math.random;

var range = _toConsumableArray(Array(4).keys());

var Loading =
/*#__PURE__*/
function (_Component) {
  _inherits(Loading, _Component);

  function Loading() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Loading);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Loading)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      currShape: 'S'
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "changeShape", function () {
      var allShapes = Object.keys(SHAPES);
      var otherShapes = allShapes.filter(function (s) {
        return s !== _this.state.currShape;
      });
      var currShape = otherShapes[round(random() * (otherShapes.length - 1))];

      _this.setState({
        currShape: currShape
      });

      _this.scheduleShapeChange();
    });

    return _this;
  }

  _createClass(Loading, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.scheduleShapeChange();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.timeoutId) {
        clearTimeout(this.timeoutId);
      }
    }
  }, {
    key: "scheduleShapeChange",
    value: function scheduleShapeChange() {
      this.timeoutId = setTimeout(this.changeShape, 1000);
    }
  }, {
    key: "render",
    value: function render() {
      var currShape = this.state.currShape;
      var shapeGrid = SHAPES[currShape];
      return _react.default.createElement("div", {
        className: "jsx-470101677" + " " + "root"
      }, _react.default.createElement("div", {
        className: "jsx-470101677" + " " + "loading"
      }, range.map(function (i) {
        return range.map(function (j) {
          return _react.default.createElement("span", {
            key: "".concat(i, "-").concat(j),
            className: "jsx-470101677" + " " + ((shapeGrid[i] && shapeGrid[i][j] ? 'cell on' : 'cell') || "")
          });
        });
      })), _react.default.createElement(_style.default, {
        styleId: "470101677",
        css: [".root.jsx-470101677{position:absolute;top:0;bottom:0;left:0;right:0;}", ".loading.jsx-470101677{position:absolute;top:50%;left:50%;-webkit-transform:translate(-37.5%,-25%);-ms-transform:translate(-37.5%,-25%);transform:translate(-37.5%,-25%);width:400px;height:400px;}", ".cell.jsx-470101677{float:left;width:25%;height:25%;background:#34495f;opacity:0;-webkit-transform:scale(0.75);-ms-transform:scale(0.75);transform:scale(0.75);-webkit-transition:opacity 0.5s,-webkit-transform 0.5s;-webkit-transition:opacity 0.5s,transform 0.5s;transition:opacity 0.5s,transform 0.5s;}", ".on.jsx-470101677{opacity:1;-webkit-transform:scale(1);-ms-transform:scale(1);transform:scale(1);}"]
      }));
    }
  }]);

  return Loading;
}(_react.Component);

exports.default = Loading;

/***/ })
/******/ ]);
//# sourceMappingURL=loading.js.map