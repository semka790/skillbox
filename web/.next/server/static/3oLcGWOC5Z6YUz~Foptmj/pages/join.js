module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 86);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Button;

var _style = _interopRequireDefault(__webpack_require__(1));

var _classnames = _interopRequireDefault(__webpack_require__(9));

var _react = _interopRequireDefault(__webpack_require__(0));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function Button(_ref) {
  var _ref$type = _ref.type,
      type = _ref$type === void 0 ? 'button' : _ref$type,
      children = _ref.children,
      _ref$bgColor = _ref.bgColor,
      bgColor = _ref$bgColor === void 0 ? '#34495f' : _ref$bgColor,
      _ref$color = _ref.color,
      color = _ref$color === void 0 ? '#fff' : _ref$color,
      _ref$colorDisabled = _ref.colorDisabled,
      colorDisabled = _ref$colorDisabled === void 0 ? 'rgba(255, 255, 255, 0.6)' : _ref$colorDisabled,
      _ref$hoverEffect = _ref.hoverEffect,
      hoverEffect = _ref$hoverEffect === void 0 ? true : _ref$hoverEffect,
      rest = _objectWithoutProperties(_ref, ["type", "children", "bgColor", "color", "colorDisabled", "hoverEffect"]);

  var classes = (0, _classnames.default)('button', {
    'hover-button': hoverEffect
  });
  return _react.default.createElement("button", _extends({
    type: type
  }, rest, {
    className: _style.default.dynamic([["210768193", [bgColor, color, colorDisabled]]]) + " " + (rest.className != null && rest.className || classes || "")
  }), children, _react.default.createElement(_style.default, {
    styleId: "210768193",
    css: [".button.__jsx-style-dynamic-selector{position:absolute;width:100%;height:100%;display:block;margin:0;padding:0;border:0;background:".concat(bgColor, ";color:").concat(color, ";font-family:-apple-system,BlinkMacSystemFont,Ubuntu, 'Helvetica Neue',sans-serif;font-size:1.1em;font-weight:600;text-transform:uppercase;outline:none;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;touch-action:manipulation;}"), ".button.__jsx-style-dynamic-selector:disabled{cursor:default;color:".concat(colorDisabled, ";}"), ".hover-button.__jsx-style-dynamic-selector{-webkit-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);transform:translate3d(0,0,0);-webkit-transition:-webkit-transform 0.2s;-webkit-transition:transform 0.2s;transition:transform 0.2s;}", ".hover-button.__jsx-style-dynamic-selector:focus,.hover-button.__jsx-style-dynamic-selector:hover{-webkit-transform:translate3d(0,-0.25em,0);-ms-transform:translate3d(0,-0.25em,0);transform:translate3d(0,-0.25em,0);}", ".hover-button.__jsx-style-dynamic-selector:active,.hover-button.__jsx-style-dynamic-selector:disabled{-webkit-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);transform:translate3d(0,0,0);}"],
    dynamic: [bgColor, color, colorDisabled]
  }));
}

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SHAPES = exports.COLORS = void 0;
var COLORS = {
  I: '#3cc7d6',
  O: '#fbb414',
  T: '#b04497',
  J: '#3993d0',
  L: '#ed652f',
  S: '#95c43d',
  Z: '#e84138'
};
exports.COLORS = COLORS;
var SHAPES = {
  I: [[0, 0, 0, 0], [1, 1, 1, 1], [0, 0, 0, 0], [0, 0, 0, 0]],
  O: [[1, 1], [1, 1]],
  T: [[0, 1, 0], [1, 1, 1], [0, 0, 0]],
  J: [[1, 0, 0], [1, 1, 1], [0, 0, 0]],
  L: [[0, 0, 1], [1, 1, 1], [0, 0, 0]],
  S: [[0, 1, 1], [1, 1, 0], [0, 0, 0]],
  Z: [[1, 1, 0], [0, 1, 1], [0, 0, 0]]
};
exports.SHAPES = SHAPES;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.gameReducer = gameReducer;
exports.gameJoinedReducer = gameJoinedReducer;
exports.getBlankGame = getBlankGame;
exports.getBlankPlayer = getBlankPlayer;
exports.getBlankPlayerRound = getBlankPlayerRound;
exports.getNextPlayerTetromino = getNextPlayerTetromino;
exports.stripGameEffects = stripGameEffects;
exports.isPlayer = isPlayer;
exports.getPlayer = getPlayer;
exports.getCurPlayer = getCurPlayer;
exports.getOtherPlayer = getOtherPlayer;
exports.allPlayersReady = allPlayersReady;
exports.addUserToGame = addUserToGame;
exports.updatePlayer = updatePlayer;
exports.getGameActionOffset = getGameActionOffset;

var _lodash = __webpack_require__(7);

var _grid4 = __webpack_require__(17);

var _tetromino = __webpack_require__(3);

var _tetromino2 = __webpack_require__(30);

var _grid5 = __webpack_require__(16);

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function gameReducer(state, action) {
  if (!state) {
    throw new Error("Game action ".concat(action.type, " called on void state"));
  }

  if (action.type === 'JOIN_GAME') {
    var _action$payload = action.payload,
        _actionId = _action$payload.actionId,
        _userId = _action$payload.userId,
        user = _action$payload.user;
    var players = state.players;

    if (players.length > 1) {
      if (isPlayer(state, user)) {
        console.warn("User ".concat(user.id, " tried to join game more than once"));
      } else {
        console.warn("User ".concat(user.id, " tried to join already full game"));
      }

      return state;
    }

    var _players = _slicedToArray(players, 1),
        player1 = _players[0];

    var game = updatePlayer(state, player1.user.id, {
      // Stop player1's game when player2 arrives
      status: 'PENDING',
      // Previous losses are irrelevant to 1vs1 game
      losses: 0
    });
    return bumpActionId(addUserToGame(game, user), _userId, _actionId);
  } // Ensure action consistency


  var _action$payload2 = action.payload,
      actionId = _action$payload2.actionId,
      userId = _action$payload2.userId;
  var offset = getGameActionOffset(state, action);

  if (offset > 0) {
    throw new Error("Refusing detached game action");
  }

  if (offset < 0) {
    console.warn("Past game action ".concat(actionId, " ignored (").concat(offset, "ms delta)"));
    return state;
  }

  var newState = gameJoinedReducer(state, action); // Don't bump player.lastActionId if action left state intact. This allows us
  // to avoid broacasting "noop" actions and minimize network activity
  // FIXME: Sometimes actions were recorded that seemed like "noop" actions when
  // played back from backfill response. Eg. Often an `ENABLE_ACCELERATION`
  // action would be recorded and then when played back it followed a state
  // that had `player.dropAcceleration: true`, which made it noop and entered
  // an infinite loop of crashed backfills. Disabling this optimization until
  // I understand what is going on.

  return newState === state ? newState : bumpActionId(newState, userId, actionId);
}

function gameJoinedReducer(state, action) {
  var userId = action.payload.userId;

  switch (action.type) {
    case 'PLAYER_READY':
      {
        var _getPlayer = getPlayer(state, userId),
            prevStatus = _getPlayer.status;

        var game = updatePlayer(state, userId, {
          status: 'READY'
        }); // Reset game when all players are ready to (re)start

        if (allPlayersReady(game) && ( // This condition allows solo players to pause and resume
        // (by clicking on the "2p insert coin" button)
        game.players.length > 1 || prevStatus !== 'PAUSE')) {
          var id = game.id,
              players = game.players;
          var round = getGameRound(game);
          return _objectSpread({}, game, {
            players: players.map(function (player) {
              return _objectSpread({}, player, getBlankPlayerRound({
                gameId: id,
                round: round
              }));
            }),
            dropFrames: _grid4.DROP_FRAMES_DEFAULT
          });
        }

        return game;
      }

    case 'PLAYER_PAUSE':
      {
        if (state.players.length > 1) {
          throw new Error('Pausing multiplayer game not allowed');
        }

        return updatePlayer(state, userId, {
          status: 'PAUSE'
        });
      }

    case 'DROP':
      {
        var rows = action.payload.rows;
        var player = getPlayer(state, userId);
        var grid = player.grid,
            activeTetromino = player.activeTetromino,
            activeTetrominoGrid = player.activeTetrominoGrid,
            activeTetrominoPosition = player.activeTetrominoPosition,
            dropAcceleration = player.dropAcceleration,
            flashYay = player.flashYay,
            quake = player.quake;

        if (player.status === 'LOST') {
          console.warn("DROP action denied for loosing player");
          return state;
        } // Clear lines generated by a previous `APPEND_PENDING_BLOCKS` action
        // NOTE: Old functionality left for posterity
        // if (hasLines(grid)) {
        //   const { clearedGrid, rowsCleared } = clearLines(grid);
        //   const blocksCleared = getBlocksFromGridRows(grid, rowsCleared);
        //   const newState = updatePlayer(state, userId, {
        //     grid: clearedGrid,
        //     blocksCleared
        //   });
        //
        //   return rewardClearedBlocks(newState, userId);
        // }
        // Drop active Tetromino until it hits something


        var newPosition = {
          x: activeTetrominoPosition.x,
          y: activeTetrominoPosition.y + rows
        }; // New active Tetromino position is available, uneventful path

        if ((0, _grid5.isPositionAvailable)(grid, activeTetrominoGrid, newPosition)) {
          return updatePlayer(state, userId, {
            activeTetrominoPosition: newPosition
          });
        } // Active Tetromino has hit the ground
        // A big frame skip could cause the Tetromino to jump more than one row.
        // We need to ensure it ends up in the bottom-most one in case the jump
        // caused the Tetromino to land


        newPosition = (0, _grid5.getBottomMostPosition)(grid, activeTetrominoGrid, newPosition); // Game over when active Tetromino lands (partially) outside the well.
        // NOTE: This is not ideal because the landed Tetromino, even though it
        // doesn't fit when it lands, could cause one or more lines which
        // after cleared could make room for the entire Tetromino. To implement
        // this we would need to somehow re-apply the part of the active Tetromino
        // that didn't fit upon landing, after the lines have been cleared.

        if (newPosition.y < 0) {
          return _objectSpread({}, state, {
            players: state.players.map(function (player) {
              var newAttrs = // TODO: Only set LOST state. Allow for draw
              player.user.id === userId ? {
                status: 'LOST',
                losses: player.losses + 1
              } : {
                status: 'WON'
              };
              return _objectSpread({}, player, newAttrs);
            })
          });
        } // This is when the active Tetromino hits the bottom of the Well and can
        // no longer be controlled


        var newGrid = (0, _grid5.transferTetrominoToGrid)(player, activeTetrominoGrid, newPosition, _tetromino.COLORS[activeTetromino]);
        var newState = state;

        var _round = getGameRound(newState);

        var drops = player.drops + 1;
        newState = updatePlayer(newState, userId, _objectSpread({
          drops: drops,
          grid: newGrid
        }, getNextPlayerTetromino({
          gameId: state.id,
          round: _round,
          drops: drops
        }), {
          // Clear acceleration after dropping Tetromino. Sometimes the key
          // events would misbehave and acceleration would remain on even after
          // releasing DOWN key
          dropAcceleration: false
        }));

        if (!(0, _grid5.hasLines)(newGrid)) {
          return newState;
        }

        var _clearLines = (0, _grid5.clearLines)(newGrid),
            clearedGrid = _clearLines.clearedGrid,
            rowsCleared = _clearLines.rowsCleared;

        var blocksCleared = (0, _grid5.getBlocksFromGridRows)(newGrid, rowsCleared);
        newState = updatePlayer(newState, userId, {
          grid: clearedGrid,
          blocksCleared: blocksCleared,
          flashYay: altFlashClass(flashYay),
          quake: dropAcceleration ? altQuakeClass(quake, rowsCleared.length) : null
        });
        newState = rewardClearedBlocks(newState, userId); // Transfer blocks from cleared lines to enemy grid 😈
        // We reference the old grid, to get the blocks of the cleared lines
        // *without* the blocks added from the just transfered active Tetromino

        return sendClearedBlocksToEnemy(newState, userId, grid, rowsCleared);
      }

    case 'APPEND_PENDING_BLOCKS':
      {
        var _player = getPlayer(state, userId);

        var _grid = _player.grid,
            blocksPending = _player.blocksPending,
            _activeTetrominoGrid = _player.activeTetrominoGrid,
            _activeTetrominoPosition = _player.activeTetrominoPosition; // XXX: The appended blocks might result in trimming existing blocks, by
        // lifting them higher than the well permits. This is odd because it
        // "trims" some blocks

        var _newGrid = (0, _grid5.appendBlocksToGrid)(_grid, blocksPending); // Push active Tetromino up if necessary


        if ((0, _grid5.isPositionAvailable)(_newGrid, _activeTetrominoGrid, _activeTetrominoPosition)) {
          return updatePlayer(state, userId, {
            grid: _newGrid,
            blocksPending: []
          });
        } // Receiving rows of blocks from enemy might cause the active Tetromino
        // to overlap with the grid, so in some cases it will be pushed up
        // mid-drop to avoid collisions. The next DROP action will instantly
        // transfer active Tetromino to wall grid in these cases


        var _newPosition = (0, _grid5.getBottomMostPosition)(_newGrid, _activeTetrominoGrid, _activeTetrominoPosition);

        return updatePlayer(state, userId, {
          // The next `DROP` event will determine whether the well is full and
          // if the game is over or not
          grid: _newGrid,
          blocksPending: [],
          activeTetrominoPosition: _newPosition
        });
      }

    case 'MOVE_LEFT':
    case 'MOVE_RIGHT':
      {
        var direction = action.type === 'MOVE_LEFT' ? -1 : 1;

        var _player2 = getPlayer(state, userId);

        var _grid2 = _player2.grid,
            _activeTetrominoGrid2 = _player2.activeTetrominoGrid,
            _activeTetrominoPosition2 = _player2.activeTetrominoPosition;

        var _newPosition2 = _objectSpread({}, _activeTetrominoPosition2, {
          x: _activeTetrominoPosition2.x + direction
        }); // Attempting to move the Tetromino outside the Well bounds or over landed
        // Tetrominoes will be ignored


        if (!(0, _grid5.isPositionAvailable)(_grid2, _activeTetrominoGrid2, _newPosition2)) {
          return state;
        }

        return updatePlayer(state, userId, {
          activeTetrominoPosition: _newPosition2
        });
      }

    case 'ROTATE':
      {
        var _player3 = getPlayer(state, userId);

        var _grid3 = _player3.grid,
            _activeTetrominoGrid3 = _player3.activeTetrominoGrid,
            _activeTetrominoPosition3 = _player3.activeTetrominoPosition;

        var _newGrid2 = (0, _grid5.rotate)(_activeTetrominoGrid3); // If the rotation causes the active Tetromino to go outside of the
        // Well bounds, its position will be adjusted to fit inside


        var _newPosition3 = (0, _grid5.fitTetrominoPositionInWellBounds)(_grid3, _newGrid2, _activeTetrominoPosition3); // If the rotation causes a collision with landed Tetrominoes than it won't
        // be applied


        if (!(0, _grid5.isPositionAvailable)(_grid3, _newGrid2, _newPosition3)) {
          return state;
        }

        return updatePlayer(state, userId, {
          activeTetrominoGrid: _newGrid2,
          activeTetrominoPosition: _newPosition3
        });
      }

    case 'ENABLE_ACCELERATION':
      {
        var _player4 = getPlayer(state, userId);

        if (_player4.dropAcceleration) {
          return state;
        }

        return updatePlayer(state, userId, {
          dropAcceleration: true
        });
      }

    case 'DISABLE_ACCELERATION':
      {
        var _player5 = getPlayer(state, userId);

        if (!_player5.dropAcceleration) {
          return state;
        }

        return updatePlayer(state, userId, {
          dropAcceleration: false
        });
      }

    case 'PING':
      {
        var time = action.payload.time;
        return updatePlayer(state, userId, {
          ping: time
        });
      }

    default:
      return state;
  }
}

function getBlankGame(_ref) {
  var id = _ref.id,
      user = _ref.user,
      _ref$dropFrames = _ref.dropFrames,
      dropFrames = _ref$dropFrames === void 0 ? _grid4.DROP_FRAMES_DEFAULT : _ref$dropFrames;
  return {
    id: id,
    players: [getBlankPlayer(id, user)],
    dropFrames: dropFrames
  };
}

function getBlankPlayer(gameId, user) {
  return _objectSpread({
    user: user,
    lastActionId: 0,
    status: 'PENDING',
    losses: 0
  }, getBlankPlayerRound({
    gameId: gameId
  }));
}

function getBlankPlayerRound() {
  var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      gameId = _ref2.gameId,
      _ref2$round = _ref2.round,
      round = _ref2$round === void 0 ? 0 : _ref2$round,
      _ref2$drops = _ref2.drops,
      drops = _ref2$drops === void 0 ? 0 : _ref2$drops;

  return _objectSpread({
    drops: 0,
    score: 0,
    lines: 0,
    grid: (0, _grid5.generateEmptyGrid)(_grid4.WELL_ROWS, _grid4.WELL_COLS),
    blocksCleared: [],
    blocksPending: []
  }, getNextPlayerTetromino({
    gameId: gameId,
    round: round,
    drops: drops
  }), {
    dropAcceleration: false
  }, getBlankPlayerEffects());
}

function getNextPlayerTetromino() {
  var _ref3 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      gameId = _ref3.gameId,
      _ref3$round = _ref3.round,
      round = _ref3$round === void 0 ? 0 : _ref3$round,
      _ref3$drops = _ref3.drops,
      drops = _ref3$drops === void 0 ? 0 : _ref3$drops;

  // Generate random Tetromino sequence per game round
  var roundId = (parseInt(gameId, 16) * (round + 1)).toString(16);
  var activeTetromino = (0, _tetromino2.getNextTetromino)(roundId, drops);
  return {
    activeTetromino: activeTetromino,
    activeTetrominoGrid: _tetromino.SHAPES[activeTetromino],
    activeTetrominoPosition: (0, _tetromino2.getInitialPositionForTetromino)(activeTetromino, _grid4.WELL_COLS),
    nextTetromino: (0, _tetromino2.getNextTetromino)(roundId, drops + 1)
  };
}

function stripGameEffects(game) {
  return _objectSpread({}, game, {
    players: game.players.map(function (player) {
      return _objectSpread({}, player, getBlankPlayerEffects());
    })
  });
}

function isPlayer(game, curUser) {
  if (!curUser) {
    return false;
  } // Flow requires us to store the current user's id aside, as the curUser
  // object may change by the time the some callback is called. Smart!


  var id = curUser.id;
  return game.players.some(function (p) {
    return p.user.id === id;
  });
}

function getPlayer(game, userId) {
  var player = (0, _lodash.find)(game.players, function (p) {
    return p.user.id === userId;
  });

  if (!player) {
    throw new Error("Player with userId ".concat(userId, " does not exist"));
  }

  return player;
}

function getCurPlayer(game, curUser) {
  if (!game.players.length) {
    throw new Error('Games must have at least one player');
  }

  return curUser && isPlayer(game, curUser) ? getPlayer(game, curUser.id) : game.players[0];
}

function getOtherPlayer(game, curPlayer) {
  // NOTE: This only works with max 2 players per game
  return (0, _lodash.find)(game.players, function (p) {
    return p !== curPlayer;
  });
}

function allPlayersReady(game) {
  return game.players.filter(function (p) {
    return p.status === 'READY';
  }).length === game.players.length;
}

function addUserToGame(game, user) {
  var id = game.id,
      players = game.players;
  return _objectSpread({}, game, {
    players: [].concat(_toConsumableArray(players), [getBlankPlayer(id, user)])
  });
}

function updatePlayer(game, userId, attrs) {
  var players = game.players;
  var player = getPlayer(game, userId);
  var playerIndex = players.indexOf(player);
  return _objectSpread({}, game, {
    players: [].concat(_toConsumableArray(players.slice(0, playerIndex)), [_objectSpread({}, player, attrs)], _toConsumableArray(players.slice(playerIndex + 1)))
  });
} // This function can have three responses:
// - 0, which means the action points to game state
// - <0, which means the action is from the past and will be discarded
// - >0, which means the action is detached and backfill is required


function getGameActionOffset(game, action) {
  // There's no previous player action to follow when user just joined
  if (action.type === 'JOIN_GAME') {
    return 0;
  }

  var _action$payload3 = action.payload,
      userId = _action$payload3.userId,
      actionId = _action$payload3.actionId,
      prevActionId = _action$payload3.prevActionId;
  var player = (0, _lodash.find)(game.players, function (p) {
    return p.user.id === userId;
  }); // Sometimes we get actions from players that aren't found in the state
  // snapshot, because it's from a time before they joined

  if (!player) {
    return actionId;
  }

  return prevActionId - player.lastActionId;
}

function rewardClearedBlocks(game, userId) {
  var dropFrames = game.dropFrames;
  var player = getPlayer(game, userId);
  var score = player.score,
      lines = player.lines,
      blocksCleared = player.blocksCleared,
      dropAcceleration = player.dropAcceleration; // TODO: Calculate cells in Tetromino. All current Tetrominoes have 4 cells

  var cells = 4; // Rudimentary scoring logic, no T-Spin and combo bonuses

  var points = dropAcceleration ? cells * 2 : cells;

  if (blocksCleared.length) {
    points += _grid4.LINE_CLEAR_BONUSES[blocksCleared.length - 1] * (lines + 1);
  }

  return _objectSpread({}, updatePlayer(game, userId, {
    score: score + points,
    lines: lines + blocksCleared.length
  }), {
    // Increase speed whenever a line is cleared (fast game)
    dropFrames: blocksCleared.length ? dropFrames - _grid4.DROP_FRAMES_DECREMENT : dropFrames
  });
}

function getGameRound(game) {
  return game.players.reduce(function (acc, next) {
    return acc + next.losses;
  }, 0);
}

function sendClearedBlocksToEnemy(game, userId, unclearedGrid, rowsCleared) {
  var curPlayer = getPlayer(game, userId);
  var enemy = getOtherPlayer(game, curPlayer);

  if (!enemy) {
    return game;
  }

  var flashNay = enemy.flashNay;
  var blocksPending = (0, _grid5.overrideBlockIds)((0, _grid5.getBlocksFromGridRows)(unclearedGrid, rowsCleared), (0, _grid5.getNextCellId)(enemy));
  return updatePlayer(game, enemy.user.id, {
    blocksPending: [].concat(_toConsumableArray(enemy.blocksPending), _toConsumableArray(blocksPending)),
    flashNay: altFlashClass(flashNay)
  });
}

function altFlashClass(flashSuffix) {
  return flashSuffix === 'b' ? 'a' : 'b';
}

var QUAKE_A = ['a1', 'a2', 'a3', 'a4'];
var QUAKE_B = ['b1', 'b2', 'b3', 'b4'];

function altQuakeClass(quakeSuffix, magnitude) {
  if ([1, 2, 3, 4].indexOf(magnitude) === -1) {
    throw new Error("Invalid quake magnitute: ".concat(magnitude));
  }

  var offset = magnitude - 1;
  return quakeSuffix === QUAKE_B[offset] ? QUAKE_A[offset] : QUAKE_B[offset];
}

function getBlankPlayerEffects() {
  return {
    flashYay: null,
    flashNay: null,
    quake: null,
    ping: null
  };
}

function bumpActionId(game, userId, actionId) {
  return updatePlayer(game, userId, {
    lastActionId: actionId
  });
}

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _FadeIn = _interopRequireDefault(__webpack_require__(35));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Screen =
/*#__PURE__*/
function (_Component) {
  _inherits(Screen, _Component);

  function Screen() {
    _classCallCheck(this, Screen);

    return _possibleConstructorReturn(this, _getPrototypeOf(Screen).apply(this, arguments));
  }

  _createClass(Screen, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          title = _this$props.title,
          message = _this$props.message,
          actions = _this$props.actions;
      return _react.default.createElement("div", {
        className: "jsx-1152015136" + " " + "screen"
      }, _react.default.createElement(_FadeIn.default, null, _react.default.createElement("h2", {
        className: "jsx-1152015136" + " " + "title"
      }, title), _react.default.createElement("div", {
        className: "jsx-1152015136" + " " + "message"
      }, message)), _react.default.createElement("div", {
        className: "jsx-1152015136" + " " + "actions"
      }, actions.map(function (action, i) {
        return _react.default.createElement("div", {
          key: i,
          className: "jsx-1152015136" + " " + "button"
        }, action);
      })), _react.default.createElement(_style.default, {
        styleId: "1152015136",
        css: [".screen.jsx-1152015136{position:absolute;top:0;bottom:0;left:calc(100% / 10);right:calc(100% / 10);color:#34495f;}", ".title.jsx-1152015136{position:absolute;top:calc(100% / 20);margin:0;padding:0;font-size:2.2em;line-height:2.2em;font-weight:600;white-space:nowrap;opacity:0.7;}", ".message.jsx-1152015136{position:absolute;top:calc(100% / 20 * 5);left:0;right:0;height:calc(100% / 20 * 11);margin:0;padding:0;}", ".message.jsx-1152015136 p{margin:1em 0;margin-top:0;font-size:1.3em;line-height:1.4em;white-space:nowrap;}", ".message.jsx-1152015136 a{color:#34495f;}", ".message.jsx-1152015136 small{font-size:0.8em;opacity:0.8;}", ".message.jsx-1152015136 .highlight{background:rgba(245,228,129,1);padding:0.15em 0;}", ".actions.jsx-1152015136{position:absolute;top:calc(100% / 20 * 17);left:0;right:0;height:calc(100% / 20 * 2);font-size:1.1em;}", ".actions.jsx-1152015136 .button.jsx-1152015136{position:relative;float:right;width:50%;height:100%;}"]
      }));
    }
  }]);

  return Screen;
}(_react.Component);

exports.default = Screen;

_defineProperty(Screen, "defaultProps", {
  actions: []
});

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addCurUserToState = addCurUserToState;
exports.createUserSession = createUserSession;
exports.getDashboard = getDashboard;
exports.getGame = getGame;
exports.createGame = createGame;
exports.backfillGameActions = backfillGameActions;
exports.getDailyStats = getDailyStats;
exports.getApiUrl = getApiUrl;
exports.Unauthorized = Unauthorized;

var _regenerator = _interopRequireDefault(__webpack_require__(6));

var _isomorphicUnfetch = _interopRequireDefault(__webpack_require__(43));

var _cookie = _interopRequireDefault(__webpack_require__(44));

var _validation = __webpack_require__(45);

var _global = __webpack_require__(12);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// NOTE: This method is strictly called on the server side
function addCurUserToState(_x, _x2) {
  return _addCurUserToState.apply(this, arguments);
}

function _addCurUserToState() {
  _addCurUserToState = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee(req, store) {
    var _cookie$parse, sessionId, res, user;

    return _regenerator.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _cookie$parse = _cookie.default.parse(req.headers.cookie || ''), sessionId = _cookie$parse.sessionId;

            if (!sessionId) {
              _context.next = 12;
              break;
            }

            _context.prev = 2;
            _context.next = 5;
            return fetchJson('/auth', {
              headers: {
                cookie: "sessionId=".concat(sessionId)
              }
            });

          case 5:
            res = _context.sent;
            user = (0, _validation.getValidUser)(res);
            store.dispatch((0, _global.auth)(user));
            _context.next = 12;
            break;

          case 10:
            _context.prev = 10;
            _context.t0 = _context["catch"](2);

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this, [[2, 10]]);
  }));
  return _addCurUserToState.apply(this, arguments);
}

function createUserSession(_x3) {
  return _createUserSession.apply(this, arguments);
}

function _createUserSession() {
  _createUserSession = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee2(userName) {
    return _regenerator.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            return _context2.abrupt("return", fetchPost('/auth', {
              userName: userName
            }));

          case 1:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));
  return _createUserSession.apply(this, arguments);
}

function getDashboard() {
  return _getDashboard.apply(this, arguments);
}

function _getDashboard() {
  _getDashboard = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee3() {
    return _regenerator.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            return _context3.abrupt("return", fetchJson("/dashboard"));

          case 1:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, this);
  }));
  return _getDashboard.apply(this, arguments);
}

function getGame(_x4) {
  return _getGame.apply(this, arguments);
}

function _getGame() {
  _getGame = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee4(gameId) {
    return _regenerator.default.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            return _context4.abrupt("return", fetchJson("/game/".concat(gameId)));

          case 1:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, this);
  }));
  return _getGame.apply(this, arguments);
}

function createGame() {
  return _createGame.apply(this, arguments);
}

function _createGame() {
  _createGame = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee5() {
    return _regenerator.default.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            return _context5.abrupt("return", fetchPost('/game'));

          case 1:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, this);
  }));
  return _createGame.apply(this, arguments);
}

function backfillGameActions(_x5) {
  return _backfillGameActions.apply(this, arguments);
}

function _backfillGameActions() {
  _backfillGameActions = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee6(req) {
    return _regenerator.default.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            return _context6.abrupt("return", fetchPost("/backfill", req));

          case 1:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, this);
  }));
  return _backfillGameActions.apply(this, arguments);
}

function getDailyStats() {
  return _getDailyStats.apply(this, arguments);
}

function _getDailyStats() {
  _getDailyStats = _asyncToGenerator(
  /*#__PURE__*/
  _regenerator.default.mark(function _callee7() {
    return _regenerator.default.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            return _context7.abrupt("return", fetchJson("/daily-stats"));

          case 1:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, this);
  }));
  return _getDailyStats.apply(this, arguments);
}

function getApiUrl(path) {
  var baseUrl =  true ? getProdBaseUrl() : undefined;
  return path ? "".concat(baseUrl).concat(path) : "".concat(baseUrl, "/");
}

function Unauthorized() {
  this.name = 'Unauthorized';
  this.message = 'Invalid user session';
}

function getProdBaseUrl() {
  // Relative paths allow us to serve the prod app from any proxy address (eg.
  // via ngrok), but server-side requests need to contain the host address
  return typeof window === 'undefined' ? 'http://localhost:3000' : '';
}

function fetchJson(urlPath, options) {
  return (0, _isomorphicUnfetch.default)(getApiUrl(urlPath), options).then(parseResponse);
}

function fetchPost(urlPath) {
  var body = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  return fetchJson(urlPath, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  });
}

function parseResponse(response) {
  if (response.status >= 200 && response.status < 300) {
    return response.json();
  } else {
    if (response.status === 401) {
      throw new Unauthorized();
    } // TODO: Forward server error if response is parsable
    // return response.json().then(
    //   response => {
    //     throw new Error(response.error);
    //   },
    //   () => {
    //     throw new Error('Server error');
    //   }
    // );


    throw new Error('Server error');
  }
}

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("classnames");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.auth = auth;
exports.unauth = unauth;
exports.loadDashboard = loadDashboard;
exports.addGame = addGame;
exports.removeGame = removeGame;
exports.openGame = openGame;
exports.closeGame = closeGame;
exports.stripGameEffects = stripGameEffects;
exports.startBackfill = startBackfill;
exports.endBackfill = endBackfill;
exports.queueGameAction = queueGameAction;
exports.updateStats = updateStats;

function auth(user) {
  return {
    type: 'AUTH',
    payload: {
      user: user
    }
  };
}

function unauth() {
  return {
    type: 'UNAUTH'
  };
}

function loadDashboard(_ref) {
  var games = _ref.games,
      stats = _ref.stats;
  return {
    type: 'LOAD_DASHBOARD',
    payload: {
      games: games,
      stats: stats
    }
  };
}

function addGame(game) {
  return {
    type: 'ADD_GAME',
    payload: {
      game: game
    }
  };
}

function removeGame(gameId) {
  return {
    type: 'REMOVE_GAME',
    payload: {
      gameId: gameId
    }
  };
}

function openGame(gameId) {
  return {
    type: 'OPEN_GAME',
    payload: {
      gameId: gameId
    }
  };
}

function closeGame() {
  return {
    type: 'CLOSE_GAME'
  };
}

function stripGameEffects() {
  return {
    type: 'STRIP_GAME_EFFECTS'
  };
}

function startBackfill(gameId, backfillId) {
  return {
    type: 'START_BACKFILL',
    payload: {
      gameId: gameId,
      backfillId: backfillId
    }
  };
}

function endBackfill(backfillId) {
  return {
    type: 'END_BACKFILL',
    payload: {
      backfillId: backfillId
    }
  };
}

function queueGameAction(action) {
  return {
    type: 'QUEUE_GAME_ACTION',
    payload: {
      action: action
    }
  };
}

function updateStats(stats) {
  return {
    type: 'UPDATE_STATS',
    payload: {
      stats: stats
    }
  };
}

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.curGameReducer = curGameReducer;
exports.getCurGame = getCurGame;
var initialState = null;

function curGameReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case 'OPEN_GAME':
      {
        var gameId = action.payload.gameId;
        return gameId;
      }

    case 'CLOSE_GAME':
      {
        return null;
      }

    default:
      return state;
  }
}

function getCurGame(_ref) {
  var games = _ref.games,
      curGame = _ref.curGame;

  if (!curGame) {
    throw new Error('Current game is missing from state');
  }

  if (!games[curGame]) {
    throw new Error("Current game points to missing game ".concat(curGame));
  }

  return games[curGame];
}

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.curUserReducer = curUserReducer;
exports.getCurUser = getCurUser;
var initialState = null;

function curUserReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case 'AUTH':
      {
        var user = action.payload.user;
        return user;
      }

    case 'UNAUTH':
      {
        return null;
      }

    default:
      return state;
  }
}

function getCurUser(state) {
  if (!state.curUser) {
    throw new Error('Current user is missing from state');
  }

  return state.curUser;
}

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateEmptyGrid = generateEmptyGrid;
exports.rotate = rotate;
exports.getExactPosition = getExactPosition;
exports.isPositionAvailable = isPositionAvailable;
exports.getBottomMostPosition = getBottomMostPosition;
exports.transferTetrominoToGrid = transferTetrominoToGrid;
exports.hasLines = hasLines;
exports.clearLines = clearLines;
exports.fitTetrominoPositionInWellBounds = fitTetrominoPositionInWellBounds;
exports.getBlocksFromGridRows = getBlocksFromGridRows;
exports.overrideBlockIds = overrideBlockIds;
exports.appendBlocksToGrid = appendBlocksToGrid;
exports.getNextCellId = getNextCellId;

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function generateEmptyGrid(rows, cols) {
  var matrix = [];

  for (var row = 0; row < rows; row++) {
    matrix[row] = [];

    for (var col = 0; col < cols; col++) {
      matrix[row][col] = null;
    }
  }

  return matrix;
}

function rotate(grid) {
  var matrix = [];
  var rows = grid.length;
  var cols = grid[0].length;

  for (var row = 0; row < rows; row++) {
    matrix[row] = [];

    for (var col = 0; col < cols; col++) {
      matrix[row][col] = grid[cols - 1 - col][row];
    }
  }

  return matrix;
}

function getExactPosition(_ref) {
  var x = _ref.x,
      y = _ref.y;
  // The position has floating numbers because of how gravity is incremented
  // with each frame
  return {
    x: Math.floor(x),
    y: Math.floor(y)
  };
}

function isPositionAvailable(grid, tetrominoGrid, position) {
  var rows = grid.length;
  var cols = grid[0].length;
  var tetrominoRows = tetrominoGrid.length;
  var tetrominoCols = tetrominoGrid[0].length;
  var tetrominoPositionInGrid = getExactPosition(position);
  var relativeRow;
  var relativeCol;

  for (var row = 0; row < tetrominoRows; row++) {
    for (var col = 0; col < tetrominoCols; col++) {
      // Ignore blank squares from the Tetromino grid
      if (tetrominoGrid[row][col]) {
        relativeRow = tetrominoPositionInGrid.y + row;
        relativeCol = tetrominoPositionInGrid.x + col; // Ensure Tetromino block is within the horizontal bounds

        if (relativeCol < 0 || relativeCol >= cols) {
          return false;
        } // Check check if Tetromino hit the bottom of the Well


        if (relativeRow >= rows) {
          return false;
        } // Tetrominoes are accepted on top of the Well (it's how they enter)


        if (relativeRow >= 0) {
          // Then if the position is not already taken inside the grid
          if (grid[relativeRow][relativeCol]) {
            return false;
          }
        }
      }
    }
  }

  return true;
}

function getBottomMostPosition(grid, tetrominoGrid, position) {
  // Snap vertical position to grid
  var y = Math.floor(position.y);

  while (!isPositionAvailable(grid, tetrominoGrid, {
    x: position.x,
    y: y
  })) {
    y -= 1;
  }

  return _objectSpread({}, position, {
    y: y
  });
}

function transferTetrominoToGrid(player, tetrominoGrid, position, color) {
  var grid = player.grid;
  var rows = tetrominoGrid.length;
  var cols = tetrominoGrid[0].length;
  var tetrominoPositionInGrid = getExactPosition(position);
  var newGrid = grid.map(function (l) {
    return l.map(function (c) {
      return c;
    });
  });
  var relativeRow;
  var relativeCol;
  var nextCellId = getNextCellId(player);

  for (var row = 0; row < rows; row++) {
    for (var col = 0; col < cols; col++) {
      // Ignore blank squares from the Tetromino grid
      if (tetrominoGrid[row][col]) {
        relativeCol = tetrominoPositionInGrid.x + col;
        relativeRow = tetrominoPositionInGrid.y + row; // When the Well is full the Tetromino will land before it enters the
        // top of the Well, so we need to protect against negative row indexes

        if (newGrid[relativeRow]) {
          newGrid[relativeRow][relativeCol] = [nextCellId++, color];
        }
      }
    }
  }

  return newGrid;
}

var createEmptyLine = function createEmptyLine(cols) {
  return _toConsumableArray(Array(cols)).map(function () {
    return null;
  });
};

var isLine = function isLine(row) {
  return !row.some(function (cell) {
    return cell === null;
  });
};

function hasLines(well) {
  return well.reduce(function (acc, rowBlocks) {
    return acc || isLine(rowBlocks);
  }, false);
}

function clearLines(grid) {
  /**
   * Clear all rows that form a complete line, from one left to right, inside
   * the Well grid. Gravity is applied to fill in the cleared lines with the
   * ones above, thus freeing up the Well for more Tetrominoes to enter.
   */
  var rows = grid.length;
  var cols = grid[0].length;
  var clearedGrid = grid.map(function (l) {
    return l;
  });
  var rowsCleared = [];

  for (var row = rows - 1; row >= 0; row--) {
    if (isLine(clearedGrid[row])) {
      for (var row2 = row; row2 >= 0; row2--) {
        clearedGrid[row2] = row2 > 0 ? clearedGrid[row2 - 1] : createEmptyLine(cols);
      } // Because the grid "falls" with every cleared line, the index of the
      // original row is smaller than the current row index by the number of
      // cleared on this occasion. We `unshift` because the lines are cleared
      // from bottom to top, but will then be applied from top to bottom


      rowsCleared.unshift(row - rowsCleared.length); // Go once more through the same row

      row++;
    }
  }

  return {
    clearedGrid: clearedGrid,
    rowsCleared: rowsCleared
  };
}

function fitTetrominoPositionInWellBounds(grid, tetrominoGrid, _ref2) {
  var x = _ref2.x,
      y = _ref2.y;
  var cols = grid[0].length;
  var tetrominoRows = tetrominoGrid.length;
  var tetrominoCols = tetrominoGrid[0].length;
  var newX = x;
  var relativeCol;

  for (var row = 0; row < tetrominoRows; row++) {
    for (var col = 0; col < tetrominoCols; col++) {
      // Ignore blank squares from the Tetromino grid
      if (tetrominoGrid[row][col]) {
        relativeCol = newX + col; // Wall kick: A Tetromino grid that steps outside of the Well grid will
        // be shifted slightly to slide back inside the Well grid

        if (relativeCol < 0) {
          newX -= relativeCol;
        } else if (relativeCol >= cols) {
          newX -= relativeCol - cols + 1;
        }
      }
    }
  }

  return {
    x: newX,
    y: y
  };
}

function getBlocksFromGridRows(grid, rows) {
  return rows.map(function (rowIndex) {
    return grid[rowIndex].map(function (block) {
      return block;
    });
  });
}

function overrideBlockIds(blocks, fromCellId) {
  var nextCellId = fromCellId;
  return blocks.map(function (blockRow) {
    return blockRow.map(function (block) {
      return block ? [nextCellId++, block[1]] : null;
    });
  });
}

function appendBlocksToGrid(grid, blocks) {
  var rows = grid.length;
  var cols = grid[0].length;
  var newGrid = generateEmptyGrid(rows, cols); // 1. Apply new block rows at the bottom, and collect top "border shape"
  // Hmm, this isn't very good, because 99% enemy blocks will create a line
  // NOTE: Old functionality left for posterity
  // const availRowsPerCol = new Array(cols).fill(rows);

  blocks.forEach(function (rowBlocks, rowIndex) {
    var absRowIndex = rows - blocks.length + rowIndex;
    rowBlocks.forEach(function (block, colIndex) {
      if (block) {
        newGrid[absRowIndex][colIndex] = block; // availRowsPerCol[colIndex] = Math.min(
        //   availRowsPerCol[colIndex],
        //   absRowIndex
        // );
      }
    });
  }); // 2. "Pour" previous blocks over new ones

  for (var colIndex = 0; colIndex < cols; colIndex++) {
    // const rowOffset = rows - availRowsPerCol[colIndex];
    var rowOffset = blocks.length;

    for (var rowIndex = rows - 1; rowIndex >= rowOffset; rowIndex--) {
      newGrid[rowIndex - rowOffset][colIndex] = grid[rowIndex][colIndex];
    }
  }

  return newGrid;
}

function getNextCellId(player) {
  var max = Math.max;
  var grid = player.grid,
      blocksCleared = player.blocksCleared,
      blocksPending = player.blocksPending;
  return max(getMaxCellIdFromGrid(grid), getMaxCellIdFromGrid(blocksCleared), getMaxCellIdFromGrid(blocksPending)) + 1;
}

function getMaxCellIdFromGrid(grid) {
  var max = Math.max;
  return max.apply(void 0, _toConsumableArray(grid.map(function (r) {
    return max.apply(void 0, _toConsumableArray(r.map(function (cell) {
      return cell ? cell[0] : 0;
    })));
  })));
}

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LINE_CLEAR_BONUSES = exports.DROP_FRAMES_ACCELERATED = exports.DROP_FRAMES_DECREMENT = exports.DROP_FRAMES_DEFAULT = exports.WELL_COLS = exports.WELL_ROWS = void 0;
var WELL_ROWS = 20;
exports.WELL_ROWS = WELL_ROWS;
var WELL_COLS = 10;
exports.WELL_COLS = WELL_COLS;
var DROP_FRAMES_DEFAULT = 48; // 1 row in less than a second

exports.DROP_FRAMES_DEFAULT = DROP_FRAMES_DEFAULT;
var DROP_FRAMES_DECREMENT = 1.5;
exports.DROP_FRAMES_DECREMENT = DROP_FRAMES_DECREMENT;
var DROP_FRAMES_ACCELERATED = 1; // 60 rows per second

exports.DROP_FRAMES_ACCELERATED = DROP_FRAMES_ACCELERATED;
var LINE_CLEAR_BONUSES = [100, 300, 500, 800];
exports.LINE_CLEAR_BONUSES = LINE_CLEAR_BONUSES;

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(0));

var _head = _interopRequireDefault(__webpack_require__(14));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Title =
/*#__PURE__*/
function (_Component) {
  _inherits(Title, _Component);

  function Title() {
    _classCallCheck(this, Title);

    return _possibleConstructorReturn(this, _getPrototypeOf(Title).apply(this, arguments));
  }

  _createClass(Title, [{
    key: "render",
    value: function render() {
      return _react.default.createElement(_head.default, null, _react.default.createElement("title", null, this.props.children));
    }
  }]);

  return Title;
}(_react.Component);

exports.default = Title;

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _tetromino = __webpack_require__(3);

var _game = __webpack_require__(4);

var _Tetromino = _interopRequireDefault(__webpack_require__(21));

var _PlayerInfo = _interopRequireDefault(__webpack_require__(42));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var GamePanel =
/*#__PURE__*/
function (_Component) {
  _inherits(GamePanel, _Component);

  function GamePanel() {
    _classCallCheck(this, GamePanel);

    return _possibleConstructorReturn(this, _getPrototypeOf(GamePanel).apply(this, arguments));
  }

  _createClass(GamePanel, [{
    key: "render",

    /**
     * The game panel contains:
     * - The logo
     * - The next Tetromino for the current player
     * - The name and score for each player
     * - Footer with credits
     */
    value: function render() {
      var _this$props = this.props,
          game = _this$props.game,
          onSelectP2 = _this$props.onSelectP2,
          showFooter = _this$props.showFooter;
      var player1 = game && game.players[0];
      var player2 = game && game.players[1];
      var nextTetromino = this.getNextTetromino();
      var showP1ReadyState = showPlayerReadyState(game, true);
      var showP2ReadyState = showPlayerReadyState(game, false);
      return _react.default.createElement("div", {
        className: "jsx-2764329408" + " " + "game-panel"
      }, _react.default.createElement("div", {
        className: "jsx-2764329408" + " " + "title"
      }, _react.default.createElement("h1", {
        className: "jsx-2764329408"
      }, "Flatris")), _react.default.createElement("div", {
        className: "jsx-2764329408" + " " + "next-label"
      }, "Next"), _react.default.createElement("div", {
        className: "jsx-2764329408" + " " + (getNextTetrominoClass(nextTetromino) || "")
      }, _react.default.createElement(_Tetromino.default, {
        key: nextTetromino,
        color: game ? _tetromino.COLORS[nextTetromino] : '#ecf0f1',
        grid: _tetromino.SHAPES[nextTetromino]
      })), _react.default.createElement("div", {
        className: "jsx-2764329408" + " " + "player1"
      }, _react.default.createElement(_PlayerInfo.default, {
        player: player1,
        wins: player2 && player2.losses,
        isPlayer1: true,
        showReadyState: showP1ReadyState
      })), _react.default.createElement("div", {
        className: "jsx-2764329408" + " " + "player2"
      }, _react.default.createElement(_PlayerInfo.default, {
        player: player2,
        wins: player1 && player2 && player1.losses,
        isPlayer1: false,
        showReadyState: showP2ReadyState,
        onSelect: onSelectP2
      })), showFooter && _react.default.createElement("div", {
        className: "jsx-2764329408" + " " + "footer"
      }, "By", ' ', _react.default.createElement("a", {
        href: "https://twitter.com/skidding",
        target: "_blank",
        className: "jsx-2764329408"
      }, "skidding")), _react.default.createElement(_style.default, {
        styleId: "2764329408",
        css: [".game-panel.jsx-2764329408{position:absolute;top:0;bottom:0;left:0;right:0;}", ".game-panel.jsx-2764329408>div.jsx-2764329408{position:absolute;left:calc(100% / 6);width:calc(100% / 6 * 4);}", ".title.jsx-2764329408{top:calc(100% / 20);}", ".title.jsx-2764329408 h1.jsx-2764329408{margin:0;padding:0;color:#34495f;font-size:2.8em;font-family:'Teko',sans-serif;font-weight:400;line-height:1.3em;text-transform:uppercase;-webkit-letter-spacing:0.02em;-moz-letter-spacing:0.02em;-ms-letter-spacing:0.02em;letter-spacing:0.02em;}", ".next-label.jsx-2764329408{top:calc(100% / 20 * 4);margin-top:-0.25em;color:rgba(155,164,171,0.8);font-family:'Teko',sans-serif;font-weight:300;font-size:1.8em;line-height:1em;-webkit-letter-spacing:0.02em;-moz-letter-spacing:0.02em;-ms-letter-spacing:0.02em;letter-spacing:0.02em;text-transform:uppercase;}", ".next-tetromino.jsx-2764329408{top:calc(100% / 20 * 5);height:calc(100% / 20 * 4);}", ".next-tetromino-I.jsx-2764329408{-webkit-transform:translate(0,-25%);-ms-transform:translate(0,-25%);transform:translate(0,-25%);}", ".player1.jsx-2764329408{position:absolute;top:calc(100% / 20 * 8);height:calc(100% / 20 * 4);}", ".player2.jsx-2764329408{position:absolute;top:calc(100% / 20 * 13);height:calc(100% / 20 * 4);}", ".footer.jsx-2764329408{position:absolute;top:calc(100% / 20 * 18);height:calc(100% / 20);color:rgba(155,164,171,0.8);font-family:'Teko',sans-serif;font-weight:300;font-size:1.8em;line-height:1.2em;-webkit-letter-spacing:0.02em;-moz-letter-spacing:0.02em;-ms-letter-spacing:0.02em;letter-spacing:0.02em;text-transform:uppercase;text-align:center;white-space:nowrap;}", ".footer.jsx-2764329408 a.jsx-2764329408{color:#9ba4ab;}"]
      }));
    }
  }, {
    key: "getNextTetromino",
    value: function getNextTetromino() {
      var _this$props2 = this.props,
          curUser = _this$props2.curUser,
          game = _this$props2.game;

      if (!game) {
        return 'S';
      }

      var curPlayer = (0, _game.getCurPlayer)(game, curUser);
      var nextTetromino = curPlayer.nextTetromino;
      return nextTetromino;
    }
  }]);

  return GamePanel;
}(_react.Component);

exports.default = GamePanel;

_defineProperty(GamePanel, "defaultProps", {
  showFooter: false
});

function getNextTetrominoClass(tetromino) {
  // We use this extra class to position tetrominoes differently from CSS
  // based on their type
  return "next-tetromino next-tetromino-".concat(tetromino);
}

function showPlayerReadyState(game, isPlayer1) {
  if (!game || (0, _game.allPlayersReady)(game)) {
    return false;
  }

  var player1 = game.players[0];

  if (isPlayer1 && player1.status === 'READY') {
    return true;
  }

  var player2 = game.players[1];

  if (!isPlayer1 && player2 && player2.status === 'READY') {
    return true;
  }

  return false;
}

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _propTypes = _interopRequireDefault(__webpack_require__(11));

var _SquareBlock = _interopRequireDefault(__webpack_require__(36));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Tetromino =
/*#__PURE__*/
function (_Component) {
  _inherits(Tetromino, _Component);

  function Tetromino() {
    _classCallCheck(this, Tetromino);

    return _possibleConstructorReturn(this, _getPrototypeOf(Tetromino).apply(this, arguments));
  }

  _createClass(Tetromino, [{
    key: "renderGridBlocks",

    /**
     * A Tetromino is a geometric shape composed of four squares, connected
     * orthogonally. Read more at http://en.wikipedia.org/wiki/Tetromino
     */
    value: function renderGridBlocks() {
      var blocks = [];
      var rows = this.props.grid.length;
      var cols = this.props.grid[0].length;

      for (var row = 0; row < rows; row++) {
        for (var col = 0; col < cols; col++) {
          if (this.props.grid[row][col]) {
            blocks.push(_react.default.createElement("div", {
              key: "".concat(row, "-").concat(col),
              style: {
                top: "".concat(row * 25, "%"),
                left: "".concat(col * 25, "%")
              },
              className: "jsx-4287887962" + " " + "grid-square-block"
            }, _react.default.createElement(_SquareBlock.default, {
              color: this.props.color
            }), _react.default.createElement(_style.default, {
              styleId: "4287887962",
              css: [".grid-square-block.jsx-4287887962{position:absolute;width:25%;height:25%;}"]
            })));
          }
        }
      }

      return blocks;
    }
  }, {
    key: "render",
    value: function render() {
      return _react.default.createElement("div", {
        className: "jsx-2115617632" + " " + "tetromino"
      }, this.renderGridBlocks(), _react.default.createElement(_style.default, {
        styleId: "2115617632",
        css: [".tetromino.jsx-2115617632{position:absolute;width:100%;height:100%;}"]
      }));
    }
  }]);

  return Tetromino;
}(_react.Component);

exports.default = Tetromino;

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.logError = logError;

/* global window */
function logError(err, customData) {
  try {
    window.Rollbar.error(err, customData);
  } catch (err2) {
    console.error('Rollbar client failed');
    console.error(err2);
  }
}

/***/ }),
/* 23 */
/***/ (function(module, exports) {

module.exports = require("next-redux-wrapper");

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createStore = createStore;

var _redux = __webpack_require__(25);

var _reduxThunk = _interopRequireDefault(__webpack_require__(26));

var _reduxDevtoolsExtension = __webpack_require__(27);

var _jsReady = __webpack_require__(28);

var _curUser = __webpack_require__(15);

var _games = __webpack_require__(29);

var _curGame = __webpack_require__(13);

var _backfills = __webpack_require__(32);

var _stats = __webpack_require__(33);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var rootReducer = (0, _redux.combineReducers)({
  jsReady: _jsReady.jsReadyReducer,
  curUser: _curUser.curUserReducer,
  games: _games.gamesReducer,
  curGame: _curGame.curGameReducer,
  backfills: _backfills.backfillsReducer,
  stats: _stats.statsReducer
});

function createStore(initialState) {
  return (0, _redux.createStore)(rootReducer, initialState, (0, _reduxDevtoolsExtension.composeWithDevTools)((0, _redux.applyMiddleware)(_reduxThunk.default)));
}

/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = require("redux-thunk");

/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = require("redux-devtools-extension");

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.jsReadyReducer = jsReadyReducer;
var initialState = false;

function jsReadyReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case 'JS_LOAD':
      {
        return true;
      }

    default:
      return state;
  }
}

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.gamesReducer = gamesReducer;

var _game = __webpack_require__(4);

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {};

function gamesReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case 'LOAD_DASHBOARD':
      {
        var games = action.payload.games;
        return getEffectlessGames( // NOTE: We don't care about order at the moment
        games.reduce(function (acc, game) {
          return _objectSpread({}, acc, _defineProperty({}, game.id, game));
        }, {}));
      }

    case 'ADD_GAME':
      {
        var game = action.payload.game;
        return _objectSpread({}, state, _defineProperty({}, game.id, (0, _game.stripGameEffects)(game)));
      }

    case 'REMOVE_GAME':
      {
        var gameId = action.payload.gameId;
        return Object.keys(state).reduce(function (acc, gId) {
          return gId !== gameId ? _objectSpread({}, acc, _defineProperty({}, gId, state[gId])) : acc;
        }, {});
      }

    case 'STRIP_GAME_EFFECTS':
      {
        return getEffectlessGames(state);
      }

    case 'JOIN_GAME':
    case 'PLAYER_READY':
    case 'PLAYER_PAUSE':
    case 'DROP':
    case 'MOVE_LEFT':
    case 'MOVE_RIGHT':
    case 'ROTATE':
    case 'ENABLE_ACCELERATION':
    case 'DISABLE_ACCELERATION':
    case 'APPEND_PENDING_BLOCKS':
    case 'PING':
      {
        var _gameId = action.payload.gameId;

        if (!_gameId) {
          throw new Error("Game action ".concat(action.type, " has no gameId"));
        }

        if (!state[_gameId]) {
          throw new Error("Game action ".concat(action.type, " for missing game ").concat(_gameId));
        }

        return _objectSpread({}, state, _defineProperty({}, _gameId, (0, _game.gameReducer)(state[_gameId], action)));
      }

    default:
      return state;
  }
}

function getEffectlessGames(games) {
  return Object.keys(games).reduce(function (acc, gameId) {
    return _objectSpread({}, acc, _defineProperty({}, gameId, (0, _game.stripGameEffects)(games[gameId])));
  }, {});
}

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getNextTetromino = getNextTetromino;
exports.getInitialPositionForTetromino = getInitialPositionForTetromino;

var _crc = __webpack_require__(31);

var _tetromino = __webpack_require__(3);

function getNextTetromino(gameId, nth) {
  // $FlowFixMe
  var tetrominos = Object.keys(_tetromino.SHAPES);
  var randNum = (0, _crc.str)(gameId + nth);
  return tetrominos[Math.abs(randNum) % tetrominos.length];
}

function getInitialPositionForTetromino(tetromino, gridCols) {
  /**
   * Generates positions a Tetromino entering the Well. The I Tetromino
   * occupies columns 4, 5, 6 and 7, the O Tetromino occupies columns 5 and
   * 6, and the remaining 5 Tetrominoes occupy columns 4, 5 and 6. Pieces
   * spawn above the visible playfield (that's why y is -2)
   */
  var grid = _tetromino.SHAPES[tetromino];
  return {
    x: Math.round(gridCols / 2) - Math.round(grid[0].length / 2),
    y: -2
  };
}

/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = require("crc-32");

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.backfillsReducer = backfillsReducer;

var _lodash = __webpack_require__(7);

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {};

function backfillsReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case 'START_BACKFILL':
      {
        var _action$payload = action.payload,
            gameId = _action$payload.gameId,
            backfillId = _action$payload.backfillId;
        return _objectSpread({}, state, _defineProperty({}, gameId, {
          backfillId: backfillId,
          queuedActions: []
        }));
      }

    case 'END_BACKFILL':
      {
        var _backfillId = action.payload.backfillId;

        var _gameId = (0, _lodash.findKey)(state, function (b) {
          return b.backfillId === _backfillId;
        });

        if (!_gameId) {
          return state;
        }

        return (0, _lodash.omit)(state, _gameId);
      }

    case 'QUEUE_GAME_ACTION':
      {
        var queuedAction = action.payload.action;
        var _queuedAction$payload = queuedAction.payload,
            actionId = _queuedAction$payload.actionId,
            _gameId2 = _queuedAction$payload.gameId;
        var backfill = state[_gameId2];

        if (!backfill) {
          console.warn("Trying to queue action ".concat(actionId, " outside backfill"));
          return state;
        }

        var _backfillId2 = backfill.backfillId,
            queuedActions = backfill.queuedActions;
        return _objectSpread({}, state, _defineProperty({}, _gameId2, {
          backfillId: _backfillId2,
          queuedActions: [].concat(_toConsumableArray(queuedActions), [queuedAction])
        }));
      }

    default:
      return state;
  }
}

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.statsReducer = statsReducer;
var initialState = {
  actionAcc: 0,
  actionLeft: 0,
  actionRight: 0,
  actionRotate: 0,
  games: 0,
  lines: 0,
  seconds: 0
};

function statsReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case 'LOAD_DASHBOARD':
    case 'UPDATE_STATS':
      {
        var stats = action.payload.stats;
        return stats;
      }

    default:
      return state;
  }
}

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _link = _interopRequireDefault(__webpack_require__(18));

var _Title = _interopRequireDefault(__webpack_require__(19));

var _Button = _interopRequireDefault(__webpack_require__(2));

var _CopyButton = _interopRequireDefault(__webpack_require__(37));

var _Screen = _interopRequireDefault(__webpack_require__(5));

var _GameFrame = _interopRequireDefault(__webpack_require__(38));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Error =
/*#__PURE__*/
function (_Component) {
  _inherits(Error, _Component);

  function Error() {
    _classCallCheck(this, Error);

    return _possibleConstructorReturn(this, _getPrototypeOf(Error).apply(this, arguments));
  }

  _createClass(Error, [{
    key: "render",
    value: function render() {
      var statusCode = this.props.statusCode;
      return _react.default.createElement(_GameFrame.default, null, _react.default.createElement(_Title.default, null, "Error :/"), statusCode === 404 ? this.render404() : this.renderMisc());
    }
  }, {
    key: "render404",
    value: function render404() {
      return _react.default.createElement(_Screen.default, {
        title: "Not Found",
        message: _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", null, "Hmm..."), _react.default.createElement("p", null, "It's either gone or", _react.default.createElement("br", null), "it never was \xAF\\_(\u30C4)_/\xAF")),
        actions: [_react.default.createElement(_link.default, {
          href: "/"
        }, _react.default.createElement(_Button.default, null, "Home"))]
      });
    }
  }, {
    key: "renderMisc",
    value: function renderMisc() {
      var error = this.props.error;
      return _react.default.createElement(_Screen.default, {
        title: "Oh Noes!",
        message: _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", {
          className: "jsx-1832570087"
        }, _react.default.createElement("strong", {
          className: "jsx-1832570087"
        }, "Something broke :/")), error && _react.default.createElement(_react.Fragment, null, _react.default.createElement("div", {
          className: "jsx-1832570087" + " " + "copy"
        }, _react.default.createElement(_CopyButton.default, {
          disabled: false,
          copyText: "".concat(error.message, "\n\n").concat(error.stack),
          defaultLabel: "Copy error",
          successLabel: "Error copied!",
          errorLabel: "Copy failed :("
        })), _react.default.createElement("p", {
          className: "jsx-1832570087"
        }, _react.default.createElement("span", {
          className: "jsx-1832570087" + " " + "highlight"
        }, "Please", ' ', _react.default.createElement("a", {
          href: getGithubIssueUrl(error),
          target: "_blank",
          className: "jsx-1832570087"
        }, "click here"), ' ', "to", _react.default.createElement("br", {
          className: "jsx-1832570087"
        }), "share what happened.")), _react.default.createElement("p", {
          className: "jsx-1832570087"
        }, "At least this page", _react.default.createElement("br", {
          className: "jsx-1832570087"
        }), "works, right?")), _react.default.createElement(_style.default, {
          styleId: "1832570087",
          css: [".copy.jsx-1832570087{position:relative;height:calc(100% / 11 * 2);margin:1em 0;font-size:1.1em;}"]
        })),
        actions: [_react.default.createElement(_link.default, {
          href: "/"
        }, _react.default.createElement(_Button.default, null, "Home"))]
      });
    }
  }]);

  return Error;
}(_react.Component);

exports.default = Error;

function getGithubIssueUrl(error) {
  var title = 'Check out this error';
  var body = getGithubIssueBody(error);
  return "https://github.com/skidding/flatris/issues/new?title=".concat(encodeURIComponent(title), "&body=").concat(encodeURIComponent(body));
}

function getGithubIssueBody(_ref) {
  var message = _ref.message,
      stack = _ref.stack;
  return "## Error\n\n".concat(message, "\n\n## Component stack trace\n\n").concat(stack);
}

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var FadeIn =
/*#__PURE__*/
function (_Component) {
  _inherits(FadeIn, _Component);

  function FadeIn() {
    _classCallCheck(this, FadeIn);

    return _possibleConstructorReturn(this, _getPrototypeOf(FadeIn).apply(this, arguments));
  }

  _createClass(FadeIn, [{
    key: "render",
    value: function render() {
      var children = this.props.children;
      return _react.default.createElement("div", {
        className: "jsx-1497761764" + " " + "fade-in"
      }, children, _react.default.createElement(_style.default, {
        styleId: "1497761764",
        css: [".fade-in.jsx-1497761764{opacity:0;-webkit-animation-name:fadeIn-jsx-1497761764;animation-name:fadeIn-jsx-1497761764;-webkit-animation-duration:0.5s;animation-duration:0.5s;-webkit-animation-delay:0.1s;animation-delay:0.1s;-webkit-animation-fill-mode:forwards;animation-fill-mode:forwards;}", "@-webkit-keyframes fadeIn-jsx-1497761764{0%{opacity:0;}100%{opacity:1;}}", "@keyframes fadeIn-jsx-1497761764{0%{opacity:0;}100%{opacity:1;}}"]
      }));
    }
  }]);

  return FadeIn;
}(_react.Component);

exports.default = FadeIn;

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireDefault(__webpack_require__(0));

var _propTypes = _interopRequireDefault(__webpack_require__(11));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Building block for Tetrominoes and the grid of the Well, occupying a 1x1
 * square block. The only configurable property square blocks have is their
 * color.
 */
var SquareBlock = function SquareBlock(_ref) {
  var color = _ref.color;
  return _react.default.createElement("div", {
    style: {
      backgroundColor: color
    },
    className: "jsx-86954139" + " " + "square-block"
  }, _react.default.createElement(_style.default, {
    styleId: "86954139",
    css: [".square-block.jsx-86954139{position:absolute;width:100%;height:100%;}"]
  }));
};

var _default = SquareBlock;
exports.default = _default;

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(0));

var _clipboard = _interopRequireDefault(__webpack_require__(41));

var _tetromino = __webpack_require__(3);

var _Button = _interopRequireDefault(__webpack_require__(2));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var CopyButton =
/*#__PURE__*/
function (_Component) {
  _inherits(CopyButton, _Component);

  function CopyButton() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, CopyButton);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(CopyButton)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      copyStatus: null
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleCopyBtnRef", function (node) {
      if (node) {
        var clipboard = new _clipboard.default(node);
        clipboard.on('success', _this.handleCopySuccess);
        clipboard.on('error', _this.handleCopyError);
        _this.clipboard = clipboard;
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleCopySuccess", function () {
      _this.setState({
        copyStatus: 'success'
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleCopyError", function () {
      _this.setState({
        copyStatus: 'error'
      });
    });

    return _this;
  }

  _createClass(CopyButton, [{
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.clipboard) {
        this.clipboard.destroy();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          disabled = _this$props.disabled,
          copyText = _this$props.copyText,
          defaultLabel = _this$props.defaultLabel,
          successLabel = _this$props.successLabel,
          errorLabel = _this$props.errorLabel;
      var copyStatus = this.state.copyStatus;
      var bgColor;

      switch (copyStatus) {
        case 'success':
          bgColor = _tetromino.COLORS.S;
          break;

        case 'error':
          bgColor = _tetromino.COLORS.Z;
          break;

        default:
          bgColor = _tetromino.COLORS.J;
      }

      return !disabled ? _react.default.createElement("div", {
        ref: this.handleCopyBtnRef,
        "data-clipboard-text": copyText
      }, _react.default.createElement(_Button.default, {
        bgColor: bgColor,
        color: "#fff"
      }, !copyStatus && defaultLabel, copyStatus === 'success' && successLabel, copyStatus === 'error' && errorLabel)) : _react.default.createElement("div", null, _react.default.createElement(_Button.default, {
        disabled: true,
        bgColor: bgColor,
        color: "#fff"
      }, defaultLabel));
    }
  }]);

  return CopyButton;
}(_react.Component);

exports.default = CopyButton;

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _GameContainer = _interopRequireDefault(__webpack_require__(39));

var _GamePanel = _interopRequireDefault(__webpack_require__(20));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var GameFrame =
/*#__PURE__*/
function (_Component) {
  _inherits(GameFrame, _Component);

  function GameFrame() {
    _classCallCheck(this, GameFrame);

    return _possibleConstructorReturn(this, _getPrototypeOf(GameFrame).apply(this, arguments));
  }

  _createClass(GameFrame, [{
    key: "render",
    value: function render() {
      var children = this.props.children;
      return _react.default.createElement(_GameContainer.default, null, _react.default.createElement("div", {
        className: "jsx-3372609668" + " " + "screen-container"
      }, children), _react.default.createElement("div", {
        className: "jsx-3372609668" + " " + "side-container"
      }, _react.default.createElement(_GamePanel.default, {
        curUser: null,
        game: null
      })), _react.default.createElement(_style.default, {
        styleId: "3372609668",
        css: [".screen-container.jsx-3372609668{position:absolute;top:0;bottom:0;left:0;right:calc(100% / 16 * 6);background:rgba(236,240,241,0.85);}", ".side-container.jsx-3372609668{position:absolute;top:0;bottom:0;right:0;left:calc(100% / 16 * 10);background:#fff;}"]
      }));
    }
  }]);

  return GameFrame;
}(_react.Component);

exports.default = GameFrame;

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireDefault(__webpack_require__(0));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var GameContainer = function GameContainer(_ref) {
  var children = _ref.children,
      outer = _ref.outer;
  return _react.default.createElement("div", {
    className: "jsx-1613050951" + " " + "outer"
  }, _react.default.createElement("div", {
    className: "jsx-1613050951" + " " + "container"
  }, children), outer, _react.default.createElement(_style.default, {
    styleId: "1613050951",
    css: [".outer{position:absolute;top:0;left:0;width:100vw;height:100vh;}", ".container{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);transform:translate(-50%,-50%);font-size:3px;}", ".container{width:80px;height:100px;margin-top:-10px;font-size:3px;}", ".controls{display:block;}", ".ctrl-side{display:none;}", "@media (min-width:120px) and (min-height:120px){.container{width:80px;height:100px;margin-top:0;font-size:3px;}.controls{display:none;}.ctrl-side{display:block;width:20px;height:40px;}}", "@media (min-width:96px) and (min-height:144px){.container{width:96px;height:120px;margin-top:-12px;font-size:3px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:144px) and (min-height:144px){.container{width:96px;height:120px;margin-top:0;font-size:3px;}.controls{display:none;}.ctrl-side{display:block;width:24px;height:48px;}}", "@media (min-width:112px) and (min-height:168px){.container{width:112px;height:140px;margin-top:-14px;font-size:4px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:168px) and (min-height:168px){.container{width:112px;height:140px;margin-top:0;font-size:4px;}.controls{display:none;}.ctrl-side{display:block;width:28px;height:56px;}}", "@media (min-width:128px) and (min-height:192px){.container{width:128px;height:160px;margin-top:-16px;font-size:4px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:192px) and (min-height:192px){.container{width:128px;height:160px;margin-top:0;font-size:4px;}.controls{display:none;}.ctrl-side{display:block;width:32px;height:64px;}}", "@media (min-width:144px) and (min-height:216px){.container{width:144px;height:180px;margin-top:-18px;font-size:5px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:216px) and (min-height:216px){.container{width:144px;height:180px;margin-top:0;font-size:5px;}.controls{display:none;}.ctrl-side{display:block;width:36px;height:72px;}}", "@media (min-width:160px) and (min-height:240px){.container{width:160px;height:200px;margin-top:-20px;font-size:6px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:240px) and (min-height:240px){.container{width:160px;height:200px;margin-top:0;font-size:6px;}.controls{display:none;}.ctrl-side{display:block;width:40px;height:80px;}}", "@media (min-width:176px) and (min-height:264px){.container{width:176px;height:220px;margin-top:-22px;font-size:6px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:288px) and (min-height:264px){.container{width:192px;height:240px;margin-top:0;font-size:7px;}.controls{display:none;}.ctrl-side{display:block;width:48px;height:96px;}}", "@media (min-width:192px) and (min-height:288px){.container{width:192px;height:240px;margin-top:-24px;font-size:7px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:312px) and (min-height:288px){.container{width:208px;height:260px;margin-top:0;font-size:8px;}.controls{display:none;}.ctrl-side{display:block;width:52px;height:104px;}}", "@media (min-width:208px) and (min-height:312px){.container{width:208px;height:260px;margin-top:-26px;font-size:8px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:336px) and (min-height:312px){.container{width:224px;height:280px;margin-top:0;font-size:8px;}.controls{display:none;}.ctrl-side{display:block;width:56px;height:112px;}}", "@media (min-width:224px) and (min-height:336px){.container{width:224px;height:280px;margin-top:-28px;font-size:8px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:360px) and (min-height:336px){.container{width:240px;height:300px;margin-top:0;font-size:9px;}.controls{display:none;}.ctrl-side{display:block;width:60px;height:120px;}}", "@media (min-width:240px) and (min-height:360px){.container{width:240px;height:300px;margin-top:-30px;font-size:9px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:384px) and (min-height:360px){.container{width:256px;height:320px;margin-top:0;font-size:9px;}.controls{display:none;}.ctrl-side{display:block;width:64px;height:128px;}}", "@media (min-width:256px) and (min-height:384px){.container{width:256px;height:320px;margin-top:-32px;font-size:9px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:408px) and (min-height:384px){.container{width:272px;height:340px;margin-top:0;font-size:10px;}.controls{display:none;}.ctrl-side{display:block;width:68px;height:136px;}}", "@media (min-width:272px) and (min-height:408px){.container{width:272px;height:340px;margin-top:-34px;font-size:10px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:432px) and (min-height:408px){.container{width:288px;height:360px;margin-top:0;font-size:11px;}.controls{display:none;}.ctrl-side{display:block;width:72px;height:144px;}}", "@media (min-width:288px) and (min-height:432px){.container{width:288px;height:360px;margin-top:-36px;font-size:11px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:456px) and (min-height:432px){.container{width:304px;height:380px;margin-top:0;font-size:11px;}.controls{display:none;}.ctrl-side{display:block;width:76px;height:152px;}}", "@media (min-width:304px) and (min-height:456px){.container{width:304px;height:380px;margin-top:-38px;font-size:11px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:480px) and (min-height:456px){.container{width:320px;height:400px;margin-top:0;font-size:12px;}.controls{display:none;}.ctrl-side{display:block;width:80px;height:160px;}}", "@media (min-width:320px) and (min-height:480px){.container{width:320px;height:400px;margin-top:-40px;font-size:12px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:504px) and (min-height:480px){.container{width:336px;height:420px;margin-top:0;font-size:12px;}.controls{display:none;}.ctrl-side{display:block;width:84px;height:168px;}}", "@media (min-width:336px) and (min-height:504px){.container{width:336px;height:420px;margin-top:-42px;font-size:12px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:528px) and (min-height:504px){.container{width:352px;height:440px;margin-top:0;font-size:13px;}.controls{display:none;}.ctrl-side{display:block;width:88px;height:176px;}}", "@media (min-width:352px) and (min-height:528px){.container{width:352px;height:440px;margin-top:-44px;font-size:13px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:576px) and (min-height:528px){.container{width:384px;height:480px;margin-top:0;font-size:14px;}.controls{display:none;}.ctrl-side{display:block;width:96px;height:192px;}}", "@media (min-width:368px) and (min-height:552px){.container{width:368px;height:460px;margin-top:-46px;font-size:14px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:600px) and (min-height:552px){.container{width:400px;height:500px;margin-top:0;font-size:15px;}.controls{display:none;}.ctrl-side{display:block;width:100px;height:200px;}}", "@media (min-width:384px) and (min-height:576px){.container{width:384px;height:480px;margin-top:-48px;font-size:14px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:624px) and (min-height:576px){.container{width:416px;height:520px;margin-top:0;font-size:16px;}.controls{display:none;}.ctrl-side{display:block;width:104px;height:208px;}}", "@media (min-width:400px) and (min-height:600px){.container{width:400px;height:500px;margin-top:-50px;font-size:15px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:648px) and (min-height:600px){.container{width:432px;height:540px;margin-top:0;font-size:16px;}.controls{display:none;}.ctrl-side{display:block;width:108px;height:216px;}}", "@media (min-width:416px) and (min-height:624px){.container{width:416px;height:520px;margin-top:-52px;font-size:16px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:672px) and (min-height:624px){.container{width:448px;height:560px;margin-top:0;font-size:17px;}.controls{display:none;}.ctrl-side{display:block;width:112px;height:224px;}}", "@media (min-width:432px) and (min-height:648px){.container{width:432px;height:540px;margin-top:-54px;font-size:16px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:696px) and (min-height:648px){.container{width:464px;height:580px;margin-top:0;font-size:17px;}.controls{display:none;}.ctrl-side{display:block;width:116px;height:232px;}}", "@media (min-width:448px) and (min-height:672px){.container{width:448px;height:560px;margin-top:-56px;font-size:17px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:720px) and (min-height:672px){.container{width:480px;height:600px;margin-top:0;font-size:18px;}.controls{display:none;}.ctrl-side{display:block;width:120px;height:240px;}}", "@media (min-width:464px) and (min-height:696px){.container{width:464px;height:580px;margin-top:-58px;font-size:17px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:744px) and (min-height:696px){.container{width:496px;height:620px;margin-top:0;font-size:19px;}.controls{display:none;}.ctrl-side{display:block;width:124px;height:248px;}}", "@media (min-width:480px) and (min-height:720px){.container{width:480px;height:600px;margin-top:-60px;font-size:18px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:768px) and (min-height:720px){.container{width:512px;height:640px;margin-top:0;font-size:19px;}.controls{display:none;}.ctrl-side{display:block;width:128px;height:256px;}}", "@media (min-width:496px) and (min-height:744px){.container{width:496px;height:620px;margin-top:-62px;font-size:19px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:792px) and (min-height:744px){.container{width:528px;height:660px;margin-top:0;font-size:20px;}.controls{display:none;}.ctrl-side{display:block;width:132px;height:264px;}}", "@media (min-width:512px) and (min-height:768px){.container{width:512px;height:640px;margin-top:-64px;font-size:19px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:816px) and (min-height:768px){.container{width:544px;height:680px;margin-top:0;font-size:20px;}.controls{display:none;}.ctrl-side{display:block;width:136px;height:272px;}}", "@media (min-width:528px) and (min-height:792px){.container{width:528px;height:660px;margin-top:-66px;font-size:20px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:864px) and (min-height:792px){.container{width:576px;height:720px;margin-top:0;font-size:22px;}.controls{display:none;}.ctrl-side{display:block;width:144px;height:288px;}}", "@media (min-width:544px) and (min-height:816px){.container{width:544px;height:680px;margin-top:-68px;font-size:20px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:888px) and (min-height:816px){.container{width:592px;height:740px;margin-top:0;font-size:22px;}.controls{display:none;}.ctrl-side{display:block;width:148px;height:296px;}}", "@media (min-width:560px) and (min-height:840px){.container{width:560px;height:700px;margin-top:-70px;font-size:21px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:912px) and (min-height:840px){.container{width:608px;height:760px;margin-top:0;font-size:23px;}.controls{display:none;}.ctrl-side{display:block;width:152px;height:304px;}}", "@media (min-width:576px) and (min-height:864px){.container{width:576px;height:720px;margin-top:-72px;font-size:22px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:936px) and (min-height:864px){.container{width:624px;height:780px;margin-top:0;font-size:24px;}.controls{display:none;}.ctrl-side{display:block;width:156px;height:312px;}}", "@media (min-width:592px) and (min-height:888px){.container{width:592px;height:740px;margin-top:-74px;font-size:22px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:960px) and (min-height:888px){.container{width:640px;height:800px;margin-top:0;font-size:24px;}.controls{display:none;}.ctrl-side{display:block;width:160px;height:320px;}}", "@media (min-width:608px) and (min-height:912px){.container{width:608px;height:760px;margin-top:-76px;font-size:23px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:984px) and (min-height:912px){.container{width:656px;height:820px;margin-top:0;font-size:25px;}.controls{display:none;}.ctrl-side{display:block;width:164px;height:328px;}}", "@media (min-width:624px) and (min-height:936px){.container{width:624px;height:780px;margin-top:-78px;font-size:24px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1008px) and (min-height:936px){.container{width:672px;height:840px;margin-top:0;font-size:25px;}.controls{display:none;}.ctrl-side{display:block;width:168px;height:336px;}}", "@media (min-width:640px) and (min-height:960px){.container{width:640px;height:800px;margin-top:-80px;font-size:24px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1032px) and (min-height:960px){.container{width:688px;height:860px;margin-top:0;font-size:26px;}.controls{display:none;}.ctrl-side{display:block;width:172px;height:344px;}}", "@media (min-width:656px) and (min-height:984px){.container{width:656px;height:820px;margin-top:-82px;font-size:25px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1056px) and (min-height:984px){.container{width:704px;height:880px;margin-top:0;font-size:27px;}.controls{display:none;}.ctrl-side{display:block;width:176px;height:352px;}}", "@media (min-width:672px) and (min-height:1008px){.container{width:672px;height:840px;margin-top:-84px;font-size:25px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1080px) and (min-height:1008px){.container{width:720px;height:900px;margin-top:0;font-size:27px;}.controls{display:none;}.ctrl-side{display:block;width:180px;height:360px;}}", "@media (min-width:688px) and (min-height:1032px){.container{width:688px;height:860px;margin-top:-86px;font-size:26px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1104px) and (min-height:1032px){.container{width:736px;height:920px;margin-top:0;font-size:28px;}.controls{display:none;}.ctrl-side{display:block;width:184px;height:368px;}}", "@media (min-width:704px) and (min-height:1056px){.container{width:704px;height:880px;margin-top:-88px;font-size:27px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1152px) and (min-height:1056px){.container{width:768px;height:960px;margin-top:0;font-size:29px;}.controls{display:none;}.ctrl-side{display:block;width:192px;height:384px;}}", "@media (min-width:720px) and (min-height:1080px){.container{width:720px;height:900px;margin-top:-90px;font-size:27px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1176px) and (min-height:1080px){.container{width:784px;height:980px;margin-top:0;font-size:30px;}.controls{display:none;}.ctrl-side{display:block;width:196px;height:392px;}}", "@media (min-width:736px) and (min-height:1104px){.container{width:736px;height:920px;margin-top:-92px;font-size:28px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1200px) and (min-height:1104px){.container{width:800px;height:1000px;margin-top:0;font-size:30px;}.controls{display:none;}.ctrl-side{display:block;width:200px;height:400px;}}", "@media (min-width:752px) and (min-height:1128px){.container{width:752px;height:940px;margin-top:-94px;font-size:28px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1224px) and (min-height:1128px){.container{width:816px;height:1020px;margin-top:0;font-size:31px;}.controls{display:none;}.ctrl-side{display:block;width:204px;height:408px;}}", "@media (min-width:768px) and (min-height:1152px){.container{width:768px;height:960px;margin-top:-96px;font-size:29px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1248px) and (min-height:1152px){.container{width:832px;height:1040px;margin-top:0;font-size:32px;}.controls{display:none;}.ctrl-side{display:block;width:208px;height:416px;}}", "@media (min-width:784px) and (min-height:1176px){.container{width:784px;height:980px;margin-top:-98px;font-size:30px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1272px) and (min-height:1176px){.container{width:848px;height:1060px;margin-top:0;font-size:32px;}.controls{display:none;}.ctrl-side{display:block;width:212px;height:424px;}}", "@media (min-width:800px) and (min-height:1200px){.container{width:800px;height:1000px;margin-top:-100px;font-size:30px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1296px) and (min-height:1200px){.container{width:864px;height:1080px;margin-top:0;font-size:33px;}.controls{display:none;}.ctrl-side{display:block;width:216px;height:432px;}}", "@media (min-width:816px) and (min-height:1224px){.container{width:816px;height:1020px;margin-top:-102px;font-size:31px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1320px) and (min-height:1224px){.container{width:880px;height:1100px;margin-top:0;font-size:33px;}.controls{display:none;}.ctrl-side{display:block;width:220px;height:440px;}}", "@media (min-width:832px) and (min-height:1248px){.container{width:832px;height:1040px;margin-top:-104px;font-size:32px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1344px) and (min-height:1248px){.container{width:896px;height:1120px;margin-top:0;font-size:34px;}.controls{display:none;}.ctrl-side{display:block;width:224px;height:448px;}}", "@media (min-width:848px) and (min-height:1272px){.container{width:848px;height:1060px;margin-top:-106px;font-size:32px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1368px) and (min-height:1272px){.container{width:912px;height:1140px;margin-top:0;font-size:35px;}.controls{display:none;}.ctrl-side{display:block;width:228px;height:456px;}}", "@media (min-width:864px) and (min-height:1296px){.container{width:864px;height:1080px;margin-top:-108px;font-size:33px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1392px) and (min-height:1296px){.container{width:928px;height:1160px;margin-top:0;font-size:35px;}.controls{display:none;}.ctrl-side{display:block;width:232px;height:464px;}}", "@media (min-width:880px) and (min-height:1320px){.container{width:880px;height:1100px;margin-top:-110px;font-size:33px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1440px) and (min-height:1320px){.container{width:960px;height:1200px;margin-top:0;font-size:36px;}.controls{display:none;}.ctrl-side{display:block;width:240px;height:480px;}}", "@media (min-width:896px) and (min-height:1344px){.container{width:896px;height:1120px;margin-top:-112px;font-size:34px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1464px) and (min-height:1344px){.container{width:976px;height:1220px;margin-top:0;font-size:37px;}.controls{display:none;}.ctrl-side{display:block;width:244px;height:488px;}}", "@media (min-width:912px) and (min-height:1368px){.container{width:912px;height:1140px;margin-top:-114px;font-size:35px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1488px) and (min-height:1368px){.container{width:992px;height:1240px;margin-top:0;font-size:38px;}.controls{display:none;}.ctrl-side{display:block;width:248px;height:496px;}}", "@media (min-width:928px) and (min-height:1392px){.container{width:928px;height:1160px;margin-top:-116px;font-size:35px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1512px) and (min-height:1392px){.container{width:1008px;height:1260px;margin-top:0;font-size:38px;}.controls{display:none;}.ctrl-side{display:block;width:252px;height:504px;}}", "@media (min-width:944px) and (min-height:1416px){.container{width:944px;height:1180px;margin-top:-118px;font-size:36px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1536px) and (min-height:1416px){.container{width:1024px;height:1280px;margin-top:0;font-size:39px;}.controls{display:none;}.ctrl-side{display:block;width:256px;height:512px;}}", "@media (min-width:960px) and (min-height:1440px){.container{width:960px;height:1200px;margin-top:-120px;font-size:36px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1560px) and (min-height:1440px){.container{width:1040px;height:1300px;margin-top:0;font-size:40px;}.controls{display:none;}.ctrl-side{display:block;width:260px;height:520px;}}", "@media (min-width:976px) and (min-height:1464px){.container{width:976px;height:1220px;margin-top:-122px;font-size:37px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1584px) and (min-height:1464px){.container{width:1056px;height:1320px;margin-top:0;font-size:40px;}.controls{display:none;}.ctrl-side{display:block;width:264px;height:528px;}}", "@media (min-width:992px) and (min-height:1488px){.container{width:992px;height:1240px;margin-top:-124px;font-size:38px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1608px) and (min-height:1488px){.container{width:1072px;height:1340px;margin-top:0;font-size:41px;}.controls{display:none;}.ctrl-side{display:block;width:268px;height:536px;}}", "@media (min-width:1008px) and (min-height:1512px){.container{width:1008px;height:1260px;margin-top:-126px;font-size:38px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1632px) and (min-height:1512px){.container{width:1088px;height:1360px;margin-top:0;font-size:41px;}.controls{display:none;}.ctrl-side{display:block;width:272px;height:544px;}}", "@media (min-width:1024px) and (min-height:1536px){.container{width:1024px;height:1280px;margin-top:-128px;font-size:39px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1656px) and (min-height:1536px){.container{width:1104px;height:1380px;margin-top:0;font-size:42px;}.controls{display:none;}.ctrl-side{display:block;width:276px;height:552px;}}", "@media (min-width:1040px) and (min-height:1560px){.container{width:1040px;height:1300px;margin-top:-130px;font-size:40px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1680px) and (min-height:1560px){.container{width:1120px;height:1400px;margin-top:0;font-size:43px;}.controls{display:none;}.ctrl-side{display:block;width:280px;height:560px;}}", "@media (min-width:1056px) and (min-height:1584px){.container{width:1056px;height:1320px;margin-top:-132px;font-size:40px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1728px) and (min-height:1584px){.container{width:1152px;height:1440px;margin-top:0;font-size:44px;}.controls{display:none;}.ctrl-side{display:block;width:288px;height:576px;}}", "@media (min-width:1072px) and (min-height:1608px){.container{width:1072px;height:1340px;margin-top:-134px;font-size:41px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1752px) and (min-height:1608px){.container{width:1168px;height:1460px;margin-top:0;font-size:44px;}.controls{display:none;}.ctrl-side{display:block;width:292px;height:584px;}}", "@media (min-width:1088px) and (min-height:1632px){.container{width:1088px;height:1360px;margin-top:-136px;font-size:41px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1776px) and (min-height:1632px){.container{width:1184px;height:1480px;margin-top:0;font-size:45px;}.controls{display:none;}.ctrl-side{display:block;width:296px;height:592px;}}", "@media (min-width:1104px) and (min-height:1656px){.container{width:1104px;height:1380px;margin-top:-138px;font-size:42px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1800px) and (min-height:1656px){.container{width:1200px;height:1500px;margin-top:0;font-size:46px;}.controls{display:none;}.ctrl-side{display:block;width:300px;height:600px;}}", "@media (min-width:1120px) and (min-height:1680px){.container{width:1120px;height:1400px;margin-top:-140px;font-size:43px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1824px) and (min-height:1680px){.container{width:1216px;height:1520px;margin-top:0;font-size:46px;}.controls{display:none;}.ctrl-side{display:block;width:304px;height:608px;}}", "@media (min-width:1136px) and (min-height:1704px){.container{width:1136px;height:1420px;margin-top:-142px;font-size:43px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1848px) and (min-height:1704px){.container{width:1232px;height:1540px;margin-top:0;font-size:47px;}.controls{display:none;}.ctrl-side{display:block;width:308px;height:616px;}}", "@media (min-width:1152px) and (min-height:1728px){.container{width:1152px;height:1440px;margin-top:-144px;font-size:44px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1872px) and (min-height:1728px){.container{width:1248px;height:1560px;margin-top:0;font-size:48px;}.controls{display:none;}.ctrl-side{display:block;width:312px;height:624px;}}", "@media (min-width:1168px) and (min-height:1752px){.container{width:1168px;height:1460px;margin-top:-146px;font-size:44px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1896px) and (min-height:1752px){.container{width:1264px;height:1580px;margin-top:0;font-size:48px;}.controls{display:none;}.ctrl-side{display:block;width:316px;height:632px;}}", "@media (min-width:1184px) and (min-height:1776px){.container{width:1184px;height:1480px;margin-top:-148px;font-size:45px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1920px) and (min-height:1776px){.container{width:1280px;height:1600px;margin-top:0;font-size:49px;}.controls{display:none;}.ctrl-side{display:block;width:320px;height:640px;}}", "@media (min-width:1200px) and (min-height:1800px){.container{width:1200px;height:1500px;margin-top:-150px;font-size:46px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1944px) and (min-height:1800px){.container{width:1296px;height:1620px;margin-top:0;font-size:49px;}.controls{display:none;}.ctrl-side{display:block;width:324px;height:648px;}}", "@media (min-width:1216px) and (min-height:1824px){.container{width:1216px;height:1520px;margin-top:-152px;font-size:46px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:1968px) and (min-height:1824px){.container{width:1312px;height:1640px;margin-top:0;font-size:50px;}.controls{display:none;}.ctrl-side{display:block;width:328px;height:656px;}}", "@media (min-width:1232px) and (min-height:1848px){.container{width:1232px;height:1540px;margin-top:-154px;font-size:47px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2016px) and (min-height:1848px){.container{width:1344px;height:1680px;margin-top:0;font-size:51px;}.controls{display:none;}.ctrl-side{display:block;width:336px;height:672px;}}", "@media (min-width:1248px) and (min-height:1872px){.container{width:1248px;height:1560px;margin-top:-156px;font-size:48px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2040px) and (min-height:1872px){.container{width:1360px;height:1700px;margin-top:0;font-size:52px;}.controls{display:none;}.ctrl-side{display:block;width:340px;height:680px;}}", "@media (min-width:1264px) and (min-height:1896px){.container{width:1264px;height:1580px;margin-top:-158px;font-size:48px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2064px) and (min-height:1896px){.container{width:1376px;height:1720px;margin-top:0;font-size:52px;}.controls{display:none;}.ctrl-side{display:block;width:344px;height:688px;}}", "@media (min-width:1280px) and (min-height:1920px){.container{width:1280px;height:1600px;margin-top:-160px;font-size:49px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2088px) and (min-height:1920px){.container{width:1392px;height:1740px;margin-top:0;font-size:53px;}.controls{display:none;}.ctrl-side{display:block;width:348px;height:696px;}}", "@media (min-width:1296px) and (min-height:1944px){.container{width:1296px;height:1620px;margin-top:-162px;font-size:49px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2112px) and (min-height:1944px){.container{width:1408px;height:1760px;margin-top:0;font-size:54px;}.controls{display:none;}.ctrl-side{display:block;width:352px;height:704px;}}", "@media (min-width:1312px) and (min-height:1968px){.container{width:1312px;height:1640px;margin-top:-164px;font-size:50px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2136px) and (min-height:1968px){.container{width:1424px;height:1780px;margin-top:0;font-size:54px;}.controls{display:none;}.ctrl-side{display:block;width:356px;height:712px;}}", "@media (min-width:1328px) and (min-height:1992px){.container{width:1328px;height:1660px;margin-top:-166px;font-size:51px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2160px) and (min-height:1992px){.container{width:1440px;height:1800px;margin-top:0;font-size:55px;}.controls{display:none;}.ctrl-side{display:block;width:360px;height:720px;}}", "@media (min-width:1344px) and (min-height:2016px){.container{width:1344px;height:1680px;margin-top:-168px;font-size:51px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2184px) and (min-height:2016px){.container{width:1456px;height:1820px;margin-top:0;font-size:56px;}.controls{display:none;}.ctrl-side{display:block;width:364px;height:728px;}}", "@media (min-width:1360px) and (min-height:2040px){.container{width:1360px;height:1700px;margin-top:-170px;font-size:52px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2208px) and (min-height:2040px){.container{width:1472px;height:1840px;margin-top:0;font-size:56px;}.controls{display:none;}.ctrl-side{display:block;width:368px;height:736px;}}", "@media (min-width:1376px) and (min-height:2064px){.container{width:1376px;height:1720px;margin-top:-172px;font-size:52px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2232px) and (min-height:2064px){.container{width:1488px;height:1860px;margin-top:0;font-size:57px;}.controls{display:none;}.ctrl-side{display:block;width:372px;height:744px;}}", "@media (min-width:1392px) and (min-height:2088px){.container{width:1392px;height:1740px;margin-top:-174px;font-size:53px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2256px) and (min-height:2088px){.container{width:1504px;height:1880px;margin-top:0;font-size:57px;}.controls{display:none;}.ctrl-side{display:block;width:376px;height:752px;}}", "@media (min-width:1408px) and (min-height:2112px){.container{width:1408px;height:1760px;margin-top:-176px;font-size:54px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2304px) and (min-height:2112px){.container{width:1536px;height:1920px;margin-top:0;font-size:59px;}.controls{display:none;}.ctrl-side{display:block;width:384px;height:768px;}}", "@media (min-width:1424px) and (min-height:2136px){.container{width:1424px;height:1780px;margin-top:-178px;font-size:54px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2328px) and (min-height:2136px){.container{width:1552px;height:1940px;margin-top:0;font-size:59px;}.controls{display:none;}.ctrl-side{display:block;width:388px;height:776px;}}", "@media (min-width:1440px) and (min-height:2160px){.container{width:1440px;height:1800px;margin-top:-180px;font-size:55px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2352px) and (min-height:2160px){.container{width:1568px;height:1960px;margin-top:0;font-size:60px;}.controls{display:none;}.ctrl-side{display:block;width:392px;height:784px;}}", "@media (min-width:1456px) and (min-height:2184px){.container{width:1456px;height:1820px;margin-top:-182px;font-size:56px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2376px) and (min-height:2184px){.container{width:1584px;height:1980px;margin-top:0;font-size:60px;}.controls{display:none;}.ctrl-side{display:block;width:396px;height:792px;}}", "@media (min-width:1472px) and (min-height:2208px){.container{width:1472px;height:1840px;margin-top:-184px;font-size:56px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2400px) and (min-height:2208px){.container{width:1600px;height:2000px;margin-top:0;font-size:61px;}.controls{display:none;}.ctrl-side{display:block;width:400px;height:800px;}}", "@media (min-width:1488px) and (min-height:2232px){.container{width:1488px;height:1860px;margin-top:-186px;font-size:57px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2424px) and (min-height:2232px){.container{width:1616px;height:2020px;margin-top:0;font-size:62px;}.controls{display:none;}.ctrl-side{display:block;width:404px;height:808px;}}", "@media (min-width:1504px) and (min-height:2256px){.container{width:1504px;height:1880px;margin-top:-188px;font-size:57px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2448px) and (min-height:2256px){.container{width:1632px;height:2040px;margin-top:0;font-size:62px;}.controls{display:none;}.ctrl-side{display:block;width:408px;height:816px;}}", "@media (min-width:1520px) and (min-height:2280px){.container{width:1520px;height:1900px;margin-top:-190px;font-size:58px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2472px) and (min-height:2280px){.container{width:1648px;height:2060px;margin-top:0;font-size:63px;}.controls{display:none;}.ctrl-side{display:block;width:412px;height:824px;}}", "@media (min-width:1536px) and (min-height:2304px){.container{width:1536px;height:1920px;margin-top:-192px;font-size:59px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2496px) and (min-height:2304px){.container{width:1664px;height:2080px;margin-top:0;font-size:64px;}.controls{display:none;}.ctrl-side{display:block;width:416px;height:832px;}}", "@media (min-width:1552px) and (min-height:2328px){.container{width:1552px;height:1940px;margin-top:-194px;font-size:59px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2520px) and (min-height:2328px){.container{width:1680px;height:2100px;margin-top:0;font-size:64px;}.controls{display:none;}.ctrl-side{display:block;width:420px;height:840px;}}", "@media (min-width:1568px) and (min-height:2352px){.container{width:1568px;height:1960px;margin-top:-196px;font-size:60px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2544px) and (min-height:2352px){.container{width:1696px;height:2120px;margin-top:0;font-size:65px;}.controls{display:none;}.ctrl-side{display:block;width:424px;height:848px;}}", "@media (min-width:1584px) and (min-height:2376px){.container{width:1584px;height:1980px;margin-top:-198px;font-size:60px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2592px) and (min-height:2376px){.container{width:1728px;height:2160px;margin-top:0;font-size:66px;}.controls{display:none;}.ctrl-side{display:block;width:432px;height:864px;}}", "@media (min-width:1600px) and (min-height:2400px){.container{width:1600px;height:2000px;margin-top:-200px;font-size:61px;}.controls{display:block;}.ctrl-side{display:none;}}", "@media (min-width:2616px) and (min-height:2400px){.container{width:1744px;height:2180px;margin-top:0;font-size:67px;}.controls{display:none;}.ctrl-side{display:block;width:436px;height:872px;}}"]
  }));
};

var _default = GameContainer;
exports.default = _default;

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _classnames = _interopRequireDefault(__webpack_require__(9));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _reactRedux = __webpack_require__(10);

var _head = _interopRequireDefault(__webpack_require__(14));

var _Error = _interopRequireDefault(__webpack_require__(34));

var _rollbarClient = __webpack_require__(22);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Layout =
/*#__PURE__*/
function (_Component) {
  _inherits(Layout, _Component);

  function Layout() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Layout);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Layout)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      error: null
    });

    return _this;
  }

  _createClass(Layout, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          jsReady = _this$props.jsReady,
          jsLoad = _this$props.jsLoad;

      if (!jsReady) {
        jsLoad();
      }
    }
  }, {
    key: "componentDidCatch",
    value: function componentDidCatch(error, _ref) {
      var componentStack = _ref.componentStack;
      (0, _rollbarClient.logError)(error);
      this.setState({
        error: {
          message: error.toString(),
          stack: componentStack
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          jsReady = _this$props2.jsReady,
          children = _this$props2.children;
      var error = this.state.error;
      var layoutClasses = (0, _classnames.default)('layout', {
        'layout-static': !jsReady
      });
      return _react.default.createElement("div", {
        className: "jsx-1436450221"
      }, _react.default.createElement(_head.default, null, _react.default.createElement("meta", {
        charSet: "utf-8",
        className: "jsx-1436450221"
      }), _react.default.createElement("meta", {
        name: "description",
        content: "A fast-paced two-player web game",
        className: "jsx-1436450221"
      }), _react.default.createElement("meta", {
        name: "viewport",
        content: "width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no",
        className: "jsx-1436450221"
      }), _react.default.createElement("meta", {
        name: "apple-mobile-web-app-capable",
        content: "yes",
        className: "jsx-1436450221"
      }), _react.default.createElement("meta", {
        name: "apple-mobile-web-app-status-bar-style",
        content: "default",
        className: "jsx-1436450221"
      }), _react.default.createElement("meta", {
        name: "theme-color",
        content: "#34495f",
        className: "jsx-1436450221"
      }), _react.default.createElement("link", {
        rel: "manifest",
        href: "/static/manifest.webmanifest",
        className: "jsx-1436450221"
      }), _react.default.createElement("link", {
        rel: "icon",
        type: "image/png",
        sizes: "16x16",
        href: "/static/favicon-16x16.png",
        className: "jsx-1436450221"
      }), _react.default.createElement("link", {
        rel: "icon",
        type: "image/png",
        sizes: "32x32",
        href: "/static/favicon-32x32.png",
        className: "jsx-1436450221"
      }), _react.default.createElement("link", {
        rel: "icon",
        type: "image/png",
        sizes: "192x192",
        href: "/static/android-chrome-192x192.png",
        className: "jsx-1436450221"
      }), _react.default.createElement("link", {
        rel: "icon",
        type: "image/png",
        sizes: "256x256",
        href: "/static/android-chrome-256x256.png",
        className: "jsx-1436450221"
      }), _react.default.createElement("link", {
        rel: "apple-touch-icon",
        sizes: "180x180",
        href: "/static/apple-touch-icon.png",
        className: "jsx-1436450221"
      })), _react.default.createElement(_style.default, {
        styleId: "1839547627",
        css: ["@font-face{font-family:'Teko';font-style:normal;font-weight:300;src:local('Teko:Light'),local('Teko-Light'), url('data:application/x-font-ttf;charset=utf-8;base64,AAEAAAARAQAABAAQR0RFRgATANcAAIWwAAAAFkdQT1MAGQAMAACFyAAAABBHU1VCZJB2eAAAhdgAAAAaT1MvMlm2lp0AAHO4AAAAYGNtYXC0KK0yAAB0GAAAAMxjdnQgHBYKhQAAgOgAAABkZnBnbYySkFgAAHTkAAALcGdhc3AAAAAQAACFqAAAAAhnbHlmhZRNWQAAARwAAG0SaGVhZAjGIf0AAHAAAAAANmhoZWEIUAAyAABzlAAAACRobXR4D7ocwwAAcDgAAANcbG9jYZ+CvA0AAG5QAAABsG1heHACTwxCAABuMAAAACBuYW1lL2ZI5QAAgUwAAAJacG9zdDoD9ioAAIOoAAAB/XByZXCiw2ouAACAVAAAAJEAAwAAAAAB9gK9AA0AEQAVAENAQAsHBAAEAAEBRwAGCAEFAQYFXgMBAAABVgIBAQERSAAEBAdWCQEHBxMHSRISDg4SFRIVFBMOEQ4REhITEhIKBRkrEyMHIxMDMxczNzMDFyMBESERAREhEfsEZDyGhjxpBGQ8h4c8/s8BkP49AfYBLtEBBAD/zs7++/4CL/2lAlv9dAK9/UMAAAIAMgAAAIcCbgADAAcAJ0AkAAMDAlYAAgIiSAAAAAFWBAEBASMBSQAABwYFBAADAAMRBQYVKzM1MxUDMwMjM1JTVQ07cHACbv5WAAACACMBmgDbAm4AAwAHABdAFAMBAQEAVgIBAAAiAUkREREQBAYYKxMzFSMnMxUjm0BAeEBAAm7U1NQAAgAeAAABogJuABsAHwBHQEQNCwIJDggCAAEJAF8QDwcDAQYEAgIDAQJeDAEKCiJIBQEDAyMDSRwcHB8cHx4dGxoZGBcWFRQTEhEREREREREREBEGHSsBIwczFSMHIzcjByM3IzUzNyM1MzczBzM3MwczBzcjBwGiOh05Qxs/G3UbPxswOh05Qxs/G3UbPxswlh11HQGOrjqmpqamOq46pqampuiurgAAAQAo/7UBHwK5ADUAo0uwElBYQDwACAcHCGMACgsACwoAbQAEBgUGBAVtAAIBAQJkDAEAAAYEAAZgAAsLB1gJAQcHIkgABQUBWAMBAQEjAUkbQDoACAcIbwAKCwALCgBtAAQGBQYEBW0AAgECcAwBAAAGBAAGYAALCwdYCQEHByJIAAUFAVgDAQEBIwFJWUAfAgAwLSopJyUkIyIgHRoVEg8ODAoJCAcFADUCNQ0GFCsTMzIdARQrARUjNSMiPQEzFRQWOwEyNj0BNCYrASI9ATQ7ATUzFTMyHQEjNTQmKwEiBh0BFBaMRE9PDEEKT0ULFC0TDAwTRE9PDEELT0ULFC4UCwsBXk/AT0tLT2BVFAsLFKoUC0+tT0tLT2BVFAsLFJcTDAAABQAeAAACHwJuAAMAEwAjAC8AOwA4QDUABQAIAgUIYAAHAAIDBwJhAAQEAFgJAQAAIkgAAwMBWAYBAQEjAUk7ODMzNDU1NTQREAoGHSsBMwEjJTU0JisBIgYdARQWOwEyNgE1NCYrASIGHQEUFjsBMjYFFRQrASI9ATQ7ATIlFRQrASI9ATQ7ATIBiD3+7zwBbwoSFxIKChIXEgr+vgoSFxIKChIXEgoBekM5Q0M5Q/6+QzlDQzlDAm79klCmEwsLE6YTCwsBO6YTCwsTphMLC2a4R0e4R+G4R0e4RwAAAwAo/90B2wJuAB8ALgA6AFBATTMjFwgEAgQbGAIFAh4BAAUDRwACBAUEAgVtAAMAA3AABAQBWAABASJIBwEFBQBZBgEAACMASTEvAgAvOjE6LSodHBoZEQ4AHwIfCAYUKyEjIj0BNDY/AS4CPQE0OwEyHQEUBg8BFzUzFRcjJwYDFRQXNz4BPQE0JisBIgYTMzI3JwcOAR0BFBYBN8BPFx0iFxUWT1xPISQYdThVSyYT1zAJHxoMEzQTDAukCgqdGxINDE90HCYUGh4eMxZnT09jMjwXEZVPl2wwDQIUYiA4BxYmH1gUCwv+EwLEEwwYGVcUCwAAAQAjAZoAaAJuAAMAE0AQAAEBAFYAAAAiAUkREAIGFisTMxUjI0VFAm7UAAABADH/TgEPAxcAEQAYQBUNBAIBAAFHAAABAG8AAQFmKyECBhYrExA7ARUHDgEdARQWHwEVIyIRMc4QDk4/P04OEM4BVgHBOQEGzLRJtMwGATkBwQAB/+P/TgDBAxcAEQAYQBUNBAIAAQFHAAEAAW8AAABmKyECBhYrExArATU3PgE9ATQmLwE1MzIRwc4QDk4/P04OEM4BD/4/OQEGzLRJtMwGATn+PwABAA8BawEfAm4AHQAdQBocGRgWFBMQDg0KCAcFDQBEAAAAIgBJEQEGFSsTNTMVFAc2NxcGBxYfAQcmJwYPASc2NyYnNxcWFyZ4PgUbPxRFGRQRGDIsCQwRGDMsEh5AEycXHQUCRSkpIRcOFDsVAxIZISU/ExoXISU6EQUVOw0HEBkAAQAZAIMBYwHrAAsAJ0AkBgUCAwIBAAEDAF4AAQEEVgAEBCUBSQAAAAsACxERERERBwYZKwEVIxUjNSM1MzUzFQFjgkaCgkYBVTyWljyWlgABAB7/dwBwAHAABAAZQBYCAQEAAUcAAAABVgABAScBSRIQAgYWKzczFQcjHlI3G3A6vwAAAQA3AQcA+wFBAAMAHkAbAAABAQBSAAAAAVYCAQEAAUoAAAADAAMRAwYVKxM1MxU3xAEHOjoAAQAeAAAAcABwAAMAGUAWAAAAAVYCAQEBIwFJAAAAAwADEQMGFSszNTMVHlJwcAAAAQAI/3gBVAK7AAMAE0AQAAABAG8AAQEnAUkREAIGFisBMwEjARBE/vhEArv8vQAAAgAyAAABNwJuAA8AGwAfQBwAAAADWAADAyJIAAEBAlgAAgIjAkkzNDUzBAYYKzcRNCYrASIGFREUFjsBMjYTERQrASI1ETQ7ATLyDBM9FAsLFD0TDEVPZ09PZ09aAboUCwsU/kYUCwsB2f4wT08B0E8AAQAUAAAAnAJuAAUAH0AcAAEBAlYDAQICIkgAAAAjAEkAAAAFAAUREQQGFisTESMRIzWcREQCbv2SAjM7AAABABkAAAEJAm4AGgBVtRABAwIBR0uwCVBYQBwAAAQCBABlAAQEAVgAAQEiSAACAgNWAAMDIwNJG0AdAAAEAgQAAm0ABAQBWAABASJIAAICA1YAAwMjA0lZtzcRFTIRBQYZKxMVIzU0OwEyHQEUBwMzFSM1EzY9ATQmKwEiBl1ET01PGomo744XCxQkFAsCFG55T083RT3+1Ts6ATc2RSgUCwsAAAEAHgAAARACbgAuAHi1CwEFBgFHS7AJUFhAKwAABwYHAGUAAwUEBANlAAYABQMGBWAABwcBWAABASJIAAQEAlkAAgIjAkkbQC0AAAcGBwAGbQADBQQFAwRtAAYABQMGBWAABwcBWAABASJIAAQEAlkAAgIjAklZQAs1ISUzEjgyEQgGHCsTFSM1NDsBMh0BFAcWHQEUKwEiPQEzFRQWOwEyNj0BNCYrATUzMjY9ATQmKwEiBmJET1RPJSVPVE9EDBMsEwwME0lJEwwMEywTDAIUZnFPT7MtAwYpvk9PcGUUCwsUpBQLPAsUnBQLCwABAAUAAAFFAm4ADgAzQDAHAQAEAUcHBgIEAgEAAQQAXwADAyJIAAUFAVYAAQEjAUkAAAAOAA4RERIREREIBhorJRUjFSM1IzUTMwMzNTMVAUU1QsmkQ6GDQro7f381Abr+THZ2AAABACgAAAEcAm4AHABhS7AJUFhAJAADBQQEA2UAAQAFAwEFYAAAAAZWAAYGIkgABAQCWQACAiMCSRtAJQADBQQFAwRtAAEABQMBBWAAAAAGVgAGBiJIAAQEAlkAAgIjAklZQAoRJTMSMyEQBwYbKwEjFTMyHQEUKwEiPQEzFRQWOwEyNj0BNCYrAREzARyvYE9PVk9FCxQsFAsLFJD0AjO0T+FPT3FmFAsLFMsTDAEqAAIAMgAAATMCbgAZACYAYUuwCVBYQCQAAwQABANlAAAABQYABWAABAQCWAACAiJIAAYGAVgAAQEjAUkbQCUAAwQABAMAbQAAAAUGAAVgAAQEAlgAAgIiSAAGBgFYAAEBIwFJWUAKMyYzEjMzIAcGGysTMzIdARQrASI1ETQ7ATIdASM1NCYrASIGFRM1NCYrARUUFjsBMjZ3bU9PY09PYk9EDBM5FAt3DBNYCxQ5EwwBYU/DT08B0E9PdWoUCwsU/katEwzMFAsLAAABAAUAAAD7Am4ABgAlQCIBAQECAUcAAQECVgMBAgIiSAAAACMASQAAAAYABhESBAYWKxMVAyMTIzX7oUSbrAJuLv3AAjM7AAADADIAAAE2Am4AFQAlADUAMEAtDgMCAgUBRwAFAAIDBQJgAAQEAVgAAQEiSAADAwBYAAAAIwBJNTU1NDg3BgYaKwEVFAcWHQEUKwEiPQE0NyY9ATQ7ATIDNTQmKwEiBh0BFBY7ATI2ETU0JisBIgYdARQWOwEyNgE2JSVPZk8lJU9mT0QMEz4TDAwTPhMMDBM+EwwMEz4TDAIfty0DBim6T0+6KQYDLbdP/eyiFAsLFKIUCwsBMJ4UCwsUnhQLCwACACgAAAEpAm4AGQAmAHRLsAlQWEAmAAIEAwMCZQgBBQAEAgUEYAAGBgBYBwEAACJIAAMDAVkAAQEjAUkbQCcAAgQDBAIDbQgBBQAEAgUEYAAGBgBYBwEAACJIAAMDAVkAAQEjAUlZQBkbGgIAIR4aJhsmFhQRDgsKCAUAGQIZCQYUKxMzMhURFCsBIj0BMxUUFjsBMjY9ASMiPQE0EzM1NCYrASIGHQEUFndjT09iT0QLFDkTDG1PZFgMEzkUCwsCbk/+ME9PdWoUCwsUqU/NT/7Q1hQLCxS3EwwAAgAeAAAAcAHlAAMABwAsQCkEAQEBAFYAAAAlSAACAgNWBQEDAyMDSQQEAAAEBwQHBgUAAwADEQYGFSsTNTMVAzUzFR5SUlIBdXBw/otwcAACAB7/dwBwAeUAAwAIAC1AKgYBAwIBRwQBAQEAVgAAACVIAAICA1YAAwMnA0kAAAgHBQQAAwADEQUGFSsTNTMVAzMVByMeUlJSNxsBdXBw/vs6vwABAB4AnQFoAdEABgAGswYDAS0rAQ0BFSU1JQFo/vUBC/62AUoBj1hYQnNOcwAAAgAeAMIBaAGsAAMABwAvQCwAAgUBAwACA14AAAEBAFIAAAABVgQBAQABSgQEAAAEBwQHBgUAAwADEQYGFSs3NSEVJTUhFR4BSv62AUrCPj6sPj4AAQAeAJ0BaAHRAAYABrMFAgEtKwElNQUVBTUBKf71AUr+tgE3WEJzTnNCAAACABQAAAEHAm4AGgAeALxLsAtQWEAtAAUEAwQFZQACAQYBAmUAAwABAgMBYAAEBABYCAEAACJIAAYGB1YJAQcHIwdJG0uwFFBYQC4ABQQDBAUDbQACAQYBAmUAAwABAgMBYAAEBABYCAEAACJIAAYGB1YJAQcHIwdJG0AvAAUEAwQFA20AAgEGAQIGbQADAAECAwFgAAQEAFgIAQAAIkgABgYHVgkBBwcjB0lZWUAbGxsCABseGx4dHBgXFBEMCgkIBwUAGgIaCgYUKxMzMh0BFCsBByMnMzI2PQE0JisBIgYdASM1NBM1MxVjVU9PEgM5BkISCgoSMBMKRUlSAm5NzE5Dfg0TsRMODhNIVk39knBwAAACAFX/wwJdAjQADwA9AEhARRUBAQI4AQkAAkcACAAFAggFYAMBAgABAAIBYAQBAAoBCQYACWEABgcHBlQABgYHWAAHBgdMPDk3NTMhJTUhEjM1MwsGHSsBFRQWOwEyNj0BNCYrASIGBzQ7ATIXNTMRMzI2NRE0JiMhIgYVERQWMyEVISI1ETQzITIVERQrATUGKwEiNQEqDBMeEhAQEh4TDEVPHiwLRC0UCwwT/r8TDAwTARX+1k9PAWpPT34FOR5PAVGmEwwQFJwUEAwITyIi/uEMEwEsFAwMFP5IEww9TwHTT0/+u08xMU8AAAIADwAAAUgCbgAHAAoALEApCgEEAgFHAAQAAAEEAF8AAgIiSAUDAgEBIwFJAAAJCAAHAAcREREGBhcrIScjByMTMxMnMwMBAxp+F0VhbWvVbTqengJu/ZLZAWUAAwA3AAABSwJuAAkAEwAgAC9ALBoBAgEBRwABAAIDAQJgAAAABFgABAQiSAADAwVYAAUFIwVJKCIhJSEjBgYaKwE1NCYrARUzMjYRNTQmKwEVMzI2AzMyHQEUBxYdARQrAQEGDBNraxMMDBNraxMMz8VPJSVPxQF6mhQL2Av+9KcTDOULAihPsy4DBii+TwAAAQAy//8BMQJuAB8AXUuwCVBYQCIAAAEDAQBlAAMCAgNjAAEBBVgABQUiSAACAgRZAAQEIwRJG0AkAAABAwEAA20AAwIBAwJrAAEBBVgABQUiSAACAgRZAAQEIwRJWUAJMzITNTMQBgYaKwEjNTQmKwEiBhURFBY7ATI2PQEzFRQrASI1ETQ7ATIVATFFDBM3FAsLFDcTDEVPYU9PYU8Btl4UCwsU/kUTDAwTXmlPTwHRT08AAgA3AAABSwJuAAcAEQAfQBwAAgIAWAAAACJIAAMDAVgAAQEjAUkhJCMgBAYYKxMzMhURFCsBNxE0JisBETMyNjfFT0/FzwwTa2sTDAJuT/4wT1oBuhQL/ggLAAABADcAAAEnAm4ACwApQCYABAAFAAQFXgADAwJWAAICIkgAAAABVgABASMBSREREREREAYGGis3MxUjETMVIxUzFSN8q/DppIaGOzsCbjvSOwAAAQA3AAABKgJuAAkAI0AgAAEAAgMBAl4AAAAEVgAEBCJIAAMDIwNJERERERAFBhkrASMVMxUjESMRMwEqroaGRfMCM9w7/uQCbgABADL//wExAm4AIQBhS7ALUFhAJAAAAQQBAGUABAADAgQDXgABAQZYAAYGIkgAAgIFWAAFBSMFSRtAJQAAAQQBAARtAAQAAwIEA14AAQEGWAAGBiJIAAICBVgABQUjBUlZQAozMhETNTMQBwYbKwEjNTQmKwEiBhURFBY7ATI2PQEjNTMVFCsBIjURNDsBMhUBMUUMEzcUCwsUNxMMMndPYU9PYU8BwFQUCwsU/kUTDAwTpTvrT08B0U9PAAEANwAAAUcCbgALACFAHgAFAAIBBQJeBAEAACJIAwEBASMBSREREREREAYGGisBMxEjESMRIxEzETMBAkVFhkVFhgJu/ZIBGf7nAm7+5gAAAQBBAAAAhQJuAAMAE0AQAAAAIkgAAQEjAUkREAIGFisTMxEjQUREAm79kgABAA8AAAD5Am4AEQBDS7AJUFhAFwAAAgEBAGUAAgIiSAABAQNZAAMDIwNJG0AYAAACAQIAAW0AAgIiSAABAQNZAAMDIwNJWbYyEzMQBAYYKzczFRQWOwEyNjURMxEUKwEiNQ9FDBMiEwxFT0xPwGYUCwsUAhT94U9PAAIANwAAAU8CbgAFAAkAK0AoBAECAAEBRwIEAgEBIkgFAwIAACMASQYGAAAGCQYJCAcABQAFEgYGFSsBAxMjAxMDETMRAU+GhEqBg85EAm7+xv7MATQBOv2SAm79kgABADcAAAEKAm4ABQAZQBYAAgIiSAAAAAFXAAEBIwFJEREQAwYXKzczFSMRM3uP00Q7OwJuAAABADcAAAGkAm4ADAAoQCUMCQQDBAEBRwAEAQABBABtAgEBASJIAwEAACMASRIREhEQBQYZKzMjETMbATMRIxEDIwNyO0trbEs7V0lXAm7+cAGQ/ZIB1/6wAVAAAAEANwAAAUICbgAJAB5AGwkEAgEAAUcDAQAAIkgCAQEBIwFJERIREAQGGCsBMxEjAxEjETMTAQM/S4E/UHwCbv2SAeL+HgJu/i8AAgAyAAABNgJuAAsAGwAoQCUAAgIAWAQBAAAiSAADAwFYAAEBIwFJAgAaFxIPCAUACwILBQYUKxMzMhURFCsBIjURNBMRNCYrASIGFREUFjsBMjaBZk9PZk/ADBM+EwwMEz4TDAJuT/4wT08B0E/97AG6FAsLFP5GFAsLAAIANwAAATsCbgAJABMAI0AgAAQAAQIEAWAAAwMAWAAAACJIAAICIwJJISQRIyAFBhkrEzMyHQEUKwEVIxM1NCYrARUzMjY3tU9PcUTACxRdXRQLAm5P1k/6AVTAFAv+CwAAAgAy/58BOQJuAA4AHgA1QDILAQADAUcAAgACcAAEBAFYAAEBIkgAAwMAWAUBAAAjAEkBAB0aFRINDAcEAA4BDgYGFCszIjURNDsBMhURFAcXIycDERQWOwEyNjURNCYrASIGgU9PZk8+QUQ/QAwTPhMMDBM+EwxPAdBPT/4wRwdiYQIU/kYUCwsUAboUCwsAAgA3AAABWwJuAAwAFgA4QDUJAQMEAUcABAYBAwAEA14HAQUFAVgAAQEiSAIBAAAjAEkNDQAADRYNFRAOAAwADBUhEQgGFysTESMRMzIdARQHEyMLARUzMjY9ATQmI3tEtU8+XkpbO10UCwsUAQT+/AJuT8xHB/77AQQBL/QLFLYUCwAAAQAoAAABHwJuAC0AcUuwC1BYQCsAAgMEAwJlAAYABwcGZQAEAAAGBABgAAMDAVgAAQEiSAAHBwVZAAUFIwVJG0AtAAIDBAMCBG0ABgAHAAYHbQAEAAAGBABgAAMDAVgAAQEiSAAHBwVZAAUFIwVJWUALMxIzNTMSMzMIBhwrNzU0JisBIj0BNDsBMh0BIzU0JisBIgYdARQWOwEyHQEUKwEiPQEzFRQWOwEyNtoME0RPT1hPRQsULhQLCxRET09XT0ULFC0TDFqqFAtPrU9PYFUUCwsUlxMMT8BPT2BVFAsLAAEABQAAARECbgAHACFAHgIBAAADVgQBAwMiSAABASMBSQAAAAcABxEREQUGFysBFSMRIxEjNQERZERkAm47/c0CMzsAAQA0AAABOAJuABEAG0AYAgEAACJIAAMDAVkAAQEjAUkzEjIQBAYYKxMzERQrASI1ETMRFBY7ATI2NfRET2ZPRAwTPhMMAm794U9PAh/97BQLCxQAAQAFAAABOgJuAAYAG0AYBgEBAAFHAgEAACJIAAEBIwFJEREQAwYXKxMzAyMDMxP1RWRtZEVVAm79kgJu/cIAAAEACgAAAhsCbgAMACFAHgwJBAMBAAFHBAMCAAAiSAIBAQEjAUkSERIREAUGGSsBMwMjCwEjAzMbATMTAdZFW2hGRWhbRUtPUk8Cbv2SAe3+EwJu/cICPv3CAAEABQAAAUkCbgALACBAHQkGAwAEAAIBRwMBAgIiSAEBAAAjAEkSEhIRBAYYKxsBIycHIxMDMxc3M8t+SlhYSn50Sk5OSgE+/sL9/QE+ATDy8gABAAUAAAE6Am4ACAAdQBoIBQIDAAEBRwIBAQEiSAAAACMASRISEAMGFyszIzUDMxsBMwPERHtFWFNFdr4BsP6TAW3+UAAAAQAFAAABDAJvAAkALUAqAQECBgEAAkYAAgIDVgQBAwMiSAAAAAFWAAEBIwFJAAAACQAJEhESBQYXKwEVAzMVITUTIzUBDMO+/v7DuQJvO/4HOzsB+TsAAAEAP/9ZAOkDDQAHACJAHwADAAABAwBeAAECAgFSAAEBAlYAAgECShERERAEBhgrEyMRMxUjETPpZWWqqgLS/MI7A7QAAQAs/3gBeQK7AAMAE0AQAAABAG8AAQEnAUkREAIGFisTMwEjLEUBCEUCu/y9AAEANf9ZAN8DDQAHAChAJQQBAwACAQMCXgABAAABUgABAQBWAAABAEoAAAAHAAcREREFBhcrExEjNTMRIzXfqmVlAw38TDsDPjsAAQAtAUcBjgKfAAYAG0AYBAEBAAFHAgEBAAFwAAAAJABJEhEQAwYXKxMzEyMLASO0U4dIaWhIAp/+qAEV/usAAAEAAP+ZAdr/1AADAB5AGwAAAQEAUgAAAAFWAgEBAAFKAAAAAwADEQMGFSsVNSEVAdpnOzsAAAEAeQIEARkCgwADAC5LsBhQWEAMAAABAHACAQEBIgFJG0AKAgEBAAFvAAAAZllACgAAAAMAAxEDBhUrExcjJ8tOPmICg39/AAIAKAAAARsB5QAPAB8ANEAxDwEFAAQBAQQCRwAFBQBYAwEAACVIBgEEBAFYAgEBASMBSRIQGhcQHxIfMzIREAcGGCsTMxEjNQYrASI1ETQ7ATIXAzMyNjURNCYrASIGFREUFtZFRQssKE9PKCwLSigSEBASKBQLCwHl/hshIU8BR08h/ncQFAEnFBAME/7PFAsAAgAtAAABIAKdAA8AHwA8QDkOAQQACQEBBQJHAAMDJEgABAQAWAYBAAAlSAAFBQFYAgEBASMBSQIAHhsWEw0MCwoIBQAPAg8HBhQrEzMyFREUKwEiJxUjETMVNhMRNCYrASIGFREUFjsBMjapKE9PKCwLRUULXgsUKBIQEBIoFAsB5k/+uE8hIQKd2CH+dAEyFAsQFP7YFBALAAEAKAAAASIB5QAfAF1LsBBQWEAiAAABAwEAZQADAgIDYwABAQVYAAUFJUgAAgIEWQAEBCMESRtAJAAAAQMBAANtAAMCAQMCawABAQVYAAUFJUgAAgIEWQAEBCMESVlACTMyEzUzEAYGGisBIzU0JisBIgYVERQWOwEyNj0BMxUUKwEiNRE0OwEyFQEiRQsUMhQLCxQyFAtFT1xPT1xPAVU2EwwME/7PFAsLFDZBT08BR09PAAIAKAAAARsCnQAPAB8AOEA1DwEFAwQBAQQCRwAAACRIAAUFA1gAAwMlSAYBBAQBWAIBAQEjAUkSEBoXEB8SHzMyERAHBhgrEzMRIzUGKwEiNRE0OwEyFwMzMjY1ETQmKwEiBhURFBbWRUULLChPTygsC0ooEhAQEigUCwsCnf1jISFPAUhPIf52EBQBKBQQCxT+zhQLAAIALQAAAScB5QAXACEAbkuwElBYQCUAAwECAgNlAAUAAQMFAV4ABgYAWAcBAAAlSAACAgRZAAQEIwRJG0AmAAMBAgEDAm0ABQABAwUBXgAGBgBYBwEAACVIAAICBFkABAQjBElZQBUCAB8cGRgUEQ8OCwgFBAAXAhcIBhQrEzMyHQEjFRQWOwEyNj0BMxUUKwEiNRE0FzM1NCYrASIGFXxcT7UMEy8UC0VPWU9FcAwTMhMMAeVPu4EUCwsULDdPTwFHT891EwwMEwABAAUAAADAAp0AEgApQCYAAAAGWAAGBiRIBAECAgFWBQEBASVIAAMDIwNJIhERERETIAcGGysTIyIGHQEzFSMRIxEjNTM1NDsBwCcTDEZGRTAwTzwCXQsUWTv+VgGqO2lPAAIAKP9zARsB5QAWACYAPkA7FgEGAAsBAwUCRwAGBgBYBAEAACVIBwEFBQNYAAMDI0gAAgIBWAABAScBSRkXIR4XJhkmMzQhIhAIBhkrEzMRFCsBNTMyNj0BBisBIjURNDsBMhcDMzI2NRE0JisBIgYVERQW1kVPlYATDAssKE9PKCwLSigSEBASKBQLCwHl/d1POwwTVCFPAUdPIf53EBQBJxQQDBP+zxQLAAABAC0AAAEgAp0AEwAxQC4SAQIAAUcABAQkSAACAgBYBQEAACVIAwEBASMBSQIAERAPDgsIBQQAEwITBgYUKxMzMhURIxE0JisBIgYVESMRMxU2qShPRQsUKBIQRUULAeVP/moBixMMEBT+egKd2SEAAgArAAAAdAKeAAMABwAnQCQEAQEBAFYAAAAkSAADAyVIAAICIwJJAAAHBgUEAAMAAxEFBhUrEzUzFQMjETMrSQJFRQJNUVH9swHlAAIAAP9zAHQCngADAA4ALkArBQEBAQBWAAAAJEgAAgIlSAAEBANYAAMDJwNJAAANCwoIBgUAAwADEQYGFSsTNTMVAxEzERQrATUzMjYrSUdFTyMOFAsCTVFR/YACGP3dTzsMAAACAC0AAAFAAp0ABQAJAC9ALAQBAgABAUcAAgIkSAQBAQElSAUDAgAAIwBJBgYAAAYJBgkIBwAFAAUSBgYVKwEHEyMDNwMRMxEBQHFxSm5uyUUB5eX/AAEA5f4bAp39YwABAC0AAAByAp0AAwATQBAAAQEkSAAAACMASREQAgYWKzMjETNyRUUCnQABAC0AAAHOAeUAIgA3QDQhHAICAAFHBAECAgBYBwYIAwAAJUgFAwIBASMBSQIAIB0bGhkYFRIPDgsIBQQAIgIiCQYUKwEzMhURIxE0JisBIgYVESMRNCYrASIGFREjETMVNjsBMhc2AVcoT0ULFCgSEEULFCgSEEVFCywoORAKAeVP/moBixMMEBT+egGLEwwQFP56AeUhISgoAAABAC0AAAEgAeUAEwAtQCoSAQIAAUcAAgIAWAQFAgAAJUgDAQEBIwFJAgAREA8OCwgFBAATAhMGBhQrEzMyFREjETQmKwEiBhURIxEzFTapKE9FCxQoEhBFRQsB5U/+agGLEwwQFP56AeUhIQACACgAAAEsAeUACwAbAChAJQACAgBYBAEAACVIAAMDAVgAAQEjAUkCABoXEg8IBQALAgsFBhQrEzMyFREUKwEiNRE0ExE0JisBIgYVERQWOwEyNndmT09mT8AMEz4TDAwTPhMMAeVP/rlPTwFHT/51ATETDAwT/s8UCwsAAgAt/3MBIAHlAA8AHwA8QDkOAQQACQEBBQJHAAQEAFgDBgIAACVIAAUFAVgAAQEjSAACAicCSQIAHhsWEw0MCwoIBQAPAg8HBhQrEzMyFREUKwEiJxUjETMVNhMRNCYrASIGFREUFjsBMjapKE9PKCwLRUULXgsUKBIQEBIoFAsB5U/+uU8hrgJyISH+dQExEwwQFP7ZFBALAAIAKP9zARsB5QAPAB8AOEA1DwEFAAQBAgQCRwAFBQBYAwEAACVIBgEEBAJYAAICI0gAAQEnAUkSEBoXEB8SHzMyERAHBhgrEzMRIzUGKwEiNRE0OwEyFwMzMjY1ETQmKwEiBhURFBbWRUULLChPTygsC0ooEhAQEigUCwsB5f2OriFPAUdPIf53EBQBJxQQDBP+zxQLAAEALQAAAM8B5QAMACpAJwoBAQABRwABAQBYAwQCAAAlSAACAiMCSQEACQgHBgQCAAwBDAUGFCsTMxUjIhURIxEzFT4BuhUeP0VFCCUB5TxL/qIB5TgXIQABACMAAAEGAeUALQBxS7ASUFhAKwACAwQDAmUABgAHBwZlAAQAAAYEAGAAAwMBWAABASVIAAcHBVkABQUjBUkbQC0AAgMEAwIEbQAGAAcABgdtAAQAAAYEAGAAAwMBWAABASVIAAcHBVkABQUjBUlZQAszEjM1MxIzMwgGHCs3NTQmKwEiPQE0OwEyHQEjNTQmKwEiBh0BFBY7ATIdARQrASI9ATMVFBY7ATI2wgwTMU9PRE9ECxQcFAsLFDFPT0NPRAsUGxMMWVwTDE9zT08xJxQLCxRfEwxPcE9PMScTDAwAAQAFAAAAuwI0ABIAKUAmAAUEBW8DAQAABFYGAQQEJUgAAQECWQACAiMCSRERERIhIxAHBhsrEyMRFBY7ARUjIjURIzUzNTMVM7tFDBMmO08sLEVFAar+tRMMQE8BWztPTwABAC0AAAEgAeUAEwAjQCAEAQEEAUcDAQAAJUgABAQBWQIBAQEjAUkzEjIREAUGGSsTMxEjNQYrASI1ETMRFBY7ATI2NdtFRQssKE9FDBMoEhAB5f4bISFPAZb+dRQLEBQAAAEABQAAARIB5QAGACFAHgEBAQABRwMCAgAAJUgAAQEjAUkAAAAGAAYREgQGFisbAjMDIwNKQUJFUG1QAeX+SwG1/hsB5QABAAUAAAH2AeUADAAhQB4MCQQDAQABRwQDAgAAJUgCAQEBIwFJEhESERAFBhkrATMDIwsBIwMzGwEzEwG0QlZpOjlpVkJIP18+AeX+GwGF/nsB5f5LAbX+SwABAAUAAAE1AeUACwAgQB0JBgMABAACAUcDAQICJUgBAQAAIwBJEhISEQQGGCs3FyMnByM3JzMXNzO8eUpOTkp5b0pFQ0r4+K+v+O2lpQABAAr/cwErAeUADgAxQC4KAQIDAUcEAQMDJUgAAgIjSAABAQBYBQEAACcASQEADAsJCAcGBAIADgEOBgYUKxcjNTMyPwEjAzMbATMDBms2ISsHDC1dRU5JRWEPjTsfMwHl/ksBtf3dTwAAAQAPAAAA+wHlAAkALUAqAQECBgEAAkYAAgIDVgQBAwMlSAAAAAFWAAEBIwFJAAAACQAJEhESBQYXKxMVAzMVIzUTIzX7p6fsp50B5Tv+kTs7AW87AAABAAX/WgEIAw0AIgAyQC8DAQIDAUcABAAFAwQFYAADAAIAAwJgAAABAQBUAAAAAVgAAQABTCElISUhKAYGGisTERQHFhURFBY7ARUjIiY9ATQmKwE1MzI2PQE0NjsBFSMiBrRLSw8TMlMoHSouExMuKh4nUzITDwKx/vtvCQpt/vkSDzsnJ/s8ODo4PfknJzsPAAEAN/+eAHwCzwADABFADgAAAQBvAAEBZhEQAgYWKxMzESM3RUUCz/zPAAEABv9aAQkDDQAiADJALyEBAwIBRwABAAACAQBgAAIAAwUCA2AABQQEBVQABQUEWAAEBQRMISUhJSEjBgYaKxMRNCYrATUzMhYdARQWOwEVIyIGHQEUBisBNTMyNjURNDcmWg8TMlMnHiouExMuKh0oUzITD0tLAawBBRIPOycn+T04Ojg8+ycnOw8SAQdtCgkAAQA0ANgBpgFhABoAMUAuBQEDAAEEAwFgAAQAAARUAAQEAFkCBgIABABNAQAXFhMSDwwKCQYEABoBGQcGFCslIiYnJiMiBh0BIzU0OwEyHgIyNj0BMxUUIwE+IDIOJCgSDEBNHRsuHC4rCkBO2BoPKRERLzxMGh4aEBMuPEwAAgAy/3cAhwHlAAMABwAlQCIEAQMDAlYAAgIlSAABAQBWAAAAJwBJBAQEBwQHEhEQBQYXKxcjEzMnNTMVh1UNO0dSiQGqVHBwAAABACj/tQEiAjEAJwC2S7AQUFhAMAAIBwcIYwAAAQMBAGUAAwICA2MABQQEBWQAAQEHWAkBBwclSAACAgRZBgEEBCMESRtLsBJQWEAxAAgHCG8AAAEDAQADbQADAgEDAmsABQQEBWQAAQEHWAkBBwclSAACAgRZBgEEBCMESRtAMAAIBwhvAAABAwEAA20AAwIBAwJrAAUEBXAAAQEHWAkBBwclSAACAgRZBgEEBCMESVlZQA4mJBEjIREiEzUzEAoGHSsBIzU0JisBIgYVERQWOwEyNj0BMxUUKwEVIzUjIjURNDsBNTMVMzIVASJFCxQyFAsLFDIUC0VPCkERT08RQQpPAVU2EwwME/7PFAsLFDZBT0tLTwFHT0xMTwAAAQAjAAABSgJuACEAcUuwC1BYQCgABQYDBgVlBwEDCAECAQMCXgAGBgRYAAQEIkgKCQIBAQBWAAAAIwBJG0ApAAUGAwYFA20HAQMIAQIBAwJeAAYGBFgABAQiSAoJAgEBAFYAAAAjAElZQBIAAAAhACEREzMSMhETERELBh0rJRUhNTI2PQEjNTM1NDsBMh0BIzU0JisBIgYdATMVIxUUBwFK/tkaFzExT1RPRAwTKxQLf38aOzs7LzN0O9NPT1tQFAsLFMg7g0MQAAIAQQBDAZMCFAAfAC8AP0A8HhYTAwMBDg0LBAMFAAICRx0cFRQEAUUMBQIARAQBAgAAAgBcAAMDAVgAAQElA0kiIConIC8iLz03BQYWKwEVFAcXBycGKwEiJwcnNyY9ATQ3JzcXNjsBMhc3FwcWAzMyNj0BNCYrASIGHQEUFgFwBikuLg4IZhAMMC4vBAQvLjEMD2YOBjAuKQahPhMMDBM+EwwMAZfZFg8oLi4CAy8uLgwT2RQMLy4xAwEvLikP/vwME8MUCwsUwxMMAAEADwAAAUkCbgAWAD5AOxMBAQABRwsKAgcGAQABBwBfBQEBBAECAwECXgkBCAgiSAADAyMDSQAAABYAFhUUERERERERERERDAYdKwEVIwczFSMVIzUjNTMnIzUzAzMbATMDAUdfE3J5RHlyE19OUERZWURQAVQ7QjifnzhCOwEa/qUBW/7mAAACADf/ngB8As8AAwAHACJAHwABAAACAQBeAAIDAwJSAAICA1YAAwIDShERERAEBhgrEyMRMwMzESN8RUVFRUUBdgFZ/ij+pwACADL/4QFRApwANwBHAJ9ACiIBCQAGAQQIAkdLsBJQWEAyAAYHAAcGZQACBAMDAmUKAQAACQgACWALAQgABAIIBGAAAwABAwFdAAcHBVgABQUkB0kbQDQABgcABwYAbQACBAMEAgNtCgEAAAkIAAlgCwEIAAQCCARgAAMAAQMBXQAHBwVYAAUFJAdJWUAfOjgCAEI/OEc6RzIvLCspJh4bFhMQDw0KADcCNwwGFCsTMzIdARQHFh0BFCsBIj0BMxUUFjsBMjY9ATQmKwEiPQE0NyY9ATQ7ATIdASM1NCYrASIGHQEUFgczMjY9ATQmKwEiBh0BFBa+RE8wCE9YT0UMEy4TDAwTRE8wCE9YT0ULFC4UCwsUVxMMDBNXFAsLAcZPU0sMEhhzT08xJhMMDBNdFAtPU0sMEhhzT08xJhQLCxRdEwzUDBNbFAsLFFsTDAACAEQCHQFMAnEAAwAHACRAIQUDBAMBAQBWAgEAACIBSQQEAAAEBwQHBgUAAwADEQYGFSsTNTMVITUzFfRY/vhYAh1UVFRUAAADADEAAAKLAm4AHwArADsAj0uwFlBYQDUAAwQABANlAAAFBQBjAAUAAQYFAWEABwcJWAAJCSJIAAQEAlgAAgIlSAoBBgYIWAAICCMISRtANwADBAAEAwBtAAAFBAAFawAFAAEGBQFhAAcHCVgACQkiSAAEBAJYAAICJUgKAQYGCFgACAgjCElZQBUiIDo3Mi8oJSArIis1MxIzMhELBhorJTUzFRQrASI9ATQ7ATIdASM1NCYrASIGHQEUFjsBMjYHMzI9ATQrASIdARQBFRQGKwEiJj0BNDY7ATIWAZZFT1xPT1xPRQwTMhMMDBMyEwyVuo2Nuo0CF2NrvmtjZGq+amTwGCNPT7FPTyMYEwwME5sUCwug0knd3UnSARpHi4SEi0eNi4sAAAIAGQEYAM4CbgAPAB4AMUAuDwEFAAQBAQQCRwYBBAIBAQQBXAAFBQBYAwEAACIFSRIQGRYQHhIeMzIREAcGGCsTMxEjNQYrASI9ATQ7ATIXAzMyNj0BNCsBIgYdARQWnTExCB8lODglHwg9JQ0LGCUOCQkCbv6qGBg45jgY/u0LDs4aCA7VDggAAAIAKwBqAbkBogAFAAsAM0AwCgcEAQQBAAFHAgEAAQEAUgIBAAABVgUDBAMBAAFKBgYAAAYLBgsJCAAFAAUSBgYVKyUnNzMHFyEnNzMHFwFseXlNfHz+63l5TXx8apycnZucnJ2bAAEAHgCxAYgBiwAFAEhLsAlQWEAYAAABAQBkAwECAQECUgMBAgIBVgABAgFKG0AXAAABAHADAQIBAQJSAwECAgFWAAECAUpZQAsAAAAFAAUREQQGFisBFSM1ITUBiDv+0QGL2pZEAAEANwEHAPsBQQADAB5AGwAAAQEAUgAAAAFWAgEBAAFKAAAAAwADEQMGFSsTNTMVN8QBBzo6AAQAIwFXAWoCqQAHABQAIAAuAFFATgEAAgEADgEEAQJHBQEDBAYEAwZtAAEABAMBBF4KAQYACAYIXAAHBwlYAAkJJEgAAAACWAACAiIASRcVLisnJB0aFSAXIBERFSIREwsGGisTNTQrARUzMiczMh0BFAcXIycjFSMHMzI9ATQrASIdARQlFRQGKwEiJj0BNDsBMtgGGxsGQEAfFSAgHwwfBGZMTGZMASI2Omc6NnBncAIPPAdJYx5CGgRXVlYfcSh5eShxmSdLSEhLJ5gAAQBSAiYBPgJgAAMAGUAWAgEBAQBWAAAAIgFJAAAAAwADEQMGFSsTNTMVUuwCJjo6AAACADoBYQEkAm4ADwAbABxAGQABAAIBAlwAAAADWAADAyIASTM0NTMEBhgrEzU0JisBIgYdARQWOwEyNjcVFCsBIj0BNDsBMuQMEywTDAwTLBMMQE1QTU1QTQG3YRMLCxNhEwsLfXNNTXNNAAACAB4AswFoAekACwAPADBALQQBAAMBAQIAAV4ABggBBwYHWgACAgVWAAUFJQJJDAwMDwwPEhEREREREAkGGysTMxUjFSM1IzUzNTMDNSEV5oKCRoKCRsgBSgGZPFBQPFD+yjw8AAABAA4BGACmAm4AGgBPtRABAwIBR0uwElBYQBkAAAQCBABlAAIAAwIDWgAEBAFYAAEBIgRJG0AaAAAEAgQAAm0AAgADAgNaAAQEAVgAAQEiBElZtzcRFTIRBQYZKxMVIzU0OwEyHQEUDwEzFSM1NzY9ATQmKwEiBj0vKz8rEE9imFYPBwoUCgcCMzNDKysfLh+SLSyeHCkMCwYGAAABABABGACqAm4ALgB2tQsBBQYBR0uwFFBYQCoAAAcGBwBlAAMFBAQDZQAEAAIEAl0ABwcBWAABASJIAAUFBlgABgYlBUkbQCwAAAcGBwAGbQADBQQFAwRtAAQAAgQCXQAHBwFYAAEBIkgABQUGWAAGBiUFSVlACzUhJTMSODIRCAYcKxMVIzU0OwEyHQEUBxYdARQrASI9ATMVFBY7ATI2PQE0JisBNTMyNj0BNCYrASIGQDAsQiwVFSxCLDAGCxgLBgYLHh4LBgYLGAsGAjMuPisrYxgCAxdpKys+LQsHBwtLCwYrBwpHCwYGAAEAdgIEARYCgwADAC5LsBhQWEAMAgEBAAFwAAAAIgBJG0AKAAABAG8CAQEBZllACgAAAAMAAxEDBhUrEzczB3ZOUmICBH9/AAEAKP9zARsB5QATAClAJgQBAQUBRwQBAAAlSAAFBQFYAgEBASNIAAMDJwNJMxERIhEQBgYaKxMzESM1BisBFSMRMxEUFjsBMjY11kVFCywyRUULFCgSEAHl/hshIY0Ccv51FAsQFAAAAgA3/3MBqQJuAAMADQAjQCAAAwMAWAUEAgAAIkgCAQEBJwFJBAQEDQQMIRIREAYGGCsBMxEjAxEjESMiPQE0MwFlRERSRElPTwJu/QUC+/0FAXNP6k8AAAEAHgEDAHABcwADAB5AGwAAAQEAUgAAAAFWAgEBAAFKAAAAAwADEQMGFSsTNTMVHlIBA3BwAAEAiP9YARH/zQALAD5LsBZQWEAWAAACAgBjAAIBAQJUAAICAVkAAQIBTRtAFQAAAgBvAAIBAQJUAAICAVkAAQIBTVm1ISMRAwYXKxc1MxUUBisBNTMyNtBBLyowJRMQSRYcLC06EQAAAQALARgAYAJuAAUAH0AcAAABAHAAAQECVgMBAgIiAUkAAAAFAAUREQQGFisTESMRIzVgMCUCbv6qASwqAAACABoBGADIAm4ACwAbACVAIgADAAEDAVwAAgIAWAQBAAAiAkkCABoXEg8IBQALAgsFBhQrEzMyHQEUKwEiPQE0EzU0JisBIgYdARQWOwEyNlI+ODg+OH4JDiEOCAgOIQ4JAm445jg45jj+6tYOCAgO1g4ICAAAAgArAGoBuQGiAAUACwAmQCMJBgMABAABAUcDAQEAAAFSAwEBAQBWAgEAAQBKEhISEQQGGCsBByM3JzMPASM3JzMBuXlNfHxNT3lNfHxNAQacm52cnJudAAIAFP93AQcB5QAaAB4ArkuwC1BYQCwAAwYCAgNlAAAEBQUAZQACAAQAAgRhAAYGB1YIAQcHJUgABQUBWQABAScBSRtLsBRQWEAtAAMGAgIDZQAABAUEAAVtAAIABAACBGEABgYHVggBBwclSAAFBQFZAAEBJwFJG0AuAAMGAgYDAm0AAAQFBAAFbQACAAQAAgRhAAYGB1YIAQcHJUgABQUBWQABAScBSVlZQBAbGxseGx4UNSERIzIQCQYbKzczFRQrASI9ATQ7ATczFyMiBh0BFBY7ATI2NQMVIzXCRU9VT08SAzkGQhIKChIwEwoEUhpWTU3MTkN+DROxEw4OEwITcHAAAwAPAAABSAMMAAcACgAOADpANwoBBAIBRwAGBQZvAAUCBW8ABAAAAQQAXwACAiJIBwMCAQEjAUkAAA4NDAsJCAAHAAcREREIBhcrIScjByMTMxMnMwM3IyczAQMafhdFYW1r1W06GT5iUp6eAm79ktkBZU9/AAMADwAAAUgDDAAHAAoADgA/QDwKAQQCAUcIAQYFBm8ABQIFbwAEAAABBABfAAICIkgHAwIBASMBSQsLAAALDgsODQwJCAAHAAcREREJBhcrIScjByMTMxMnMwM3ByM3AQMafhdFYW1r1W06gWI+Tp6eAm79ktkBZc5/fwADAA8AAAFIAwwABwAKABEAQUA+CwEFBgoBBAICRwAGBQZvBwEFAgVvAAQAAAEEAF8AAgIiSAgDAgEBIwFJAAAREA8ODQwJCAAHAAcREREJBhcrIScjByMTMxMnMwM1ByM3MxcjAQMafhdFYW1r1W06NExVV1VMnp4Cbv2S2QFlo1R/fwADAA8AAAFIAucABwAKACIASkBHCgEEAgFHCAEGAAoFBgpgAAcJAQUCBwVhAAQAAAEEAF8AAgIiSAsDAgEBIwFJAAAiIB8cGRgVFBMQDQwJCAAHAAcREREMBhcrIScjByMTMxMnMwMnFSM1NDY7ATIWMjY9ATMVFAYrASImIyIBAxp+F0VhbWvVbTo/PiEnBhc6FQg+HygFGD0KEZ6eAm79ktkBZVoLDiUnIwoPCg4lJyMABAAPAAABSALhAAcACgAOABIASEBFCgEEAgFHCwgKAwYHAQUCBgVeAAQAAAEEAF8AAgIiSAkDAgEBIwFJDw8LCwAADxIPEhEQCw4LDg0MCQgABwAHERERDAYXKyEnIwcjEzMTJzMDJxUjNSEVIzUBAxp+F0VhbWvVbTosWAEIWJ6eAm79ktkBZaNUVFRUAAQADwAAAUgDIAAHAAoAFgAmAElARgoBBAIBRwAGAAgHBghgAAcKAQUCBwVgAAQAAAEEAF8AAgIiSAkDAgEBIwFJDQsAACUiHRoTEAsWDRYJCAAHAAcRERELBhcrIScjByMTMxMnMwM3IyI9ATQ7ATIdARQnFRQWOwEyNj0BNCYrASIGAQMafhdFYW1r1W06HjxBQTxBiwoSIBIKChIgEgqengJu/ZLZAWVPQRFBQRFBTAURCgoRBREKCgAAAgAOAAAB+AJuAA8AEgBBQD4RAQYFAUcABgAHCAYHXgkBCAACAAgCXgAFBQRWAAQEIkgAAAABVgMBAQEjAUkQEBASEBIREREREREREAoGHCslMxUjNSMHIxMhFSMVMxUjBxEDAU2r8H04ReIBAaSGhkVoOzuengJuO9I7TQEk/twAAAIAMv9YATECbgAfACsAuEuwCVBYQDAAAAEDAQBlAAMCAgNjAAYECAgGZQAIAAcIB10AAQEFWAAFBSJIAAICBFkABAQjBEkbS7AWUFhAMgAAAQMBAANtAAMCAQMCawAGBAgIBmUACAAHCAddAAEBBVgABQUiSAACAgRZAAQEIwRJG0AzAAABAwEAA20AAwIBAwJrAAYECAQGCG0ACAAHCAddAAEBBVgABQUiSAACAgRZAAQEIwRJWVlADCEjEzMyEzUzEAkGHSsBIzU0JisBIgYVERQWOwEyNj0BMxUUKwEiNRE0OwEyFQM1MxUUBisBNTMyNgExRQwTNxQLCxQ3EwxFT2FPT2FPnEEvKjAlExABtl4UCwsU/kUTDAwTXmlPTwHRT0/9mBYcLC06EQAAAgAqAAABJwMMAAsADwA1QDIABwYHbwAGAgZvAAQABQAEBV4AAwMCVgACAiJIAAAAAVYAAQEjAUkREREREREREAgGHCs3MxUjETMVIxUzFSMTIyczfKvw6aSGhk4+YlI7OwJuO9I7AWd/AAIANwAAASgDDAALAA8AQkA/CQEHBgdvAAYCBm8ABAgBBQAEBV4AAwMCVgACAiJIAAAAAVYAAQEjAUkMDAAADA8MDw4NAAsACxERERERCgYZKxMVMxUjETMVIxUzFRMHIzd8q/DppIYmYj5OASbrOwJuO9I7AeZ/fwAAAgAsAAABLQMMAAsAEgBGQEMMAQYHAUcABwYHbwgBBgIGbwAECQEFAAQFXgADAwJWAAICIkgAAAABVgABASMBSQAAEhEQDw4NAAsACxERERERCgYZKxMVMxUjETMVIxUzFQMHIzczFyN8q/DppIZWNExVV1VMASbrOwJuO9I7AbtUf38AAwAlAAABLQLhAAMADwATAERAQQsJAgAIAQEEAAFeAAYKAQcCBgdeAAUFBFYABAQiSAACAgNWAAMDIwNJEBAEBBATEBMSEQQPBA8REREREhEQDAYbKxMzFSMDFTMVIxEzFSMVMxUDFSM11VhYWavw6aSGhVgC4VT+mes7Am470jsBu1RUAAAC/+IAAACFAwwAAwAHACdAJAQBAQABbwAAAgBvAAICIkgAAwMjA0kAAAcGBQQAAwADEQUGFSsTFyMnFzMRIzROPmJfREQDDH9/nv2SAAACAEEAAADoAwwAAwAHACdAJAAAAQBvBAEBAgFvAAICIkgAAwMjA0kAAAcGBQQAAwADEQUGFSsTNzMPATMRI0hOUmJFREQCjX9/H/2SAAAC/+QAAADlAwwABgAKAC9ALAUBAQABRwAAAQBvBQICAQMBbwADAyJIAAQEIwRJAAAKCQgHAAYABhERBgYWKwM3MxcjJwcXMxEjHFVXVUw1NBFERAKNf39UVB/9kgAAA//dAAAA5QLhAAMABwALAC5AKwQBAgcFBgMDAAIDXgAAACJIAAEBIwFJCAgEBAgLCAsKCQQHBAcSERAIBhcrEzMRIxM1MxUhNTMVQURETFj++FgCbv2SAo1UVFRUAAIABQAAAUsCbgALABkALUAqBQEDBgECBwMCXgAEBABYAAAAIkgABwcBWAABASMBSSERESQRESMgCAYcKxMzMhURFCsBESM1MxcRNCYrARUzFSMVMzI2N8VPT8UyMs8ME2tQUGsTDAJuT/4wTwEbO/wBuhQL3TvgCwAAAgA3AAABQgLnABcAIQBFQEIhHAIHBgFHBQEDAAEAAwFgAAQCCgIABgQAYQkBBgYiSAgBBwcjB0kBACAfHh0bGhkYExIPDg0KBwYEAgAXARYLBhQrEyImIyIdASM1NDY7ATIWMjY9ATMVFAYjFzMRIwMRIxEzE+0YPQoRPiEnBhc6FQg+HygRP0uBP1B8Ao0jGAsOJScjCg8KDiUnH/2SAeL+HgJu/i8AAwAoAAABNgMMAAsAGwAfADZAMwAFBAVvAAQABG8AAgIAWAYBAAAiSAADAwFYAAEBIwFJAgAfHh0cGhcSDwgFAAsCCwcGFCsTMzIVERQrASI1ETQTETQmKwEiBhURFBY7ATI2AyMnM4FmT09mT8AMEz4TDAwTPhMMKj5iUgJuT/4wT08B0E/97AG6FAsLFP5GFAsLAkd/AAADADIAAAE2AwwACwAbAB8AO0A4BwEFBAVvAAQABG8AAgIAWAYBAAAiSAADAwFYAAEBIwFJHBwCABwfHB8eHRoXEg8IBQALAgsIBhQrEzMyFREUKwEiNRE0ExE0JisBIgYVERQWOwEyNhMHIzeBZk9PZk/ADBM+EwwMEz4TDDpiPk4Cbk/+ME9PAdBP/ewBuhQLCxT+RhQLCwLGf38AAAMAMgAAATYDDAALABsAIgA/QDwcAQQFAUcABQQFbwYBBAAEbwACAgBYBwEAACJIAAMDAVgAAQEjAUkCACIhIB8eHRoXEg8IBQALAgsIBhQrEzMyFREUKwEiNRE0ExE0JisBIgYVERQWOwEyNgMHIzczFyOBZk9PZk/ADBM+EwwMEz4TDD80TFVXVUwCbk/+ME9PAdBP/ewBuhQLCxT+RhQLCwKbVH9/AAMAMgAAATYC5wALABsAMwBGQEMHAQUACQQFCWAABggBBAAGBGEAAgIAWAoBAAAiSAADAwFYAAEBIwFJAgAzMTAtKikmJSQhHh0aFxIPCAUACwILCwYUKxMzMhURFCsBIjURNBMRNCYrASIGFREUFjsBMjYDFSM1NDY7ATIWMjY9ATMVFAYrASImIyKBZk9PZk/ADBM+EwwMEz4TDH4+IScGFzoVCD4fKAUYPQoRAm5P/jBPTwHQT/3sAboUCwsU/kYUCwsCUgsOJScjCg8KDiUnIwAABAAwAAABOALhAAMADwAfACMAPUA6CQcCAAYBAQIAAV4ABAQCWAgBAgIiSAAFBQNYAAMDIwNJICAGBCAjICMiIR4bFhMMCQQPBg8REAoGFisTMxUjBzMyFREUKwEiNRE0ExE0JisBIgYVERQWOwEyNgMVIzXgWFhfZk9PZk/ADBM+EwwMEz4TDGpYAuFUH0/+ME9PAdBP/ewBuhQLCxT+RhQLCwKbVFQAAQAZAKEBRQHNAAsABrMJAwEtKwEHFwcnByc3JzcXNwFFaWktaWktaWktaWkBoGlpLWlpLWlpLWlpAAMAN//sATsCggAVAB0AJQBoQBEUEQIEAh8XAgUECQYCAAUDR0uwGFBYQB8AAwMiSAAEBAJYAAICIkgABQUAWAAAACNIAAEBIwFJG0AfAAMCA28AAQABcAAEBAJYAAICIkgABQUAWAAAACMASVlACTU2EjUSMgYGGisBERQrASInByM3JjURNDsBMhc3MwcWBxETJisBIgYTEQMWOwEyNgE7T2YKDgcwDw9PZgoOBzAPD8F3BhM/Ewx+dwUTPxQMAh/+ME8CFi0TIwHQTwIWLRMr/qYBdAUM/i0BWv6MBQsAAgA0AAABOAMMABEAFQAnQCQABQQFbwAEAARvAgEAACJIAAMDAVkAAQEjAUkREzMSMhAGBhorEzMRFCsBIjURMxEUFjsBMjY1AyMnM/RET2ZPRAwTPhMMHz5iUgJu/eFPTwIf/ewUCwsUAjN/AAACADQAAAE4AwwAEQAVAC1AKgYBBQQFbwAEAARvAgEAACJIAAMDAVkAAQEjAUkSEhIVEhUUMxIyEAcGGSsTMxEUKwEiNREzERQWOwEyNjUTByM39ERPZk9EDBM+EwxCYj5OAm794U9PAh/97BQLCxQCsn9/AAIANAAAATgDDAARABgAL0AsEgEEBQFHAAUEBW8GAQQABG8CAQAAIkgAAwMBWQABASMBSRERFDMSMhAHBhsrEzMRFCsBIjURMxEUFjsBMjY1AwcjNzMXI/RET2ZPRAwTPhMMPzRMVVdVTAJu/eFPTwIf/ewUCwsUAodUf38AAAMAMgAAAToC4QADABUAGQAvQCwIBwIABgEBAgABXgQBAgIiSAAFBQNZAAMDIwNJFhYWGRYZEzMSMhIREAkGGysTMxUjExEzERQrASI1ETMRFBY7ATI2AxUjNeJYWBJET2ZPRAwTPhMMalgC4VT9zQIU/eFPTwIf/ewUCwsCm1RUAAACAAUAAAE6AwwACAAMAC9ALAgFAgMAAQFHBQEEAwRvAAMBA28CAQEBIkgAAAAjAEkJCQkMCQwTEhIQBgYYKzMjNQMzGwEzAxMHIzfERHtFWFNFdl9iPk6+AbD+kwFt/lACTn9/AAACADcAAAE7Am4ACwAVAClAJgAFAAECBQFgAAMDIkgABAQAWAAAACVIAAICIwJJISQRESMgBgYaKxMzMh0BFCsBFSMRMxM1NCYrARUzMjZ7cU9PcUREfAsUXV0UCwHxT9ZPfQJu/mnAEwz+DAABAC0AAAGFAk0AOwBzS7ANUFhAKwAAAgEBAGUABgAEAwYEYAADAAcIAwdgAAgAAgAIAmAAAQEFWQkBBQUjBUkbQCwAAAIBAgABbQAGAAQDBgRgAAMABwgDB2AACAACAAgCYAABAQVZCQEFBSMFSVlADjo3NTQyEzUjNTMQCgYdKzczFRQWOwEyNj0BNCYrASI9ATQzMjY9ATQmKwEiBhURIxE0OwEyHQEUBisBIgYdARQWOwEyHQEUKwEiNbhDCxQJEwwMEx5PSxoODBN5Ew1ET6BPJCAPFAwMFB5PTy9PlDsTDAwTXBMMT0FPCRAjFAwNE/4RAf5PT0QfIgwTLRMMT3BPTwAAAwAoAAABGwKDAA8AHwAjAHRACg8BBQAEAQEEAkdLsBhQWEAlAAYHAAcGAG0ABwciSAAFBQBYAwEAACVICAEEBAFYAgEBASMBSRtAIgAHBgdvAAYABm8ABQUAWAMBAAAlSAgBBAQBWAIBAQEjAUlZQBMSECMiISAaFxAfEh8zMhEQCQYYKxMzESM1BisBIjURNDsBMhcDMzI2NRE0JisBIgYVERQWEyMnM9ZFRQssKE9PKCwLSigSEBASKBQLC2U+YlIB5f4bISFPAUdPIf53EBQBJxQQDBP+zxQLAcl/AAADACgAAAEqAoMADwAfACMAekAKDwEFAAQBAQQCR0uwGFBYQCYABgcABwYAbQkBBwciSAAFBQBYAwEAACVICAEEBAFYAgEBASMBSRtAIwkBBwYHbwAGAAZvAAUFAFgDAQAAJUgIAQQEAVgCAQEBIwFJWUAXICASECAjICMiIRoXEB8SHzMyERAKBhgrEzMRIzUGKwEiNRE0OwEyFwMzMjY1ETQmKwEiBhURFBYTByM31kVFCywoT08oLAtKKBIQEBIoFAsLsmI+TgHl/hshIU8BR08h/ncQFAEnFBAME/7PFAsCSH9/AAMAJgAAAScCgwAPAB8AJgB8QA4gAQYHDwEFAAQBAQQDR0uwGFBYQCYIAQYHAAcGAG0ABwciSAAFBQBYAwEAACVICQEEBAFYAgEBASMBSRtAIwAHBgdvCAEGAAZvAAUFAFgDAQAAJUgJAQQEAVgCAQEBIwFJWUAVEhAmJSQjIiEaFxAfEh8zMhEQCgYYKxMzESM1BisBIjURNDsBMhcDMzI2NRE0JisBIgYVERQWEwcjNzMXI9ZFRQssKE9PKCwLSigSEBASKBQLCy40TFVXVUwB5f4bISFPAUdPIf53EBQBJxQQDBP+zxQLAh1Uf38AAAMAIgAAARwCcAAXACcANwBPQEwnAQsGHAEHCgJHAAUDAQEGBQFhAAICAFgEAQAAIkgACwsGWAkBBgYlSAwBCgoHWAgBBwcjB0kqKDIvKDcqNyYjMhETETMSITMQDQYdKxMzFRQGKwEiJiMiHQEjNTQ2OwEyFjI2NQczESM1BisBIjURNDsBMhcDMzI2NRE0JisBIgYVERQW3j4fKAUYPQoRPiEnBhc6FQgIRUULLChPTygsC0ooEhAQEigUCwsCcA4lJyMYCw4lJyMKD4H+GyEhTwFHTyH+dxAUAScUEAwT/s8UCwAABAAcAAABJAJxAAMAEwAjACcAS0BIEwEHAggBAwYCRwgBAQEAVgsJAgAAIkgABwcCWAUBAgIlSAoBBgYDWAQBAwMjA0kkJBYUJCckJyYlHhsUIxYjMzIREREQDAYaKxMzFSMXMxEjNQYrASI1ETQ7ATIXAzMyNjURNCYrASIGFREUFgMVIzXMWFgKRUULLChPTygsC0ooEhAQEigUCwsEWAJxVDj+GyEhTwFHTyH+dxAUAScUEAwT/s8UCwI2VFQABAAoAAABGwKiAA8AHwArADsAU0BQDwEFAAQBAQQCRwAICwEGAAgGYAAJCQdYAAcHJEgABQUAWAMBAAAlSAoBBAQBWAIBAQEjAUkiIBIQOjcyLyglICsiKxoXEB8SHzMyERAMBhgrEzMRIzUGKwEiNRE0OwEyFwMzMjY1ETQmKwEiBhURFBYTIyI9ATQ7ATIdARQnFRQWOwEyNj0BNCYrASIG1kVFCywoT08oLAtKKBIQEBIoFAsLTjxBQTxBiwoSIBIKChIgEgoB5f4bISFPAUdPIf53EBQBJxQQDBP+zxQLAdRBEUFBEUFMBREKChEFEQoKAAMALQAAAdUB5QAwADoARADpQBgpAQQGGgEFBBkBCApBQBgDAQgRAQIABUdLsAtQWEAyAAUECgQFZQABCAAAAWUACgwBCAEKCF4NCQIEBAZYBwEGBiVIDgsCAAACWQMBAgIjAkkbS7ASUFhAMwAFBAoEBQptAAEIAAABZQAKDAEIAQoIXg0JAgQEBlgHAQYGJUgOCwIAAAJZAwECAiMCSRtANAAFBAoEBQptAAEIAAgBAG0ACgwBCAEKCF4NCQIEBAZYBwEGBiVIDgsCAAACWQMBAgIjAklZWUAfPTszMQAAO0Q9RDc2MTozOgAwADA0MhM1NDITMw8GHCslFRQWOwEyNj0BMxUUKwEiJicOASsBIj0BNzUmKwEiBh0BIzU0OwEyFhc+ATsBMh0BJyMiBh0BMzU0JgMzMjY3NQcVFBYBIAwTLxQLRU9QFB8EBB8WR0+uAx8oEwxFT0cWHwQEHxRTT2QyEwxwDPMoERABaQzbgRQLCxQsN08XDw8XT3lHfR4ME1NeTxcPDxdPu88ME3V1Ewz+kQ0RdypMFAsAAAIAKP9YASIB5QAfACsAuEuwEFBYQDAAAAEDAQBlAAMCAgNjAAYECAgGZQAIAAcIB10AAQEFWAAFBSVIAAICBFkABAQjBEkbS7AWUFhAMgAAAQMBAANtAAMCAQMCawAGBAgIBmUACAAHCAddAAEBBVgABQUlSAACAgRZAAQEIwRJG0AzAAABAwEAA20AAwIBAwJrAAYECAQGCG0ACAAHCAddAAEBBVgABQUlSAACAgRZAAQEIwRJWVlADCEjEzMyEzUzEAkGHSsBIzU0JisBIgYVERQWOwEyNj0BMxUUKwEiNRE0OwEyFQM1MxUUBisBNTMyNgEiRQsUMhQLCxQyFAtFT1xPT1xPmkEvKjAlExABVTYTDAwT/s8UCwsUNkFPTwFHT0/+IRYcLC06EQAAAwAlAAABJwKDABcAIQAlAMVLsBJQWEAyAAcIAAgHAG0AAwECAgNlAAUAAQMFAV8ACAgiSAAGBgBYCQEAACVIAAICBFkABAQjBEkbS7AYUFhAMwAHCAAIBwBtAAMBAgEDAm0ABQABAwUBXwAICCJIAAYGAFgJAQAAJUgAAgIEWQAEBCMESRtAMAAIBwhvAAcAB28AAwECAQMCbQAFAAEDBQFfAAYGAFgJAQAAJUgAAgIEWQAEBCMESVlZQBkCACUkIyIfHBkYFBEPDgsIBQQAFwIXCgYUKxMzMh0BIxUUFjsBMjY9ATMVFCsBIjURNBczNTQmKwEiBhU3IyczfFxPtQwTLxQLRU9ZT0VwDBMyEwxTPmJSAeVPu4EUCwsULDdPTwFHT891EwwME3l/AAADAC0AAAEqAoMAFwAhACUAzEuwElBYQDMABwgACAcAbQADAQICA2UABQABAwUBXwoBCAgiSAAGBgBYCQEAACVIAAICBFkABAQjBEkbS7AYUFhANAAHCAAIBwBtAAMBAgEDAm0ABQABAwUBXwoBCAgiSAAGBgBYCQEAACVIAAICBFkABAQjBEkbQDEKAQgHCG8ABwAHbwADAQIBAwJtAAUAAQMFAV8ABgYAWAkBAAAlSAACAgRZAAQEIwRJWVlAHSIiAgAiJSIlJCMfHBkYFBEPDgsIBQQAFwIXCwYUKxMzMh0BIxUUFjsBMjY9ATMVFCsBIjURNBczNTQmKwEiBhU3ByM3fFxPtQwTLxQLRU9ZT0VwDBMyEwy4Yj5OAeVPu4EUCwsULDdPTwFHT891EwwME/h/fwAAAwArAAABLAKDABcAIQAoANG1IgEHCAFHS7ASUFhAMwkBBwgACAcAbQADAQICA2UABQABAwUBXgAICCJIAAYGAFgKAQAAJUgAAgIEWQAEBCMESRtLsBhQWEA0CQEHCAAIBwBtAAMBAgEDAm0ABQABAwUBXgAICCJIAAYGAFgKAQAAJUgAAgIEWQAEBCMESRtAMQAIBwhvCQEHAAdvAAMBAgEDAm0ABQABAwUBXgAGBgBYCgEAACVIAAICBFkABAQjBElZWUAbAgAoJyYlJCMfHBkYFBEPDgsIBQQAFwIXCwYUKxMzMh0BIxUUFjsBMjY9ATMVFCsBIjURNBczNTQmKwEiBhU3ByM3MxcjfFxPtQwTLxQLRU9ZT0VwDBMyEww5NExVV1VMAeVPu4EUCwsULDdPTwFHT891EwwME81Uf38AAAQAIAAAASgCcQADABsAJQApAJJLsBJQWEAyAAUDBAQFZQAHAAMFBwNeCQEBAQBWDAoCAAAiSAAICAJYCwECAiVIAAQEBlkABgYjBkkbQDMABQMEAwUEbQAHAAMFBwNeCQEBAQBWDAoCAAAiSAAICAJYCwECAiVIAAQEBlkABgYjBklZQB8mJgYEJikmKSgnIyAdHBgVExIPDAkIBBsGGxEQDQYWKxMzFSMHMzIdASMVFBY7ATI2PQEzFRQrASI1ETQXMzU0JisBIgYVNxUjNdBYWFRcT7UMEy8UC0VPWU9FcAwTMhMMBlgCcVQ4T7uBFAsLFCw3T08BR0/PdRMMDBPmVFQAAv/OAAAAcgKDAAMABwBLS7AYUFhAGQAAAQMBAANtBAEBASJIAAMDJUgAAgIjAkkbQBYEAQEAAW8AAAMAbwADAyVIAAICIwJJWUAOAAAHBgUEAAMAAxEFBhUrExcjJxMjETMgTj5ipEVFAoN/f/19AeUAAgAtAAAAzQKDAAMABwBLS7AYUFhAGQQBAQADAAEDbQAAACJIAAMDJUgAAgIjAkkbQBYAAAEAbwQBAQMBbwADAyVIAAICIwJJWUAOAAAHBgUEAAMAAxEFBhUrEzczBxMjETMtTlJiB0VFAgR/f/38AeUAAv/QAAAA0QKDAAYACgBVtQUBAQABR0uwGFBYQBoFAgIBAAQAAQRtAAAAIkgABAQlSAADAyMDSRtAFwAAAQBvBQICAQQBbwAEBCVIAAMDIwNJWUAPAAAKCQgHAAYABhERBgYWKwM3MxcjJwcTIxEzMFVXVUw1NFZFRQIEf39UVP38AeUAA//MAAAA1AJxAAMABwALADJALwcDBgMBAQBWAgEAACJIAAUFJUgABAQjBEkEBAAACwoJCAQHBAcGBQADAAMRCAYVKxM1MxUhNTMVEyMRM3xY/vhYTkVFAh1UVFRU/eMB5QACACgAAAEpAm4AGAAnADBALRcWFRQPDg0MCAECAUcAAQADBAEDYQACAiJIAAQEAFgAAAAjAEk1KhcjMgUGGSsBFRQrASI1ETQ7ASYnBzU3JiczFhc3FQcWAzU0JyMiBh0BFBY7ATI2ASlPY09PXRMgXEEcI18TD1k+RkUEVBQLCxQ5EwwBNeZPTwD/Tz0vIzAZIxwTEyIwGGz+pNAgGAwT6RQLCwAAAgAiAAABIAJwABMAKwBNQEoSAQIAAUcABwkBBQAHBWEACgoGWAgBBgYiSAACAgBYBAsCAAAlSAMBAQEjAUkCACspKCUiIR4dHBkWFREQDw4LCAUEABMCEwwGFCsTMzIVESMRNCYrASIGFREjETMVNicVIzU0NjsBMhYyNj0BMxUUBisBIiYjIqkoT0ULFCgSEEVFCx0+IScGFzoVCD4fKAUYPQoRAeVP/moBixMMEBT+egHlISE8Cw4lJyMKDwoOJScjAAMAJgAAASwCgwALABsAHwBkS7AYUFhAIwAEBQAFBABtAAUFIkgAAgIAWAYBAAAlSAADAwFYAAEBIwFJG0AgAAUEBW8ABAAEbwACAgBYBgEAACVIAAMDAVgAAQEjAUlZQBMCAB8eHRwaFxIPCAUACwILBwYUKxMzMhURFCsBIjURNBMRNCYrASIGFREUFjsBMjYDIyczd2ZPT2ZPwAwTPhMMDBM+EwwiPmJSAeVP/rlPTwFHT/51ATETDAwT/s8UCwsBvn8AAAMAKAAAASwCgwALABsAHwBqS7AYUFhAJAAEBQAFBABtBwEFBSJIAAICAFgGAQAAJUgAAwMBWAABASMBSRtAIQcBBQQFbwAEAARvAAICAFgGAQAAJUgAAwMBWAABASMBSVlAFxwcAgAcHxwfHh0aFxIPCAUACwILCAYUKxMzMhURFCsBIjURNBMRNCYrASIGFREUFjsBMjYTByM3d2ZPT2ZPwAwTPhMMDBM+Eww+Yj5OAeVP/rlPTwFHT/51ATETDAwT/s8UCwsCPX9/AAMAJwAAASwCgwALABsAIgBvtRwBBAUBR0uwGFBYQCQGAQQFAAUEAG0ABQUiSAACAgBYBwEAACVIAAMDAVgAAQEjAUkbQCEABQQFbwYBBAAEbwACAgBYBwEAACVIAAMDAVgAAQEjAUlZQBUCACIhIB8eHRoXEg8IBQALAgsIBhQrEzMyFREUKwEiNRE0ExE0JisBIgYVERQWOwEyNgMHIzczFyN3Zk9PZk/ADBM+EwwMEz4TDEE0TFVXVUwB5U/+uU9PAUdP/nUBMRMMDBP+zxQLCwISVH9/AAMAJgAAASwCcAALABsAMwBIQEUABggBBAAGBGEACQkFWAcBBQUiSAACAgBYCgEAACVIAAMDAVgAAQEjAUkCADMxMC0qKSYlJCEeHRoXEg8IBQALAgsLBhQrEzMyFREUKwEiNRE0ExE0JisBIgYVERQWOwEyNgMVIzU0NjsBMhYyNj0BMxUUBisBIiYjIndmT09mT8AMEz4TDAwTPhMMhD4hJwYXOhUIPh8oBRg9ChEB5U/+uU9PAUdP/nUBMRMMDBP+zxQLCwHbCw4lJyMKDwoOJScjAAAEACUAAAEtAnEAAwAPAB8AIwA/QDwGAQEBAFYJBwIAACJIAAQEAlgIAQICJUgABQUDWAADAyMDSSAgBgQgIyAjIiEeGxYTDAkEDwYPERAKBhYrEzMVIwczMhURFCsBIjURNBMRNCYrASIGFREUFjsBMjYDFSM11VhYXmZPT2ZPwAwTPhMMDBM+EwxrWAJxVDhP/rlPTwFHT/51ATETDAwT/s8UCwsCK1RUAAMAGQCDAWMB6wADAAcACwAsQCkABAYBBQAEBV4AAAABAAFaAAMDAlYAAgIlA0kICAgLCAsSEREREAcGGSs3MxUjETMVIwc1IRWbRkZGRoIBStNQAWhQgjw8AAMALf/sATEB+QAVAB0AJQBoQBEUEQIEAh8XAgUECQYCAAUDR0uwGFBYQB8AAwMlSAAEBAJYAAICJUgABQUAWQAAACNIAAEBIwFJG0AfAAMCA28AAQABcAAEBAJYAAICJUgABQUAWQAAACMASVlACTU2EyUTIgYGGisBERQrASInByM3JjURNDsBMhc3MwcWBxETJisBIgYTEQMWOwEyNgExT2YIDgkwERFPZggOCTAREcJ0CgpAFAyAdAoKQBQMAZb+uU8CFioUJQFHTwIWKhQr/v8BHwIM/rEBAf7hAgwAAgAoAAABIAKDABMAFwBatQQBAQQBR0uwGFBYQB8ABQYABgUAbQAGBiJIAwEAACVIAAQEAVkCAQEBIwFJG0AcAAYFBm8ABQAFbwMBAAAlSAAEBAFZAgEBASMBSVlAChETMxIyERAHBhsrEzMRIzUGKwEiNREzERQWOwEyNjUDIycz20VFCywoT0UMEygSEBM+YlIB5f4bISFPAZb+dRQLEBQBpX8AAAIALQAAASkCgwATABcAYbUEAQEEAUdLsBhQWEAgAAUGAAYFAG0HAQYGIkgDAQAAJUgABAQBWQIBAQEjAUkbQB0HAQYFBm8ABQAFbwMBAAAlSAAEBAFZAgEBASMBSVlADxQUFBcUFxQzEjIREAgGGisTMxEjNQYrASI1ETMRFBY7ATI2NRMHIzfbRUULLChPRQwTKBIQTmI+TgHl/hshIU8Blv51FAsQFAIkf38AAAIAIQAAASICgwATABoAYkAKFAEFBgQBAQQCR0uwGFBYQCAHAQUGAAYFAG0ABgYiSAMBAAAlSAAEBAFZAgEBASMBSRtAHQAGBQZvBwEFAAVvAwEAACVIAAQEAVkCAQEBIwFJWUALEREUMxIyERAIBhwrEzMRIzUGKwEiNREzERQWOwEyNjUDByM3Mxcj20VFCywoT0UMEygSEDo0TFVXVUwB5f4bISFPAZb+dRQLEBQB+VR/fwAAAwAdAAABJQJxAAMAFwAbADlANggBAwYBRwcBAQEAVgkIAgAAIkgFAQICJUgABgYDWQQBAwMjA0kYGBgbGBsUMxIyEREREAoGHCsTMxUjFzMRIzUGKwEiNREzERQWOwEyNjUDFSM1zVhYDkVFCywoT0UMEygSEGZYAnFUOP4bISFPAZb+dRQLEBQCElRUAAIACv9zASsCgwADABIAdLUOAQQFAUdLsBhQWEAlBwEBAAUAAQVtAAAAIkgGAQUFJUgABAQjSAADAwJYCAECAicCSRtAIgAAAQBvBwEBBQFvBgEFBSVIAAQEI0gAAwMCWAgBAgInAklZQBgFBAAAEA8NDAsKCAYEEgUSAAMAAxEJBhUrEzczBwMjNTMyPwEjAzMbATMDBoBOUmJTNiErBwwtXUVOSUVhDwIEf3/9bzsfMwHl/ksBtf3dTwAAAgAt/3MBIAKdAA8AHwBAQD0OAQQACQEBBQJHAAMDJEgABAQAWAYBAAAlSAAFBQFYAAEBI0gAAgInAkkCAB4bFhMNDAsKCAUADwIPBwYUKxMzMhURFCsBIicVIxEzFTYTETQmKwEiBhURFBY7ATI2qShPTygsC0VFC14LFCgSEBASKBQLAeVP/rlPIa4DKtkh/nUBMRMMEBT+2RQQCwADAAr/cwErAnEADgASABYAT0BMCgECAwFHCwgKAwYGBVYHAQUFIkgEAQMDJUgAAgIjSAABAQBYCQEAACcASRMTDw8BABMWExYVFA8SDxIREAwLCQgHBgQCAA4BDgwGFCsXIzUzMj8BIwMzGwEzAwYTNTMVITUzFWs2ISsHDC1dRU5JRWEPClj++FiNOx8zAeX+SwG1/d1PAqpUVFRUAAEALQAAAHIB5QADABNAEAABASVIAAAAIwBJERACBhYrMyMRM3JFRQHlAAIAMgAAAewCbgAXACcAPEA5DwEFAwQBAQACRwAGAAcABgdeCAEFBQNYBAEDAyJICQEAAAFYAgEBASMBSSYjNBERERIzMhEQCgYdKyUzFSM1BisBIjURNDsBMhc1MxUjFTMVIwcRNCYrASIGFREUFjsBMjYBQavmEiZNT09MJxLfpIaGTwwTPhMMDBM+Eww7OxMTTwHQTxMTO9I7zAG6FAsLFP5GFAsLAAMALQAAAdUB5QAlADQAPgCUQA4jAQcAMwEJBxYBBAIDR0uwElBYQCoAAwECAgNlAAkAAQMJAV4KDAIHBwBYBgsCAAAlSAgBAgIEWQUBBAQjBEkbQCsAAwECAQMCbQAJAAEDCQFeCgwCBwcAWAYLAgAAJUgIAQICBFkFAQQEIwRJWUAhKCYCADw5NjUwLSY0KDQhHhsYFBEPDgsIBQQAJQIlDQYUKwEzMh0BIxUUFjsBMjY9ATMVFCsBIiYnDgErASI1ETQ7ATIWFz4BByMiBhURFBY7ATI2NxEmFzM1NCYrASIGFQEzU0+1DBMvFAtFT1AUHwQEHxZHT09HFh8EBB9mKBMMDBMoERABA0hwDBMyEwwB5U+7gRQLCxQsN08XDw8XTwFHTxcPDxc7DBP+zxQLDREBMx6UdRMMDBMAAAEARwIEAUgCgwAGADi1BQEBAAFHS7AYUFhADQMCAgEAAXAAAAAiAEkbQAsAAAEAbwMCAgEBZllACwAAAAYABhERBAYWKxM3MxcjJwdHVVdVTDU0AgR/f1RUAAIAaQH6AScCjQAPABsAHEAZAAEAAgECXAAAAANYAAMDJABJMzQ1MwQGGCsTNTQmKwEiBh0BFBY7ATI2NxUUKwEiPQE0OwEy9AoSIBIKChIgEgozQTxBQTxBAkEFEQoKEQURCgocEUFBEUEAAAEASwIWAUUCcAAXACtAKAAEAgYCAAQAXQABAQNYBQEDAyIBSQEAExIPDg0KBwYEAgAXARYHBhQrEyImIyIdASM1NDY7ATIWMjY9ATMVFAYj+Rg9ChE+IScGFzoVCD4fKAIWIxgLDiUnIwoPCg4lJwABABQBBwFeAUEAAwAeQBsAAAEBAFIAAAABVgIBAQABSgAAAAMAAxEDBhUrEzUhFRQBSgEHOjoAAAEAAAEHAd8BQQADAB5AGwAAAQEAUgAAAAFWAgEBAAFKAAAAAwADEQMGFSsRNSEVAd8BBzo6AAEAHgF1AHACbgAEABlAFgIBAAEBRwAAAAFWAAEBIgBJEhACBhYrEyM1NzNwUjcbAXU6vwABAB4BdQBwAm4ABAAZQBYCAQEAAUcAAQEAVgAAACIBSRIQAgYWKxMzFQcjHlI3GwJuOr8AAQAe/3cAcABwAAQAGUAWAgEBAAFHAAAAAVYAAQEnAUkSEAIGFis3MxUHIx5SNxtwOr8AAAIAHgF1APQCbgAEAAkAHkAbBwICAAEBRwIBAAABVgMBAQEiAEkSERIQBAYYKxMjNTczFyM1NzNwUjcbhFI3GwF1Or/5Or8AAAIAHgF1APQCbgAEAAkAHkAbBwICAQABRwMBAQEAVgIBAAAiAUkSERIQBAYYKxMzFQcjJzMVByOiUjcbhFI3GwJuOr/5Or8AAAIAHv93APQAcAAEAAkAHkAbBwICAQABRwIBAAABVgMBAQEnAUkSERIQBAYYKzczFQcjJzMVByOiUjcbhFI3G3A6v/k6vwABAEcAwgFFAcAABwAYQBUAAAEBAFQAAAABWAABAAFMExICBhYrEjQ2MhYUBiJHS2hLS2gBDGpKSmpKAAADADcAAAH7AGsAAwAHAAsAL0AsBAICAAABVggFBwMGBQEBIwFJCAgEBAAACAsICwoJBAcEBwYFAAMAAxEJBhUrITUzFSE1MxUhNTMVAZ9c/vBc/vBca2tra2trAAEAKwBqAPEBogAFACVAIgQBAgEAAUcAAAEBAFIAAAABVgIBAQABSgAAAAUABRIDBhUrNyc3MwcXpHl5TXx8apycnZsAAQArAGoA8QGiAAUAH0AcAwACAAEBRwABAAABUgABAQBWAAABAEoSEQIGFisTByM3JzPxeU18fE0BBpybnQABAAj/eAFUArsAAwATQBAAAAEAbwABAScBSREQAgYWKwEzASMBEET++EQCu/y9AAABABn//wFIAm4ALwCUS7AJUFhANwAAAQIBAGUABwUGBgdlDAECCwEDBAIDXgoBBAkBBQcEBV4AAQENWAANDSJIAAYGCFkACAgjCEkbQDkAAAECAQACbQAHBQYFBwZtDAECCwEDBAIDXgoBBAkBBQcEBV4AAQENWAANDSJIAAYGCFkACAgjCElZQBYuKykoJyYlJCMiMhMzEREREzMQDgYdKwEjNTQmKwEiBh0BMxUjFTMVIxUUFjsBMjY9ATMVFCsBIj0BIzUzNSM1MzU0OwEyFQFIRQsUNxMMX19fXwwTNxQLRU9hTzAwMDBPYU8Btl4UCwsUhjo5O4cTDAwTXmlPT5I7OTqRT08AAAEAGQEZAWMBVQADAB5AGwAAAQEAUgAAAAFWAgEBAAFKAAAAAwADEQMFFSsTNSEVGQFKARk8PAAAAQAI/3gBVAK7AAMAEUAOAAABAG8AAQFmERACBRYrATMBIwEQRP74RAK7/L0AAAAAAQAAANcAgAAKAAAAAAACAEAAUABzAAAAugtwAAAAAAAAAE0ATQBNAE0ATQBNAHMAkADkAXcB5gJhAncCoQLLAwwDNANOA2kDgQOZA9ID8QRDBLoE7QVFBaoFzgYuBpwGxAbuBwUHLwdFB88IRQhzCLsJFQlDCWwJkQnvChcKLQpqCpkKtAriCwcLRQt2C74MAAxxDJMMvQzdDQoNMw1WDYINpA27DeAOAA4bDj8OiA7VDy8Peg/fEBAQZhCeEMQQ9hEmETsRiRG/Ef0SShKVEsETMhNjE5QTtxPkFAsUQBRrFLQUyRUSFVEVURV2FgUWahbQFxQXOBfgGAQYmBjdGRAZQxleGcUZ3hoUGkcalRsLGy8bYxuPG6ob3hv9HDgcZBzmHSEdXx2hHfoeQR6fHuAfdx+sH+kgKyBuIJUgvCDrIRohVyGrIfciRiKaIwQjWCN1I+UkHCRWJJUk1iUJJT8lwiYyJqUnHSeQJ/Aoaik5KdAqZysCK6MsJSxeLJcs2S0KLV0tvy4iLogu9C9fL7Qv4TBRMKQw+zFWMZ0x+zJKMpkyrjMCM6EzzjQENDw0WDRzNI00pzTBNOU1CTUsNUo1eDWaNbk10TZWNnI2iQABAAAAARsjTpU2i18PPPUACwPoAAAAAM/ls+gAAAAA1TIQJv6c/osF8wNcAAAACAACAAAAAAAAAf4AAAAAAAABTQAAAAAAAAAAAAAAggAAALkAMgD+ACMBwAAeAUcAKAI9AB4BogAoAIsAIwD1ADEA9f/jAS4ADwF8ABkAjgAeATMANwCOAB4BgAAIAWkAMgDTABQBIgAZAUIAHgFFAAUBQQAoAWAAMgEAAAUBaAAyAVsAKACOAB4AjgAeAYYAHgGGAB4BhgAeATQAFAKLAFUBVwAPAX0ANwFZADIBfQA3ATsANwEvADcBXgAyAX4ANwDGAEEBLQAPAVkANwEPADcB2wA3AXkANwFoADIBWQA3AWgAMgFsADcBRwAoARYABQFsADQBPwAFAiUACgFOAAUBPwAFAREABQE1AD8BgQAsATUANQG7AC0B2gAAAZAAeQFIACgBSAAtAUUAKAFIACgBSgAtAMUABQFIACgBTQAtAJ8AKwCfAAABRQAtAJ8ALQH2AC0BTQAtAVQAKAFIAC0BSAAoANQALQEpACMAxQAFAU0ALQEXAAUB+wAFAToABQE1AAoBCgAPAQ8ABQCzADcBDwAGAdgANACCAAAAuQAyAUUAKAFjACMBxgBBAVgADwCzADcBgwAyAZAARAKqADEA7AAZAecAKwGwAB4BMwA3AY0AIwGQAFIBZAA6AYYAHgCzAA4AxAAQAZAAdgFTACgB6gA3AMoAHgGQAIgAfgALAOIAGgHnACsBNAAUAVcADwFXAA8BVwAPAVcADwFXAA8BVwAPAhAADgFZADIBOwAqATsANwE7ACwBOwAlAMb/4gDGAEEAxv/kAMb/3QF9AAUBeQA3AWgAKAFoADIBaAAyAWgAMgFoADABXgAZAXIANwFsADQBbAA0AWwANAFsADIBPwAFAVkANwGoAC0BSAAoAUgAKAFIACYBSAAiAUgAHAFIACgB/QAtAUUAKAFKACUBSgAtAUgAKwFIACAAn//OAJ8ALQCf/9AAn//MAVsAKAFNACIBVAAmAVQAKAFUACcBVAAmAVQAJQF8ABkBXgAtAU0AKAFNAC0BSAAhAUgAHQE1AAoBSAAtATUACgCfAC0CBQAyAgYALQGQAEcBkABpAZAASwFyABQB3wAAAI4AHgCOAB4AjgAeARIAHgESAB4BEgAeAYwARwH2ADcBHwArASEAKwGAAAgBcAAZAXwAGQGAAAgAAQAAA77+JQAABnD+nPrFBfMAAQAAAAAAAAAAAAAAAAAAANcABAFaASwABQAAAooCWAAAAEsCigJYAAABXgAyASMAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASVRGTwDAAAAiFQO+/iUAAAO+AdsgAACTAAAAAAHlAm4AAAAgAAcAAAACAAAAAwAAABQAAwABAAAAFAAEALgAAAAqACAABAAKAAAADQB+ALsA/wExAVMCxgLaAtwgFCAaIB4gIiAmIDogRCCsIhIiFf//AAAAAAANACAAoAC/ATEBUgLGAtoC3CATIBggHCAiICYgOSBEIKwiEiIV//8AA//3/+X/xP/B/5D/cP3+/ev96uC04LHgsOCt4KrgmOCP4Cjew97BAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwACwgsABVWEVZICBLuAANUUuwBlNaWLA0G7AoWWBmIIpVWLACJWG5CAAIAGNjI2IbISGwAFmwAEMjRLIAAQBDYEItsAEssCBgZi2wAiwgZCCwwFCwBCZasigBCkNFY0VSW1ghIyEbilggsFBQWCGwQFkbILA4UFghsDhZWSCxAQpDRWNFYWSwKFBYIbEBCkNFY0UgsDBQWCGwMFkbILDAUFggZiCKimEgsApQWGAbILAgUFghsApgGyCwNlBYIbA2YBtgWVlZG7ABK1lZI7AAUFhlWVktsAMsIEUgsAQlYWQgsAVDUFiwBSNCsAYjQhshIVmwAWAtsAQsIyEjISBksQViQiCwBiNCsQEKQ0VjsQEKQ7ACYEVjsAMqISCwBkMgiiCKsAErsTAFJbAEJlFYYFAbYVJZWCNZISCwQFNYsAErGyGwQFkjsABQWGVZLbAFLLAHQyuyAAIAQ2BCLbAGLLAHI0IjILAAI0JhsAJiZrABY7ABYLAFKi2wBywgIEUgsAtDY7gEAGIgsABQWLBAYFlmsAFjYESwAWAtsAgssgcLAENFQiohsgABAENgQi2wCSywAEMjRLIAAQBDYEItsAosICBFILABKyOwAEOwBCVgIEWKI2EgZCCwIFBYIbAAG7AwUFiwIBuwQFlZI7AAUFhlWbADJSNhRESwAWAtsAssICBFILABKyOwAEOwBCVgIEWKI2EgZLAkUFiwABuwQFkjsABQWGVZsAMlI2FERLABYC2wDCwgsAAjQrILCgNFWCEbIyFZKiEtsA0ssQICRbBkYUQtsA4ssAFgICCwDENKsABQWCCwDCNCWbANQ0qwAFJYILANI0JZLbAPLCCwEGJmsAFjILgEAGOKI2GwDkNgIIpgILAOI0IjLbAQLEtUWLEEZERZJLANZSN4LbARLEtRWEtTWLEEZERZGyFZJLATZSN4LbASLLEAD0NVWLEPD0OwAWFCsA8rWbAAQ7ACJUKxDAIlQrENAiVCsAEWIyCwAyVQWLEBAENgsAQlQoqKIIojYbAOKiEjsAFhIIojYbAOKiEbsQEAQ2CwAiVCsAIlYbAOKiFZsAxDR7ANQ0dgsAJiILAAUFiwQGBZZrABYyCwC0NjuAQAYiCwAFBYsEBgWWawAWNgsQAAEyNEsAFDsAA+sgEBAUNgQi2wEywAsQACRVRYsA8jQiBFsAsjQrAKI7ACYEIgYLABYbUQEAEADgBCQopgsRIGK7ByKxsiWS2wFCyxABMrLbAVLLEBEystsBYssQITKy2wFyyxAxMrLbAYLLEEEystsBkssQUTKy2wGiyxBhMrLbAbLLEHEystsBwssQgTKy2wHSyxCRMrLbAeLACwDSuxAAJFVFiwDyNCIEWwCyNCsAojsAJgQiBgsAFhtRAQAQAOAEJCimCxEgYrsHIrGyJZLbAfLLEAHistsCAssQEeKy2wISyxAh4rLbAiLLEDHistsCMssQQeKy2wJCyxBR4rLbAlLLEGHistsCYssQceKy2wJyyxCB4rLbAoLLEJHistsCksIDywAWAtsCosIGCwEGAgQyOwAWBDsAIlYbABYLApKiEtsCsssCorsCoqLbAsLCAgRyAgsAtDY7gEAGIgsABQWLBAYFlmsAFjYCNhOCMgilVYIEcgILALQ2O4BABiILAAUFiwQGBZZrABY2AjYTgbIVktsC0sALEAAkVUWLABFrAsKrABFTAbIlktsC4sALANK7EAAkVUWLABFrAsKrABFTAbIlktsC8sIDWwAWAtsDAsALABRWO4BABiILAAUFiwQGBZZrABY7ABK7ALQ2O4BABiILAAUFiwQGBZZrABY7ABK7AAFrQAAAAAAEQ+IzixLwEVKi2wMSwgPCBHILALQ2O4BABiILAAUFiwQGBZZrABY2CwAENhOC2wMiwuFzwtsDMsIDwgRyCwC0NjuAQAYiCwAFBYsEBgWWawAWNgsABDYbABQ2M4LbA0LLECABYlIC4gR7AAI0KwAiVJiopHI0cjYSBYYhshWbABI0KyMwEBFRQqLbA1LLAAFrAEJbAEJUcjRyNhsAlDK2WKLiMgIDyKOC2wNiywABawBCWwBCUgLkcjRyNhILAEI0KwCUMrILBgUFggsEBRWLMCIAMgG7MCJgMaWUJCIyCwCEMgiiNHI0cjYSNGYLAEQ7ACYiCwAFBYsEBgWWawAWNgILABKyCKimEgsAJDYGQjsANDYWRQWLACQ2EbsANDYFmwAyWwAmIgsABQWLBAYFlmsAFjYSMgILAEJiNGYTgbI7AIQ0awAiWwCENHI0cjYWAgsARDsAJiILAAUFiwQGBZZrABY2AjILABKyOwBENgsAErsAUlYbAFJbACYiCwAFBYsEBgWWawAWOwBCZhILAEJWBkI7ADJWBkUFghGyMhWSMgILAEJiNGYThZLbA3LLAAFiAgILAFJiAuRyNHI2EjPDgtsDgssAAWILAII0IgICBGI0ewASsjYTgtsDkssAAWsAMlsAIlRyNHI2GwAFRYLiA8IyEbsAIlsAIlRyNHI2EgsAUlsAQlRyNHI2GwBiWwBSVJsAIlYbkIAAgAY2MjIFhiGyFZY7gEAGIgsABQWLBAYFlmsAFjYCMuIyAgPIo4IyFZLbA6LLAAFiCwCEMgLkcjRyNhIGCwIGBmsAJiILAAUFiwQGBZZrABYyMgIDyKOC2wOywjIC5GsAIlRlJYIDxZLrErARQrLbA8LCMgLkawAiVGUFggPFkusSsBFCstsD0sIyAuRrACJUZSWCA8WSMgLkawAiVGUFggPFkusSsBFCstsD4ssDUrIyAuRrACJUZSWCA8WS6xKwEUKy2wPyywNiuKICA8sAQjQoo4IyAuRrACJUZSWCA8WS6xKwEUK7AEQy6wKystsEAssAAWsAQlsAQmIC5HI0cjYbAJQysjIDwgLiM4sSsBFCstsEEssQgEJUKwABawBCWwBCUgLkcjRyNhILAEI0KwCUMrILBgUFggsEBRWLMCIAMgG7MCJgMaWUJCIyBHsARDsAJiILAAUFiwQGBZZrABY2AgsAErIIqKYSCwAkNgZCOwA0NhZFBYsAJDYRuwA0NgWbADJbACYiCwAFBYsEBgWWawAWNhsAIlRmE4IyA8IzgbISAgRiNHsAErI2E4IVmxKwEUKy2wQiywNSsusSsBFCstsEMssDYrISMgIDywBCNCIzixKwEUK7AEQy6wKystsEQssAAVIEewACNCsgABARUUEy6wMSotsEUssAAVIEewACNCsgABARUUEy6wMSotsEYssQABFBOwMiotsEcssDQqLbBILLAAFkUjIC4gRoojYTixKwEUKy2wSSywCCNCsEgrLbBKLLIAAEErLbBLLLIAAUErLbBMLLIBAEErLbBNLLIBAUErLbBOLLIAAEIrLbBPLLIAAUIrLbBQLLIBAEIrLbBRLLIBAUIrLbBSLLIAAD4rLbBTLLIAAT4rLbBULLIBAD4rLbBVLLIBAT4rLbBWLLIAAEArLbBXLLIAAUArLbBYLLIBAEArLbBZLLIBAUArLbBaLLIAAEMrLbBbLLIAAUMrLbBcLLIBAEMrLbBdLLIBAUMrLbBeLLIAAD8rLbBfLLIAAT8rLbBgLLIBAD8rLbBhLLIBAT8rLbBiLLA3Ky6xKwEUKy2wYyywNyuwOystsGQssDcrsDwrLbBlLLAAFrA3K7A9Ky2wZiywOCsusSsBFCstsGcssDgrsDsrLbBoLLA4K7A8Ky2waSywOCuwPSstsGossDkrLrErARQrLbBrLLA5K7A7Ky2wbCywOSuwPCstsG0ssDkrsD0rLbBuLLA6Ky6xKwEUKy2wbyywOiuwOystsHAssDorsDwrLbBxLLA6K7A9Ky2wciyzCQQCA0VYIRsjIVlCK7AIZbADJFB4sAEVMC0AS7gAMlJYsQEBjlmwAbkIAAgAY3CxAAVCsy0ZAgAqsQAFQrUgCA4HAggqsQAFQrUqBhcFAggqsQAHQrsIQAPAAAIACSqxAAlCuwBAAEAAAgAJKrEDAESxJAGIUViwQIhYsQNkRLEmAYhRWLoIgAABBECIY1RYsQMARFlZWVm1IggQBwIMKrgB/4WwBI2xAgBEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABFAEUBmAA7ADsDLQJuAm4AAP8pA+j+DAMtAnMCbgAA/ykD6P4MAEQARAA7ADsCbgAAAp0B5QAA/3MD6P4MAm4AAAKdAeUAAP9zA+j+DAAAAAgAZgADAAEECQAAAHAAAAADAAEECQABABQAcAADAAEECQACAA4AhAADAAEECQADACoAkgADAAEECQAEABQAcAADAAEECQAFAPAAvAADAAEECQAGABQBrAADAAEECQAOADQBwABDAG8AcAB5AHIAaQBnAGgAdAAgADIAMAAxADQAIABJAG4AZABpAGEAbgAgAFQAeQBwAGUAIABGAG8AdQBuAGQAcgB5AC4AIABBAGwAbAAgAHIAaQBnAGgAdABzACAAcgBlAHMAZQByAHYAZQBkAC4AVABlAGsAbwAgAEwAaQBnAGgAdABSAGUAZwB1AGwAYQByADEALgAxADAANgA7AEkAVABGAE8AOwBUAGUAawBvAC0ATABpAGcAaAB0AFYAZQByAHMAaQBvAG4AIAAxAC4AMQAwADYAOwBQAFMAIAAxAC4AMAA7AGgAbwB0AGMAbwBuAHYAIAAxAC4AMAAuADcAOAA7AG0AYQBrAGUAbwB0AGYALgBsAGkAYgAyAC4ANQAuADYAMQA5ADMAMAA7ACAAdAB0AGYAYQB1AHQAbwBoAGkAbgB0ACAAKAB2ADEALgAxACkAIAAtAGwAIAA3ACAALQByACAAMgA4ACAALQBHACAANQAwACAALQB4ACAAMQAzACAALQBEACAAbABhAHQAbgAgAC0AZgAgAGQAZQB2AGEAIAAtAHcAIABHAFQAZQBrAG8ALQBMAGkAZwBoAHQAaAB0AHQAcAA6AC8ALwBzAGMAcgBpAHAAdABzAC4AcwBpAGwALgBvAHIAZwAvAE8ARgBMAAAAAgAAAAAAAP+1ADIAAAAAAAAAAAAAAAAAAAAAAAAAAADXAAAAAQACAQIBAwADAAQABQAGAAcACAAJAAoACwAMAA0ADgAPABAAEQASABMAFAAVABYAFwAYABkAGgAbABwAHQAeAB8AIAAhACIAIwAkACUAJgAnACgAKQAqACsALAAtAC4ALwAwADEAMgAzADQANQA2ADcAOAA5ADoAOwA8AD0APgA/AEAAQQBCAEMARABFAEYARwBIAEkASgBLAEwATQBOAE8AUABRAFIAUwBUAFUAVgBXAFgAWQBaAFsAXABdAF4AXwBgAGEBBACjAIQAhQC9AJYA6ACGAI4AiwCdAKkApAEFAIoA2gCDAJMA8gDzAI0BBgCIAMMA3gDxAJ4AqgCiAK0AyQDHAK4AYgBjAJAAZADLAGUAyADKAM8AzADNAM4A6QBmANMA0ADRAK8AZwDwAJEA1gDUANUAaADrAO0AiQBqAGkAawBtAGwAbgCgAG8AcQBwAHIAcwB1AHQAdgB3AOoAeAB6AHkAewB9AHwAuAChAH8AfgCAAIEA7ADuALoA1wCwALEA2ADdANkAsgCzALYAtwDEALQAtQDFAIcAqwC+AL8AvAEHAO8BCAROVUxMAkNSB3VuaTAwQTAHdW5pMDBBRAd1bmkwMEI1BEV1cm8HdW5pMjIxNQAAAAABAAH//wAPAAEAAAAMAAAAAAAAAAIAAQADANYAAQAAAAEAAAAKAAwADgAAAAAAAAABAAAACgAWABgAAWRldmEACAAAAAAAAAAAAAA=') format('truetype');}", "@font-face{font-family:'Teko';font-style:normal;font-weight:400;src:local('Teko:Regular'),local('Teko-Regular'), url('data:application/x-font-ttf;charset=utf-8;base64,AAEAAAARAQAABAAQR0RFRgATANcAAIbwAAAAFkdQT1MAGQAMAACHCAAAABBHU1VCZJB2eAAAhxgAAAAaT1MvMloflr8AAHTkAAAAYGNtYXC0KK0yAAB1RAAAAMxjdnQgHFoK7AAAghQAAABkZnBnbYySkFgAAHYQAAALcGdhc3AAAAAQAACG6AAAAAhnbHlmvLIqFgAAARwAAG4+aGVhZAkzIegAAHEsAAAANmhoZWEIvP+pAAB0wAAAACRobXR4J8IbcAAAcWQAAANcbG9jYcbU4+UAAG98AAABsG1heHACTwxGAABvXAAAACBuYW1lMepK4gAAgngAAAJucG9zdDoD9ioAAIToAAAB/XByZXCiw2ouAACBgAAAAJEAAwAAAAAB9gK9AA0AEQAVAG9ACQsHBAAEAAEBR0uwHFBYQCEABggBBQEGBV4DAQAAAVYCAQEBEUgABAQHVgkBBwcTB0kbQB8ABggBBQEGBV4CAQEDAQAEAQBeAAQEB1YJAQcHEwdJWUAWEhIODhIVEhUUEw4RDhESEhMSEgoFGSsTIwcjEwMzFzM3MwMXIwERIREBESER+wRkPIaGPGkEZDyHhzz+zwGQ/j0B9gEu0QEEAP/Ozv77/gIv/aUCW/10Ar39QwAAAgAyAAAAnwJyAAMABwAnQCQAAwMCVgACAiJIAAAAAVYEAQEBIwFJAAAHBgUEAAMAAxEFBhUrMzUzFQMzAyM1ZWhtEUt7ewJy/loAAAIAJAGTAQACcgADAAcAF0AUAwEBAQBWAgEAACIBSRERERAEBhgrEzMVIyczFSOtU1OJU1MCct/f3wACAB4AAAHSAnIAGwAfAHpLsBZQWEAoEA8HAwEGBAICAwECXgwBCgoiSA4IAgAACVYNCwIJCSVIBQEDAyMDSRtAJg0LAgkOCAIAAQkAXxAPBwMBBgQCAgMBAl4MAQoKIkgFAQMDIwNJWUAeHBwcHxwfHh0bGhkYFxYVFBMSEREREREREREQEQYdKwEjBzMVIwcjNyMHIzcjNTM3IzUzNzMHMzczBzMHNyMHAdJCHj9LHVAdcR1RHTZCHj5KHVAdch1QHTawHnIdAYymRaGhoaFFpkWhoaGh66amAAEAJv+zAUQCvwA1AFFATgAIAAsKCAtgDAEAAAYEAAZhAAUAAgUCWgAKCgdYCQEHByJIAAQEAVgDAQEBIwFJAgAwLSopJyUkIyEgHRoVEg8ODAoJCAYFADUCNQ0GFCsTMzIdARQrARUjNSMiPQEzFRQWOwEyNj0BNCYrASI9ATQ7ATUzFTMyHQEjNTQmKwEiBh0BFBafSVxeBlYFXVsMEioSCwsSSltdB1YFXVsMEioSDAwBZFi0WE1NWFxOEgsLEpkSC1mlWE1NWFxOEgsLEosSCwAABQAdAAACZAJyAAMAEwAjAC8APQBkS7AWUFhAIQcBBQgBAgMFAmEABAQAWAkBAAAiSAADAwFYBgEBASMBSRtAJwAFAAgCBQhgAAcAAgMHAmEABAQAWAkBAAAiSAADAwFYBgEBASMBSVlADjw5MzM0NTU1NBEQCgYdKwEzASMlNTQmKwEiBh0BFBY7ATI2ATU0JisBIgYdARQWOwEyNgUVFCsBIj0BNDsBMiUVFCsBIj0BNDY7ATIWAa1L/txKAZAJERURCgoRFREJ/pcKEBYRCQkRFhAKAbNNQ05OQ03+l05CTiUpQiklAnL9jliiEQoKEaIRCgoBMqERCgoRoRIKC2O4TU24TdO4TU24JyYmAAADACb/3QICAnIAIQAwADoAUEBNMyUXCAQCBBsYAgUCIAEABQNHAAIEBQQCBW0AAwADcAAEBAFYAAEBIkgHAQUFAFkGAQAAIwBJMjECADE6MjovLB8eGhkRDgAhAiEIBhQrISMiPQE0Nj8BLgI9ATQ7ATIdARQGDwEXNTMVFAcXIycGAxUUFzc+AT0BNCYrASIGEzMnBw4BHQEUFgFT0F0aIx4YFhZeYVwkKBdzSQJTYCMR1yoGIBkMEi4SCwWtlRgRDQxYaCEpFhYeHjIWYFhYXTM7Gg+NTY4IDmUqBwIPWx8xBRUiHVISCwv+JrMQCxcXTRILAAABACQBkwB9AnIAAwATQBAAAQEAVgAAACIBSREQAgYWKxMzFSMkWVkCct8AAAEAL/9RARsDGgARACxAKQUBAQAMAQMCAkcAAAABAgABYAACAwMCVAACAgNYAAMCA0wiExIiBAYYKxM0NjsBFQcGERUQHwEVIyImNS9vbg8NhYUND25vAXfL2EwBCP6zhv6zCAFL18wAAf/r/1EA1wMaABEALEApDAECAwUBAAECRwADAAIBAwJgAAEAAAFUAAEBAFgAAAEATCITEiIEBhgrNxQGKwE1NzYRNRAvATUzMhYV129uDw2FhQ0Pbm/0zNdLAQgBTYYBTQgBTNjLAAABABABXwExAnIAIAAdQBofHRwaFxYTEA8MCQgFDQBEAAAAIgBJEQEGFSsTNTMVFAc2PwEXBwYHFh8BBycmJwYPASc3NjcmJzcWFyZ7SwcYISEYIxkjFBYVPRUVCRAPFT4WFRUbRBdEFwcCTiQkIRwPCgtICgkEEB4dLB4dGCEWHCwdHRECFkcWDxwAAAEAFwB9AYEB9QALACdAJAYFAgMCAQABAwBeAAEBBFYABAQlAUkAAAALAAsREREREQcGGSsBFSMVIzUjNTM1MxUBgYhaiIhaAV5Kl5dKl5cAAQAg/3QAhQB7AAQAGUAWAgEBAAFHAAAAAVYAAQEnAUkSEAIGFis3MxUHIyBlRSB7QcYAAAEAMAEDAQoBSQADAB5AGwAAAQEAUgAAAAFWAgEBAAFKAAAAAwADEQMGFSsTNTMVMNoBA0ZGAAEAIAAAAIUAewADABlAFgAAAAFWAgEBASMBSQAAAAMAAxEDBhUrMzUzFSBle3sAAAEAAv+IAWcCvAADACZLsBxQWEALAAABAG8AAQEnAUkbQAkAAAEAbwABAWZZtBEQAgYWKwEzASMBE1T+71QCvPzMAAIALwAAAVcCcgAPABsAH0AcAAAAA1gAAwMiSAABAQJYAAICIwJJMzQ1MwQGGCs3ETQmKwEiBhURFBY7ATI2ExEUKwEiNRE0OwEy/AwSNhILCxI2EgxbXW1eXm1dZgGmEgsLEv5aEgsLAcb+PlhYAcJYAAEAFAAAALcCcgAFAB9AHAABAQJWAwECAiJIAAAAIwBJAAAABQAFEREEBhYrExEjESM1t1tIAnL9jgIpSQAAAQAZAAABLgJyABoAVbUQAQMCAUdLsAlQWEAcAAAEAgQAZQAEBAFYAAEBIkgAAgIDVgADAyMDSRtAHQAABAIEAAJtAAQEAVgAAQEiSAACAgNWAAMDIwNJWbc3ERUyEQUGGSsTFSM1NDsBMh0BFAcDMxUhNRM2PQE0JisBIgZzWl1WXiOHrv7smRwLEyASDAIMZnRYWD9GQ/73SUYBIDVDLhILCwABAB0AAAE0AnIALgB4tQsBBQYBR0uwCVBYQCsAAAcGBwBlAAMFBAQDZQAGAAUDBgVgAAcHAVgAAQEiSAAEBAJZAAICIwJJG0AtAAAHBgcABm0AAwUEBQMEbQAGAAUDBgVgAAcHAVgAAQEiSAAEBAJZAAICIwJJWUALNSElMxI4MhEIBhwrExUjNTQ7ATIdARQHFh0BFCsBIj0BMxUUFjsBMjY9ATQmKwE1MzI2PQE0JisBIgZ3Wl1cXigoXlxdWgwSJxIMDBJLSxIMDBInEgwCDF5sWFieNQkJMaxYWGtdEgsLEpYSC0gLEo4SCwsAAQAGAAABawJyAA4AM0AwBwEEBQFHBwYCBAIBAAEEAF8AAwMiSAAFBQFWAAEBIwFJAAAADgAOERESERERCAYaKyUVIxUjNSM1EzMDMzUzFQFrOFfWtFq4gFfGSnx8SwGr/lR4eAAAAQAoAAABQwJyABwAYUuwCVBYQCQAAgQDAwJlAAAABAIABGAABgYFVgAFBSJIAAMDAVkAAQEjAUkbQCUAAgQDBAIDbQAAAAQCAARgAAYGBVYABQUiSAADAwFZAAEBIwFJWUAKERElMxIzIAcGGysTMzIdARQrASI9ATMVFBY7ATI2PQE0JisBESEVI4RhXl5gXVsMEikSDAwSoAEXvQGHWNdYWG9hEgsLErsSDAEzSQACAC8AAAFUAnIAGQAmAGFLsAlQWEAkAAMEAAQDZQAAAAUGAAVgAAQEAlgAAgIiSAAGBgFYAAEBIwFJG0AlAAMEAAQDAG0AAAAFBgAFYAAEBAJYAAICIkgABgYBWAABASMBSVlACjMmMxIzMyAHBhsrEzMyHQEUKwEiNRE0OwEyHQEjNTQmKwEiBhUTNTQmKwEVFBY7ATI2iW5dXWpeXmddWQwSMxIMcQwSUwwSNRIMAWhZt1hYAcJYWGxeEgsLEv5anREMuhILCwAAAQAGAAABHgJyAAYAJUAiAQEBAgFHAAEBAlYDAQICIkgAAAAjAEkAAAAGAAYREgQGFisBFQMjEyM1AR6oXKO3AnI4/cYCJ0sAAwAvAAABVgJyABUAJQA1ADBALQ4DAgIFAUcABQACAwUCYAAEBAFYAAEBIkgAAwMAWAAAACMASTU1NTQ4NwYGGisBFRQHFh0BFCsBIj0BNDcmPQE0OwEyAzU0JisBIgYdARQWOwEyNhE1NCYrASIGHQEUFjsBMjYBVicnXW1dJyddbV1ZCxI7EgsLEjsSCwsSOxILCxI7EgsCGqE1CQswqFhYqDALCTWhWP30lhEMDBGWEgwMASeSEQwMEZIRDAwAAgAmAAABSwJyABkAJgB0S7AJUFhAJgACBAMDAmUIAQUABAIFBGAABgYAWAcBAAAiSAADAwFZAAEBIwFJG0AnAAIEAwQCA20IAQUABAIFBGAABgYAWAcBAAAiSAADAwFZAAEBIwFJWUAZGxoCACEeGiYbJhYUEQ4LCggFABkCGQkGFCsTMzIVERQrASI9ATMVFBY7ATI2PQEjIj0BNBMzNTQmKwEiBh0BFBaDal5eZ15aCxMzEgttXXhSCxI1EgwMAnJY/j5YWGtdEgsLEppZwVj+1cUSCwsSqBEMAAIAIABGAIUBogADAAcAL0AsAAAEAQECAAFeAAIDAwJSAAICA1YFAQMCA0oEBAAABAcEBwYFAAMAAxEGBhUrEzUzFQc1MxUgZWVlASd7e+F7ewACACD/dACFAegAAwAIAC1AKgYBAwIBRwQBAQEAVgAAACVIAAICA1YAAwMnA0kAAAgHBQQAAwADEQUGFSsTNTMVBzMVByMgZWVlRSABbXt78kHGAAABAB0AkgGHAeAABgAGswYDAS0rAQ0BFSU1JQGH/usBFf6WAWoBkFZXUXpceAAAAgAdALkBhwG5AAMABwAvQCwAAgUBAwACA14AAAEBAFIAAAABVgQBAQABSgQEAAAEBwQHBgUAAwADEQYGFSs3NSEVJTUhFR0Bav6WAWq5TEy0TEwAAQAdAJIBhwHgAAYABrMFAgEtKwElNQUVBTUBMv7rAWr+lgE6VlB4XHpRAAACABQAAAEuAnIAGgAeALxLsAtQWEAtAAUEAwQFZQABAwICAWUAAwACBgMCXgAEBABYCAEAACJIAAYGB1YJAQcHIwdJG0uwFFBYQC4ABQQDBAUDbQABAwICAWUAAwACBgMCXgAEBABYCAEAACJIAAYGB1YJAQcHIwdJG0AvAAUEAwQFA20AAQMCAwECbQADAAIGAwJeAAQEAFgIAQAAIkgABgYHVgkBBwcjB0lZWUAbGxsCABseGx4dHBgXFBEMCgkIBwUAGgIaCgYUKxMzMh0BFCsBByMnMzI2PQE0JisBIgYdASM1NBM1MxVxYF1dDQRMCEsRCwsRLBELW1NlAnJXtldCgw0RoRENDRFIVVf9jnt7AAACAEz/ugKKAj8ADgA8AEhARRQBAQI3AQkAAkcACAAFAggFYAMBAgABAAIBYAQBAAoBCQYACWEABgcHBlQABgYHWAAHBgdMOzg2NDMhJTUhEjM0MwsGHSsBFRQWOwEyPQE0JisBIgYHNDsBMhc1MxEzMjY1ETQmIyEiBhURFBYzIRUhIjURNDMhMhURFCsBNQYrASI1AT4LEh0gDxEdEgtVWBswDVIsEwsLE/6oEgsLEgEt/rpaWgGKWluRBzsbWAFMmhEMI44TDwsGVicn/ugLEgEnEgwMEv5GEQxIWAHVWFj+wlgzM1YAAgAMAAABdQJyAAcACgAsQCkKAQQCAUcABAAAAQQAXwACAiJIBQMCAQEjAUkAAAkIAAcABxEREQYGFyshJyMHIxMzEyczAwEaGYQXWm+Dd+1tOZGRAnL9jtoBSgADADQAAAFrAnIACQATACAAL0AsGgECAQFHAAEAAgMBAmAAAAAEWAAEBCJIAAMDBVgABQUjBUkoIiElISMGBhorATU0JisBFTMyNhE1NCYrARUzMjYDMzIdARQHFh0BFCsBARAMEmRkEgwMEmRkEgzc2V4oKF7ZAX+PEgvJC/73mhIL1AsCIFifNQkKMKtYAAABAC///wFUAnIAHwBdS7AJUFhAIgAAAQMBAGUAAwICA2MAAQEFWAAFBSJIAAICBFkABAQjBEkbQCQAAAEDAQADbQADAgEDAmsAAQEFWAAFBSJIAAICBFkABAQjBElZQAkzMhM1MxAGBhorASM1NCYrASIGFREUFjsBMjY9ATMVFCsBIjURNDsBMhUBVFwLEjMSDAwSMxILXF5qXV1qXgGuYBILCxL+VRILCxJfallZAcJYWAACADYAAAFtAnIABwARAB9AHAACAgBYAAAAIkgAAwMBWAABASMBSSEkIyAEBhgrEzMyFREUKwE3ETQmKwERMzI2NtpdXdrcDBJjYxIMAnJY/j5YZAGqEgv+HAsAAAEANAAAAUQCcgALAClAJgAEAAUABAVeAAMDAlYAAgIiSAAAAAFWAAEBIwFJEREREREQBgYaKzczFSERIRUjFTMVI4+1/vABCa6QkElJAnJJwUkAAAEANAAAAUUCcgAJACNAIAABAAIDAQJeAAAABFYABAQiSAADAyMDSREREREQBQYZKwEjFTMVIxEjESEBRbaQkFsBEQIpzkf+7AJyAAABAC///wFUAnIAIQBhS7ALUFhAJAAAAQQBAGUABAADAgQDXgABAQZYAAYGIkgAAgIFWAAFBSMFSRtAJQAAAQQBAARtAAQAAwIEA14AAQEGWAAGBiJIAAICBVgABQUjBUlZQAozMhETNTMQBwYbKwEjNTQmKwEiBhURFBY7ATI2PQEjNTMVFCsBIjURNDsBMhUBVFsMEjMSDAwSMxIMMo1da11da10Bu1MSCwsS/lUSCwsSmUbqWVkBwlhYAAEANAAAAW0CcgALACFAHgAFAAIBBQJeBAEAACJIAwEBASMBSREREREREAYGGisBMxEjESMRIxEzETMBE1pahVpahQJy/Y4BE/7tAnL+6gAAAQA8AAAAlgJyAAMAE0AQAAAAIkgAAQEjAUkREAIGFisTMxEjPFpaAnL9jgABAA4AAAEdAnIAEQBDS7AJUFhAFwAAAgEBAGUAAgIiSAABAQNZAAMDIwNJG0AYAAACAQIAAW0AAgIiSAABAQNZAAMDIwNJWbYyEzMQBAYYKzczFRQWOwEyNjURMxEUKwEiNQ5bDBIdEgxbXVVdzmgSCwsSAgz95lhYAAIANAAAAXYCcgAFAAkAK0AoBAECAAEBRwIEAgEBIkgFAwIAACMASQYGAAAGCQYJCAcABQAFEgYGFSsBAxMjAxMDETMRAXaEg2J/geFaAnL+xv7IATgBOv2OAnL9jgABADQAAAEpAnIABQAZQBYAAgIiSAAAAAFXAAEBIwFJEREQAwYXKzczFSMRM46b9VpJSQJyAAABADQAAAHQAnIADAAoQCUMCQQDBAEBRwAEAQABBABtAgEBASJIAwEAACMASRIREhEQBQYZKzMjETMbATMRIxEDIwODT2Nra2NPVlJWAnL+fAGE/Y4Byv6+AUEAAAEANAAAAWYCcgAJAB5AGwkEAgEAAUcDAQAAIkgCAQEBIwFJERIREAQGGCsBMxEjAxEjETMTARNTW4RTX4ACcv2OAcT+PAJy/kkAAgAvAAABWQJyAAsAGwAoQCUAAgIAWAQBAAAiSAADAwFYAAEBIwFJAgAaFxIPCAUACwILBQYUKxMzMhURFCsBIjURNBMRNCYrASIGFREUFjsBMjaNb11db17QDBI6EgsLEjoSDAJyWP4+WFgBwlj99AGmEgsLEv5aEgsLAAIANAAAAV4CcgAJABMAI0AgAAQAAQIEAWAAAwMAWAAAACJIAAICIwJJISQRIyAFBhkrEzMyHQEUKwEVIxM1NCYrARUzMjY0zV1dc1rQDBJYWBIMAnJY0FjyAVi0EgvvDAAAAgAv/6UBaAJyAA4AHgA1QDILAQADAUcAAgACcAAEBAFYAAEBIkgAAwMAWAUBAAAjAEkBAB0aFRINDAcEAA4BDgYGFCszIjURNDsBMhURFAcXIycDERQWOwEyNjURNCYrASIGjV5eb104R2BCPAsSOhIMDBI6EgtYAcJYWP4+RBBfWwIM/loSCwsSAaYSCwsAAgA0AAABfgJyAAwAFgA4QDUJAQMEAUcABAYBAwAEA14HAQUFAVgAAQEiSAIBAAAjAEkNDQAADRYNFRAOAAwADBUhEQgGFys3FSMRMzIdARQHFyMnAxUzMjY9ATQmI45azV1DY2FeMVgSDAwS/f0CcljETAv//QEs5AsSqhILAAABACYAAAFEAnIALQBxS7ALUFhAKwACAwQDAmUABgAHBwZlAAQAAAYEAGAAAwMBWAABASJIAAcHBVkABQUjBUkbQC0AAgMEAwIEbQAGAAcABgdtAAQAAAYEAGAAAwMBWAABASJIAAcHBVkABQUjBUlZQAszEjM1MxIzMwgGHCs3NTQmKwEiPQE0OwEyHQEjNTQmKwEiBh0BFBY7ATIdARQrASI9ATMVFBY7ATI26AsSSltdYl1bDBIqEgwMEklcXmFdWwwSKhILZpkSC1mlWFhcThILCxKLEgtYtFhYXE4SCwsAAQAFAAABMQJyAAcAIUAeAgEAAANWBAEDAyJIAAEBIwFJAAAABwAHERERBQYXKwEVIxEjESM1ATFpWmkCckn91wIpSQABADEAAAFcAnIAEQAbQBgCAQAAIkgAAwMBWQABASMBSTMSMhAEBhgrATMRFCsBIjURMxEUFjsBMjY1AQFbXm9eWwwSORMLAnL95lhYAhr99BILCxIAAAEABQAAAWcCcgAGABtAGAYBAQABRwIBAAAiSAABASMBSREREAMGFysBMwMjAzMTAQpdcYBxXVMCcv2OAnL94QABAAoAAAJXAnIADAAhQB4MCQQDAQABRwQDAgAAIkgCAQEBIwFJEhESERAFBhkrATMDIwsBIwMzGwEzEwH8W2R9RkV9ZF1GUWRRAnL9jgHW/ioCcv3gAiD94AABAAQAAAFxAnIACwAgQB0JBgMABAACAUcDAQICIkgBAQAAIwBJEhISEQQGGCsbASMnByMTAzMXNzPrhmRUVGGEe2RISWIBQP7A7u4BQAEy4eEAAQAEAAABZQJyAAgAHUAaCAUCAwABAUcCAQEBIkgAAAAjAEkSEhADBhcrMyM1AzMbATMD5VuGXFdSXIDGAaz+pwFZ/lQAAAEACAAAATECcwAJAC1AKgEBAgYBAAJGAAICA1YEAQMDIkgAAAABVgABASMBSQAAAAkACRIREgUGFysBFQMzFSE1EyM1ATHNyP7czcICc0n+H0lJAeFJAAABADr/WwD5Aw8ABwAiQB8AAwAAAQMAXgABAgIBUgABAQJWAAIBAkoREREQBAYYKxMjETMVIxEz+WRkv78CxvzeSQO0AAEAH/+IAYQCvAADACZLsBxQWEALAAABAG8AAQEnAUkbQAkAAAEAbwABAWZZtBEQAgYWKxMzASMfVAERVAK8/MwAAAEALP9bAOoDDwAHAChAJQQBAwACAQMCXgABAAABUgABAQBWAAABAEoAAAAHAAcREREFBhcrExEjNTMRIzXqvmNjAw/8TEkDIkkAAQA3AT0BsgKfAAYAG0AYBAEBAAFHAgEBAAFwAAAAJABJEhEQAwYXKxMzEyMLASO7coVeYF9eAp/+ngEO/vIAAAEAAP+OAdr/1QADAE9LsAtQWEARAAABAQBSAAAAAVYCAQEAAUobS7AWUFhADAAAAAFWAgEBAScBSRtAEQAAAQEAUgAAAAFWAgEBAAFKWVlACgAAAAMAAxEDBhUrFTUhFQHackdHAAEAdAIHASoCiwADAC5LsBxQWEAMAAABAHACAQEBJAFJG0AKAgEBAAFvAAAAZllACgAAAAMAAxEDBhUrExcjJ9hSTWkCi4SEAAIAJgAAATwB6AAPAB8ANEAxDwEFAAQBAQQCRwAFBQBYAwEAACVIBgEEBAFYAgEBASMBSRIQGhcQHxIfMzIREAcGGCsTMxEjNQYrASI1ETQ7ATIXAzMyNjURNCYrASIGFREUFuJaWg0xJFpaJDENRCMSDw8SIxIMDAHo/hgnJ1gBN1kn/ogQEwEQExALEv7kEgsAAgAsAAABQgKdAA8AHwA8QDkOAQQACQEBBQJHAAMDJEgABAQAWAYBAAAlSAAFBQFYAgEBASMBSQIAHhsWEw0MCwoIBQAPAg8HBhQrEzMyFREUKwEiJxUjETMVNhMRNCYrASIGFREUFjsBMjbEI1tbIzENWloNVQwSIxIPDxIjEgwB6Vn+yFgnJwKd2yf+fQEdEQwQE/7vExALAAEAJgAAAUAB6AAfAF1LsBBQWEAiAAABAwEAZQADAgIDYwABAQVYAAUFJUgAAgIEWQAEBCMESRtAJAAAAQMBAANtAAMCAQMCawABAQVYAAUFJUgAAgIEWQAEBCMESVlACTMyEzUzEAYGGisBIzU0JisBIgYVERQWOwEyNj0BMxUUKwEiNRE0OwEyFQFAWQwSKxIMDBIrEgxZXl9dXV9eAU40EgsLEv7kEgsLEjRCWFgBN1lZAAIAJgAAATwCnQAPAB8AOEA1DwEFAwQBAQQCRwAAACRIAAUFA1gAAwMlSAYBBAQBWAIBAQEjAUkSEBoXEB8SHzMyERAHBhgrEzMRIzUGKwEiNRE0OwEyFwMzMjY1ETQmKwEiBhURFBbiWloNMSRaWiQxDUQjEg8PEiMSDAwCnf1jJydYAThZJ/6HEBMBERMQDBH+4xILAAIAKgAAAUcB6AAXACEAbkuwElBYQCUAAwECAgNlAAUAAQMFAV4ABgYAWAcBAAAlSAACAgRZAAQEIwRJG0AmAAMBAgEDAm0ABQABAwUBXgAGBgBYBwEAACVIAAICBFkABAQjBElZQBUCAB8cGRgUEQ8OCwgFBAAXAhcIBhQrEzMyHQEjFRQWOwEyNj0BMxUUKwEiNRE0FzM1NCYrASIGFYdiXsUMEi4SDFhdYF1YbAsSMRIMAehZt3QRDAwSKDVYWAE3Wc5qEQwMEQABAAYAAADZAp0AEgApQCYAAAAGWAAGBiRIBAECAgFWBQEBASVIAAMDIwNJIhERERETIAcGGysTIyIGHQEzFSMRIxEjNTM1NDsB2SoSDEhIWjExXUUCUQsSTEn+YQGfSV1YAAIAJv92ATwB6AAWACYAPkA7FgEGAAsBAwUCRwAGBgBYBAEAACVIBwEFBQNYAAMDI0gAAgIBWQABAScBSRkXIR4XJhkmMzQhIhAIBhkrEzMRFCsBNTMyNj0BBisBIjURNDsBMhcDMzI2NRE0JisBIgYVERQW4lpep40SDA0xJFpaJDENRCMSDw8SIxIMDAHo/eZYRwwRUydZATBZJ/6OEBMBChMQCxL+6hEMAAABACwAAAFCAp0AEwAxQC4SAQIAAUcABAQkSAACAgBYBQEAACVIAwEBASMBSQIAERAPDgsIBQQAEwITBgYUKxMzMhURIxE0JisBIgYVESMRMxU2xCNbWgwSIxIPWloNAehZ/nEBghILEBP+hAKd3CcAAgAqAAAAiAKeAAMABwAnQCQEAQEBAFYAAAAkSAADAyVIAAICIwJJAAAHBgUEAAMAAxEFBhUrEzUzFQMjETMqXgJaWgJFWVn9uwHoAAL//P92AIgCngADAA4ALkArBQEBAQBWAAAAJEgAAgIlSAAEBANZAAMDJwNJAAANCwoIBgUAAwADEQYGFSsTNTMVAxEzERQrATUzMjYqXlxaXS0SEgwCRVlZ/ZUCDv3mWEcMAAACACwAAAFiAp0ABQAJAC9ALAQBAgABAUcAAgIkSAQBAQElSAUDAgAAIwBJBgYAAAYJBgkIBwAFAAUSBgYVKwEHFyMDNwMRMxEBYnBwYGxs1loB6On/AP/p/hgCnf1jAAABACwAAACGAp0AAwATQBAAAQEkSAAAACMASREQAgYWKzMjETOGWloCnQABACwAAAH9AegAIQA3QDQgGwICAAFHBAECAgBYBwYIAwAAJUgFAwIBASMBSQIAHxwaGRgXFBEODQsIBQQAIQIhCQYUKwEzMhURIxE0JisBIhURIxE0JisBIgYVESMRMxU2OwEyFzYBgCNaWgsSJCBaDBIjEg9aWg0xI0ATDAHoWf5xAYISCyP+hAGCEgsQE/6EAegnJy0tAAEALAAAAUIB6AATAC1AKhIBAgABRwACAgBYBAUCAAAlSAMBAQEjAUkCABEQDw4LCAUEABMCEwYGFCsTMzIVESMRNCYrASIGFREjETMVNsQjW1oMEiMSD1paDQHoWf5xAYISCxAT/oQB6CcnAAIAJwAAAUsB6AALABsAKEAlAAICAFgEAQAAJUgAAwMBWAABASMBSQIAGhcSDwgFAAsCCwUGFCsTMzIVERQrASI1ETQTETQmKwEiBhURFBY7ATI2g2tdXWtcywwSNxILCxI3EgwB6Fn+yVhYATdZ/n0BHhIMDBL+4hIMDAACACz/dgFCAegADwAfADxAOQ4BBAAJAQEFAkcABAQAWAMGAgAAJUgABQUBWAABASNIAAICJwJJAgAeGxYTDQwLCggFAA8CDwcGFCsTMzIVERQrASInFSMRMxU2ExE0JisBIgYVERQWOwEyNsQjW1sjMQ1aWg1VDBIjEg8PEiMSDAHoWf7JWCexAnInJ/5+ARwSCxAT/vATEAsAAgAm/3YBPAHoAA8AHwA4QDUPAQUABAECBAJHAAUFAFgDAQAAJUgGAQQEAlgAAgIjSAABAScBSRIQGhcQHxIfMzIREAcGGCsTMxEjNQYrASI1ETQ7ATIXAzMyNjURNCYrASIGFREUFuJaWg0xJFpaJDENRCMSDw8SIxIMDAHo/Y6xJ1gBN1kn/ogQEwEQExALEv7kEgsAAQAsAAAA6QHpAAwAKkAnCgEBAAFHAAEBAFgDBAIAACVIAAICIwJJAQAJCAcGBAIADAEMBQYUKxMzFSMiFREjETMVPgHVFCQ/WloIKQHpTkr+rwHoPBojAAEAIQAAAScB6AAtAKZLsBJQWEArAAIDBAMCZQAGAAcHBmUABAAABgQAYAADAwFYAAEBJUgABwcFWQAFBSMFSRtLsBRQWEAsAAIDBAMCZQAGAAcABgdtAAQAAAYEAGAAAwMBWAABASVIAAcHBVkABQUjBUkbQC0AAgMEAwIEbQAGAAcABgdtAAQAAAYEAGAAAwMBWAABASVIAAcHBVkABQUjBUlZWUALMxIzNTMSMzMIBhwrNzU0JisBIj0BNDsBMh0BIzU0JisBIgYdARQWOwEyHQEUKwEiPQEzFRQWOwEyNs8MEjNdXUleWAwSGBIMDBI0XF1KXVgMEhgSDGJREgtYZ1lZLyYSCwsSVBILWWRYWDAmEgsLAAABAAUAAADRAjsAEgApQCYABQQFbwMBAAAEVgYBBAQlSAABAQJZAAICIwJJEREREiEjEAcGGysTIxEUFjsBFSMiNREjNTM1MxUz0UMLEiZAXi4uW0MBn/7LEgtNWAFHSVNTAAEAKwAAAUEB6AATACNAIAQBAQQBRwMBAAAlSAAEBAFZAgEBASMBSTMSMhEQBQYZKxMzESM1BisBIjURMxEUFjsBMjY151paDTEkWloMEiMSDwHo/hgnJ1gBkP5+EgsQEwAAAQAEAAABOQHoAAYAIUAeAQEBAAFHAwICAAAlSAABASMBSQAAAAYABhESBAYWKxsCMwMjA15AQVpagVoB6P5kAZz+GAHoAAEABQAAAhoB6AAMACFAHgwJBAMBAAFHBAMCAAAlSAIBAQEjAUkSERIREAUGGSsBMwMjCwEjAzMbATMTAcVVWnc6OndZVUE/az0B6P4YAXH+jwHo/mUBm/5lAAEABQAAAVIB6AALACBAHQkGAwAEAAIBRwMBAgIlSAEBAAAjAEkSEhIRBAYYKzcXIycHIzcnMxc3M9h6X0lHXnhxYD8+Xvr6qan576CgAAEACP92AU4B6AAOADFALgoBAgMBRwQBAwMlSAACAiNIAAEBAFkFAQAAJwBJAQAMCwkIBwYEAgAOAQ4GBhQrFyM1MzI/ASMDMxsBMwMGdDwpKwkKMWZcSkVbZxaKSCEpAeD+ZwGZ/elbAAABAA4AAAEWAegACQAtQCoIAQEDAQMCRgABAQJWAAICJUgEAQMDAFYAAAAjAEkAAAAJAAkREhEFBhcrJRUhNRMjNTMVAwEW/vipnvuqSEhIAVhISP6oAAEAB/9cARMDDwAhADJALwMBAgMBRwAEAAUDBAVgAAMAAgADAmAAAAEBAFQAAAABWAABAAFMISQhJSEoBgYaKxMVFAcWHQEUFjsBFSMiJj0BNCYrATUzMjY9ATQ7ARUjIgbGTEwOEi1SLycoKxERKyhWUi0SDgKq+24LCW3+EQ5HLizxODRGNTnuWkcNAAABADb/igCNAucAAwAmS7AaUFhACwAAAQBvAAEBJwFJG0AJAAABAG8AAQFmWbQREAIGFisTMxEjNldXAuf8owAAAQAH/1wBEgMPACIAMkAvIQEDAgFHAAEAAAIBAGAAAgADBQIDYAAFBAQFVAAFBQRYAAQFBEwhJSElISMGBhorEzU0JisBNTMyFh0BFBY7ARUjIgYdARQGKwE1MzI2PQE0NyZUDhItUi8mKCwQECwoJi9SLRIOTEwBr/sRDUcvK+45NUY0OPEsLkcOEf5tCQsAAQA9ANUBzwFpABsAMUAuBQEDAAEEAwFgAAQAAARUAAQEAFkCBgIABABNAQAYFxQSDwwKCQYEABsBGgcGFCslIiYnJiMiBh0BIzU0OwEyFhcWMzI2PQEzFRQjAVgeMg4lJxIMU1QlHDAOJisRClNV1RkQKBARL0JRGRAoDxMuQVIAAAIAMv92AJ8B6AADAAcAJUAiBAEDAwJWAAICJUgAAQEAVgAAACcASQQEBAcEBxIREAUGFysXIxMzJzUzFZ9tEUtZZYoBplF7ewAAAQAm/7QBQAI1ACcAN0A0AAgAAQAIAWAAAgAFAgVaAAAAB1gJAQcHJUgAAwMEWAYBBAQjBEkmJBITIRISEzUzEAoGHSsBIzU0JisBIgYVERQWOwEyNj0BMxUUKwEVIzUjIjURNDsBNTMVMzIVAUBZDBIrEgwMEisSDFleAVYIXV0IVgFeAU40EgsLEv7kEgsLEjRCWExMWAE3WU1NWQABACAAAAFsAnIAIQBxS7ALUFhAKAAFBgMGBWUHAQMIAQIBAwJeAAYGBFgABAQiSAoJAgEBAFYAAAAjAEkbQCkABQYDBgUDbQcBAwgBAgEDAl4ABgYEWAAEBCJICgkCAQEAVgAAACMASVlAEgAAACEAIRETMxIyERMREQsGHSslFSE1PgE9ASM1MzU0OwEyHQEjNTQmKwEiBh0BMxUjFRQHAWz+tBsVMDBdXl1bCxInEgyDgxpJSUcBLDJlR8hYWFxOEgsLErpHcUARAAACADoAPAGwAiAAHwAvAD1AOh4dFBMEAwEODQQDBAACAkccFQIBRQwFAgBEBAECAAACAFwAAwMBWAABASUDSSIgKicgLyIvPTcFBhYrARUUBxcHJwYrASInByc3Jj0BNDcnNxc2OwEyFzcXBxYHMzI2PQE0JisBIgYdARQWAY0HKjowEgthEhExOi8FBzE6NxILYRAHNjosCa80GRAQGTQYEBABhrYeEio6MAIDMTovFBe2HRIxOjcCATY6LBX1DxmlGg8PGqUZDwAAAQANAAABcgJyABYAPkA7EwEABwFHCwoCBwYBAAEHAF8FAQEEAQIDAQJeCQEICCJIAAMDIwNJAAAAFgAWFRQREREREREREREMBh0rARUjBzMVIxUjNSM1MycjNTMDMxsBMwMBamQSdnxbfXcTZE9YW1hXW1YBWkU7QpiYQjtFARj+tQFL/ugAAAIANP+fAI8C0gADAAcAIkAfAAEAAAIBAF4AAgMDAlIAAgIDVgADAgNKEREREAQGGCsTIxEzAzMRI49bW1tbWwF3AVv+KP6lAAIANf/XAW4CnAA3AEcAkUAKLwEIAhMBBgkCR0uwFFBYQDEAAAECAQBlAAQGBQUEZQACCgEICQIIYAAJAAYECQZgAAUAAwUDXQABAQdYAAcHJAFJG0AzAAABAgEAAm0ABAYFBgQFbQACCgEICQIIYAAJAAYECQZgAAUAAwUDXQABAQdYAAcHJAFJWUATOjhCPzhHOkc4NTMSODUzEAsGHCsBIzU0JisBIgYdARQWOwEyHQEUBxYdARQrASI9ATMVFBY7ATI2PQE0JisBIj0BNDcmPQE0OwEyFQcjIgYdARQWOwEyNj0BNCYBbloMEigTCwsTQl0pCl5eXVoMEikSCwsSQ10rC11eXXZMEgwMEkwSCwsCFiMSDAwSURIMWFFHERQeZ1lZLSMSCwsSUhILWU1JERYdaFhYwAwRWhILCxJaEQwAAAIAQQIdAWQCewADAAcAJEAhBQMEAwEBAFYCAQAAIgFJBAQAAAQHBAcGBQADAAMRBgYVKwE1MxUhNTMVAP9l/t1mAh1eXl5eAAMALQAAAp4CcgAfAC0APQCPS7AYUFhANQADBAAEA2UAAAUFAGMABQABBgUBYQAHBwlYAAkJIkgABAQCWAACAiVICgEGBghYAAgIIwhJG0A3AAMEAAQDAG0AAAUEAAVrAAUAAQYFAWEABwcJWAAJCSJIAAQEAlgAAgIlSAoBBgYIWAAICCMISVlAFSIgPDk0MSkmIC0iLTUzEjMyEQsGGislNTMVFCsBIj0BNDsBMh0BIzU0JisBIgYdARQWOwEyNgczMjY9ATQrASIdARQWARUUBisBIiY9ATQ2OwEyFgGXUFRaVFRaVFALESsRCwsRKxELia9PQpGvkUIB32Zzv3NmZ3K/cmf0GidQUK5RUSYZEQsLEZQRCgqfaWRJ1tZJZGkBET+NiYmNP46PjwAAAgAZARoA4wJyAA8AHQAxQC4PAQUABAEBBAJHBgEEAgEBBAFcAAUFAFgDAQAAIgVJEhAYFRAdEh0zMhEQBwYYKxMzESM1BisBIj0BNDsBMhcDMzI9ATQrASIGHQEUFqc8PQkgJEBAIyIJPCUWFiUOCAkCcv6oGBg/2j8b/vcYwBkHDcgNCAACACUAaAHFAZ4ABQALADNAMAoHBAEEAQABRwIBAAEBAFICAQAAAVYFAwQDAQABSgYGAAAGCwYLCQgABQAFEgYGFSslJzczBxchJzczBxcBZnZ2X3l5/td3d154eGibm5yam5ucmgABACIAoQGgAZAABQBIS7AJUFhAGAAAAQEAZAMBAgEBAlIDAQICAVYAAQIBShtAFwAAAQBwAwECAQECUgMBAgIBVgABAgFKWUALAAAABQAFEREEBhYrARUjNSE1AaBH/skBkO+UWwABADABAwEKAUkAAwAeQBsAAAEBAFIAAAABVgIBAQABSgAAAAMAAxEDBhUrEzUzFTDaAQNGRgAEACIBWgF8ArQABwAUACIAMQCKQAsBAAIBAA4BBAECR0uwFlBYQC4FAQMEBgQDBm0AAQAEAwEEXgoBBgAIBghcAAcHCVgACQkkSAAAAAJYAAICIgBJG0AsBQEDBAYEAwZtAAkABwIJB2AAAQAEAwEEXgoBBgAIBghcAAAAAlgAAgIiAElZQBUXFTEuKSYeGxUiFyIRERUiERMLBhorEzU0KwEVMzInMzIdARQHFyMnIxUjFzMyNj0BNCsBIh0BFBYlFRQGKwEiJj0BNDY7ATLgBhoaBkRGIxYhJiAKJANhLCNPYVAkAQk5P2pAODk/angCFzYHQ2AgPRwEU1FRIDg4KHZ2KDg4liNOTExOI05PAAEATAInAVgCawADABlAFgIBAQEAVgAAACIBSQAAAAMAAxEDBhUrEzUhFUwBDAInREQAAgA2AVcBRAJyAA8AGwAcQBkAAQACAQJcAAAAA1gAAwMiAEkzNDUzBAYYKxM1NCYrASIGHQEUFjsBMjY3FRQrASI9ATQ7ATLwDBIqEgwMEioTC1RbWFtbWFsBtV8RCwsRXxELC3duVlZuVwAAAgAdAKsBhwH5AAsADwAwQC0EAQADAQECAAFeAAYIAQcGB1oAAgIFVgAFBSUCSQwMDA8MDxIRERERERAJBhsrATMVIxUjNSM1MzUzAzUhFQD/iIhaiIha4gFqAapIT09IT/6ySUkAAQAOARoAugJyABoAT7UQAQMCAUdLsBRQWEAZAAAEAgQAZQACAAMCA1oABAQBWAABASIESRtAGgAABAIEAAJtAAIAAwIDWgAEBAFYAAEBIgRJWbc3ERUyEQUGGSsTFSM1NDsBMh0BFA8BMxUjNTc2PQE0JisBIgZJOzNEMxZOZqxdEQYKEgoHAjMzQjAwITEigTMwlBkqEgkHBwAAAQAPARoAvQJyAC4AdrULAQUGAUdLsBZQWEAqAAAHBgcAZQADBQQEA2UABAACBAJdAAcHAVgAAQEiSAAFBQZYAAYGJQVJG0AsAAAHBgcABm0AAwUEBQMEbQAEAAIEAl0ABwcBWAABASJIAAUFBlgABgYlBUlZQAs1ISUzEjgyEQgGHCsTFSM1NDsBMh0BFAcWHQEUKwEiPQEzFRQWOwEyNj0BNCYrATUzMjY9ATQmKwEiBkw9NEczFhYzRzQ9BgoVCgYGCh8fCgYGChUKBgIzLDswMFgeBAQcXjAwOysKBgYKRgoGLwcJRAkHBwABAHYCBwEsAosAAwAuS7AcUFhADAIBAQABcAAAACQASRtACgAAAQBvAgEBAWZZQAoAAAADAAMRAwYVKxM3Mwd2UmRpAgeEhAABACj/dgE+AegAEwApQCYEAQEFAUcEAQAAJUgABQUBWAIBAQEjSAADAycDSTMRESIREAYGGisTMxEjNQYrARUjETMRFBY7ATI2NeRaWg0xJFpaDBIjEg8B6P4YJyeKAnL+fhILEBMAAAIAMf92Ab0CcgADAA0AI0AgAAMDAFgFBAIAACJIAgEBAScBSQQEBA0EDCESERAGBhgrATMRIwMRIxEjIj0BNDMBa1JSTFI/XV0Ccv0EAvz9BAFsWOBYAAABACIBAwCHAXwAAwAeQBsAAAEBAFIAAAABVgIBAQABSgAAAAMAAxEDBhUrEzUzFSJlAQN5eQABAIX/RQEj/84ACQA+S7AUUFhAFgAAAgIAYwACAQECVAACAgFZAAECAU0bQBUAAAIAbwACAQECVAACAgFZAAECAU1ZtSEiEQMGFysXNTMVFCsBNTMyzlVlOSghVCIpYEYAAAEACwEaAG8CcgAFAB9AHAAAAQBwAAEBAlYDAQICIgFJAAAABQAFEREEBhYrExEjESM1bz0nAnL+qAEpLwAAAgAaARoA3gJyAAsAGwAlQCIAAwABAwFcAAICAFgEAQAAIgJJAgAaFxIPCAUACwILBQYUKxMzMh0BFCsBIj0BNBM1NCYrASIGHQEUFjsBMjZbQkFBQkGICQ0hDQgIDSENCQJyP9o/P9o//uzQDQgIDdAMCAgAAAIAJQBoAcUBngAFAAsAJkAjCQYDAAQAAQFHAwEBAAABUgMBAQEAVgIBAAEAShISEhEEBhgrAQcjNyczDwEjNyczAcV3Xnh4XlR3Xnl5XgEDm5qcm5uanAACABT/dgEuAegAGgAeAK5LsAtQWEAsAAIDBAMCZQAABAUFAGUAAwAEAAMEYAAGBgdWCAEHByVIAAUFAVkAAQEnAUkbS7AUUFhALQACAwQDAmUAAAQFBAAFbQADAAQAAwRgAAYGB1YIAQcHJUgABQUBWQABAScBSRtALgACAwQDAgRtAAAEBQQABW0AAwAEAAMEYAAGBgdWCAEHByVIAAUFAVkAAQEnAUlZWUAQGxsbHhseFDUhESMyEAkGGys3MxUUKwEiPQE0OwE3MxcjIgYdARQWOwEyNjUTFSM101tdYF1dDQRMCEsRCwsRLBELCGUiVVdXtldCgw0RoRENDRECDnt7AAMADAAAAXUDFQAHAAoADgA6QDcKAQQCAUcABgUGbwAFAgVvAAQAAAEEAF8AAgIiSAcDAgEBIwFJAAAODQwLCQgABwAHERERCAYXKyEnIwcjEzMTJzMDNyMnMwEaGYQXWm+Dd+1tOSBNaWSRkQJy/Y7aAUpthAADAAwAAAF1AxUABwAKAA4AP0A8CgEEAgFHCAEGBQZvAAUCBW8ABAAAAQQAXwACAiJIBwMCAQEjAUkLCwAACw4LDg0MCQgABwAHERERCQYXKyEnIwcjEzMTJzMDNwcjNwEaGYQXWm+Dd+1tOZFpTVKRkQJy/Y7aAUrxhIQAAwAMAAABdQMVAAcACgARAEFAPgsBBQYKAQQCAkcABgUGbwcBBQIFbwAEAAABBABfAAICIkgIAwIBASMBSQAAERAPDg0MCQgABwAHERERCQYXKyEnIwcjEzMTJzMDNQcjNzMXIwEaGYQXWm+Dd+1tOTReYGVfXZGRAnL9jtoBSr5RhIQAAwAMAAABdQL4AAcACgAiAE9ATAoBBAIBRwAHBgoGB2UACgUGClQIAQYJAQUCBgVgAAQAAAEEAF8AAgIiSAsDAgEBIwFJAAAiIB8cGRgVFBMQDQwJCAAHAAcREREMBhcrIScjByMTMxMnMwMnFSM1NDY7ATIWMjY9ATMVFAYrASImIyIBGhmEF1pvg3ftbTlBTiQqCRk/FAhNIisHGkIJEZGRAnL9jtoBSn0PFigoJgkPDhcnKCYAAAQADAAAAXUC7wAHAAoADgASAEhARQoBBAIBRwsICgMGBwEFAgYFXgAEAAABBABfAAICIkgJAwIBASMBSQ8PCwsAAA8SDxIREAsOCw4NDAkIAAcABxEREQwGFyshJyMHIxMzEyczAycVIzUhFSM1ARoZhBdab4N37W05K2YBI2WRkQJy/Y7aAUrLXl5eXgAEAAwAAAF1Ay8ABwAKABYAJgBJQEYKAQQCAUcABgAIBwYIYAAHCgEFAgcFYAAEAAABBABfAAICIkgJAwIBASMBSQ0LAAAlIh0aExALFg0WCQgABwAHERERCwYXKyEnIwcjEzMTJzMDNyMiPQE0OwEyHQEUJxUUFjsBMjY9ATQmKwEiBgEaGYQXWm+Dd+1tOSFBSUlBSZQJESERCQkRIREJkZECcv2O2gFKbkYRRkYRRlIHDwoKDwcPCgoAAAIACwAAAiwCcgAPABIAQUA+EQEGBQFHAAYABwgGB14JAQgAAgAIAl4ABQUEVgAEBCJIAAAAAVYDAQEBIwFJEBAQEhASERERERERERAKBhwrJTMVITUjByMTIRUjFTMVIwcRAwF3tf7ygzVb6AEyrpCQWWlJSZWVAnJJwUlCASX+2wACAC//RQFUAnIAHwApALhLsAlQWEAwAAABAwEAZQADAgIDYwAGBAgIBmUACAAHCAddAAEBBVgABQUiSAACAgRZAAQEIwRJG0uwFFBYQDIAAAEDAQADbQADAgEDAmsABgQICAZlAAgABwgHXQABAQVYAAUFIkgAAgIEWQAEBCMESRtAMwAAAQMBAANtAAMCAQMCawAGBAgEBghtAAgABwgHXQABAQVYAAUFIkgAAgIEWQAEBCMESVlZQAwhIhMzMhM1MxAJBh0rASM1NCYrASIGFREUFjsBMjY9ATMVFCsBIjURNDsBMhUDNTMVFCsBNTMyAVRcCxIzEgwMEjMSC1xeal1dal67VWU5KCEBrmASCwsS/lUSCwsSX2pZWQHCWFj9kiIpYEYAAAIAKQAAAUQDFQALAA8ANUAyAAcGB28ABgIGbwAEAAUABAVeAAMDAlYAAgIiSAAAAAFWAAEBIwFJERERERERERAIBhwrNzMVIREhFSMVMxUjEyMnM4+1/vABCa6QkFBNaWRJSQJyScFJAXKEAAIANAAAAUUDFQALAA8AQkA/CQEHBgdvAAYCBm8ABAgBBQAEBV4AAwMCVgACAiJIAAAAAVYAAQEjAUkMDAAADA8MDw4NAAsACxERERERCgYZKxMVMxUhESEVIxUzFRMHIzePtf7wAQmukCZpTVIBH9ZJAnJJwUkB9oSEAAACACgAAAFMAxUACwASAEZAQwwBBgcBRwAHBgdvCAEGAgZvAAQJAQUABAVeAAMDAlYAAgIiSAAAAAFWAAEBIwFJAAASERAPDg0ACwALEREREREKBhkrExUzFSERIRUjFTMVAwcjNzMXI4+1/vABCa6QZTReYGVfXQEf1kkCcknBSQHDUYSEAAMAJQAAAUgC7wADAA8AEwBEQEELCQIACAEBBAABXgAGCgEHAgYHXgAFBQRWAAQEIkgAAgIDVgADAyMDSRAQBAQQExATEhEEDwQPERERERIREAwGGysTMxUjAxUzFSERIRUjFTMVAxUjNeNlZVS1/vABCa6QlGYC717+jtZJAnJJwUkB0F5eAAAC/9kAAACWAxUAAwAHACdAJAQBAQABbwAAAgBvAAICIkgAAwMjA0kAAAcGBQQAAwADEQUGFSsTFyMnFzMRIz1STWljWloDFYSEo/2OAAACADwAAAD7AxUAAwAHACdAJAAAAQBvBAEBAgFvAAICIkgAAwMjA0kAAAcGBQQAAwADEQUGFSsTNzMPATMRI0VSZGlWWloCkYSEH/2OAAAC/9gAAAD8AxUABgAKAC9ALAUBAQABRwAAAQBvBQICAQMBbwADAyJIAAQEIwRJAAAKCQgHAAYABhERBgYWKwM3MxcjJwcXMxEjKGBlX101NAZaWgKRhIRRUR/9jgAAA//WAAAA+QLvAAMABwALAC5AKwQBAgcFBgMDAAIDXgAAACJIAAEBIwFJCAgEBAgLCAsKCQQHBAcSERAIBhcrEzMRIxM1MxUhNTMVPFpaWGX+3WYCcv2OApFeXl5eAAIAAwAAAW0CcgALABkALUAqBQEDBgECBwMCXgAEBABYAAAAIkgABwcBWAABASMBSSERESQRESMgCAYcKxMzMhURFCsBESM1MxcRNCYrARUzFSMVMzI2NtpdXdozM9wMEmNLS2MSDAJyWP4+WAEYR/sBqhILzEfRCwAAAgA0AAABZgL4ABcAIQBKQEchHAIHBgFHAAQDAQMEZQABAAMBVAUBAwIKAgAGAwBgCQEGBiJICAEHByMHSQEAIB8eHRsaGRgTEg8ODQoHBgQCABcBFgsGFCsBIiYjIh0BIzU0NjsBMhYyNj0BMxUUBiMXMxEjAxEjETMTAQUaQgkRTiQqCRk/FAhNIisHU1uEU1+AApImFw8WKCgmCQ8OFycoIP2OAcT+PAJy/kkAAwArAAABWQMVAAsAGwAfADZAMwAFBAVvAAQABG8AAgIAWAYBAAAiSAADAwFYAAEBIwFJAgAfHh0cGhcSDwgFAAsCCwcGFCsTMzIVERQrASI1ETQTETQmKwEiBhURFBY7ATI2AyMnM41vXV1vXtAMEjoSCwsSOhIMHk1pZAJyWP4+WFgBwlj99AGmEgsLEv5aEgsLAj2EAAADAC8AAAFZAxUACwAbAB8AO0A4BwEFBAVvAAQABG8AAgIAWAYBAAAiSAADAwFYAAEBIwFJHBwCABwfHB8eHRoXEg8IBQALAgsIBhQrEzMyFREUKwEiNRE0ExE0JisBIgYVERQWOwEyNhMHIzeNb11db17QDBI6EgsLEjoSDE9pTVICclj+PlhYAcJY/fQBphILCxL+WhILCwLBhIQAAAMALwAAAVkDFQALABsAIgA/QDwcAQQFAUcABQQFbwYBBAAEbwACAgBYBwEAACJIAAMDAVgAAQEjAUkCACIhIB8eHRoXEg8IBQALAgsIBhQrEzMyFREUKwEiNRE0ExE0JisBIgYVERQWOwEyNgMHIzczFyONb11db17QDBI6EgsLEjoSDDw0XmBlX10Cclj+PlhYAcJY/fQBphILCxL+WhILCwKOUYSEAAMALwAAAVkC+AALABsAMwBLQEgABgUJBQZlAAkEBQlUBwEFCAEEAAUEYAACAgBYCgEAACJIAAMDAVgAAQEjAUkCADMxMC0qKSYlJCEeHRoXEg8IBQALAgsLBhQrEzMyFREUKwEiNRE0ExE0JisBIgYVERQWOwEyNgMVIzU0NjsBMhYyNj0BMxUUBisBIiYjIo1vXV1vXtAMEjoSCwsSOhIMek4kKgkZPxQITSIrBxpCCRECclj+PlhYAcJY/fQBphILCxL+WhILCwJNDxYoKCYJDw4XJygmAAQALwAAAVkC7wALABsAHwAjAERAQQoHCQMFBgEEAAUEXgACAgBYCAEAACJIAAMDAVgAAQEjAUkgIBwcAgAgIyAjIiEcHxwfHh0aFxIPCAUACwILCwYUKxMzMhURFCsBIjURNBMRNCYrASIGFREUFjsBMjYDFSM1IRUjNY1vXV1vXtAMEjoSCwsSOhIMZmYBI2UCclj+PlhYAcJY/fQBphILCxL+WhILCwKbXl5eXgAAAQAYAJIBYgHcAAsABrMJAwEtKwEHFwcnByc3JzcXNwFibW04bW04bW04bW0BpG1tOG1tOG1tOG1tAAMAMv/sAWAChgAVAB0AJQBoQBEUEQIEAh8XAgUECQYCAAUDR0uwGlBYQB8AAwMiSAAEBAJYAAICIkgABQUAWQAAACNIAAEBIwFJG0AfAAMCA28AAQABcAAEBAJYAAICIkgABQUAWQAAACMASVlACTU2EjUSMgYGGisBERQrASInByM3JjURNDsBMhc3MwcWBxETJisBIgYTEQMWOwEyNgFdXW8SDwk1EhFebxUNCTUTENJ6BBo+Ewt7eQQYPhMMAhr+PlgDFzEWJQHCWAQYMhQv/skBRQ8M/kABM/68DQwAAgAxAAABXAMVABEAFQAnQCQABQQFbwAEAARvAgEAACJIAAMDAVkAAQEjAUkREzMSMhAGBhorATMRFCsBIjURMxEUFjsBMjY1AyMnMwEBW15vXlsMEjkTCxRNaWQCcv3mWFgCGv30EgsLEgIrhAACADEAAAFcAxUAEQAVAC1AKgYBBQQFbwAEAARvAgEAACJIAAMDAVkAAQEjAUkSEhIVEhUUMxIyEAcGGSsBMxEUKwEiNREzERQWOwEyNjUTByM3AQFbXm9eWwwSORMLU2lNUgJy/eZYWAIa/fQSCwsSAq+EhAAAAgAxAAABXAMVABEAGAAvQCwSAQQFAUcABQQFbwYBBAAEbwIBAAAiSAADAwFZAAEBIwFJEREUMxIyEAcGGysBMxEUKwEiNREzERQWOwEyNjUDByM3MxcjAQFbXm9eWwwSORMLOzReYGVfXQJy/eZYWAIa/fQSCwsSAnxRhIQAAwAxAAABXALvABEAFQAZADZAMwkHCAMFBgEEAAUEXgIBAAAiSAADAwFZAAEBIwFJFhYSEhYZFhkYFxIVEhUUMxIyEAoGGSsBMxEUKwEiNREzERQWOwEyNjUDFSM1IRUjNQEBW15vXlsMEjkTC2ZmASNlAnL95lhYAhr99BILCxICiV5eXl4AAAIABAAAAWUDFQAIAAwAL0AsCAUCAwABAUcFAQQDBG8AAwEDbwIBAQEiSAAAACMASQkJCQwJDBMSEhAGBhgrMyM1AzMbATMDEwcjN+VbhlxXUlyAYGlNUsYBrP6nAVn+VAJPhIQAAAIANQAAAV0CcgALABUAKUAmAAUAAQIFAWAAAwMiSAAEBABYAAAAJUgAAgIjAkkhJBERIyAGBhorEzMyHQEUKwEVIxEzEzU0JisBFTMyNo9xXV1xWlp0DBJWVhIMAfdYzFh7AnL+brISC+wLAAEALQAAAb8CWAA6AHNLsBBQWEArAAACAQEAZQAGAAQDBgRgAAMABwgDB2AACAACAAgCYAABAQVZCQEFBSMFSRtALAAAAgECAAFtAAYABAMGBGAAAwAHCAMHYAAIAAIACAJgAAEBBVkJAQUFIwVJWUAOOTY1MzITNSM1MxAKBh0rNzMVFBY7ATI2PQE0JisBIj0BNDMyNj0BNCYrASIGFREjETQ7ATIdARQrASIGHQEUFjsBMh0BFCsBIjXIWQsSCxILCxImW1gXDQsSgRINWlu8W1ASEgwMEiVdW0BclzQRDAsSURILWThWCBAfEgwMEv4RAgBYV0NJCxIlEgtYZlhYAAMAJgAAATwCiwAPAB8AIwB0QAoPAQUABAEBBAJHS7AcUFhAJQAGBwAHBgBtAAcHJEgABQUAWAMBAAAlSAgBBAQBWAIBAQEjAUkbQCIABwYHbwAGAAZvAAUFAFgDAQAAJUgIAQQEAVgCAQEBIwFJWUATEhAjIiEgGhcQHxIfMzIREAkGGCsTMxEjNQYrASI1ETQ7ATIXAzMyNjURNCYrASIGFREUFhMjJzPiWloNMSRaWiQxDUQjEg8PEiMSDAxpTWlkAej+GCcnWAE3WSf+iBATARATEAsS/uQSCwG+hAAAAwAmAAABSAKLAA8AHwAjAHpACg8BBQAEAQEEAkdLsBxQWEAmAAYHAAcGAG0JAQcHJEgABQUAWAMBAAAlSAgBBAQBWAIBAQEjAUkbQCMJAQcGB28ABgAGbwAFBQBYAwEAACVICAEEBAFYAgEBASMBSVlAFyAgEhAgIyAjIiEaFxAfEh8zMhEQCgYYKxMzESM1BisBIjURNDsBMhcDMzI2NRE0JisBIgYVERQWEwcjN+JaWg0xJFpaJDENRCMSDw8SIxIMDLxpTVIB6P4YJydYATdZJ/6IEBMBEBMQCxL+5BILAkKEhAADACQAAAFIAosADwAfACYAfEAOIAEGBw8BBQAEAQEEA0dLsBxQWEAmCAEGBwAHBgBtAAcHJEgABQUAWAMBAAAlSAkBBAQBWAIBAQEjAUkbQCMABwYHbwgBBgAGbwAFBQBYAwEAACVICQEEBAFYAgEBASMBSVlAFRIQJiUkIyIhGhcQHxIfMzIREAoGGCsTMxEjNQYrASI1ETQ7ATIXAzMyNjURNCYrASIGFREUFhMHIzczFyPiWloNMSRaWiQxDUQjEg8PEiMSDAwqNF5gZV9dAej+GCcnWAE3WSf+iBATARATEAsS/uQSCwIPUYSEAAADACMAAAE8AnwADwAfADcAXkBbDwEFAAQBAQQCRwAIBwsHCGUACwsHWAkBBwciSAoBBgYHWAkBBwciSAAFBQBYAwEAACVIDAEEBAFYAgEBASMBSRIQNzU0MS4tKikoJSIhGhcQHxIfMzIREA0GGCsTMxEjNQYrASI1ETQ7ATIXAzMyNjURNCYrASIGFREUFgMVIzU0NjsBMhYyNj0BMxUUBisBIiYjIuJaWg0xJFpaJDENRCMSDw8SIxIMDBtOJCoJGT8UCE0iKwcaQgkRAej+GCcnWAE3WSf+iBATARATEAsS/uQSCwHcDxYoKCYJDw4XJygmAAAEACEAAAFEAnsAAwATACMAJwBLQEgTAQcCCAEDBgJHCAEBAQBWCwkCAAAiSAAHBwJYBQECAiVICgEGBgNYBAEDAyMDSSQkFhQkJyQnJiUeGxQjFiMzMhERERAMBhorEzMVIxczESM1BisBIjURNDsBMhcDMzI2NRE0JisBIgYVERQWAxUjNd9lZQNaWg0xJFpaJDENRCMSDw8SIxIMDAVmAnteNf4YJydYATdZJ/6IEBMBEBMQCxL+5BILAjJeXgAEACYAAAE8Aq0ADwAfACsAOwBTQFAPAQUABAEBBAJHAAgLAQYACAZgAAkJB1gABwckSAAFBQBYAwEAACVICgEEBAFYAgEBASMBSSIgEhA6NzIvKCUgKyIrGhcQHxIfMzIREAwGGCsTMxEjNQYrASI1ETQ7ATIXAzMyNjURNCYrASIGFREUFhMjIj0BNDsBMh0BFCcVFBY7ATI2PQE0JisBIgbiWloNMSRaWiQxDUQjEg8PEiMSDAxMQUlJQUmUCREhEQkJESERCQHo/hgnJ1gBN1kn/ogQEwEQExALEv7kEgsBx0YRRkYRRlIHDwoKDwcPCgoAAwArAAACAwHoADIAPABGAPFAGCoBBQcbAQYFGgEACkNCGQMCABIBAwEFR0uwC1BYQDIABgUKBQZlAAIAAQECZQAKDAEAAgoAXg0JAgUFB1gIAQcHJUgOCwIBAQNZBAEDAyMDSRtLsBJQWEAzAAYFCgUGCm0AAgABAQJlAAoMAQACCgBeDQkCBQUHWAgBBwclSA4LAgEBA1kEAQMDIwNJG0A0AAYFCgUGCm0AAgABAAIBbQAKDAEAAgoAXg0JAgUFB1gIAQcHJUgOCwIBAQNZBAEDAyMDSVlZQCc/PTUzAQA9Rj9GOTgzPDU8LywoJSMiHxwXFBANCwoHBAAyATEPBhQrJSMVFBY7ATI2PQEzFRQrASImJw4BKwEiPQE3NSYrASIGHQEjNTQ7ATIWFz4BOwEyHQEjJyMiBh0BMzU0JgMzMjY3NQcVFBYB+bkLEi0SDFldTRomBwYlGkZavAMeIxIMWlpHGSYGBiYbTl0KbDASC2oL/CMQEAFiDNdyEgsLEig1WBgSEhhYck1qHgsSS1hZGRESGFmzxAsSaGgSC/6pDRFoJ0ISCwAAAgAm/0UBQAHoAB8AKQC4S7AQUFhAMAAAAQMBAGUAAwICA2MABgQICAZlAAgABwgHXQABAQVYAAUFJUgAAgIEWQAEBCMESRtLsBRQWEAyAAABAwEAA20AAwIBAwJrAAYECAgGZQAIAAcIB10AAQEFWAAFBSVIAAICBFkABAQjBEkbQDMAAAEDAQADbQADAgEDAmsABgQIBAYIbQAIAAcIB10AAQEFWAAFBSVIAAICBFkABAQjBElZWUAMISITMzITNTMQCQYdKwEjNTQmKwEiBhURFBY7ATI2PQEzFRQrASI1ETQ7ATIVAzUzFRQrATUzMgFAWQwSKxIMDBIrEgxZXl9dXV9etFVlOSghAU40EgsLEv7kEgsLEjRCWFgBN1lZ/h0iKWBGAAADACMAAAFHAosAFwAhACUAxUuwElBYQDIABwgACAcAbQADAQICA2UABQABAwUBXwAICCRIAAYGAFgJAQAAJUgAAgIEWQAEBCMESRtLsBxQWEAzAAcIAAgHAG0AAwECAQMCbQAFAAEDBQFfAAgIJEgABgYAWAkBAAAlSAACAgRZAAQEIwRJG0AwAAgHCG8ABwAHbwADAQIBAwJtAAUAAQMFAV8ABgYAWAkBAAAlSAACAgRZAAQEIwRJWVlAGQIAJSQjIh8cGRgUEQ8OCwgFBAAXAhcKBhQrEzMyHQEjFRQWOwEyNj0BMxUUKwEiNRE0FzM1NCYrASIGFTcjJzOHYl7FDBIuEgxYXWBdWGwLEjESDFdNaWQB6Fm3dBEMDBIoNVhYATdZzmoRDAwRg4QAAAMAKgAAAUcCiwAXACEAJQDMS7ASUFhAMwAHCAAIBwBtAAMBAgIDZQAFAAEDBQFfCgEICCRIAAYGAFgJAQAAJUgAAgIEWQAEBCMESRtLsBxQWEA0AAcIAAgHAG0AAwECAQMCbQAFAAEDBQFfCgEICCRIAAYGAFgJAQAAJUgAAgIEWQAEBCMESRtAMQoBCAcIbwAHAAdvAAMBAgEDAm0ABQABAwUBXwAGBgBYCQEAACVIAAICBFkABAQjBElZWUAdIiICACIlIiUkIx8cGRgUEQ8OCwgFBAAXAhcLBhQrEzMyHQEjFRQWOwEyNj0BMxUUKwEiNRE0FzM1NCYrASIGFRMHIzeHYl7FDBIuEgxYXWBdWGwLEjESDMVpTVIB6Fm3dBEMDBIoNVhYATdZzmoRDAwRAQeEhAADACgAAAFMAosAFwAhACgA0bUiAQcIAUdLsBJQWEAzCQEHCAAIBwBtAAMBAgIDZQAFAAEDBQFeAAgIJEgABgYAWAoBAAAlSAACAgRZAAQEIwRJG0uwHFBYQDQJAQcIAAgHAG0AAwECAQMCbQAFAAEDBQFeAAgIJEgABgYAWAoBAAAlSAACAgRZAAQEIwRJG0AxAAgHCG8JAQcAB28AAwECAQMCbQAFAAEDBQFeAAYGAFgKAQAAJUgAAgIEWQAEBCMESVlZQBsCACgnJiUkIx8cGRgUEQ8OCwgFBAAXAhcLBhQrEzMyHQEjFRQWOwEyNj0BMxUUKwEiNRE0FzM1NCYrASIGFTcHIzczFyOHYl7FDBIuEgxYXWBdWGwLEjESDDg0XmBlX10B6Fm3dBEMDBIoNVhYATdZzmoRDAwR1FGEhAAABAAiAAABRwJ7ABcAIQAlACkAmkuwElBYQDMAAwECAgNlAAUAAQMFAV4JAQcHCFYNCgwDCAgiSAAGBgBYCwEAACVIAAICBFkABAQjBEkbQDQAAwECAQMCbQAFAAEDBQFeCQEHBwhWDQoMAwgIIkgABgYAWAsBAAAlSAACAgRZAAQEIwRJWUAlJiYiIgIAJikmKSgnIiUiJSQjHxwZGBQRDw4LCAUEABcCFw4GFCsTMzIdASMVFBY7ATI2PQEzFRQrASI1ETQXMzU0JisBIgYVNxUjNSEVIzWHYl7FDBIuEgxYXWBdWGwLEjESDAZmASNlAehZt3QRDAwSKDVYWAE3Wc5qEQwMEfdeXl5eAAL/ygAAAIYCiwADAAcAS0uwHFBYQBkAAAEDAQADbQQBAQEkSAADAyVIAAICIwJJG0AWBAEBAAFvAAADAG8AAwMlSAACAiMCSVlADgAABwYFBAADAAMRBQYVKxMXIycTIxEzLlJNabxaWgKLhIT9dQHoAAIALAAAAOUCiwADAAcAS0uwHFBYQBkEAQEAAwABA20AAAAkSAADAyVIAAICIwJJG0AWAAABAG8EAQEDAW8AAwMlSAACAiMCSVlADgAABwYFBAADAAMRBQYVKxM3MwcTIxEzL1JkaQpaWgIHhIT9+QHoAAL/xwAAAOsCiwAGAAoAVbUFAQEAAUdLsBxQWEAaBQICAQAEAAEEbQAAACRIAAQEJUgAAwMjA0kbQBcAAAEAbwUCAgEEAW8ABAQlSAADAyMDSVlADwAACgkIBwAGAAYREQYGFisDNzMXIycHEyMRMzlgZV9dNTRhWloCB4SEUVH9+QHoAAP/xwAAAOoCewADAAcACwAyQC8HAwYDAQEAVgIBAAAiSAAFBSVIAAQEIwRJBAQAAAsKCQgEBwQHBgUAAwADEQgGFSsTNTMVITUzFRMjETOFZf7dZllaWgIdXl5eXv3jAegAAgAmAAABSwJyABgAJwAwQC0XFhUUDw4NDAgBAgFHAAEAAwQBA2EAAgIiSAAEBABYAAAAIwBJNSoXIzIFBhkrARUUKwEiPQE0OwEmJwc1NyYnMxYXNxUHFgM1NCcjIgYdARQWOwEyNgFLXmpdXVsRH18+ISZ9FwtZOUZaA1ASDAwSNRIMATXdWFjvWTIwIjcWJx4XDiA3FWz+scYWFwsS1hILCwACACIAAAFCAnwAEwArAFdAVBIBAgABRwAHBgoGB2UACgoGWAgBBgYiSAkBBQUGWAgBBgYiSAACAgBYBAsCAAAlSAMBAQEjAUkCACspKCUiIR4dHBkWFREQDw4LCAUEABMCEwwGFCsTMzIVESMRNCYrASIGFREjETMVNicVIzU0NjsBMhYyNj0BMxUUBisBIiYjIsQjW1oMEiMSD1paDSNOJCoJGT8UCE0iKwcaQgkRAehZ/nEBghILEBP+hAHoJyc9DxYoKCYJDw4XJygmAAMAJgAAAUsCiwALABsAHwBkS7AcUFhAIwAEBQAFBABtAAUFJEgAAgIAWAYBAAAlSAADAwFZAAEBIwFJG0AgAAUEBW8ABAAEbwACAgBYBgEAACVIAAMDAVkAAQEjAUlZQBMCAB8eHRwaFxIPCAUACwILBwYUKxMzMhURFCsBIjURNBMRNCYrASIGFREUFjsBMjYDIyczg2tdXWtcywwSNxILCxI3EgwWTWlkAehZ/slYWAE3Wf59AR4SDAwS/uISDAwBtIQAAAMAJwAAAUsCiwALABsAHwBqS7AcUFhAJAAEBQAFBABtBwEFBSRIAAICAFgGAQAAJUgAAwMBWAABASMBSRtAIQcBBQQFbwAEAARvAAICAFgGAQAAJUgAAwMBWAABASMBSVlAFxwcAgAcHxwfHh0aFxIPCAUACwILCAYUKxMzMhURFCsBIjURNBMRNCYrASIGFREUFjsBMjYTByM3g2tdXWtcywwSNxILCxI3EgxUaU1SAehZ/slYWAE3Wf59AR4SDAwS/uISDAwCOISEAAMAJQAAAUsCiwALABsAIgBvtRwBBAUBR0uwHFBYQCQGAQQFAAUEAG0ABQUkSAACAgBYBwEAACVIAAMDAVgAAQEjAUkbQCEABQQFbwYBBAAEbwACAgBYBwEAACVIAAMDAVgAAQEjAUlZQBUCACIhIB8eHRoXEg8IBQALAgsIBhQrEzMyFREUKwEiNRE0ExE0JisBIgYVERQWOwEyNgMHIzczFyODa11da1zLDBI3EgsLEjcSDDs0XmBlX10B6Fn+yVhYATdZ/n0BHhIMDBL+4hIMDAIFUYSEAAMAJgAAAUsCfAALABsAMwBSQE8ABgUJBQZlAAkJBVgHAQUFIkgIAQQEBVgHAQUFIkgAAgIAWAoBAAAlSAADAwFYAAEBIwFJAgAzMTAtKikmJSQhHh0aFxIPCAUACwILCwYUKxMzMhURFCsBIjURNBMRNCYrASIGFREUFjsBMjYDFSM1NDY7ATIWMjY9ATMVFAYrASImIyKDa11da1zLDBI3EgsLEjcSDH5OJCoJGT8UCE0iKwcaQgkRAehZ/slYWAE3Wf59AR4SDAwS/uISDAwB0g8WKCgmCQ8OFycoJgAABAAnAAABSwJ7AAsAGwAfACMARkBDBgEEBAVWCgcJAwUFIkgAAgIAWAgBAAAlSAADAwFYAAEBIwFJICAcHAIAICMgIyIhHB8cHx4dGhcSDwgFAAsCCwsGFCsTMzIVERQrASI1ETQTETQmKwEiBhURFBY7ATI2AxUjNSEVIzWDa11da1zLDBI3EgsLEjcSDGVmASNlAehZ/slYWAE3Wf59AR4SDAwS/uISDAwCKF5eXl4AAAMAGQB9AYMB9QADAAcACwAsQCkABAYBBQAEBV4AAAABAAFaAAMDAlYAAgIlA0kICAgLCAsSEREREAcGGSs3MxUjETMVIwc1IRWhWlpaWogBatRXAXhYiUpKAAMAKf/sAVEB/AAVAB0AJQBoQBEUEQIEAh8XAgUECQYCAAUDR0uwGlBYQB8AAwMlSAAEBAJYAAICJUgABQUAWQAAACNIAAEBIwFJG0AfAAMCA28AAQABcAAEBAJYAAICJUgABQUAWQAAACMASVlACTU2EjUSMgYGGisBERQrASInByM3JjURNDsBMhc3MwcWBxU3JisBIgYTNQcWOwEyNgFPXWsRDQs1FRNcaxAPCjYVE85wBRI6Ewx3cAYSOhMLAY/+yVgDFy4WKAE3WQMXLhYw3PUFC/7F2/QFCwACACkAAAFBAosAEwAXAFq1BAEBBAFHS7AcUFhAHwAFBgAGBQBtAAYGJEgDAQAAJUgABAQBWQIBAQEjAUkbQBwABgUGbwAFAAVvAwEAACVIAAQEAVkCAQEBIwFJWUAKERMzEjIREAcGGysTMxEjNQYrASI1ETMRFBY7ATI2NQMjJzPnWloNMSRaWgwSIxIPCE1pZAHo/hgnJ1gBkP5+EgsQEwGbhAAAAgArAAABRQKLABMAFwBhtQQBAQQBR0uwHFBYQCAABQYABgUAbQcBBgYkSAMBAAAlSAAEBAFZAgEBASMBSRtAHQcBBgUGbwAFAAVvAwEAACVIAAQEAVkCAQEBIwFJWUAPFBQUFxQXFDMSMhEQCAYaKxMzESM1BisBIjURMxEUFjsBMjY1EwcjN+daWg0xJFpaDBIjEg9eaU1SAej+GCcnWAGQ/n4SCxATAh+EhAAAAgAgAAABRAKLABMAGgBiQAoUAQUGBAEBBAJHS7AcUFhAIAcBBQYABgUAbQAGBiRIAwEAACVIAAQEAVkCAQEBIwFJG0AdAAYFBm8HAQUABW8DAQAAJUgABAQBWQIBAQEjAUlZQAsRERQzEjIREAgGHCsTMxEjNQYrASI1ETMRFBY7ATI2NQMHIzczFyPnWloNMSRaWgwSIxIPNTReYGVfXQHo/hgnJ1gBkP5+EgsQEwHsUYSEAAADACAAAAFDAnsAAwAXABsAOUA2CAEDBgFHBwEBAQBWCQgCAAAiSAUBAgIlSAAGBgNZBAEDAyMDSRgYGBsYGxQzEjIREREQCgYcKxMzFSMXMxEjNQYrASI1ETMRFBY7ATI2NQMVIzXeZWUJWloNMSRaWgwSIxIPYWYCe141/hgnJ1gBkP5+EgsQEwIPXl4AAgAI/3YBTgKLAAMAEgB0tQ4BBAUBR0uwHFBYQCUHAQEABQABBW0AAAAkSAYBBQUlSAAEBCNIAAMDAlkIAQICJwJJG0AiAAABAG8HAQEFAW8GAQUFJUgABAQjSAADAwJZCAECAicCSVlAGAUEAAAQDw0MCwoIBgQSBRIAAwADEQkGFSsTNzMHAyM1MzI/ASMDMxsBMwMGiFJkaWE8KSsJCjFmXEpFW2cWAgeEhP1vSCEpAeD+ZwGZ/elbAAACACv/dgFBAp0ADwAdAEBAPQ4BBAAJAQEFAkcAAwMkSAAEBABYBgEAACVIAAUFAVgAAQEjSAACAicCSQIAHBkWEw0MCwoIBQAPAg8HBhQrEzMyFREUKwEiJxUjETMVNhMRNCYrASIVERQ7ATI2wyNbWyMxDVpZDVULEiQgICQSCwHoWf7JWCSuAyfcJ/5+ARwSCyP+8CMLAAMACP92AU4CewAOABIAFgBPQEwKAQIDAUcLCAoDBgYFVgcBBQUiSAQBAwMlSAACAiNIAAEBAFkJAQAAJwBJExMPDwEAExYTFhUUDxIPEhEQDAsJCAcGBAIADgEODAYUKxcjNTMyPwEjAzMbATMDBhM1MxUhNTMVdDwpKwkKMWZcSkVbZxYGZf7dZopIISkB4P5nAZn96VsCp15eXl4AAQAsAAAAhgHoAAMAE0AQAAEBJUgAAAAjAEkREAIGFiszIxEzhlpaAegAAgA1AAACGQJyABcAJwA8QDkPAQUDBAEBAAJHAAYABwAGB14IAQUFA1gEAQMDIkgJAQAAAVgCAQEBIwFJJiM0EREREjMyERAKBh0rJTMVIzUGKwEiNRE0OwEyFzUzFSMVMxUjBxE0JisBIgYVERQWOwEyNgFktf4VJk5dXU0mFveukJBfDBI6EgsLEjoSDEdHExNYAcJYExNHxEe8AaoSCwsS/lYSCwsAAwArAAACAwHoACUANAA+AJRADiMBBwAzAQkHFgEEAgNHS7ASUFhAKgADAQICA2UACQABAwkBXgoMAgcHAFgGCwIAACVICAECAgRZBQEEBCMESRtAKwADAQIBAwJtAAkAAQMJAV4KDAIHBwBYBgsCAAAlSAgBAgIEWQUBBAQjBElZQCEoJgIAPDk2NTAtJjQoNCEeGxgUEQ8OCwgFBAAlAiUNBhQrATMyHQEjFRQWOwEyNj0BMxUUKwEiJicOASsBIjURNDsBMhYXPgEHIyIGFREUFjsBMjY3ESYXMzU0JisBIgYVAVhOXcMLEi0SDFldTRomBwYlGkZaWkcZJgYGJncjEgwMEiMQEAEDXGoLEjASCwHoWbhyEgsLEig1WBgSEhhYATdZGRESGEkLEv7kEgsNEQEaHoRoEgsLEgAAAQBAAgcBZAKLAAYAOLUFAQEAAUdLsBxQWEANAwICAQABcAAAACQASRtACwAAAQBvAwICAQFmWUALAAAABgAGEREEBhYrEzczFyMnB0BgZV9dNTQCB4SEUVEAAgBpAfsBPAKYAA8AGwAcQBkAAQACAQJcAAAAA1gAAwMkAEkzNDUzBAYYKxM1NCYrASIGHQEUFjsBMjY3FRQrASI9ATQ7ATL9CREhEQkJESERCT9JQUlJQUkCRgcPCgoPBw8KChsRRkYRRgAAAQBGAhYBXgJ8ABcANkAzAAQDAQMEZQABAQNYBQEDAyJIAgYCAAADWAUBAwMiAEkBABMSDw4NCgcGBAIAFwEWBwYUKwEiJiMiHQEjNTQ2OwEyFjI2PQEzFRQGIwEKGkIJEU4kKgkZPxQITSIrAhYmFw8WKCgmCQ8OFycoAAEAGQEDAXQBSQADAB5AGwAAAQEAUgAAAAFWAgEBAAFKAAAAAwADEQMGFSsTNSEVGQFbAQNGRgAAAQAKAQMCDAFJAAMAHkAbAAABAQBSAAAAAVYCAQEAAUoAAAADAAMRAwYVKxM1IRUKAgIBA0ZGAAABACABbACFAnIABAAZQBYCAQABAUcAAAABVgABASIASRIQAgYWKxMjNTczhWVGHwFsQMYAAQAgAWwAhQJyAAQAGUAWAgEBAAFHAAEBAFYAAAAiAUkSEAIGFisTMxUHIyBlRh8CckHFAAEAIP90AIUAewAEABlAFgIBAQABRwAAAAFWAAEBJwFJEhACBhYrNzMVByMgZUUge0HGAAACACABbAEaAnIABAAJAB5AGwcCAgABAUcCAQAAAVYDAQEBIgBJEhESEAQGGCsTIzU3MxMjNTczhWVGH5VlRh8BbEDG/vpAxgACACABbAEaAnIABAAJAB5AGwcCAgEAAUcDAQEBAFYCAQAAIgFJEhESEAQGGCsTMxUHIwMzFQcjtWVGH5VlRh8CckHFAQZBxQACACD/dAEaAHsABAAJAB5AGwcCAgEAAUcCAQAAAVYDAQEBJwFJEhESEAQGGCs3MxUHIwMzFQcjtWVGH5VlRh97QcYBB0HGAAABAEMAvgFJAcQABwAYQBUAAAEBAFQAAAABWAABAAFMExICBhYrEjQ2MhYUBiJDTWxNTWwBC2xNTWxNAAADADQAAAImAHcAAwAHAAsAL0AsBAICAAABVggFBwMGBQEBIwFJCAgEBAAACAsICwoJBAcEBwYFAAMAAxEJBhUrITUzFSE1MxUhNTMVAblt/tFs/tFtd3d3d3d3AAEAJQBoAPwBngAFACVAIgQBAgEAAUcAAAEBAFIAAAABVgIBAQABSgAAAAUABRIDBhUrNyc3MwcXnHd3YHh4aJubnJoAAQAlAGgA/AGeAAUAH0AcAwACAAEBRwABAAABUgABAQBWAAABAEoSEQIGFisTByM3JzP8d2B5eWABA5uanAABAAL/iAFnArwAAwAmS7AcUFhACwAAAQBvAAEBJwFJG0AJAAABAG8AAQFmWbQREAIGFisBMwEjARNU/u9UArz8zAABABf//wFqAnIALwCUS7AJUFhANwAAAQIBAGUABwUGBgdlDAECCwEDBAIDXgoBBAkBBQcEBV4AAQENWAANDSJIAAYGCFkACAgjCEkbQDkAAAECAQACbQAHBQYFBwZtDAECCwEDBAIDXgoBBAkBBQcEBV4AAQENWAANDSJIAAYGCFkACAgjCElZQBYuKykoJyYlJCMiMhMzEREREzMQDgYdKwEjNTQmKwEiBh0BMxUjFTMVIxUUFjsBMjY9ATMVFCsBIj0BIzUzNSM1MzU0OwEyFQFqWwwSMxILV1dXVwsSMxIMW11qXi4uLi5eal0BrmASCwsSeUIzQ3oSCwsSX2pZWYVDM0KFWFgAAAEAFwEUAYEBXgADAB5AGwAAAQEAUgAAAAFWAgEBAAFKAAAAAwADEQMFFSsTNSEVFwFqARRKSgAAAQAC/4gBZwK8AAMAEUAOAAABAG8AAQFmERACBRYrATMBIwETVP7vVAK8/MwAAAAAAQAAANcAhAAKAAAAAAACAEAAUABzAAAAugtwAAAAAAAAAGMAYwBjAGMAYwBjAIkApgETAX0CBQKAApYCygL+A0QDbAOGA6EDuQPaBBMEMgSEBPsFLgWGBesGDwZvBt0HBgcwB0cHcQeHCBEIhQizCPsJVQmDCa0J0woxClkKbwqsCtsK9gskC0kLhwu4DAAMQAyxDNMM/g0eDUsNdA2XDcMN5Q4GDisOSw5+DqIO6w84D5IP3RBCEHMQyREBEScRWRGJEZ4R6hIgEl4SqxL2EyITrhPfFBAUMxRgFIcUvBTnFS4VThWWFdcV1xX8FksWsRcWF1oXfhggGEQY2xkeGVEZhBmfGicaQBp2Gqka9xttG5EbxRvxHAwcPRxcHJccwx1FHYAdvh4AHlweox8BH0If1iAMIEogjSDRIPghHyFOIX0huiIRIl0irCMAI2wjxCPhJFEkiCTDJQIlRiV5Ja8mMCagJxMniygGKGYo4Cm1Kkkq4Ct7LBwsoizbLRQtVi2HLdkuQC6jLwkvdS/lMD4wazDZMSwxgzHeMiUygzLPMx4zMzOHNCY0UzSJNMc04zT/NRk1MzVNNXE1lTW5Ndc2BTYnNkY2ZzbsNwg3HwABAAAAARsjWY8Xy18PPPUACwPoAAAAAM/ls+IAAAAA1TIQJv52/nYGhgNiAAAACAACAAAAAAAAAf4AAAAAAAABTQAAAAAAAAAAAAAAkQAAANEAMgEkACQB8AAeAWkAJgKBAB0BzQAmAKEAJAEIAC8BCP/rAUAAEAGYABcApQAgATsAMAClACABhgACAYYALwDrABQBSAAZAWMAHQFrAAYBZgAoAX4ALwEjAAYBhQAvAXoAJgClACAApQAgAaQAHQGkAB0BpAAdAVYAFAK4AEwBgQAMAZkANAF5AC8BnAA2AVcANAFMADQBfgAvAaEANADSADwBTgAOAYAANAEtADQCBAA0AZoANAGJAC8BegA0AYkALwGMADQBaQAmATYABQGNADEBbAAFAmEACgF1AAQBaQAEATkACAE3ADoBhgAfATcALAHyADcB2gAAAaUAdAFoACYBaAAsAV8AJgFoACYBaAAqAN4ABgFoACYBbQAsALIAKgCy//wBZQAsALIALAIkACwBbQAsAXIAJwFoACwBaAAmAO4ALAFIACEA2wAFAW0AKwE9AAQCHwAFAVYABQFWAAgBJAAOARsABwDFADYBGwAHAgwAPQCRAAAA0QAyAV8AJgGDACAB5AA6AX8ADQDFADQBowA1AaUAQQK9AC0BAQAZAeoAJQHMACIBOwAwAZ4AIgGlAEwBfwA2AaQAHQDKAA4A1gAPAaUAdgFzACgB+wAxANkAIgGlAIUAkQALAPgAGgHqACUBVgAUAYEADAGBAAwBgQAMAYEADAGBAAwBgQAMAkMACwF5AC8BVwApAVcANAFXACgBYgAlANL/2QDSADwA0v/YANL/1gGcAAMBmgA0AYkAKwGJAC8BiQAvAYkALwGFAC8BegAYAZEAMgGNADEBjQAxAY0AMQGNADEBaQAEAYEANQHdAC0BaAAmAWgAJgFoACQBaAAjAWgAIQFoACYCLQArAV8AJgFoACMBaAAqAWYAKAFmACIAsv/KALIALACy/8cAsv/HAXoAJgFtACIBcgAmAXIAJwFyACUBcgAmAXIAJwGcABkBegApAW0AKQFtACsBaQAgAWwAIAFWAAgBagArAV0ACACyACwCNAA1AkgAKwGlAEABpQBpAaUARgGkABkCGAAKAKYAIACmACAApQAgAToAIAE6ACABOgAgAYwAQwIrADQBIgAlASMAJQGGAAIBjwAXAbgAFwGGAAIAAQAAA77+JQAABnD+dvo8BoYAAQAAAAAAAAAAAAAAAAAAANcABAF5AZAABQAAAooCWAAAAEsCigJYAAABXgAyASQAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASVRGTwDAAAAiFQO+/iUAAAO+AdsgAACTAAAAAAHoAnIAAAAgAAcAAAACAAAAAwAAABQAAwABAAAAFAAEALgAAAAqACAABAAKAAAADQB+ALsA/wExAVMCxgLaAtwgFCAaIB4gIiAmIDogRCCsIhIiFf//AAAAAAANACAAoAC/ATEBUgLGAtoC3CATIBggHCAiICYgOSBEIKwiEiIV//8AA//3/+X/xP/B/5D/cP3+/ev96uC04LHgsOCt4KrgmOCP4Cjew97BAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwACwgsABVWEVZICBLuAANUUuwBlNaWLA0G7AoWWBmIIpVWLACJWG5CAAIAGNjI2IbISGwAFmwAEMjRLIAAQBDYEItsAEssCBgZi2wAiwgZCCwwFCwBCZasigBCkNFY0VSW1ghIyEbilggsFBQWCGwQFkbILA4UFghsDhZWSCxAQpDRWNFYWSwKFBYIbEBCkNFY0UgsDBQWCGwMFkbILDAUFggZiCKimEgsApQWGAbILAgUFghsApgGyCwNlBYIbA2YBtgWVlZG7ABK1lZI7AAUFhlWVktsAMsIEUgsAQlYWQgsAVDUFiwBSNCsAYjQhshIVmwAWAtsAQsIyEjISBksQViQiCwBiNCsQEKQ0VjsQEKQ7ACYEVjsAMqISCwBkMgiiCKsAErsTAFJbAEJlFYYFAbYVJZWCNZISCwQFNYsAErGyGwQFkjsABQWGVZLbAFLLAHQyuyAAIAQ2BCLbAGLLAHI0IjILAAI0JhsAJiZrABY7ABYLAFKi2wBywgIEUgsAtDY7gEAGIgsABQWLBAYFlmsAFjYESwAWAtsAgssgcLAENFQiohsgABAENgQi2wCSywAEMjRLIAAQBDYEItsAosICBFILABKyOwAEOwBCVgIEWKI2EgZCCwIFBYIbAAG7AwUFiwIBuwQFlZI7AAUFhlWbADJSNhRESwAWAtsAssICBFILABKyOwAEOwBCVgIEWKI2EgZLAkUFiwABuwQFkjsABQWGVZsAMlI2FERLABYC2wDCwgsAAjQrILCgNFWCEbIyFZKiEtsA0ssQICRbBkYUQtsA4ssAFgICCwDENKsABQWCCwDCNCWbANQ0qwAFJYILANI0JZLbAPLCCwEGJmsAFjILgEAGOKI2GwDkNgIIpgILAOI0IjLbAQLEtUWLEEZERZJLANZSN4LbARLEtRWEtTWLEEZERZGyFZJLATZSN4LbASLLEAD0NVWLEPD0OwAWFCsA8rWbAAQ7ACJUKxDAIlQrENAiVCsAEWIyCwAyVQWLEBAENgsAQlQoqKIIojYbAOKiEjsAFhIIojYbAOKiEbsQEAQ2CwAiVCsAIlYbAOKiFZsAxDR7ANQ0dgsAJiILAAUFiwQGBZZrABYyCwC0NjuAQAYiCwAFBYsEBgWWawAWNgsQAAEyNEsAFDsAA+sgEBAUNgQi2wEywAsQACRVRYsA8jQiBFsAsjQrAKI7ACYEIgYLABYbUQEAEADgBCQopgsRIGK7ByKxsiWS2wFCyxABMrLbAVLLEBEystsBYssQITKy2wFyyxAxMrLbAYLLEEEystsBkssQUTKy2wGiyxBhMrLbAbLLEHEystsBwssQgTKy2wHSyxCRMrLbAeLACwDSuxAAJFVFiwDyNCIEWwCyNCsAojsAJgQiBgsAFhtRAQAQAOAEJCimCxEgYrsHIrGyJZLbAfLLEAHistsCAssQEeKy2wISyxAh4rLbAiLLEDHistsCMssQQeKy2wJCyxBR4rLbAlLLEGHistsCYssQceKy2wJyyxCB4rLbAoLLEJHistsCksIDywAWAtsCosIGCwEGAgQyOwAWBDsAIlYbABYLApKiEtsCsssCorsCoqLbAsLCAgRyAgsAtDY7gEAGIgsABQWLBAYFlmsAFjYCNhOCMgilVYIEcgILALQ2O4BABiILAAUFiwQGBZZrABY2AjYTgbIVktsC0sALEAAkVUWLABFrAsKrABFTAbIlktsC4sALANK7EAAkVUWLABFrAsKrABFTAbIlktsC8sIDWwAWAtsDAsALABRWO4BABiILAAUFiwQGBZZrABY7ABK7ALQ2O4BABiILAAUFiwQGBZZrABY7ABK7AAFrQAAAAAAEQ+IzixLwEVKi2wMSwgPCBHILALQ2O4BABiILAAUFiwQGBZZrABY2CwAENhOC2wMiwuFzwtsDMsIDwgRyCwC0NjuAQAYiCwAFBYsEBgWWawAWNgsABDYbABQ2M4LbA0LLECABYlIC4gR7AAI0KwAiVJiopHI0cjYSBYYhshWbABI0KyMwEBFRQqLbA1LLAAFrAEJbAEJUcjRyNhsAlDK2WKLiMgIDyKOC2wNiywABawBCWwBCUgLkcjRyNhILAEI0KwCUMrILBgUFggsEBRWLMCIAMgG7MCJgMaWUJCIyCwCEMgiiNHI0cjYSNGYLAEQ7ACYiCwAFBYsEBgWWawAWNgILABKyCKimEgsAJDYGQjsANDYWRQWLACQ2EbsANDYFmwAyWwAmIgsABQWLBAYFlmsAFjYSMgILAEJiNGYTgbI7AIQ0awAiWwCENHI0cjYWAgsARDsAJiILAAUFiwQGBZZrABY2AjILABKyOwBENgsAErsAUlYbAFJbACYiCwAFBYsEBgWWawAWOwBCZhILAEJWBkI7ADJWBkUFghGyMhWSMgILAEJiNGYThZLbA3LLAAFiAgILAFJiAuRyNHI2EjPDgtsDgssAAWILAII0IgICBGI0ewASsjYTgtsDkssAAWsAMlsAIlRyNHI2GwAFRYLiA8IyEbsAIlsAIlRyNHI2EgsAUlsAQlRyNHI2GwBiWwBSVJsAIlYbkIAAgAY2MjIFhiGyFZY7gEAGIgsABQWLBAYFlmsAFjYCMuIyAgPIo4IyFZLbA6LLAAFiCwCEMgLkcjRyNhIGCwIGBmsAJiILAAUFiwQGBZZrABYyMgIDyKOC2wOywjIC5GsAIlRlJYIDxZLrErARQrLbA8LCMgLkawAiVGUFggPFkusSsBFCstsD0sIyAuRrACJUZSWCA8WSMgLkawAiVGUFggPFkusSsBFCstsD4ssDUrIyAuRrACJUZSWCA8WS6xKwEUKy2wPyywNiuKICA8sAQjQoo4IyAuRrACJUZSWCA8WS6xKwEUK7AEQy6wKystsEAssAAWsAQlsAQmIC5HI0cjYbAJQysjIDwgLiM4sSsBFCstsEEssQgEJUKwABawBCWwBCUgLkcjRyNhILAEI0KwCUMrILBgUFggsEBRWLMCIAMgG7MCJgMaWUJCIyBHsARDsAJiILAAUFiwQGBZZrABY2AgsAErIIqKYSCwAkNgZCOwA0NhZFBYsAJDYRuwA0NgWbADJbACYiCwAFBYsEBgWWawAWNhsAIlRmE4IyA8IzgbISAgRiNHsAErI2E4IVmxKwEUKy2wQiywNSsusSsBFCstsEMssDYrISMgIDywBCNCIzixKwEUK7AEQy6wKystsEQssAAVIEewACNCsgABARUUEy6wMSotsEUssAAVIEewACNCsgABARUUEy6wMSotsEYssQABFBOwMiotsEcssDQqLbBILLAAFkUjIC4gRoojYTixKwEUKy2wSSywCCNCsEgrLbBKLLIAAEErLbBLLLIAAUErLbBMLLIBAEErLbBNLLIBAUErLbBOLLIAAEIrLbBPLLIAAUIrLbBQLLIBAEIrLbBRLLIBAUIrLbBSLLIAAD4rLbBTLLIAAT4rLbBULLIBAD4rLbBVLLIBAT4rLbBWLLIAAEArLbBXLLIAAUArLbBYLLIBAEArLbBZLLIBAUArLbBaLLIAAEMrLbBbLLIAAUMrLbBcLLIBAEMrLbBdLLIBAUMrLbBeLLIAAD8rLbBfLLIAAT8rLbBgLLIBAD8rLbBhLLIBAT8rLbBiLLA3Ky6xKwEUKy2wYyywNyuwOystsGQssDcrsDwrLbBlLLAAFrA3K7A9Ky2wZiywOCsusSsBFCstsGcssDgrsDsrLbBoLLA4K7A8Ky2waSywOCuwPSstsGossDkrLrErARQrLbBrLLA5K7A7Ky2wbCywOSuwPCstsG0ssDkrsD0rLbBuLLA6Ky6xKwEUKy2wbyywOiuwOystsHAssDorsDwrLbBxLLA6K7A9Ky2wciyzCQQCA0VYIRsjIVlCK7AIZbADJFB4sAEVMC0AS7gAMlJYsQEBjlmwAbkIAAgAY3CxAAVCsy0ZAgAqsQAFQrUgCA4HAggqsQAFQrUqBhcFAggqsQAHQrsIQAPAAAIACSqxAAlCuwBAAEAAAgAJKrEDAESxJAGIUViwQIhYsQNkRLEmAYhRWLoIgAABBECIY1RYsQMARFlZWVm1IggQBwIMKrgB/4WwBI2xAgBEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABaAFoBsQBGAEYDMwJyAnIAAP8ZA+j+DAM2AnQCcgAA/xkD6P4MAFkAWQBHAEcCcgAAAp0B6AAA/3YD6P4MAnIAAAKdAegAAP92A+j+DAAAAAgAZgADAAEECQAAAHAAAAADAAEECQABAAgAcAADAAEECQACAA4AeAADAAEECQADAC4AhgADAAEECQAEABgAtAADAAEECQAFAPAAzAADAAEECQAGABgBvAADAAEECQAOADQB1ABDAG8AcAB5AHIAaQBnAGgAdAAgADIAMAAxADQAIABJAG4AZABpAGEAbgAgAFQAeQBwAGUAIABGAG8AdQBuAGQAcgB5AC4AIABBAGwAbAAgAHIAaQBnAGgAdABzACAAcgBlAHMAZQByAHYAZQBkAC4AVABlAGsAbwBSAGUAZwB1AGwAYQByADEALgAxADAANgA7AEkAVABGAE8AOwBUAGUAawBvAC0AUgBlAGcAdQBsAGEAcgBUAGUAawBvACAAUgBlAGcAdQBsAGEAcgBWAGUAcgBzAGkAbwBuACAAMQAuADEAMAA2ADsAUABTACAAMQAuADAAOwBoAG8AdABjAG8AbgB2ACAAMQAuADAALgA3ADgAOwBtAGEAawBlAG8AdABmAC4AbABpAGIAMgAuADUALgA2ADEAOQAzADAAOwAgAHQAdABmAGEAdQB0AG8AaABpAG4AdAAgACgAdgAxAC4AMQApACAALQBsACAANwAgAC0AcgAgADIAOAAgAC0ARwAgADUAMAAgAC0AeAAgADEAMwAgAC0ARAAgAGwAYQB0AG4AIAAtAGYAIABkAGUAdgBhACAALQB3ACAARwBUAGUAawBvAC0AUgBlAGcAdQBsAGEAcgBoAHQAdABwADoALwAvAHMAYwByAGkAcAB0AHMALgBzAGkAbAAuAG8AcgBnAC8ATwBGAEwAAAACAAAAAAAA/7UAMgAAAAAAAAAAAAAAAAAAAAAAAAAAANcAAAABAAIBAgEDAAMABAAFAAYABwAIAAkACgALAAwADQAOAA8AEAARABIAEwAUABUAFgAXABgAGQAaABsAHAAdAB4AHwAgACEAIgAjACQAJQAmACcAKAApACoAKwAsAC0ALgAvADAAMQAyADMANAA1ADYANwA4ADkAOgA7ADwAPQA+AD8AQABBAEIAQwBEAEUARgBHAEgASQBKAEsATABNAE4ATwBQAFEAUgBTAFQAVQBWAFcAWABZAFoAWwBcAF0AXgBfAGAAYQEEAKMAhACFAL0AlgDoAIYAjgCLAJ0AqQCkAQUAigDaAIMAkwDyAPMAjQEGAIgAwwDeAPEAngCqAKIArQDJAMcArgBiAGMAkABkAMsAZQDIAMoAzwDMAM0AzgDpAGYA0wDQANEArwBnAPAAkQDWANQA1QBoAOsA7QCJAGoAaQBrAG0AbABuAKAAbwBxAHAAcgBzAHUAdAB2AHcA6gB4AHoAeQB7AH0AfAC4AKEAfwB+AIAAgQDsAO4AugDXALAAsQDYAN0A2QCyALMAtgC3AMQAtAC1AMUAhwCrAL4AvwC8AQcA7wEIBE5VTEwCQ1IHdW5pMDBBMAd1bmkwMEFEB3VuaTAwQjUERXVybwd1bmkyMjE1AAAAAAEAAf//AA8AAQAAAAwAAAAAAAAAAgABAAMA1gABAAAAAQAAAAoADAAOAAAAAAAAAAEAAAAKABYAGAABZGV2YQAIAAAAAAAAAAAAAA==') format('truetype');}", "html,body{margin:0;padding:0;font-family:-apple-system,BlinkMacSystemFont,Ubuntu, 'Helvetica Neue',sans-serif;font-size:16px;background:#fff;}"]
      }), _react.default.createElement("div", {
        className: "jsx-1436450221" + " " + (layoutClasses || "")
      }, error ? _react.default.createElement(_Error.default, {
        error: error
      }) : children), _react.default.createElement(_style.default, {
        styleId: "799073679",
        css: [".layout.jsx-1436450221{-webkit-transition:filter 1s;transition:filter 1s;}", ".layout-static.jsx-1436450221{-webkit-filter:grayscale(100%);filter:grayscale(100%);}"]
      }));
    }
  }]);

  return Layout;
}(_react.Component);

function mapStateToProps(_ref2) {
  var jsReady = _ref2.jsReady;
  return {
    jsReady: jsReady
  };
}

function mapDispatchToProps(dispatch) {
  return {
    jsLoad: function jsLoad() {
      return dispatch({
        type: 'JS_LOAD'
      });
    }
  };
}

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(Layout);

exports.default = _default;

/***/ }),
/* 41 */
/***/ (function(module, exports) {

module.exports = require("clipboard");

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _classnames = _interopRequireDefault(__webpack_require__(9));

var _react = _interopRequireWildcard(__webpack_require__(0));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var PlayerInfo =
/*#__PURE__*/
function (_Component) {
  _inherits(PlayerInfo, _Component);

  function PlayerInfo() {
    _classCallCheck(this, PlayerInfo);

    return _possibleConstructorReturn(this, _getPrototypeOf(PlayerInfo).apply(this, arguments));
  }

  _createClass(PlayerInfo, [{
    key: "renderMissingPlayer",
    value: function renderMissingPlayer() {
      var _this$props = this.props,
          isPlayer1 = _this$props.isPlayer1,
          onSelect = _this$props.onSelect;
      var classes = (0, _classnames.default)('player-info', {
        selectable: Boolean(onSelect)
      });
      return _react.default.createElement("div", {
        onClick: onSelect,
        className: "jsx-1285042675" + " " + (classes || "")
      }, _react.default.createElement("div", {
        className: "jsx-1285042675" + " " + "centered"
      }, _react.default.createElement("div", {
        className: "jsx-1285042675" + " " + "player"
      }, isPlayer1 ? '1p' : '2p'), _react.default.createElement("div", {
        className: "jsx-1285042675" + " " + "insert-coin"
      }, "insert coin")), _react.default.createElement(_style.default, {
        styleId: "1285042675",
        css: [".player-info.jsx-1285042675{position:absolute;width:100%;height:100%;background:#ecf0f1;color:#9ba4ab;font-family:'Teko',sans-serif;font-weight:300;-webkit-letter-spacing:0.02em;-moz-letter-spacing:0.02em;-ms-letter-spacing:0.02em;letter-spacing:0.02em;text-transform:uppercase;white-space:nowrap;text-align:center;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:default;}", ".selectable.jsx-1285042675{background:#ecf0f1 radial-gradient(rgba(255,255,255,0.8) 0%,transparent 50%);background-size:400% 400%;background-position:100% 100%;-webkit-animation:wave-jsx-1285042675 5s 5s linear infinite;animation:wave-jsx-1285042675 5s 5s linear infinite;cursor:pointer;}", ".centered.jsx-1285042675{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);transform:translate(-50%,-50%);}", ".player.jsx-1285042675{font-size:3.2em;line-height:0.9em;font-weight:400;}", ".insert-coin.jsx-1285042675{font-size:1.6em;line-height:1em;}", "@-webkit-keyframes wave-jsx-1285042675{0%{background-position:100% 100%;}20%{background-position:0% 0%;}100%{background-position:0% 0%;}}", "@keyframes wave-jsx-1285042675{0%{background-position:100% 100%;}20%{background-position:0% 0%;}100%{background-position:0% 0%;}}"]
      }));
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          player = _this$props2.player,
          wins = _this$props2.wins,
          showReadyState = _this$props2.showReadyState;

      if (!player) {
        return this.renderMissingPlayer();
      }

      var user = player.user,
          score = player.score,
          lines = player.lines;
      var showWins = typeof wins === 'number';
      var humanizedScore = humanizeNumber(score);
      return _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "player-info"
      }, _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "name"
      }, _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "vcentered ellipsis"
      }, user.name)), _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "score"
      }, showWins && _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "score-row"
      }, _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "label vcentered"
      }, "Wins"), _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "value vcentered"
      }, wins)), _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "score-row"
      }, _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "label vcentered"
      }, "Score"), _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "value vcentered"
      }, humanizedScore)), !showWins && _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "score-row"
      }, _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "label vcentered"
      }, "Lines"), _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "value vcentered"
      }, lines))), showReadyState && _react.default.createElement("div", {
        className: "jsx-2355917317" + " " + "status ready"
      }, _react.default.createElement("span", {
        className: "jsx-2355917317"
      }, "Ready")), _react.default.createElement(_style.default, {
        styleId: "2355917317",
        css: [".player-info.jsx-2355917317{position:absolute;width:100%;height:100%;color:#34495f;font-family:'Teko',sans-serif;font-size:1.8em;line-height:1em;font-weight:300;text-transform:uppercase;-webkit-letter-spacing:0.02em;-moz-letter-spacing:0.02em;-ms-letter-spacing:0.02em;letter-spacing:0.02em;}", ".name.jsx-2355917317{position:relative;height:calc(100% / 3);white-space:nowrap;font-weight:400;}", ".ellipsis.jsx-2355917317{overflow:hidden;text-overflow:ellipsis;}", ".score.jsx-2355917317{position:absolute;top:calc(100% / 3);width:100%;height:calc(100% / 3 * 2);}", ".score-row.jsx-2355917317{position:relative;width:100%;height:50%;}", ".label.jsx-2355917317{position:absolute;top:0;left:0;color:rgba(155,164,171,0.8);}", ".value.jsx-2355917317{position:absolute;top:0;right:0;color:#3993d0;}", ".ready.jsx-2355917317{position:absolute;top:calc(100% / 3);bottom:0;left:0;right:0;background:rgba(57,147,208,0.85);color:#fff;font-size:1.2em;font-weight:600;text-transform:uppercase;text-align:center;}", ".ready.jsx-2355917317 span.jsx-2355917317{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-35%);-ms-transform:translate(-50%,-35%);transform:translate(-50%,-35%);text-shadow:1px 1px 0 rgba(0,0,0,0.1);}", ".vcentered.jsx-2355917317{position:absolute;max-width:100%;top:50%;-webkit-transform:translate(0,-40%);-ms-transform:translate(0,-40%);transform:translate(0,-40%);}"]
      }));
    }
  }]);

  return PlayerInfo;
}(_react.Component);

exports.default = PlayerInfo;

function humanizeNumber(nr) {
  if (nr < 10000) {
    return String(nr);
  }

  var round = Math.round;
  var thousands = nr / 1000;
  var rounded = thousands > 100 ? round(thousands) : round(thousands * 10) / 10;
  return "".concat(rounded, "K");
}

/***/ }),
/* 43 */
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),
/* 44 */
/***/ (function(module, exports) {

module.exports = require("cookie");

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getValidUser = getValidUser;

function getValidUser(user) {
  if (!user || typeof user.id !== 'string' || typeof user.name !== 'string') {
    throw new Error("Invalid user: ".concat(String(user)));
  }

  return {
    id: user.id,
    name: user.name
  };
}

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SocketProvider = void 0;

var _regenerator = _interopRequireDefault(__webpack_require__(6));

var _propTypes = __webpack_require__(11);

var _lodash = __webpack_require__(7);

var _react = __webpack_require__(0);

var _api = __webpack_require__(8);

var _socket = __webpack_require__(47);

var _rollbarClient = __webpack_require__(22);

var _backfill = __webpack_require__(49);

var _game = __webpack_require__(4);

var _global = __webpack_require__(12);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _getSocket = (0, _socket.getSocket)(),
    subscribe = _getSocket.subscribe,
    keepGameAlive = _getSocket.keepGameAlive,
    broadcastAction = _getSocket.broadcastAction,
    onGameAction = _getSocket.onGameAction,
    offGameAction = _getSocket.offGameAction,
    onGameKeepAlive = _getSocket.onGameKeepAlive,
    offGameKeepAlive = _getSocket.offGameKeepAlive,
    onGameRemoved = _getSocket.onGameRemoved,
    offGameRemoved = _getSocket.offGameRemoved,
    onGameSync = _getSocket.onGameSync,
    offGameSync = _getSocket.offGameSync,
    onStatsUpdate = _getSocket.onStatsUpdate,
    offStatsUpdate = _getSocket.offStatsUpdate;

var SocketProvider =
/*#__PURE__*/
function (_Component) {
  _inherits(SocketProvider, _Component);

  function SocketProvider() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, SocketProvider);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(SocketProvider)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "pendingBackfills", {});

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "pendingGames", []);

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleGameAction", function (action) {
      // console.log('[SOCKET] On game-action', action);
      var _this$getStore = _this.getStore(),
          getState = _this$getStore.getState,
          dispatch = _this$getStore.dispatch;

      var state = getState();
      var backfills = state.backfills;
      var _action$payload = action.payload,
          actionId = _action$payload.actionId,
          gameId = _action$payload.gameId;

      if (backfills[gameId]) {
        dispatch((0, _global.queueGameAction)(action));
      } else {
        var game = state.games[gameId];

        if (!game) {
          _this.loadGame(gameId);
        } else {
          var offset = (0, _game.getGameActionOffset)(game, action);

          if (offset > 0) {
            // The action will be discarded for now, but it will show up again
            // during backfill
            console.log("Requesting backfill due to detached action ".concat(actionId));

            _this.startBackfill(gameId);
          } else if (offset < 0) {
            // Sometimes we get receive some delayed actions via websocket that
            // were already returned by latest backfill
            console.log("Discarding past game action ".concat(actionId));
          } else {
            dispatch(action);
          }
        }
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleGameKeepAlive", function (gameId) {
      var _this$getStore2 = _this.getStore(),
          getState = _this$getStore2.getState;

      var _getState = getState(),
          games = _getState.games;

      if (!games[gameId]) {
        _this.loadGame(gameId);
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleGameRemoved", function (gameId) {
      console.log('Received server notice of removed game', gameId);

      var _this$getStore3 = _this.getStore(),
          dispatch = _this$getStore3.dispatch;

      dispatch((0, _global.removeGame)(gameId));
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleGameSync", function (game) {
      console.log('Received game state from server', game.id);

      var _this$getStore4 = _this.getStore(),
          getState = _this$getStore4.getState,
          dispatch = _this$getStore4.dispatch;

      var _getState2 = getState(),
          games = _getState2.games;

      if (games[game.id]) {
        // NOTE: Use ADD_GAME action to update an existing game
        dispatch((0, _global.addGame)(game));
      } else {
        console.warn('Recevied game sync for missing game', game.id);
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleStatsUpdate", function (stats) {
      var _this$getStore5 = _this.getStore(),
          dispatch = _this$getStore5.dispatch;

      dispatch((0, _global.updateStats)(stats));
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleSubscribe", function (roomId) {
      subscribe(roomId);

      if (roomId !== 'global') {
        _this.startBackfill(roomId);
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleBackfillComplete", function (_ref) {
      var gameId = _ref.gameId,
          backfillId = _ref.backfillId,
          backfillRes = _ref.backfillRes;

      if (!_this.pendingBackfills[gameId]) {
        console.warn("Game id for completed backfill missing");
        return;
      }

      if (backfillId !== _this.pendingBackfills[gameId]) {
        console.warn("Completed backfill doesn't match pending backfill id");
        return;
      } // Up to this point we can consider the backfill to have been canceled (via
      // unmount) or invalidated (via newer backfill for same gamed id). From here
      // on the backfill either succeded or failed, but the response is expected.


      var _this$getStore6 = _this.getStore(),
          getState = _this$getStore6.getState,
          dispatch = _this$getStore6.dispatch;

      var _getState3 = getState(),
          backfills = _getState3.backfills,
          games = _getState3.games;

      var game = games[gameId];
      _this.pendingBackfills = (0, _lodash.omit)(_this.pendingBackfills, gameId);
      dispatch((0, _global.endBackfill)(backfillId));

      if (!backfillRes) {
        console.warn("Backfill failed, removing game ".concat(gameId, " from state"));
        dispatch((0, _global.removeGame)(gameId));
        return;
      }

      if (!game) {
        console.warn("Backfill completed for missing game ".concat(gameId));
        return;
      }

      var actions = backfillRes.actions;
      var queuedActions = backfills[gameId].queuedActions;
      var mergedActions = [].concat(_toConsumableArray(actions), _toConsumableArray(queuedActions));
      var uniqActions = (0, _lodash.uniqWith)(mergedActions, compareGameActions);
      var sortedActions = (0, _lodash.sortBy)(uniqActions, function (a) {
        return a.payload.actionId;
      }); // TODO: Dispatch events at an interval, to convey the rhythm in
      // which the actions were originally performed

      try {
        sortedActions.forEach(dispatch);
        var numDupes = mergedActions.length - uniqActions.length;
        console.log("Backfilled ".concat(uniqActions.length, " actions (").concat(numDupes, " dupes)."));
      } catch (err) {
        // Error minigation: Sometimes client state gets out of sync with server
        // state. In this case we remove the client state and await for a new
        // game action, which will then fetch the game from scratch from server.
        dispatch((0, _global.removeGame)(gameId));
        (0, _rollbarClient.logError)("Backfill failed", {
          game: game,
          backfillRes: backfillRes,
          queuedActions: queuedActions,
          sortedActions: sortedActions
        });
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleBroadcastGameAction", function (action) {
      var _this$getStore7 = _this.getStore(),
          getState = _this$getStore7.getState,
          dispatch = _this$getStore7.dispatch;

      var _getState4 = getState(),
          backfills = _getState4.backfills,
          curGame = _getState4.curGame;

      if (!curGame) {
        console.warn('Action broadcast with no current game denied');
        return;
      } // Disallow user to mutate state until it's up to date with server


      if (backfills[curGame]) {
        console.warn('Action broadcast denied while backfilling');
        return;
      }

      var prevGames = getState().games; // I don't know how to statically determine that the game related thunk
      // actions will return a GameAction type $FlowFixMe

      var resAction = dispatch(action);
      var gameId = resAction.payload.gameId; // COOL: Don't broacast actions that don't alter the state (ie. MOVE_RIGHT
      // actions when the falling Tetromino is already hitting the right wall)

      if (getState().games[gameId] === prevGames[gameId]) {// The following console.log call is only useful for debugging (it floods
        // the console!)
        // console.log(`Not broadcasting noop action ${resAction.type}`);
      } else {
        broadcastAction(resAction);
      } // Allow callers to chain broadcasted actions


      return resAction;
    });

    return _this;
  }

  _createClass(SocketProvider, [{
    key: "getChildContext",
    value: function getChildContext() {
      return {
        subscribe: this.handleSubscribe,
        keepGameAlive: keepGameAlive,
        broadcastGameAction: this.handleBroadcastGameAction,
        onGameKeepAlive: onGameKeepAlive,
        offGameKeepAlive: offGameKeepAlive
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      onGameAction(this.handleGameAction);
      onGameKeepAlive(this.handleGameKeepAlive);
      onGameRemoved(this.handleGameRemoved);
      onGameSync(this.handleGameSync);
      onStatsUpdate(this.handleStatsUpdate);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      var _this2 = this;

      var _this$getStore8 = this.getStore(),
          dispatch = _this$getStore8.dispatch;

      offGameAction(this.handleGameAction);
      offGameKeepAlive(this.handleGameKeepAlive);
      offGameRemoved(this.handleGameRemoved);
      offGameSync(this.handleGameSync);
      offStatsUpdate(this.handleStatsUpdate);
      Object.keys(this.pendingBackfills).forEach(function (gameId) {
        dispatch((0, _global.endBackfill)(_this2.pendingBackfills[gameId]));
      }); // All future backfill callbacks will be ignored

      this.pendingBackfills = {};
    }
  }, {
    key: "loadGame",
    value: function () {
      var _loadGame = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(gameId) {
        var _this$getStore9, dispatch, game;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!(this.pendingGames.indexOf(gameId) !== -1)) {
                  _context.next = 2;
                  break;
                }

                return _context.abrupt("return");

              case 2:
                console.log("Detected new game ".concat(gameId, "...")); // Actions come quick, so make sure to not request the game multiple times

                this.pendingGames = [].concat(_toConsumableArray(this.pendingGames), [gameId]);
                _this$getStore9 = this.getStore(), dispatch = _this$getStore9.dispatch;
                _context.next = 7;
                return (0, _api.getGame)(gameId);

              case 7:
                game = _context.sent;
                dispatch((0, _global.addGame)(game));
                this.pendingGames = (0, _lodash.without)(this.pendingGames, gameId);

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function loadGame(_x) {
        return _loadGame.apply(this, arguments);
      }

      return loadGame;
    }()
  }, {
    key: "startBackfill",
    value: function startBackfill(gameId) {
      console.log("Backfilling ".concat(gameId, "..."));

      var _this$getStore10 = this.getStore(),
          getState = _this$getStore10.getState,
          dispatch = _this$getStore10.dispatch;

      var _getState5 = getState(),
          games = _getState5.games;

      var backfillId = (0, _backfill.requestBackfill)(games[gameId], this.handleBackfillComplete);
      this.pendingBackfills[gameId] = backfillId;
      dispatch((0, _global.startBackfill)(gameId, backfillId));
    }
  }, {
    key: "render",
    value: function render() {
      return this.props.children;
    }
  }, {
    key: "getStore",
    value: function getStore() {
      return this.props.store;
    }
  }]);

  return SocketProvider;
}(_react.Component);

exports.SocketProvider = SocketProvider;

_defineProperty(SocketProvider, "childContextTypes", {
  subscribe: _propTypes.func.isRequired,
  keepGameAlive: _propTypes.func.isRequired,
  broadcastGameAction: _propTypes.func.isRequired,
  onGameKeepAlive: _propTypes.func.isRequired,
  offGameKeepAlive: _propTypes.func.isRequired
});

function compareGameActions(a1, a2) {
  return a1.payload.actionId === a2.payload.actionId && a1.payload.userId === a2.payload.userId && a1.payload.gameId === a2.payload.gameId;
}

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getSocket = getSocket;

var _socket = _interopRequireDefault(__webpack_require__(48));

var _api = __webpack_require__(8);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var socket;

function getSocket() {
  if (!socket) {
    socket = (0, _socket.default)((0, _api.getApiUrl)());
  }

  function subscribe(roomId) {
    console.log('[SOCKET] subscribe', roomId);
    socket.emit('subscribe', roomId);
  }

  function keepGameAlive(gameId) {
    console.log('[SOCKET] keep-alive', gameId);
    socket.emit('game-keep-alive', gameId);
  }

  function broadcastAction(action) {
    // console.log('[SOCKET] game-action', resAction);
    socket.emit('game-action', action);
  }

  function onGameAction(handler) {
    socket.on('game-action', handler);
  }

  function offGameAction(handler) {
    socket.off('game-action', handler);
  }

  function onGameKeepAlive(handler) {
    socket.on('game-keep-alive', handler);
  }

  function offGameKeepAlive(handler) {
    socket.off('game-keep-alive', handler);
  }

  function onGameRemoved(handler) {
    socket.on('game-removed', handler);
  }

  function offGameRemoved(handler) {
    socket.off('game-removed', handler);
  }

  function onGameSync(handler) {
    socket.on('game-sync', handler);
  }

  function offGameSync(handler) {
    socket.off('game-sync', handler);
  }

  function onStatsUpdate(handler) {
    socket.on('stats', handler);
  }

  function offStatsUpdate(handler) {
    socket.off('stats', handler);
  }

  return {
    subscribe: subscribe,
    keepGameAlive: keepGameAlive,
    broadcastAction: broadcastAction,
    onGameAction: onGameAction,
    offGameAction: offGameAction,
    onGameKeepAlive: onGameKeepAlive,
    offGameKeepAlive: offGameKeepAlive,
    onGameRemoved: onGameRemoved,
    offGameRemoved: offGameRemoved,
    onGameSync: onGameSync,
    offGameSync: offGameSync,
    onStatsUpdate: onStatsUpdate,
    offStatsUpdate: offStatsUpdate
  };
}

/***/ }),
/* 48 */
/***/ (function(module, exports) {

module.exports = require("socket.io-client");

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.requestBackfill = requestBackfill;

var _api = __webpack_require__(8);

var lastBackfillId = 0;

function requestBackfill(game, onComplete) {
  var gameId = game.id;
  var backfillId = ++lastBackfillId;
  var backfillReq = getBackfillReq(game);
  console.log('Backfill request', backfillReq);
  (0, _api.backfillGameActions)(backfillReq).then(function (backfillRes) {
    onComplete({
      gameId: gameId,
      backfillId: backfillId,
      backfillRes: backfillRes
    });
  }, function () {
    // A null backfillRes will signal an error
    onComplete({
      gameId: gameId,
      backfillId: backfillId,
      backfillRes: null
    });
  });
  return backfillId;
}

function getBackfillReq(game) {
  return {
    gameId: game.id,
    players: game.players.map(function (p) {
      return {
        userId: p.user.id,
        from: p.lastActionId
      };
    })
  };
}

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.withSocket = withSocket;

var _regenerator = _interopRequireDefault(__webpack_require__(6));

var _propTypes = __webpack_require__(11);

var _react = _interopRequireWildcard(__webpack_require__(0));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function withSocket(CompType) {
  var syncActions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var SocketConnect =
  /*#__PURE__*/
  function (_Component) {
    _inherits(SocketConnect, _Component);

    function SocketConnect() {
      var _getPrototypeOf2;

      var _this;

      _classCallCheck(this, SocketConnect);

      for (var _len = arguments.length, _args = new Array(_len), _key = 0; _key < _len; _key++) {
        _args[_key] = arguments[_key];
      }

      _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(SocketConnect)).call.apply(_getPrototypeOf2, [this].concat(_args)));

      _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "createActionHandler", function (actionName) {
        return (
          /*#__PURE__*/
          _asyncToGenerator(
          /*#__PURE__*/
          _regenerator.default.mark(function _callee() {
            var broadcastGameAction,
                actionCreator,
                _args2 = arguments;
            return _regenerator.default.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    broadcastGameAction = _this.context.broadcastGameAction;
                    actionCreator = syncActions[actionName]; // NOTE: This must only run on the client!

                    return _context.abrupt("return", broadcastGameAction(actionCreator.apply(void 0, _args2)));

                  case 3:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }))
        );
      });

      return _this;
    }

    _createClass(SocketConnect, [{
      key: "getBoundHandlers",
      value: function getBoundHandlers() {
        var _this2 = this;

        return Object.keys(syncActions).reduce(function (acc, actionName) {
          return _objectSpread({}, acc, _defineProperty({}, actionName, _this2.createActionHandler(actionName)));
        }, {});
      }
    }, {
      key: "render",
      value: function render() {
        return _react.default.createElement(CompType, _extends({}, this.props, this.context, this.getBoundHandlers()));
      }
    }]);

    return SocketConnect;
  }(_react.Component);

  _defineProperty(SocketConnect, "displayName", "SocketConnect(".concat(CompType.displayName || CompType.name || 'UnnamedComponent', ")"));

  _defineProperty(SocketConnect, "contextTypes", {
    subscribe: _propTypes.func.isRequired,
    keepGameAlive: _propTypes.func.isRequired,
    broadcastGameAction: _propTypes.func.isRequired,
    onGameKeepAlive: _propTypes.func.isRequired,
    offGameKeepAlive: _propTypes.func.isRequired
  });

  return SocketConnect;
}

/***/ }),
/* 51 */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = PointerButton;

var _react = _interopRequireDefault(__webpack_require__(0));

var _Button = _interopRequireDefault(__webpack_require__(2));

var _events = __webpack_require__(67);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function PointerButton(_ref) {
  var children = _ref.children,
      onPress = _ref.onPress,
      onRelease = _ref.onRelease,
      rest = _objectWithoutProperties(_ref, ["children", "onPress", "onRelease"]);

  var pointerDownEvent = (0, _events.getPointerDownEvent)();
  var pointerUpEvent = (0, _events.getPointerUpEvent)();

  var props = _objectSpread({}, rest);

  if (!rest.disabled) {
    if (pointerDownEvent) {
      props = _objectSpread({}, props, _defineProperty({}, pointerDownEvent, onPress));
    }

    if (onRelease && pointerUpEvent) {
      props = _objectSpread({}, props, _defineProperty({}, pointerUpEvent, onRelease));
    }
  }

  return _react.default.createElement(_Button.default, props, children);
}

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ACTION_STATS_FLUSH_DELAY = exports.ACTION_STATS_FLUSH_INTERVAL = exports.GAME_EXPIRE_TIMEOUT = exports.GAME_INACTIVE_TIMEOUT = void 0;
var sec = 1000;
var min = 60 * sec;
var GAME_INACTIVE_TIMEOUT = 30 * sec;
exports.GAME_INACTIVE_TIMEOUT = GAME_INACTIVE_TIMEOUT;
var GAME_EXPIRE_TIMEOUT = 15 * min;
exports.GAME_EXPIRE_TIMEOUT = GAME_EXPIRE_TIMEOUT;
var ACTION_STATS_FLUSH_INTERVAL = 5 * sec;
exports.ACTION_STATS_FLUSH_INTERVAL = ACTION_STATS_FLUSH_INTERVAL;
var ACTION_STATS_FLUSH_DELAY = 1 * sec;
exports.ACTION_STATS_FLUSH_DELAY = ACTION_STATS_FLUSH_DELAY;

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _game = __webpack_require__(4);

var _Well = _interopRequireDefault(__webpack_require__(55));

var _GamePanel = _interopRequireDefault(__webpack_require__(20));

var _Flash = _interopRequireDefault(__webpack_require__(57));

var _Quake = _interopRequireDefault(__webpack_require__(58));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var GamePreview =
/*#__PURE__*/
function (_Component) {
  _inherits(GamePreview, _Component);

  function GamePreview() {
    _classCallCheck(this, GamePreview);

    return _possibleConstructorReturn(this, _getPrototypeOf(GamePreview).apply(this, arguments));
  }

  _createClass(GamePreview, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          curUser = _this$props.curUser,
          game = _this$props.game,
          screen = _this$props.screen,
          onSelectP2 = _this$props.onSelectP2,
          showFooter = _this$props.showFooter;
      var curPlayer = (0, _game.getCurPlayer)(game, curUser);
      var otherPlayer = (0, _game.getOtherPlayer)(game, curPlayer);
      return _react.default.createElement(_Quake.default, {
        player1: curPlayer,
        player2: otherPlayer
      }, _react.default.createElement("div", {
        className: "jsx-608550019" + " " + "well-container"
      }, otherPlayer && _react.default.createElement("div", {
        className: "jsx-608550019" + " " + "enemy-well"
      }, this.renderWell(otherPlayer)), _react.default.createElement(_Flash.default, {
        player: curPlayer
      }, this.renderWell(curPlayer))), screen, _react.default.createElement("div", {
        className: "jsx-608550019" + " " + "side-container"
      }, _react.default.createElement(_GamePanel.default, {
        curUser: curUser,
        game: game,
        onSelectP2: onSelectP2,
        showFooter: showFooter
      })), _react.default.createElement(_style.default, {
        styleId: "608550019",
        css: [".well-container.jsx-608550019{position:absolute;top:0;bottom:0;left:0;right:calc(100% / 16 * 6);background:#ecf0f1;}", ".enemy-well.jsx-608550019{position:absolute;top:0;bottom:0;left:0;right:0;opacity:0.15;-webkit-filter:grayscale(80%);filter:grayscale(80%);}", ".side-container.jsx-608550019{position:absolute;top:0;bottom:0;right:0;left:calc(100% / 16 * 10);background:#fff;}"]
      }));
    }
  }, {
    key: "renderWell",
    value: function renderWell(player) {
      var grid = player.grid,
          blocksCleared = player.blocksCleared,
          blocksPending = player.blocksPending,
          activeTetromino = player.activeTetromino,
          activeTetrominoGrid = player.activeTetrominoGrid,
          activeTetrominoPosition = player.activeTetrominoPosition;
      return _react.default.createElement(_Well.default, {
        grid: grid,
        blocksCleared: blocksCleared,
        blocksPending: blocksPending,
        activeTetromino: activeTetromino,
        activeTetrominoGrid: activeTetrominoGrid,
        activeTetrominoPosition: activeTetrominoPosition
      });
    }
  }]);

  return GamePreview;
}(_react.Component);

_defineProperty(GamePreview, "defaultProps", {
  showFooter: false
});

var _default = GamePreview;
exports.default = _default;

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _tetromino = __webpack_require__(3);

var _grid = __webpack_require__(16);

var _WellGrid = _interopRequireDefault(__webpack_require__(56));

var _Tetromino = _interopRequireDefault(__webpack_require__(21));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Well =
/*#__PURE__*/
function (_Component) {
  _inherits(Well, _Component);

  function Well() {
    _classCallCheck(this, Well);

    return _possibleConstructorReturn(this, _getPrototypeOf(Well).apply(this, arguments));
  }

  _createClass(Well, [{
    key: "shouldComponentUpdate",

    /**
     * A rectangular vertical shaft, where Tetrominoes fall into during a Flatris
     * game. The Well has configurable size and speed. Tetromino pieces can be
     * inserted inside the well and they will fall until they hit the bottom, and
     * eventually fill it. Whenever the pieces form a straight horizontal line it
     * will be cleared, emptying up space and allowing more pieces to enter
     * afterwards.
     */
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.grid !== this.props.grid || nextProps.blocksCleared !== this.props.blocksCleared || nextProps.blocksPending !== this.props.blocksPending || nextProps.activeTetromino !== this.props.activeTetromino || nextProps.activeTetrominoGrid !== this.props.activeTetrominoGrid || nextProps.activeTetrominoPosition !== this.props.activeTetrominoPosition;
    }
  }, {
    key: "getNumberOfRows",
    value: function getNumberOfRows() {
      return this.props.grid.length;
    }
  }, {
    key: "getNumberOfCols",
    value: function getNumberOfCols() {
      return this.props.grid[0].length;
    }
  }, {
    key: "getActiveTetrominoStyles",
    value: function getActiveTetrominoStyles() {
      var rows = this.getNumberOfRows();
      var cols = this.getNumberOfCols();

      var _getExactPosition = (0, _grid.getExactPosition)(this.props.activeTetrominoPosition),
          x = _getExactPosition.x,
          y = _getExactPosition.y;

      return {
        top: "".concat(100 / rows * y, "%"),
        left: "".concat(100 / cols * x, "%")
      };
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          grid = _this$props.grid,
          blocksCleared = _this$props.blocksCleared,
          blocksPending = _this$props.blocksPending,
          activeTetromino = _this$props.activeTetromino,
          activeTetrominoGrid = _this$props.activeTetrominoGrid;
      return _react.default.createElement("div", {
        className: "jsx-2914085764" + " " + "well"
      }, activeTetromino ? _react.default.createElement("div", {
        style: this.getActiveTetrominoStyles(),
        className: "jsx-2914085764" + " " + "active-tetromino"
      }, _react.default.createElement(_Tetromino.default, {
        color: _tetromino.COLORS[activeTetromino],
        grid: activeTetrominoGrid
      })) : null, _react.default.createElement(_WellGrid.default, {
        grid: grid,
        blocksCleared: blocksCleared,
        blocksPending: blocksPending
      }), _react.default.createElement(_style.default, {
        styleId: "2914085764",
        css: [".well.jsx-2914085764{position:absolute;width:100%;height:100%;overflow:hidden;}", ".well.jsx-2914085764 .active-tetromino.jsx-2914085764{position:absolute;width:40%;height:20%;will-change:top,left;}"]
      }));
    }
  }]);

  return Well;
}(_react.Component);

var _default = Well;
exports.default = _default;

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _SquareBlock = _interopRequireDefault(__webpack_require__(36));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var WellGrid =
/*#__PURE__*/
function (_Component) {
  _inherits(WellGrid, _Component);

  function WellGrid() {
    _classCallCheck(this, WellGrid);

    return _possibleConstructorReturn(this, _getPrototypeOf(WellGrid).apply(this, arguments));
  }

  _createClass(WellGrid, [{
    key: "shouldComponentUpdate",

    /**
     * Grid rendering for the Tetrominoes that landed inside the Well.
     */
    value: function shouldComponentUpdate(nextProps) {
      return nextProps.grid !== this.props.grid || nextProps.blocksCleared !== this.props.blocksCleared || nextProps.blocksPending !== this.props.blocksPending;
    }
  }, {
    key: "renderGridBlock",
    value: function renderGridBlock(block, row, col) {
      var grid = this.props.grid;
      var rows = grid.length;
      var cols = grid[0].length;
      var widthPercent = 100 / cols;
      var heightPercent = 100 / rows;
      return _react.default.createElement("div", {
        key: block[0],
        style: {
          width: "".concat(widthPercent, "%"),
          height: "".concat(heightPercent, "%"),
          top: "".concat(row * heightPercent, "%"),
          left: "".concat(col * widthPercent, "%")
        },
        className: "jsx-845335831" + " " + "grid-square-block"
      }, _react.default.createElement(_SquareBlock.default, {
        color: block[1]
      }), _react.default.createElement(_style.default, {
        styleId: "845335831",
        css: [".grid-square-block.jsx-845335831{position:absolute;-webkit-transition:top 0.1s linear;transition:top 0.1s linear;}"]
      }));
    }
  }, {
    key: "renderGridBlocks",
    value: function renderGridBlocks() {
      var _this = this;

      var _this$props = this.props,
          grid = _this$props.grid,
          blocksCleared = _this$props.blocksCleared,
          blocksPending = _this$props.blocksPending;
      var blocks = [];
      var rows = grid.length;
      grid.forEach(function (rowBlocks, rowIndex) {
        rowBlocks.forEach(function (block, colIndex) {
          if (block) {
            blocks.push(_this.renderGridBlock(block, rowIndex, colIndex));
          }
        });
      }); // Cleared blocks transition top-to-bottom, outside the visible grid well

      blocksCleared.forEach(function (rowBlocks, rowIndex) {
        rowBlocks.forEach(function (block, colIndex) {
          if (block) {
            blocks.push(_this.renderGridBlock(block, rows + rowIndex, colIndex));
          }
        });
      }); // Pending blocks transition bottom-to-top, inside the visible grid well

      blocksPending.forEach(function (rowBlocks, rowIndex) {
        rowBlocks.forEach(function (block, colIndex) {
          if (block) {
            blocks.push(_this.renderGridBlock(block, rows + rowIndex, colIndex));
          }
        });
      });
      return blocks;
    }
  }, {
    key: "render",
    value: function render() {
      return _react.default.createElement("div", {
        className: "jsx-1915500079" + " " + "well-grid"
      }, this.renderGridBlocks(), _react.default.createElement(_style.default, {
        styleId: "1915500079",
        css: [".well-grid.jsx-1915500079{position:absolute;width:100%;height:100%;}"]
      }));
    }
  }]);

  return WellGrid;
}(_react.Component);

var _default = WellGrid;
exports.default = _default;

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Flash =
/*#__PURE__*/
function (_Component) {
  _inherits(Flash, _Component);

  function Flash() {
    _classCallCheck(this, Flash);

    return _possibleConstructorReturn(this, _getPrototypeOf(Flash).apply(this, arguments));
  }

  _createClass(Flash, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          children = _this$props.children,
          player = _this$props.player;
      var classesYay = ['flash'];
      var classesNay = ['flash'];

      if (player) {
        if (player.flashYay) {
          classesYay.push("yay-".concat(player.flashYay));
        }

        if (player.flashNay) {
          classesNay.push("nay-".concat(player.flashNay));
        }
      }

      return _react.default.createElement("div", {
        className: "jsx-124931555" + " " + (classesYay.join(' ') || "")
      }, _react.default.createElement("div", {
        className: "jsx-124931555" + " " + (classesNay.join(' ') || "")
      }, children), _react.default.createElement(_style.default, {
        styleId: "124931555",
        css: [".flash.jsx-124931555{position:absolute;top:0;bottom:0;left:0;right:0;}", ".yay-a.jsx-124931555{-webkit-animation:greenFlashA-jsx-124931555 0.5s;animation:greenFlashA-jsx-124931555 0.5s;}", ".yay-b.jsx-124931555{-webkit-animation:greenFlashB-jsx-124931555 0.5s;animation:greenFlashB-jsx-124931555 0.5s;}", "@-webkit-keyframes greenFlashA-jsx-124931555{from{background:rgba(81,196,60,0.5);}to{background:rgba(81,196,60,0);}}", "@keyframes greenFlashA-jsx-124931555{from{background:rgba(81,196,60,0.5);}to{background:rgba(81,196,60,0);}}", "@-webkit-keyframes greenFlashB-jsx-124931555{from{background:rgba(81,196,60,0.5);}to{background:rgba(81,196,60,0);}}", "@keyframes greenFlashB-jsx-124931555{from{background:rgba(81,196,60,0.5);}to{background:rgba(81,196,60,0);}}", ".nay-a.jsx-124931555{-webkit-animation:redFlashA-jsx-124931555 0.5s;animation:redFlashA-jsx-124931555 0.5s;}", ".nay-b.jsx-124931555{-webkit-animation:redFlashB-jsx-124931555 0.5s;animation:redFlashB-jsx-124931555 0.5s;}", "@-webkit-keyframes redFlashA-jsx-124931555{from{background:rgba(232,65,56,0.3);}to{background:rgba(232,65,56,0);}}", "@keyframes redFlashA-jsx-124931555{from{background:rgba(232,65,56,0.3);}to{background:rgba(232,65,56,0);}}", "@-webkit-keyframes redFlashB-jsx-124931555{from{background:rgba(232,65,56,0.3);}to{background:rgba(232,65,56,0);}}", "@keyframes redFlashB-jsx-124931555{from{background:rgba(232,65,56,0.3);}to{background:rgba(232,65,56,0);}}"]
      }));
    }
  }]);

  return Flash;
}(_react.Component);

var _default = Flash;
exports.default = _default;

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Quake =
/*#__PURE__*/
function (_Component) {
  _inherits(Quake, _Component);

  function Quake() {
    _classCallCheck(this, Quake);

    return _possibleConstructorReturn(this, _getPrototypeOf(Quake).apply(this, arguments));
  }

  _createClass(Quake, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          children = _this$props.children,
          player1 = _this$props.player1,
          player2 = _this$props.player2;
      var classes1 = ['quake'];
      var classes2 = ['quake'];

      if (player1 && player1.quake) {
        classes1.push("quake-".concat(player1.quake));
      }

      if (player2 && player2.quake) {
        classes2.push("quake-".concat(player2.quake));
      }

      return _react.default.createElement("div", {
        className: "jsx-2611135330" + " " + (classes1.join(' ') || "")
      }, _react.default.createElement("div", {
        className: "jsx-2611135330" + " " + (classes2.join(' ') || "")
      }, children), _react.default.createElement(_style.default, {
        styleId: "2611135330",
        css: [".quake.jsx-2611135330{position:absolute;top:0;bottom:0;left:0;right:0;}", ".quake-a1.jsx-2611135330{-webkit-animation:quakeA1-jsx-2611135330 0.5s ease-out;animation:quakeA1-jsx-2611135330 0.5s ease-out;-webkit-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);transform:translate3d(0,0,0);}", ".quake-a2.jsx-2611135330{-webkit-animation:quakeA2-jsx-2611135330 0.5s ease-out;animation:quakeA2-jsx-2611135330 0.5s ease-out;-webkit-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);transform:translate3d(0,0,0);}", ".quake-a3.jsx-2611135330{-webkit-animation:quakeA3-jsx-2611135330 0.5s ease-out;animation:quakeA3-jsx-2611135330 0.5s ease-out;-webkit-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);transform:translate3d(0,0,0);}", ".quake-a4.jsx-2611135330{-webkit-animation:quakeA4-jsx-2611135330 0.5s ease-out;animation:quakeA4-jsx-2611135330 0.5s ease-out;-webkit-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);transform:translate3d(0,0,0);}", ".quake-b1.jsx-2611135330{-webkit-animation:quakeB1-jsx-2611135330 0.5s ease-out;animation:quakeB1-jsx-2611135330 0.5s ease-out;-webkit-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);transform:translate3d(0,0,0);}", ".quake-b2.jsx-2611135330{-webkit-animation:quakeB2-jsx-2611135330 0.5s ease-out;animation:quakeB2-jsx-2611135330 0.5s ease-out;-webkit-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);transform:translate3d(0,0,0);}", ".quake-b3.jsx-2611135330{-webkit-animation:quakeB3-jsx-2611135330 0.5s ease-out;animation:quakeB3-jsx-2611135330 0.5s ease-out;-webkit-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);transform:translate3d(0,0,0);}", ".quake-b4.jsx-2611135330{-webkit-animation:quakeB4-jsx-2611135330 0.5s ease-out;animation:quakeB4-jsx-2611135330 0.5s ease-out;-webkit-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);transform:translate3d(0,0,0);}", "@-webkit-keyframes quakeA1-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-0.25px,0);-ms-transform:translate3d(0,-0.25px,0);transform:translate3d(0,-0.25px,0);}25%{-webkit-transform:translate3d(0,0.5px,0);-ms-transform:translate3d(0,0.5px,0);transform:translate3d(0,0.5px,0);}37.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}50%{-webkit-transform:translate3d(0,1px,0);-ms-transform:translate3d(0,1px,0);transform:translate3d(0,1px,0);}62.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}75%{-webkit-transform:translate3d(0,0.5px,0);-ms-transform:translate3d(0,0.5px,0);transform:translate3d(0,0.5px,0);}87.5%{-webkit-transform:translate3d(0,-0.25px,0);-ms-transform:translate3d(0,-0.25px,0);transform:translate3d(0,-0.25px,0);}}", "@keyframes quakeA1-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-0.25px,0);-ms-transform:translate3d(0,-0.25px,0);transform:translate3d(0,-0.25px,0);}25%{-webkit-transform:translate3d(0,0.5px,0);-ms-transform:translate3d(0,0.5px,0);transform:translate3d(0,0.5px,0);}37.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}50%{-webkit-transform:translate3d(0,1px,0);-ms-transform:translate3d(0,1px,0);transform:translate3d(0,1px,0);}62.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}75%{-webkit-transform:translate3d(0,0.5px,0);-ms-transform:translate3d(0,0.5px,0);transform:translate3d(0,0.5px,0);}87.5%{-webkit-transform:translate3d(0,-0.25px,0);-ms-transform:translate3d(0,-0.25px,0);transform:translate3d(0,-0.25px,0);}}", "@-webkit-keyframes quakeA2-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-0.5px,0);-ms-transform:translate3d(0,-0.5px,0);transform:translate3d(0,-0.5px,0);}25%{-webkit-transform:translate3d(0,1px,0);-ms-transform:translate3d(0,1px,0);transform:translate3d(0,1px,0);}37.5%{-webkit-transform:translate3d(0,-2px,0);-ms-transform:translate3d(0,-2px,0);transform:translate3d(0,-2px,0);}50%{-webkit-transform:translate3d(0,2px,0);-ms-transform:translate3d(0,2px,0);transform:translate3d(0,2px,0);}62.5%{-webkit-transform:translate3d(0,-2px,0);-ms-transform:translate3d(0,-2px,0);transform:translate3d(0,-2px,0);}75%{-webkit-transform:translate3d(0,1px,0);-ms-transform:translate3d(0,1px,0);transform:translate3d(0,1px,0);}87.5%{-webkit-transform:translate3d(0,-0.5px,0);-ms-transform:translate3d(0,-0.5px,0);transform:translate3d(0,-0.5px,0);}}", "@keyframes quakeA2-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-0.5px,0);-ms-transform:translate3d(0,-0.5px,0);transform:translate3d(0,-0.5px,0);}25%{-webkit-transform:translate3d(0,1px,0);-ms-transform:translate3d(0,1px,0);transform:translate3d(0,1px,0);}37.5%{-webkit-transform:translate3d(0,-2px,0);-ms-transform:translate3d(0,-2px,0);transform:translate3d(0,-2px,0);}50%{-webkit-transform:translate3d(0,2px,0);-ms-transform:translate3d(0,2px,0);transform:translate3d(0,2px,0);}62.5%{-webkit-transform:translate3d(0,-2px,0);-ms-transform:translate3d(0,-2px,0);transform:translate3d(0,-2px,0);}75%{-webkit-transform:translate3d(0,1px,0);-ms-transform:translate3d(0,1px,0);transform:translate3d(0,1px,0);}87.5%{-webkit-transform:translate3d(0,-0.5px,0);-ms-transform:translate3d(0,-0.5px,0);transform:translate3d(0,-0.5px,0);}}", "@-webkit-keyframes quakeA3-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-0.75px,0);-ms-transform:translate3d(0,-0.75px,0);transform:translate3d(0,-0.75px,0);}25%{-webkit-transform:translate3d(0,1.5px,0);-ms-transform:translate3d(0,1.5px,0);transform:translate3d(0,1.5px,0);}37.5%{-webkit-transform:translate3d(0,-3px,0);-ms-transform:translate3d(0,-3px,0);transform:translate3d(0,-3px,0);}50%{-webkit-transform:translate3d(0,3px,0);-ms-transform:translate3d(0,3px,0);transform:translate3d(0,3px,0);}62.5%{-webkit-transform:translate3d(0,-3px,0);-ms-transform:translate3d(0,-3px,0);transform:translate3d(0,-3px,0);}75%{-webkit-transform:translate3d(0,1.5px,0);-ms-transform:translate3d(0,1.5px,0);transform:translate3d(0,1.5px,0);}87.5%{-webkit-transform:translate3d(0,-0.75px,0);-ms-transform:translate3d(0,-0.75px,0);transform:translate3d(0,-0.75px,0);}}", "@keyframes quakeA3-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-0.75px,0);-ms-transform:translate3d(0,-0.75px,0);transform:translate3d(0,-0.75px,0);}25%{-webkit-transform:translate3d(0,1.5px,0);-ms-transform:translate3d(0,1.5px,0);transform:translate3d(0,1.5px,0);}37.5%{-webkit-transform:translate3d(0,-3px,0);-ms-transform:translate3d(0,-3px,0);transform:translate3d(0,-3px,0);}50%{-webkit-transform:translate3d(0,3px,0);-ms-transform:translate3d(0,3px,0);transform:translate3d(0,3px,0);}62.5%{-webkit-transform:translate3d(0,-3px,0);-ms-transform:translate3d(0,-3px,0);transform:translate3d(0,-3px,0);}75%{-webkit-transform:translate3d(0,1.5px,0);-ms-transform:translate3d(0,1.5px,0);transform:translate3d(0,1.5px,0);}87.5%{-webkit-transform:translate3d(0,-0.75px,0);-ms-transform:translate3d(0,-0.75px,0);transform:translate3d(0,-0.75px,0);}}", "@-webkit-keyframes quakeA4-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}25%{-webkit-transform:translate3d(0,2px,0);-ms-transform:translate3d(0,2px,0);transform:translate3d(0,2px,0);}37.5%{-webkit-transform:translate3d(0,-4px,0);-ms-transform:translate3d(0,-4px,0);transform:translate3d(0,-4px,0);}50%{-webkit-transform:translate3d(0,4px,0);-ms-transform:translate3d(0,4px,0);transform:translate3d(0,4px,0);}62.5%{-webkit-transform:translate3d(0,-4px,0);-ms-transform:translate3d(0,-4px,0);transform:translate3d(0,-4px,0);}75%{-webkit-transform:translate3d(0,2px,0);-ms-transform:translate3d(0,2px,0);transform:translate3d(0,2px,0);}87.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}}", "@keyframes quakeA4-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}25%{-webkit-transform:translate3d(0,2px,0);-ms-transform:translate3d(0,2px,0);transform:translate3d(0,2px,0);}37.5%{-webkit-transform:translate3d(0,-4px,0);-ms-transform:translate3d(0,-4px,0);transform:translate3d(0,-4px,0);}50%{-webkit-transform:translate3d(0,4px,0);-ms-transform:translate3d(0,4px,0);transform:translate3d(0,4px,0);}62.5%{-webkit-transform:translate3d(0,-4px,0);-ms-transform:translate3d(0,-4px,0);transform:translate3d(0,-4px,0);}75%{-webkit-transform:translate3d(0,2px,0);-ms-transform:translate3d(0,2px,0);transform:translate3d(0,2px,0);}87.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}}", "@-webkit-keyframes quakeB1-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-0.25px,0);-ms-transform:translate3d(0,-0.25px,0);transform:translate3d(0,-0.25px,0);}25%{-webkit-transform:translate3d(0,0.5px,0);-ms-transform:translate3d(0,0.5px,0);transform:translate3d(0,0.5px,0);}37.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}50%{-webkit-transform:translate3d(0,1px,0);-ms-transform:translate3d(0,1px,0);transform:translate3d(0,1px,0);}62.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}75%{-webkit-transform:translate3d(0,0.5px,0);-ms-transform:translate3d(0,0.5px,0);transform:translate3d(0,0.5px,0);}87.5%{-webkit-transform:translate3d(0,-0.25px,0);-ms-transform:translate3d(0,-0.25px,0);transform:translate3d(0,-0.25px,0);}}", "@keyframes quakeB1-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-0.25px,0);-ms-transform:translate3d(0,-0.25px,0);transform:translate3d(0,-0.25px,0);}25%{-webkit-transform:translate3d(0,0.5px,0);-ms-transform:translate3d(0,0.5px,0);transform:translate3d(0,0.5px,0);}37.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}50%{-webkit-transform:translate3d(0,1px,0);-ms-transform:translate3d(0,1px,0);transform:translate3d(0,1px,0);}62.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}75%{-webkit-transform:translate3d(0,0.5px,0);-ms-transform:translate3d(0,0.5px,0);transform:translate3d(0,0.5px,0);}87.5%{-webkit-transform:translate3d(0,-0.25px,0);-ms-transform:translate3d(0,-0.25px,0);transform:translate3d(0,-0.25px,0);}}", "@-webkit-keyframes quakeB2-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-0.5px,0);-ms-transform:translate3d(0,-0.5px,0);transform:translate3d(0,-0.5px,0);}25%{-webkit-transform:translate3d(0,1px,0);-ms-transform:translate3d(0,1px,0);transform:translate3d(0,1px,0);}37.5%{-webkit-transform:translate3d(0,-2px,0);-ms-transform:translate3d(0,-2px,0);transform:translate3d(0,-2px,0);}50%{-webkit-transform:translate3d(0,2px,0);-ms-transform:translate3d(0,2px,0);transform:translate3d(0,2px,0);}62.5%{-webkit-transform:translate3d(0,-2px,0);-ms-transform:translate3d(0,-2px,0);transform:translate3d(0,-2px,0);}75%{-webkit-transform:translate3d(0,1px,0);-ms-transform:translate3d(0,1px,0);transform:translate3d(0,1px,0);}87.5%{-webkit-transform:translate3d(0,-0.5px,0);-ms-transform:translate3d(0,-0.5px,0);transform:translate3d(0,-0.5px,0);}}", "@keyframes quakeB2-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-0.5px,0);-ms-transform:translate3d(0,-0.5px,0);transform:translate3d(0,-0.5px,0);}25%{-webkit-transform:translate3d(0,1px,0);-ms-transform:translate3d(0,1px,0);transform:translate3d(0,1px,0);}37.5%{-webkit-transform:translate3d(0,-2px,0);-ms-transform:translate3d(0,-2px,0);transform:translate3d(0,-2px,0);}50%{-webkit-transform:translate3d(0,2px,0);-ms-transform:translate3d(0,2px,0);transform:translate3d(0,2px,0);}62.5%{-webkit-transform:translate3d(0,-2px,0);-ms-transform:translate3d(0,-2px,0);transform:translate3d(0,-2px,0);}75%{-webkit-transform:translate3d(0,1px,0);-ms-transform:translate3d(0,1px,0);transform:translate3d(0,1px,0);}87.5%{-webkit-transform:translate3d(0,-0.5px,0);-ms-transform:translate3d(0,-0.5px,0);transform:translate3d(0,-0.5px,0);}}", "@-webkit-keyframes quakeB3-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-0.75px,0);-ms-transform:translate3d(0,-0.75px,0);transform:translate3d(0,-0.75px,0);}25%{-webkit-transform:translate3d(0,1.5px,0);-ms-transform:translate3d(0,1.5px,0);transform:translate3d(0,1.5px,0);}37.5%{-webkit-transform:translate3d(0,-3px,0);-ms-transform:translate3d(0,-3px,0);transform:translate3d(0,-3px,0);}50%{-webkit-transform:translate3d(0,3px,0);-ms-transform:translate3d(0,3px,0);transform:translate3d(0,3px,0);}62.5%{-webkit-transform:translate3d(0,-3px,0);-ms-transform:translate3d(0,-3px,0);transform:translate3d(0,-3px,0);}75%{-webkit-transform:translate3d(0,1.5px,0);-ms-transform:translate3d(0,1.5px,0);transform:translate3d(0,1.5px,0);}87.5%{-webkit-transform:translate3d(0,-0.75px,0);-ms-transform:translate3d(0,-0.75px,0);transform:translate3d(0,-0.75px,0);}}", "@keyframes quakeB3-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-0.75px,0);-ms-transform:translate3d(0,-0.75px,0);transform:translate3d(0,-0.75px,0);}25%{-webkit-transform:translate3d(0,1.5px,0);-ms-transform:translate3d(0,1.5px,0);transform:translate3d(0,1.5px,0);}37.5%{-webkit-transform:translate3d(0,-3px,0);-ms-transform:translate3d(0,-3px,0);transform:translate3d(0,-3px,0);}50%{-webkit-transform:translate3d(0,3px,0);-ms-transform:translate3d(0,3px,0);transform:translate3d(0,3px,0);}62.5%{-webkit-transform:translate3d(0,-3px,0);-ms-transform:translate3d(0,-3px,0);transform:translate3d(0,-3px,0);}75%{-webkit-transform:translate3d(0,1.5px,0);-ms-transform:translate3d(0,1.5px,0);transform:translate3d(0,1.5px,0);}87.5%{-webkit-transform:translate3d(0,-0.75px,0);-ms-transform:translate3d(0,-0.75px,0);transform:translate3d(0,-0.75px,0);}}", "@-webkit-keyframes quakeB4-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}25%{-webkit-transform:translate3d(0,2px,0);-ms-transform:translate3d(0,2px,0);transform:translate3d(0,2px,0);}37.5%{-webkit-transform:translate3d(0,-4px,0);-ms-transform:translate3d(0,-4px,0);transform:translate3d(0,-4px,0);}50%{-webkit-transform:translate3d(0,4px,0);-ms-transform:translate3d(0,4px,0);transform:translate3d(0,4px,0);}62.5%{-webkit-transform:translate3d(0,-4px,0);-ms-transform:translate3d(0,-4px,0);transform:translate3d(0,-4px,0);}75%{-webkit-transform:translate3d(0,2px,0);-ms-transform:translate3d(0,2px,0);transform:translate3d(0,2px,0);}87.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}}", "@keyframes quakeB4-jsx-2611135330{12.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}25%{-webkit-transform:translate3d(0,2px,0);-ms-transform:translate3d(0,2px,0);transform:translate3d(0,2px,0);}37.5%{-webkit-transform:translate3d(0,-4px,0);-ms-transform:translate3d(0,-4px,0);transform:translate3d(0,-4px,0);}50%{-webkit-transform:translate3d(0,4px,0);-ms-transform:translate3d(0,4px,0);transform:translate3d(0,4px,0);}62.5%{-webkit-transform:translate3d(0,-4px,0);-ms-transform:translate3d(0,-4px,0);transform:translate3d(0,-4px,0);}75%{-webkit-transform:translate3d(0,2px,0);-ms-transform:translate3d(0,2px,0);transform:translate3d(0,2px,0);}87.5%{-webkit-transform:translate3d(0,-1px,0);-ms-transform:translate3d(0,-1px,0);transform:translate3d(0,-1px,0);}}"]
      }));
    }
  }]);

  return Quake;
}(_react.Component);

var _default = Quake;
exports.default = _default;

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.joinGame = joinGame;
exports.playerReady = playerReady;
exports.playerPause = playerPause;
exports.drop = drop;
exports.moveLeft = moveLeft;
exports.moveRight = moveRight;
exports.rotate = rotate;
exports.enableAcceleration = enableAcceleration;
exports.disableAcceleration = disableAcceleration;
exports.appendPendingBlocks = appendPendingBlocks;
exports.ping = ping;

var _game = __webpack_require__(4);

var _curGame = __webpack_require__(13);

var _curUser = __webpack_require__(15);

function joinGame(gameId, user) {
  return {
    type: 'JOIN_GAME',
    payload: {
      actionId: getActionId(0),
      prevActionId: 0,
      userId: user.id,
      gameId: gameId,
      user: user
    }
  };
}

function playerReady() {
  return decorateGameAction(function (_ref) {
    var actionId = _ref.actionId,
        prevActionId = _ref.prevActionId,
        userId = _ref.userId,
        gameId = _ref.gameId;
    return {
      type: 'PLAYER_READY',
      payload: {
        actionId: actionId,
        prevActionId: prevActionId,
        userId: userId,
        gameId: gameId
      }
    };
  });
}

function playerPause() {
  return decorateGameAction(function (_ref2) {
    var actionId = _ref2.actionId,
        prevActionId = _ref2.prevActionId,
        userId = _ref2.userId,
        gameId = _ref2.gameId;
    return {
      type: 'PLAYER_PAUSE',
      payload: {
        actionId: actionId,
        prevActionId: prevActionId,
        userId: userId,
        gameId: gameId
      }
    };
  });
}

function drop(rows) {
  return decorateGameAction(function (_ref3) {
    var actionId = _ref3.actionId,
        prevActionId = _ref3.prevActionId,
        userId = _ref3.userId,
        gameId = _ref3.gameId;
    return {
      type: 'DROP',
      payload: {
        actionId: actionId,
        prevActionId: prevActionId,
        userId: userId,
        gameId: gameId,
        rows: rows
      }
    };
  });
}

function moveLeft() {
  return decorateGameAction(function (_ref4) {
    var actionId = _ref4.actionId,
        prevActionId = _ref4.prevActionId,
        userId = _ref4.userId,
        gameId = _ref4.gameId;
    return {
      type: 'MOVE_LEFT',
      payload: {
        actionId: actionId,
        prevActionId: prevActionId,
        userId: userId,
        gameId: gameId
      }
    };
  });
}

function moveRight() {
  return decorateGameAction(function (_ref5) {
    var actionId = _ref5.actionId,
        prevActionId = _ref5.prevActionId,
        userId = _ref5.userId,
        gameId = _ref5.gameId;
    return {
      type: 'MOVE_RIGHT',
      payload: {
        actionId: actionId,
        prevActionId: prevActionId,
        userId: userId,
        gameId: gameId
      }
    };
  });
}

function rotate() {
  return decorateGameAction(function (_ref6) {
    var actionId = _ref6.actionId,
        prevActionId = _ref6.prevActionId,
        userId = _ref6.userId,
        gameId = _ref6.gameId;
    return {
      type: 'ROTATE',
      payload: {
        actionId: actionId,
        prevActionId: prevActionId,
        userId: userId,
        gameId: gameId
      }
    };
  });
}

function enableAcceleration() {
  return decorateGameAction(function (_ref7) {
    var actionId = _ref7.actionId,
        prevActionId = _ref7.prevActionId,
        userId = _ref7.userId,
        gameId = _ref7.gameId;
    return {
      type: 'ENABLE_ACCELERATION',
      payload: {
        actionId: actionId,
        prevActionId: prevActionId,
        userId: userId,
        gameId: gameId
      }
    };
  });
}

function disableAcceleration() {
  return decorateGameAction(function (_ref8) {
    var actionId = _ref8.actionId,
        prevActionId = _ref8.prevActionId,
        userId = _ref8.userId,
        gameId = _ref8.gameId;
    return {
      type: 'DISABLE_ACCELERATION',
      payload: {
        actionId: actionId,
        prevActionId: prevActionId,
        userId: userId,
        gameId: gameId
      }
    };
  });
}

function appendPendingBlocks() {
  return decorateGameAction(function (_ref9) {
    var actionId = _ref9.actionId,
        prevActionId = _ref9.prevActionId,
        userId = _ref9.userId,
        gameId = _ref9.gameId;
    return {
      type: 'APPEND_PENDING_BLOCKS',
      payload: {
        actionId: actionId,
        prevActionId: prevActionId,
        userId: userId,
        gameId: gameId
      }
    };
  });
}

function ping() {
  return decorateGameAction(function (_ref10) {
    var actionId = _ref10.actionId,
        prevActionId = _ref10.prevActionId,
        userId = _ref10.userId,
        gameId = _ref10.gameId;
    return {
      type: 'PING',
      payload: {
        actionId: actionId,
        prevActionId: prevActionId,
        userId: userId,
        gameId: gameId,
        time: Date.now()
      }
    };
  });
}

function decorateGameAction(fn) {
  return function (dispatch, getState) {
    var state = getState();
    var userId = (0, _curUser.getCurUser)(state).id;
    var curGame = (0, _curGame.getCurGame)(state);
    var player = (0, _game.getPlayer)(curGame, userId);
    var prevActionId = player.lastActionId;
    var actionId = getActionId(prevActionId);
    return dispatch(fn({
      actionId: actionId,
      prevActionId: prevActionId,
      userId: userId,
      gameId: curGame.id
    }));
  };
}

function getActionId(prevActionId) {
  // Ensure action ids never duplicate (only relevant if two actions occur
  // within the same millisecond)
  return Math.max(Date.now(), prevActionId + 1);
}

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _regenerator = _interopRequireDefault(__webpack_require__(6));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _reactRedux = __webpack_require__(10);

var _global = __webpack_require__(12);

var _user = __webpack_require__(61);

var _api = __webpack_require__(8);

var _Button = _interopRequireDefault(__webpack_require__(2));

var _FlatrisIntro = _interopRequireDefault(__webpack_require__(62));

var _Multiplayer = _interopRequireDefault(__webpack_require__(63));

var _ZeroSum = _interopRequireDefault(__webpack_require__(64));

var _HowToPlay = _interopRequireDefault(__webpack_require__(65));

var _Screen = _interopRequireDefault(__webpack_require__(5));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ONBOARDING_SCREENS = {
  intro: _FlatrisIntro.default,
  '1vs1': _Multiplayer.default,
  '0sum': _ZeroSum.default,
  howto: _HowToPlay.default
};
var ONBOARDING_STEPS = Object.keys(ONBOARDING_SCREENS);

var Auth =
/*#__PURE__*/
function (_Component) {
  _inherits(Auth, _Component);

  function Auth() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Auth);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Auth)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      name: '',
      pendingAuth: false,
      user: null,
      onboardingStep: 'intro',
      hasSubmitted: false
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleInputRef", function (node) {
      _this.nameField = node;

      _this.focusOnNameField();
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "focusOnNameField", function () {
      if (_this.nameField && _this.props.jsReady) {
        _this.nameField.focus();
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleNameChange", function (e) {
      _this.setState({
        name: e.target.value
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleGo",
    /*#__PURE__*/
    function () {
      var _ref = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(e) {
        var name, user;
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                e.preventDefault();
                name = _this.state.name;

                if (!name) {
                  _context.next = 8;
                  break;
                }

                _this.setState({
                  pendingAuth: true
                });

                _context.next = 6;
                return (0, _api.createUserSession)(name);

              case 6:
                user = _context.sent;

                // Add the user to state and delay adding it to the app state until the
                // user seems the onboarding screen
                _this.setState({
                  pendingAuth: false,
                  user: user
                });

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }());

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleOnboardingNext", function () {
      var auth = _this.props.auth;
      var _this$state = _this.state,
          user = _this$state.user,
          onboardingStep = _this$state.onboardingStep;

      if (!user) {
        throw new Error('Onboarding with missing user');
      }

      var step = ONBOARDING_STEPS.indexOf(onboardingStep);
      var nextStep = ONBOARDING_STEPS[step + 1];

      if (nextStep) {
        _this.setState({
          onboardingStep: nextStep
        });
      } else {
        _this.setState({
          hasSubmitted: true
        });

        auth(user);
      }
    });

    return _this;
  }

  _createClass(Auth, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (this.props.jsReady && !prevProps.jsReady) {
        this.focusOnNameField();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var jsReady = this.props.jsReady;
      var _this$state2 = this.state,
          name = _this$state2.name,
          pendingAuth = _this$state2.pendingAuth,
          user = _this$state2.user,
          onboardingStep = _this$state2.onboardingStep,
          hasSubmitted = _this$state2.hasSubmitted; // Greet user with onboarding after they authenticate

      if (user) {
        var OnboardingScreen = ONBOARDING_SCREENS[onboardingStep];
        return _react.default.createElement(OnboardingScreen, {
          disabled: hasSubmitted,
          onNext: this.handleOnboardingNext
        });
      }

      return _react.default.createElement("form", {
        onSubmit: this.handleGo,
        className: "jsx-1986851731"
      }, _react.default.createElement(_Screen.default, {
        title: "One sec...",
        message: _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", {
          onClick: this.focusOnNameField,
          className: "jsx-1986851731"
        }, "Enter your name"), _react.default.createElement("p", {
          className: "jsx-1986851731"
        }, _react.default.createElement("input", {
          type: "text",
          value: name,
          onChange: this.handleNameChange,
          disabled: !jsReady || pendingAuth,
          ref: this.handleInputRef,
          placeholder: "Monkey",
          maxLength: _user.MAX_NAME_LENGTH,
          className: "jsx-1986851731"
        })), _react.default.createElement("p", {
          className: "jsx-1986851731"
        }, _react.default.createElement("small", {
          className: "jsx-1986851731"
        }, "Fake names allowed ;-)"))),
        actions: [_react.default.createElement(_Button.default, {
          type: "submit",
          disabled: !jsReady || !name || pendingAuth
        }, "Enter")]
      }), _react.default.createElement(_style.default, {
        styleId: "1986851731",
        css: ["input.jsx-1986851731{box-sizing:border-box;width:100%;padding:0.8em;border:0;background:#fff;color:#666;box-shadow:inset 0.25em 0.25em 0 0 rgba(0,0,0,0.2);font-size:1em;text-transform:uppercase;outline:none;-webkit-transition:box-shadow 0.5s;transition:box-shadow 0.5s;}", "input.jsx-1986851731:focus{box-shadow:inset 0.25em 0.25em 0 0 #3993d0;color:#333;}", "input.jsx-1986851731::-webkit-input-placeholder{color:#ccc;font-weight:300;}", "input.jsx-1986851731::-moz-placeholder{color:#ccc;font-weight:300;}", "input.jsx-1986851731:-ms-input-placeholder{color:#ccc;font-weight:300;}", "input.jsx-1986851731::placeholder{color:#ccc;font-weight:300;}", "input.jsx-1986851731:disabled{cursor:not-allowed;}"]
      }));
    }
  }]);

  return Auth;
}(_react.Component);

function mapStateToProps(_ref2) {
  var jsReady = _ref2.jsReady;
  return {
    jsReady: jsReady
  };
}

var mapDispatchToProps = {
  auth: _global.auth
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(Auth);

exports.default = _default;

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MAX_NAME_LENGTH = void 0;
var MAX_NAME_LENGTH = 14;
exports.MAX_NAME_LENGTH = MAX_NAME_LENGTH;

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(0));

var _Screen = _interopRequireDefault(__webpack_require__(5));

var _Button = _interopRequireDefault(__webpack_require__(2));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var FlatrisIntro =
/*#__PURE__*/
function (_Component) {
  _inherits(FlatrisIntro, _Component);

  function FlatrisIntro() {
    _classCallCheck(this, FlatrisIntro);

    return _possibleConstructorReturn(this, _getPrototypeOf(FlatrisIntro).apply(this, arguments));
  }

  _createClass(FlatrisIntro, [{
    key: "render",
    value: function render() {
      var onNext = this.props.onNext;
      return _react.default.createElement(_Screen.default, {
        title: "Greetings!",
        message: _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", null, _react.default.createElement("span", {
          className: "highlight"
        }, "Flatris is a fast-paced", _react.default.createElement("br", null), "two-player game.", _react.default.createElement("br", null))), _react.default.createElement("p", null, _react.default.createElement("strong", null, "Geometric shapes"), _react.default.createElement("br", null), "fall from above until", _react.default.createElement("br", null), "they hit the ground."), _react.default.createElement("p", null, "Place them to", ' ', _react.default.createElement("strong", null, "form", _react.default.createElement("br", null), "lines"), ", which clears", _react.default.createElement("br", null), "blocks and buys time!")),
        actions: [_react.default.createElement(_Button.default, {
          onClick: onNext
        }, "I see")]
      });
    }
  }]);

  return FlatrisIntro;
}(_react.Component);

exports.default = FlatrisIntro;

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(0));

var _Screen = _interopRequireDefault(__webpack_require__(5));

var _Button = _interopRequireDefault(__webpack_require__(2));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Multiplayer =
/*#__PURE__*/
function (_Component) {
  _inherits(Multiplayer, _Component);

  function Multiplayer() {
    _classCallCheck(this, Multiplayer);

    return _possibleConstructorReturn(this, _getPrototypeOf(Multiplayer).apply(this, arguments));
  }

  _createClass(Multiplayer, [{
    key: "render",
    value: function render() {
      var onNext = this.props.onNext;
      return _react.default.createElement(_Screen.default, {
        title: "One vs one",
        message: _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", null, _react.default.createElement("span", {
          className: "highlight"
        }, "Both players receive", _react.default.createElement("br", null), "the same sequence of", _react.default.createElement("br", null), "geometric shapes.")), _react.default.createElement("p", null, "Each of you controls a", _react.default.createElement("br", null), "separate game. You see", _react.default.createElement("br", null), _react.default.createElement("strong", null, "your rival's shadow"), ",", _react.default.createElement("br", null), "but you don't interact."), _react.default.createElement("p", null, "Except in one way...")),
        actions: [_react.default.createElement(_Button.default, {
          onClick: onNext
        }, "OK...")]
      });
    }
  }]);

  return Multiplayer;
}(_react.Component);

exports.default = Multiplayer;

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(0));

var _Screen = _interopRequireDefault(__webpack_require__(5));

var _Button = _interopRequireDefault(__webpack_require__(2));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var ZeroSum =
/*#__PURE__*/
function (_Component) {
  _inherits(ZeroSum, _Component);

  function ZeroSum() {
    _classCallCheck(this, ZeroSum);

    return _possibleConstructorReturn(this, _getPrototypeOf(ZeroSum).apply(this, arguments));
  }

  _createClass(ZeroSum, [{
    key: "render",
    value: function render() {
      var onNext = this.props.onNext;
      return _react.default.createElement(_Screen.default, {
        title: "Zero sum",
        message: _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", null, _react.default.createElement("span", {
          className: "highlight"
        }, "Every line you clear is", _react.default.createElement("br", null), "added to your opponent", _react.default.createElement("br", null), "and vice versa.")), _react.default.createElement("p", null, _react.default.createElement("strong", null, "This is not a friendly", _react.default.createElement("br", null), "match."), ' ', "One player's", _react.default.createElement("br", null), "loss is the other's win!"), _react.default.createElement("p", null, "Play fast to survive.")),
        actions: [_react.default.createElement(_Button.default, {
          onClick: onNext
        }, "Aha!")]
      });
    }
  }]);

  return ZeroSum;
}(_react.Component);

exports.default = ZeroSum;

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(0));

var _Screen = _interopRequireDefault(__webpack_require__(5));

var _Button = _interopRequireDefault(__webpack_require__(2));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var HowToPlay =
/*#__PURE__*/
function (_Component) {
  _inherits(HowToPlay, _Component);

  function HowToPlay() {
    _classCallCheck(this, HowToPlay);

    return _possibleConstructorReturn(this, _getPrototypeOf(HowToPlay).apply(this, arguments));
  }

  _createClass(HowToPlay, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          disabled = _this$props.disabled,
          onNext = _this$props.onNext;
      return _react.default.createElement(_Screen.default, {
        title: "How to play",
        message: _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", null, "Press ", _react.default.createElement("strong", null, "left"), " or ", _react.default.createElement("strong", null, "right"), " to", _react.default.createElement("br", null), "move the falling shape.", _react.default.createElement("br", null), "Press ", _react.default.createElement("strong", null, "up"), " to rotate and", _react.default.createElement("br", null), _react.default.createElement("strong", null, "down"), " to drop faster."), _react.default.createElement("p", null, _react.default.createElement("span", {
          className: "highlight"
        }, "Use the keyboard on", _react.default.createElement("br", null), "desktop or the screen", _react.default.createElement("br", null), "buttons on mobile.")), _react.default.createElement("p", null, "Ready to kick ass?")),
        actions: [_react.default.createElement(_Button.default, {
          disabled: disabled,
          onClick: onNext
        }, "Got it")]
      });
    }
  }]);

  return HowToPlay;
}(_react.Component);

exports.default = HowToPlay;

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Shake =
/*#__PURE__*/
function (_Component) {
  _inherits(Shake, _Component);

  function Shake() {
    _classCallCheck(this, Shake);

    return _possibleConstructorReturn(this, _getPrototypeOf(Shake).apply(this, arguments));
  }

  _createClass(Shake, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          children = _this$props.children,
          time = _this$props.time;

      if (!time) {
        return children;
      }

      return _react.default.createElement("div", {
        key: time,
        className: "jsx-109255757" + " " + "shake"
      }, children, _react.default.createElement(_style.default, {
        styleId: "109255757",
        css: [".shake.jsx-109255757{position:absolute;top:0;bottom:0;left:0;right:0;-webkit-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0);transform:translate3d(0,0,0);-webkit-animation:shake-jsx-109255757 0.5s ease-out;animation:shake-jsx-109255757 0.5s ease-out;}", "@-webkit-keyframes shake-jsx-109255757{12.5%{-webkit-transform:translate3d(2px,-1px,0);-ms-transform:translate3d(2px,-1px,0);transform:translate3d(2px,-1px,0);}25%{-webkit-transform:translate3d(-4px,2px,0);-ms-transform:translate3d(-4px,2px,0);transform:translate3d(-4px,2px,0);}37.5%{-webkit-transform:translate3d(4px,-4px,0);-ms-transform:translate3d(4px,-4px,0);transform:translate3d(4px,-4px,0);}50%{-webkit-transform:translate3d(-4px,4px,0);-ms-transform:translate3d(-4px,4px,0);transform:translate3d(-4px,4px,0);}62.5%{-webkit-transform:translate3d(2px,-4px,0);-ms-transform:translate3d(2px,-4px,0);transform:translate3d(2px,-4px,0);}75%{-webkit-transform:translate3d(-1px,2px,0);-ms-transform:translate3d(-1px,2px,0);transform:translate3d(-1px,2px,0);}87.5%{-webkit-transform:translate3d(-1px,-1px,0);-ms-transform:translate3d(-1px,-1px,0);transform:translate3d(-1px,-1px,0);}}", "@keyframes shake-jsx-109255757{12.5%{-webkit-transform:translate3d(2px,-1px,0);-ms-transform:translate3d(2px,-1px,0);transform:translate3d(2px,-1px,0);}25%{-webkit-transform:translate3d(-4px,2px,0);-ms-transform:translate3d(-4px,2px,0);transform:translate3d(-4px,2px,0);}37.5%{-webkit-transform:translate3d(4px,-4px,0);-ms-transform:translate3d(4px,-4px,0);transform:translate3d(4px,-4px,0);}50%{-webkit-transform:translate3d(-4px,4px,0);-ms-transform:translate3d(-4px,4px,0);transform:translate3d(-4px,4px,0);}62.5%{-webkit-transform:translate3d(2px,-4px,0);-ms-transform:translate3d(2px,-4px,0);transform:translate3d(2px,-4px,0);}75%{-webkit-transform:translate3d(-1px,2px,0);-ms-transform:translate3d(-1px,2px,0);transform:translate3d(-1px,2px,0);}87.5%{-webkit-transform:translate3d(-1px,-1px,0);-ms-transform:translate3d(-1px,-1px,0);transform:translate3d(-1px,-1px,0);}}"]
      }));
    }
  }]);

  return Shake;
}(_react.Component);

exports.default = Shake;

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isMobileDevice = isMobileDevice;
exports.getPointerDownEvent = getPointerDownEvent;
exports.getPointerUpEvent = getPointerUpEvent;

function isMobileDevice() {
  return getWindow() && 'ontouchstart' in getWindow();
}

function getPointerDownEvent() {
  // SSR
  if (!getWindow()) {
    return null;
  }

  return isMobileDevice() ? 'onTouchStart' : 'onMouseDown';
}

function getPointerUpEvent() {
  // SSR
  if (!getWindow()) {
    return null;
  }

  return isMobileDevice() ? 'onTouchEnd' : 'onMouseUp';
}

function getWindow() {
  return global.window;
}

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Left;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireDefault(__webpack_require__(0));

var _PointerButton = _interopRequireDefault(__webpack_require__(52));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function Left(_ref) {
  var onPress = _ref.onPress,
      rest = _objectWithoutProperties(_ref, ["onPress"]);

  return _react.default.createElement(_PointerButton.default, _extends({}, rest, {
    bgColor: "#fff",
    hoverEffect: false,
    onPress: onPress
  }), _react.default.createElement("svg", {
    viewBox: "0 0 24 24",
    className: "jsx-952678030"
  }, _react.default.createElement("path", {
    d: "M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z",
    className: "jsx-952678030"
  })), _react.default.createElement(_style.default, {
    styleId: "952678030",
    css: ["svg.jsx-952678030{fill:#34495f;-webkit-transform:scale(0.6);-ms-transform:scale(0.6);transform:scale(0.6);}", ".button:disabled svg.jsx-952678030{fill:rgba(52,73,95,0.6);}"]
  }));
}

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Right;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireDefault(__webpack_require__(0));

var _PointerButton = _interopRequireDefault(__webpack_require__(52));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function Right(_ref) {
  var onPress = _ref.onPress,
      rest = _objectWithoutProperties(_ref, ["onPress"]);

  return _react.default.createElement(_PointerButton.default, _extends({}, rest, {
    bgColor: "#fff",
    hoverEffect: false,
    onPress: onPress
  }), _react.default.createElement("svg", {
    viewBox: "0 0 24 24",
    className: "jsx-952678030"
  }, _react.default.createElement("path", {
    d: "M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z",
    className: "jsx-952678030"
  })), _react.default.createElement(_style.default, {
    styleId: "952678030",
    css: ["svg.jsx-952678030{fill:#34495f;-webkit-transform:scale(0.6);-ms-transform:scale(0.6);transform:scale(0.6);}", ".button:disabled svg.jsx-952678030{fill:rgba(52,73,95,0.6);}"]
  }));
}

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Rotate;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireDefault(__webpack_require__(0));

var _PointerButton = _interopRequireDefault(__webpack_require__(52));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function Rotate(_ref) {
  var onPress = _ref.onPress,
      rest = _objectWithoutProperties(_ref, ["onPress"]);

  return _react.default.createElement(_PointerButton.default, _extends({}, rest, {
    bgColor: "#fff",
    hoverEffect: false,
    onPress: onPress
  }), _react.default.createElement("svg", {
    viewBox: "0 0 24 24",
    className: "jsx-1513795926"
  }, _react.default.createElement("path", {
    d: "M12 5V1L7 6l5 5V7c3.31 0 6 2.69 6 6s-2.69 6-6 6-6-2.69-6-6H4c0 4.42 3.58 8 8 8s8-3.58 8-8-3.58-8-8-8z",
    className: "jsx-1513795926"
  })), _react.default.createElement(_style.default, {
    styleId: "1513795926",
    css: ["svg.jsx-1513795926{fill:#34495f;-webkit-transform:scale(0.6);-ms-transform:scale(0.6);transform:scale(0.6);-webkit-transform-origin:50% 50%;-ms-transform-origin:50% 50%;transform-origin:50% 50%;-webkit-transform:scale(-0.6,0.6);-ms-transform:scale(-0.6,0.6);transform:scale(-0.6,0.6);}", ".button:disabled svg.jsx-1513795926{fill:rgba(52,73,95,0.6);}"]
  }));
}

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Drop;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireDefault(__webpack_require__(0));

var _PointerButton = _interopRequireDefault(__webpack_require__(52));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function Drop(_ref) {
  var onPress = _ref.onPress,
      rest = _objectWithoutProperties(_ref, ["onPress"]);

  return _react.default.createElement(_PointerButton.default, _extends({}, rest, {
    bgColor: "#fff",
    hoverEffect: false,
    onPress: onPress
  }), _react.default.createElement("svg", {
    viewBox: "0 0 24 24",
    className: "jsx-952678030"
  }, _react.default.createElement("path", {
    d: "M20 12l-1.41-1.41L13 16.17V4h-2v12.17l-5.58-5.59L4 12l8 8 8-8z",
    className: "jsx-952678030"
  })), _react.default.createElement(_style.default, {
    styleId: "952678030",
    css: ["svg.jsx-952678030{fill:#34495f;-webkit-transform:scale(0.6);-ms-transform:scale(0.6);transform:scale(0.6);}", ".button:disabled svg.jsx-952678030{fill:rgba(52,73,95,0.6);}"]
  }));
}

/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(0));

var _url = __webpack_require__(96);

var _CopyButton = _interopRequireDefault(__webpack_require__(37));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var CopyGameLinkButton =
/*#__PURE__*/
function (_Component) {
  _inherits(CopyGameLinkButton, _Component);

  function CopyGameLinkButton() {
    _classCallCheck(this, CopyGameLinkButton);

    return _possibleConstructorReturn(this, _getPrototypeOf(CopyGameLinkButton).apply(this, arguments));
  }

  _createClass(CopyGameLinkButton, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          disabled = _this$props.disabled,
          gameId = _this$props.gameId;
      return _react.default.createElement(_CopyButton.default, {
        disabled: disabled,
        copyText: (0, _url.getShareUrl)(gameId),
        defaultLabel: "Copy link",
        successLabel: "Link copied!",
        errorLabel: "Copy failed :("
      });
    }
  }]);

  return CopyGameLinkButton;
}(_react.Component);

exports.default = CopyGameLinkButton;

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _Button = _interopRequireDefault(__webpack_require__(2));

var _CopyGameLinkButton = _interopRequireDefault(__webpack_require__(72));

var _Screen = _interopRequireDefault(__webpack_require__(5));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Invite =
/*#__PURE__*/
function (_Component) {
  _inherits(Invite, _Component);

  function Invite() {
    _classCallCheck(this, Invite);

    return _possibleConstructorReturn(this, _getPrototypeOf(Invite).apply(this, arguments));
  }

  _createClass(Invite, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          disabled = _this$props.disabled,
          gameId = _this$props.gameId,
          onPlay = _this$props.onPlay;
      return _react.default.createElement(_Screen.default, {
        title: "1+1=3",
        message: _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", {
          className: "jsx-1832570087"
        }, _react.default.createElement("strong", {
          className: "jsx-1832570087"
        }, "Playing with a ", _react.default.createElement("del", {
          className: "jsx-1832570087"
        }, "friend"), _react.default.createElement("br", {
          className: "jsx-1832570087"
        }), "rival is more fun!")), _react.default.createElement("div", {
          className: "jsx-1832570087" + " " + "copy"
        }, _react.default.createElement(_CopyGameLinkButton.default, {
          disabled: disabled,
          gameId: gameId
        })), _react.default.createElement("p", {
          className: "jsx-1832570087"
        }, "Send the link and", _react.default.createElement("br", {
          className: "jsx-1832570087"
        }), "warm up until the", _react.default.createElement("br", {
          className: "jsx-1832570087"
        }), "other person arrives."), _react.default.createElement(_style.default, {
          styleId: "1832570087",
          css: [".copy.jsx-1832570087{position:relative;height:calc(100% / 11 * 2);margin:1em 0;font-size:1.1em;}"]
        })),
        actions: [_react.default.createElement(_Button.default, {
          disabled: disabled,
          onClick: onPlay
        }, "Play")]
      });
    }
  }]);

  return Invite;
}(_react.Component);

exports.default = Invite;

/***/ }),
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(87);


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _regenerator = _interopRequireDefault(__webpack_require__(6));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _router = _interopRequireDefault(__webpack_require__(51));

var _reactRedux = __webpack_require__(10);

var _nextReduxWrapper = _interopRequireDefault(__webpack_require__(23));

var _store = __webpack_require__(24);

var _global = __webpack_require__(12);

var _api = __webpack_require__(8);

var _SocketProvider = __webpack_require__(46);

var _Title = _interopRequireDefault(__webpack_require__(19));

var _Layout = _interopRequireDefault(__webpack_require__(40));

var _CurGameOfElse = _interopRequireDefault(__webpack_require__(88));

var _FlatrisGame = _interopRequireDefault(__webpack_require__(89));

var _Error = _interopRequireDefault(__webpack_require__(34));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var JoinPage =
/*#__PURE__*/
function (_Component) {
  _inherits(JoinPage, _Component);

  function JoinPage() {
    _classCallCheck(this, JoinPage);

    return _possibleConstructorReturn(this, _getPrototypeOf(JoinPage).apply(this, arguments));
  }

  _createClass(JoinPage, [{
    key: "render",
    value: function render() {
      var statusCode = this.props.statusCode;

      if (statusCode) {
        return _react.default.createElement(_Layout.default, null, _react.default.createElement(_Error.default, {
          statusCode: statusCode
        }));
      }

      return _react.default.createElement(_Layout.default, null, _react.default.createElement(_Title.default, null, "Play Flatris"), _react.default.createElement(_reactRedux.ReactReduxContext.Consumer, null, function (_ref) {
        var store = _ref.store;
        return _react.default.createElement(_SocketProvider.SocketProvider, {
          store: store
        }, _react.default.createElement(_CurGameOfElse.default, {
          else: function _else() {
            return _router.default.replace('/');
          }
        }, _react.default.createElement(_FlatrisGame.default, null)));
      }));
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(_ref2) {
        var req, res, query, store, getState, dispatch, gameId, _getState, games, game, statusCode;

        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                req = _ref2.req, res = _ref2.res, query = _ref2.query, store = _ref2.store;

                if (!req) {
                  _context.next = 4;
                  break;
                }

                _context.next = 4;
                return (0, _api.addCurUserToState)(req, store);

              case 4:
                getState = store.getState, dispatch = store.dispatch;
                gameId = query.g;
                _getState = getState(), games = _getState.games; // No need to request game again if it's already in store (client-side)
                // Action backfill mechanism will take care of updating the game's state

                if (games[gameId]) {
                  _context.next = 22;
                  break;
                }

                _context.prev = 8;
                _context.next = 11;
                return (0, _api.getGame)(gameId);

              case 11:
                game = _context.sent;
                dispatch((0, _global.addGame)(game));
                _context.next = 20;
                break;

              case 15:
                _context.prev = 15;
                _context.t0 = _context["catch"](8);
                // TODO: Identify and signal 500s differently
                statusCode = 404; // Both client and server render appropriate 404 page, but server will
                // also set 404 status code if this page is opened directly

                if (res) {
                  res.statusCode = statusCode;
                }

                return _context.abrupt("return", {
                  statusCode: statusCode
                });

              case 20:
                _context.next = 23;
                break;

              case 22:
                // Strip game effects to prevent triggering the last effect whenever
                // opening an existing game
                dispatch((0, _global.stripGameEffects)());

              case 23:
                dispatch((0, _global.openGame)(gameId));
                return _context.abrupt("return", {
                  statusCode: false
                });

              case 25:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[8, 15]]);
      }));

      function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      }

      return getInitialProps;
    }()
  }]);

  return JoinPage;
}(_react.Component);

var _default = (0, _nextReduxWrapper.default)(_store.createStore)(JoinPage);

exports.default = _default;

/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = __webpack_require__(0);

var _reactRedux = __webpack_require__(10);

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// The purpose of this component is to ensure the child component is only
// rendered when state.curGame is valid, and to allow the parent component to
// respond accordingly otherwise, when state.curGame isn't valid.
// One practical example is when a user leaves a game page open for a long time
// and the game gets removed from the server due to inactivity. Upon user's
// return, the cur game will point to a missing game. Because we capture this,
// however, we can safely redirect to / when this happens.
var CurGameOfElse =
/*#__PURE__*/
function (_Component) {
  _inherits(CurGameOfElse, _Component);

  function CurGameOfElse() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, CurGameOfElse);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(CurGameOfElse)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      elseTriggered: false
    });

    return _this;
  }

  _createClass(CurGameOfElse, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.curGameOrElse();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      this.curGameOrElse();
    }
  }, {
    key: "render",
    value: function render() {
      return hasCurGame(this.props) ? this.props.children : null;
    }
  }, {
    key: "curGameOrElse",
    value: function curGameOrElse() {
      if (!this.state.elseTriggered && !hasCurGame(this.props)) {
        this.setState({
          elseTriggered: true
        }, this.props.else);
      }
    }
  }]);

  return CurGameOfElse;
}(_react.Component);

function hasCurGame(_ref) {
  var games = _ref.games,
      curGame = _ref.curGame;
  return Boolean(curGame && games[curGame]);
}

var mapStateToProps = function mapStateToProps(_ref2) {
  var games = _ref2.games,
      curGame = _ref2.curGame;
  return {
    games: games,
    curGame: curGame
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps)(CurGameOfElse);

exports.default = _default;

/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _reactRedux = __webpack_require__(10);

var _lodash = __webpack_require__(7);

var _keys = __webpack_require__(90);

var _timeouts = __webpack_require__(53);

var _game = __webpack_require__(4);

var _curGame = __webpack_require__(13);

var _game2 = __webpack_require__(59);

var _gameFrame = __webpack_require__(91);

var _SocketConnect = __webpack_require__(50);

var _events = __webpack_require__(67);

var _GameContainer = _interopRequireDefault(__webpack_require__(39));

var _Button = _interopRequireDefault(__webpack_require__(2));

var _GamePreview = _interopRequireDefault(__webpack_require__(54));

var _PortraitControls = _interopRequireDefault(__webpack_require__(93));

var _LandscapeControls = _interopRequireDefault(__webpack_require__(94));

var _FadeIn = _interopRequireDefault(__webpack_require__(35));

var _Auth = _interopRequireDefault(__webpack_require__(60));

var _NewGame = _interopRequireDefault(__webpack_require__(95));

var _Invite = _interopRequireDefault(__webpack_require__(73));

var _JoinGame = _interopRequireDefault(__webpack_require__(97));

var _GameFull = _interopRequireDefault(__webpack_require__(98));

var _GetReady = _interopRequireDefault(__webpack_require__(99));

var _WaitingForOther = _interopRequireDefault(__webpack_require__(100));

var _GameOver = _interopRequireDefault(__webpack_require__(101));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// NOTE: This component will crash if state.curGame isn't populated!
var FlatrisGame =
/*#__PURE__*/
function (_Component) {
  _inherits(FlatrisGame, _Component);

  function FlatrisGame() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, FlatrisGame);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(FlatrisGame)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      isWatching: false,
      isMobile: false
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "keepAlive", function () {
      var _this$props = _this.props,
          game = _this$props.game,
          keepGameAlive = _this$props.keepGameAlive;
      keepGameAlive(game.id);
      var timeout = _timeouts.GAME_INACTIVE_TIMEOUT - 1000;
      _this.keepAliveTimeout = setTimeout(_this.keepAlive, timeout);
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleSelectP2", function () {
      var _this$props2 = _this.props,
          curUser = _this$props2.curUser,
          game = _this$props2.game,
          playerPause = _this$props2.playerPause;

      var _getCurPlayer = (0, _game.getCurPlayer)(game, curUser),
          status = _getCurPlayer.status;

      if (game.players.length > 1) {
        throw new Error('Game already has two players');
      } // READY status for a solo player means the game is running
      // Is status is LOST the player will see the invite screen anyway


      if (status === 'READY') {
        playerPause();
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleWatch", function () {
      _this.setState({
        isWatching: true
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleMenu", function () {
      _this.setState({
        isWatching: false
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleJoin", function () {
      var _this$props3 = _this.props,
          curUser = _this$props3.curUser,
          game = _this$props3.game,
          joinGame = _this$props3.joinGame;

      if (curUser) {
        joinGame(game.id, curUser);
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleReady", function () {
      _this.props.playerReady();
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handlePing", function () {
      var _this$props4 = _this.props,
          curUser = _this$props4.curUser,
          game = _this$props4.game;
      var hasJoined = (0, _game.isPlayer)(game, curUser);

      if (hasJoined) {
        var _getCurPlayer2 = (0, _game.getCurPlayer)(game, curUser),
            _ping = _getCurPlayer2.ping;

        var now = Date.now(); // Prevent flooding the network with hysterical pings

        if (!_ping || now - _ping > 500) {
          _this.props.ping();
        }
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "tEnableAcceleration", (0, _lodash.throttle)(_this.props.enableAcceleration, _keys.KEY_DELAY));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "tRotate", (0, _lodash.throttle)(_this.props.rotate, _keys.KEY_DELAY));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "tMoveLeft", (0, _lodash.throttle)(_this.props.moveLeft, _keys.KEY_DELAY));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "tMoveRight", (0, _lodash.throttle)(_this.props.moveRight, _keys.KEY_DELAY));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleKeyDown", function (e) {
      // Prevent page from scrolling when pressing arrow keys
      if ([_keys.UP, _keys.DOWN, _keys.LEFT, _keys.RIGHT, _keys.SPACE].indexOf(e.keyCode) !== -1) {
        e.preventDefault();
      }

      switch (e.keyCode) {
        case _keys.DOWN:
        case _keys.SPACE:
          _this.tRotate.cancel();

          _this.tMoveLeft.cancel();

          _this.tMoveRight.cancel();

          _this.tEnableAcceleration();

          break;

        case _keys.UP:
          _this.tEnableAcceleration.cancel();

          _this.tMoveLeft.cancel();

          _this.tMoveRight.cancel();

          _this.tRotate();

          break;

        case _keys.LEFT:
          _this.tEnableAcceleration.cancel();

          _this.tRotate.cancel();

          _this.tMoveRight.cancel();

          _this.tMoveLeft();

          break;

        case _keys.RIGHT:
          _this.tEnableAcceleration.cancel();

          _this.tRotate.cancel();

          _this.tMoveLeft.cancel();

          _this.tMoveRight();

          break;

        default:
      }
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleKeyUp", function (e) {
      if (e.keyCode === _keys.DOWN) {
        _this.tEnableAcceleration.cancel();

        _this.tRotate.cancel();

        _this.tMoveLeft.cancel();

        _this.tMoveRight.cancel();

        var _disableAcceleration = _this.props.disableAcceleration;

        _disableAcceleration();
      }
    });

    return _this;
  }

  _createClass(FlatrisGame, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props5 = this.props,
          curUser = _this$props5.curUser,
          game = _this$props5.game,
          subscribe = _this$props5.subscribe; // Subscribe to socket messages for this game

      subscribe(game.id); // Start game if playing user returned after possibly being disconnected

      if ((0, _game.isPlayer)(game, curUser) && (0, _game.allPlayersReady)(game)) {
        this.startGame();
      }

      if ((0, _events.isMobileDevice)()) {
        this.setState({
          isMobile: true
        });
      }

      this.keepAlive();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var prevGame = prevProps.game;
      var _this$props6 = this.props,
          curUser = _this$props6.curUser,
          game = _this$props6.game,
          appendPendingBlocks = _this$props6.appendPendingBlocks;

      if (curUser && (0, _game.isPlayer)(game, curUser)) {
        if ((0, _game.allPlayersReady)(game) && !(0, _game.allPlayersReady)(prevGame)) {
          this.startGame();
        } else if (!(0, _game.allPlayersReady)(game) && (0, _game.allPlayersReady)(prevGame)) {
          this.stopGame();
        }

        var player = (0, _game.getPlayer)(game, curUser.id);

        if (player.blocksPending.length) {
          // Ensure enemy blocks have been rendered "under the fold", before
          // transitioning them into the visible wall. It's weird, but without
          // setTimeout() pending blocks don't get rendered before they are
          // appended (Redux action batching?) and the transition doesn't occur
          setTimeout(function () {
            appendPendingBlocks();
          });
        }
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.stopGame();
      this.cancelKeepAlive();
    }
  }, {
    key: "cancelKeepAlive",
    value: function cancelKeepAlive() {
      if (this.keepAliveTimeout) {
        clearTimeout(this.keepAliveTimeout);
      }
    }
  }, {
    key: "attachKeyEvents",
    value: function attachKeyEvents() {
      // window isn't available on the server side, but nor is componentDidMount
      // called on the server
      window.addEventListener('keydown', this.handleKeyDown);
      window.addEventListener('keyup', this.handleKeyUp);
    }
  }, {
    key: "detachKeyEvents",
    value: function detachKeyEvents() {
      // window isn't available on the server side, but nor is componentWillUnmount
      // called on the server
      window.removeEventListener('keydown', this.handleKeyDown);
      window.removeEventListener('keyup', this.handleKeyUp);
    }
  }, {
    key: "startGame",
    value: function startGame() {
      var _this$props7 = this.props,
          drop = _this$props7.drop,
          runGameFrame = _this$props7.runGameFrame;
      this.attachKeyEvents();
      runGameFrame(drop);
    }
  }, {
    key: "stopGame",
    value: function stopGame() {
      this.detachKeyEvents();
      (0, _gameFrame.cancelGameFrame)();
    }
  }, {
    key: "renderScreens",
    value: function renderScreens() {
      var _this$props8 = this.props,
          jsReady = _this$props8.jsReady,
          curUser = _this$props8.curUser,
          game = _this$props8.game,
          backfills = _this$props8.backfills;
      var isWatching = this.state.isWatching;
      var hasJoined = (0, _game.isPlayer)(game, curUser);
      var disabled = Boolean(!jsReady || backfills[game.id]); // P1 is the current user's player, P2 is the other (in multiplayer games)

      var curPlayer = (0, _game.getCurPlayer)(game, curUser);
      var otherPlayer = (0, _game.getOtherPlayer)(game, curPlayer);

      if (isWatching) {
        return this.renderScreen(this.renderMenuBtn(), false);
      }

      if (!hasJoined && otherPlayer) {
        return this.renderScreen(_react.default.createElement(_GameFull.default, {
          disabled: disabled,
          onWatch: this.handleWatch
        }));
      }

      if (!curUser) {
        return this.renderScreen(_react.default.createElement(_Auth.default, null));
      }

      if (!hasJoined) {
        return this.renderScreen(_react.default.createElement(_JoinGame.default, {
          disabled: disabled,
          onWatch: this.handleWatch,
          onJoin: this.handleJoin
        }));
      } // No screen when current user joined and game is running


      if ((0, _game.allPlayersReady)(game)) {
        return null;
      }

      if (curPlayer.status === 'LOST' || curPlayer.status === 'WON') {
        return this.renderScreen(_react.default.createElement(_GameOver.default, {
          disabled: disabled,
          curUser: curUser,
          game: game,
          onRestart: this.handleReady
        }));
      }

      if (!otherPlayer) {
        if (curPlayer.status === 'PAUSE') {
          return this.renderScreen(_react.default.createElement(_Invite.default, {
            disabled: disabled,
            gameId: game.id,
            onPlay: this.handleReady
          }));
        } // curPlayer status is 'PENDING', because if it wouldn've been 'READY'
        // allPlayersReady(game) would've returned true


        return this.renderScreen(_react.default.createElement(_NewGame.default, {
          disabled: disabled,
          gameId: game.id,
          onPlay: this.handleReady
        }));
      }

      if (curPlayer.status === 'READY') {
        return this.renderScreen(_react.default.createElement(_WaitingForOther.default, {
          disabled: disabled,
          curPlayer: curPlayer,
          onPing: this.handlePing
        }));
      } // curPlayer.status === 'PENDING'


      return this.renderScreen(_react.default.createElement(_GetReady.default, {
        disabled: disabled,
        otherPlayer: otherPlayer,
        onReady: this.handleReady
      }));
    }
  }, {
    key: "renderScreen",
    value: function renderScreen(content) {
      var showOverlay = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
      return _react.default.createElement("div", {
        className: _style.default.dynamic([["1975499457", [showOverlay ? 'rgba(236, 240, 241, 0.85)' : 'transparent']]]) + " " + "screen-container"
      }, content, _react.default.createElement(_style.default, {
        styleId: "1975499457",
        css: [".screen-container.__jsx-style-dynamic-selector{position:absolute;top:0;bottom:0;left:0;right:calc(100% / 16 * 6);background:".concat(showOverlay ? 'rgba(236, 240, 241, 0.85)' : 'transparent', ";}")],
        dynamic: [showOverlay ? 'rgba(236, 240, 241, 0.85)' : 'transparent']
      }));
    }
  }, {
    key: "renderMenuBtn",
    value: function renderMenuBtn() {
      return _react.default.createElement("div", {
        className: "jsx-313741800" + " " + "menu-btn"
      }, _react.default.createElement(_Button.default, {
        onClick: this.handleMenu
      }, "Menu"), _react.default.createElement(_style.default, {
        styleId: "313741800",
        css: [".menu-btn.jsx-313741800{position:absolute;top:calc(100% / 20 * 17);left:calc(100% / 10 * 5);width:calc(100% / 10 * 4);height:calc(100% / 20 * 2);font-size:1.1em;opacity:0.5;-webkit-transition:opacity 0.5s;transition:opacity 0.5s;}", ".menu-btn.jsx-313741800:hover{opacity:1;}"]
      }));
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props9 = this.props,
          jsReady = _this$props9.jsReady,
          curUser = _this$props9.curUser,
          game = _this$props9.game;
      var isMobile = this.state.isMobile;
      var hasJoined = (0, _game.isPlayer)(game, curUser);
      var curPlayer = (0, _game.getCurPlayer)(game, curUser);
      var otherPlayer = (0, _game.getOtherPlayer)(game, curPlayer);
      return _react.default.createElement(_GameContainer.default, {
        outer: isMobile && _react.default.createElement(_FadeIn.default, null, _react.default.createElement(_LandscapeControls.default, null))
      }, _react.default.createElement(_GamePreview.default, {
        curUser: curUser,
        game: game,
        screen: this.renderScreens(),
        onSelectP2: // I know, right...
        // We only want to enable this on READY state (when game is running
        // for solo player), because in other states (new game and game
        // over) we already show the invite screen.
        jsReady && hasJoined && curPlayer.status === 'READY' && !otherPlayer ? this.handleSelectP2 : undefined,
        showFooter: true
      }), _react.default.createElement(_PortraitControls.default, null));
    }
  }]);

  return FlatrisGame;
}(_react.Component);

var mapStateToProps = function mapStateToProps(state) {
  var jsReady = state.jsReady,
      curUser = state.curUser,
      backfills = state.backfills;
  return {
    jsReady: jsReady,
    curUser: curUser,
    game: (0, _curGame.getCurGame)(state),
    backfills: backfills
  };
};

var mapDispatchToProps = {
  runGameFrame: _gameFrame.runGameFrame
};
var syncActions = {
  joinGame: _game2.joinGame,
  playerReady: _game2.playerReady,
  playerPause: _game2.playerPause,
  drop: _game2.drop,
  moveLeft: _game2.moveLeft,
  moveRight: _game2.moveRight,
  rotate: _game2.rotate,
  enableAcceleration: _game2.enableAcceleration,
  disableAcceleration: _game2.disableAcceleration,
  appendPendingBlocks: _game2.appendPendingBlocks,
  ping: _game2.ping
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _SocketConnect.withSocket)(FlatrisGame, syncActions));

exports.default = _default;

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KEY_DELAY = exports.SPACE = exports.RIGHT = exports.LEFT = exports.DOWN = exports.UP = void 0;
var UP = 38;
exports.UP = UP;
var DOWN = 40;
exports.DOWN = DOWN;
var LEFT = 37;
exports.LEFT = LEFT;
var RIGHT = 39;
exports.RIGHT = RIGHT;
var SPACE = 32;
exports.SPACE = SPACE;
var ACTIONS_PER_SEC = 20;
var KEY_DELAY = 1000 / ACTIONS_PER_SEC;
exports.KEY_DELAY = KEY_DELAY;

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.runGameFrame = runGameFrame;
exports.cancelGameFrame = cancelGameFrame;

var _raf = _interopRequireDefault(__webpack_require__(92));

var _grid = __webpack_require__(17);

var _game = __webpack_require__(4);

var _curGame = __webpack_require__(13);

var _curUser = __webpack_require__(15);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* global performance */
var now = typeof performance !== 'undefined' && typeof performance.now === 'function' ? function () {
  return performance.now();
} : function () {
  return Date.now();
};
var FPS = 60;
var frameDuration = 1000 / FPS; // This changes too fast (60fps) to keep it in the store's state

var yProgress = 0;

function runGameFrame(drop) {
  return function (dispatch, getState) {
    cancelGameFrame();
    scheduleFrame(function (frames) {
      if (frames > 3) {
        console.warn("Perf degrated: ".concat(frames - 1, " frames skipped."));
      }

      var state = getState();
      var userId = (0, _curUser.getCurUser)(state).id;
      var game = (0, _curGame.getCurGame)(state);
      var dropFrames = game.dropFrames;
      var player = (0, _game.getPlayer)(game, userId);
      var dropAcceleration = player.dropAcceleration; // Stop animation when game ended (players change status to WON/LOST)

      if (!(0, _game.allPlayersReady)(game)) {
        return;
      }

      var framesPerDrop = dropAcceleration ? _grid.DROP_FRAMES_ACCELERATED : dropFrames;
      yProgress += frames / framesPerDrop;

      if (yProgress > 1) {
        var _rows = Math.floor(yProgress);

        drop(_rows);
        yProgress %= 1;
      }

      dispatch(runGameFrame(drop));
    });
  };
}

function cancelGameFrame() {
  _raf.default.cancel(animationHandle);
}

var animationHandle;
var timeBegin;

function scheduleFrame(cb) {
  timeBegin = now();
  animationHandle = (0, _raf.default)(function () {
    var timeEnd = now();
    cb((timeEnd - timeBegin) / frameDuration);
  });
}

/***/ }),
/* 92 */
/***/ (function(module, exports) {

module.exports = require("raf");

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _reactRedux = __webpack_require__(10);

var _game = __webpack_require__(59);

var _SocketConnect = __webpack_require__(50);

var _game2 = __webpack_require__(4);

var _curGame = __webpack_require__(13);

var _Left = _interopRequireDefault(__webpack_require__(68));

var _Right = _interopRequireDefault(__webpack_require__(69));

var _Rotate = _interopRequireDefault(__webpack_require__(70));

var _Drop = _interopRequireDefault(__webpack_require__(71));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var PortraitControls =
/*#__PURE__*/
function (_Component) {
  _inherits(PortraitControls, _Component);

  function PortraitControls() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, PortraitControls);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(PortraitControls)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleLeftPress", function (e) {
      e.preventDefault();
      var moveLeft = _this.props.moveLeft;
      moveLeft();
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleRightPress", function (e) {
      e.preventDefault();
      var moveRight = _this.props.moveRight;
      moveRight();
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleRotatePress", function (e) {
      e.preventDefault();
      var rotate = _this.props.rotate;
      rotate();
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleDropPress", function (e) {
      e.preventDefault();
      var enableAcceleration = _this.props.enableAcceleration;
      enableAcceleration();
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleDropRelease", function (e) {
      e.preventDefault();
      var disableAcceleration = _this.props.disableAcceleration;
      disableAcceleration();
    });

    return _this;
  }

  _createClass(PortraitControls, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          curUser = _this$props.curUser,
          game = _this$props.game,
          backfills = _this$props.backfills;
      var isGameRunning = (0, _game2.isPlayer)(game, curUser) && (0, _game2.allPlayersReady)(game);
      var disabled = Boolean(!isGameRunning || backfills[game.id]);
      return _react.default.createElement("div", {
        className: "jsx-352384719" + " " + "controls"
      }, _react.default.createElement("div", {
        className: "jsx-352384719" + " " + "button"
      }, _react.default.createElement(_Rotate.default, {
        disabled: disabled,
        onPress: this.handleRotatePress
      })), _react.default.createElement("div", {
        className: "jsx-352384719" + " " + "button"
      }, _react.default.createElement(_Left.default, {
        disabled: disabled,
        onPress: this.handleLeftPress
      })), _react.default.createElement("div", {
        className: "jsx-352384719" + " " + "button"
      }, _react.default.createElement(_Right.default, {
        disabled: disabled,
        onPress: this.handleRightPress
      })), _react.default.createElement("div", {
        className: "jsx-352384719" + " " + "button"
      }, _react.default.createElement(_Drop.default, {
        disabled: disabled,
        onPress: this.handleDropPress,
        onRelease: this.handleDropRelease
      })), _react.default.createElement(_style.default, {
        styleId: "352384719",
        css: [".controls.jsx-352384719{position:absolute;top:100%;height:calc(100% / 20 * 4);left:0;right:0;}", ".controls.jsx-352384719 .button.jsx-352384719{position:relative;float:left;width:25%;height:100%;}"]
      }));
    }
  }]);

  return PortraitControls;
}(_react.Component);

var mapStateToProps = function mapStateToProps(state) {
  var curUser = state.curUser,
      backfills = state.backfills;
  return {
    curUser: curUser,
    game: (0, _curGame.getCurGame)(state),
    backfills: backfills
  };
};

var syncActions = {
  moveLeft: _game.moveLeft,
  moveRight: _game.moveRight,
  rotate: _game.rotate,
  enableAcceleration: _game.enableAcceleration,
  disableAcceleration: _game.disableAcceleration
};

var _default = (0, _reactRedux.connect)(mapStateToProps)((0, _SocketConnect.withSocket)(PortraitControls, syncActions));

exports.default = _default;

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _reactRedux = __webpack_require__(10);

var _game = __webpack_require__(59);

var _SocketConnect = __webpack_require__(50);

var _game2 = __webpack_require__(4);

var _curGame = __webpack_require__(13);

var _Left = _interopRequireDefault(__webpack_require__(68));

var _Right = _interopRequireDefault(__webpack_require__(69));

var _Rotate = _interopRequireDefault(__webpack_require__(70));

var _Drop = _interopRequireDefault(__webpack_require__(71));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LandscapeControls =
/*#__PURE__*/
function (_Component) {
  _inherits(LandscapeControls, _Component);

  function LandscapeControls() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, LandscapeControls);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(LandscapeControls)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleLeftPress", function (e) {
      e.preventDefault();
      var moveLeft = _this.props.moveLeft;
      moveLeft();
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleRightPress", function (e) {
      e.preventDefault();
      var moveRight = _this.props.moveRight;
      moveRight();
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleRotatePress", function (e) {
      e.preventDefault();
      var rotate = _this.props.rotate;
      rotate();
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleDropPress", function (e) {
      e.preventDefault();
      var enableAcceleration = _this.props.enableAcceleration;
      enableAcceleration();
    });

    return _this;
  }

  _createClass(LandscapeControls, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          curUser = _this$props.curUser,
          game = _this$props.game,
          backfills = _this$props.backfills;
      var isGameRunning = (0, _game2.isPlayer)(game, curUser) && (0, _game2.allPlayersReady)(game);
      var disabled = Boolean(!isGameRunning || backfills[game.id]);
      return _react.default.createElement(_react.Fragment, null, _react.default.createElement("div", {
        className: "jsx-2197599076" + " " + "ctrl-side left"
      }, _react.default.createElement("div", {
        className: "jsx-2197599076" + " " + "button"
      }, _react.default.createElement(_Left.default, {
        disabled: disabled,
        onPress: this.handleLeftPress
      })), _react.default.createElement("div", {
        className: "jsx-2197599076" + " " + "button"
      }, _react.default.createElement(_Rotate.default, {
        disabled: disabled,
        onPress: this.handleRotatePress
      }))), _react.default.createElement("div", {
        className: "jsx-2197599076" + " " + "ctrl-side right"
      }, _react.default.createElement("div", {
        className: "jsx-2197599076" + " " + "button"
      }, _react.default.createElement(_Right.default, {
        disabled: disabled,
        onPress: this.handleRightPress
      })), _react.default.createElement("div", {
        className: "jsx-2197599076" + " " + "button"
      }, _react.default.createElement(_Drop.default, {
        disabled: disabled,
        onPress: this.handleDropPress
      }))), _react.default.createElement(_style.default, {
        styleId: "2197599076",
        css: [".ctrl-side.jsx-2197599076{position:absolute;top:50%;-webkit-transform:translate(0,-50%);-ms-transform:translate(0,-50%);transform:translate(0,-50%);}", ".left.jsx-2197599076{left:0;}", ".right.jsx-2197599076{right:0;}", ".left.jsx-2197599076 .button.jsx-2197599076,.right.jsx-2197599076 .button.jsx-2197599076{position:relative;float:left;width:100%;height:50%;}"]
      }));
    }
  }]);

  return LandscapeControls;
}(_react.Component);

var mapStateToProps = function mapStateToProps(state) {
  var curUser = state.curUser,
      backfills = state.backfills;
  return {
    curUser: curUser,
    game: (0, _curGame.getCurGame)(state),
    backfills: backfills
  };
};

var syncActions = {
  moveLeft: _game.moveLeft,
  moveRight: _game.moveRight,
  rotate: _game.rotate,
  enableAcceleration: _game.enableAcceleration,
  disableAcceleration: _game.disableAcceleration
};

var _default = (0, _reactRedux.connect)(mapStateToProps)((0, _SocketConnect.withSocket)(LandscapeControls, syncActions));

exports.default = _default;

/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _style = _interopRequireDefault(__webpack_require__(1));

var _react = _interopRequireWildcard(__webpack_require__(0));

var _Button = _interopRequireDefault(__webpack_require__(2));

var _CopyGameLinkButton = _interopRequireDefault(__webpack_require__(72));

var _Screen = _interopRequireDefault(__webpack_require__(5));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var NewGame =
/*#__PURE__*/
function (_Component) {
  _inherits(NewGame, _Component);

  function NewGame() {
    _classCallCheck(this, NewGame);

    return _possibleConstructorReturn(this, _getPrototypeOf(NewGame).apply(this, arguments));
  }

  _createClass(NewGame, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          disabled = _this$props.disabled,
          gameId = _this$props.gameId,
          onPlay = _this$props.onPlay;
      return _react.default.createElement(_Screen.default, {
        title: "New game",
        message: _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", {
          className: "jsx-1832570087"
        }, _react.default.createElement("strong", {
          className: "jsx-1832570087"
        }, "Invite a friend to", _react.default.createElement("br", {
          className: "jsx-1832570087"
        }), "battle, or play solo.")), _react.default.createElement("div", {
          className: "jsx-1832570087" + " " + "copy"
        }, _react.default.createElement(_CopyGameLinkButton.default, {
          disabled: disabled,
          gameId: gameId
        })), _react.default.createElement("p", {
          className: "jsx-1832570087"
        }, "Send the link and", _react.default.createElement("br", {
          className: "jsx-1832570087"
        }), "warm up until the", _react.default.createElement("br", {
          className: "jsx-1832570087"
        }), "other person arrives."), _react.default.createElement(_style.default, {
          styleId: "1832570087",
          css: [".copy.jsx-1832570087{position:relative;height:calc(100% / 11 * 2);margin:1em 0;font-size:1.1em;}"]
        })),
        actions: [_react.default.createElement(_Button.default, {
          disabled: disabled,
          onClick: onPlay
        }, "Play")]
      });
    }
  }]);

  return NewGame;
}(_react.Component);

exports.default = NewGame;

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getShareUrl = getShareUrl;

/* global window */
function getShareUrl(gameId) {
  if (typeof window === 'undefined') {
    return '';
  } // NOTE: This code only works in to browser (ie. not on the server). SSR
  // will return a disabled copy button.


  var _window$location = window.location,
      protocol = _window$location.protocol,
      host = _window$location.host;
  return "".concat(protocol, "//").concat(host, "/join/").concat(gameId);
}

/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(0));

var _Button = _interopRequireDefault(__webpack_require__(2));

var _Screen = _interopRequireDefault(__webpack_require__(5));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var JoinGame =
/*#__PURE__*/
function (_Component) {
  _inherits(JoinGame, _Component);

  function JoinGame() {
    _classCallCheck(this, JoinGame);

    return _possibleConstructorReturn(this, _getPrototypeOf(JoinGame).apply(this, arguments));
  }

  _createClass(JoinGame, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          disabled = _this$props.disabled,
          onWatch = _this$props.onWatch,
          onJoin = _this$props.onJoin;
      return _react.default.createElement(_Screen.default, {
        title: "Join game",
        message: _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", null, _react.default.createElement("strong", null, "Room for one more!")), _react.default.createElement("p", null, "You can watch, or", _react.default.createElement("br", null), "you can play."), _react.default.createElement("p", null, "What will it be?")),
        actions: [_react.default.createElement(_Button.default, {
          disabled: disabled,
          onClick: onJoin
        }, "Join"), _react.default.createElement(_Button.default, {
          disabled: disabled,
          bgColor: "#fff",
          color: "#34495f",
          colorDisabled: "rgba(52, 73, 95, 0.6)",
          onClick: onWatch
        }, "Watch")]
      });
    }
  }]);

  return JoinGame;
}(_react.Component);

exports.default = JoinGame;

/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(0));

var _link = _interopRequireDefault(__webpack_require__(18));

var _Button = _interopRequireDefault(__webpack_require__(2));

var _Screen = _interopRequireDefault(__webpack_require__(5));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var GameFull =
/*#__PURE__*/
function (_Component) {
  _inherits(GameFull, _Component);

  function GameFull() {
    _classCallCheck(this, GameFull);

    return _possibleConstructorReturn(this, _getPrototypeOf(GameFull).apply(this, arguments));
  }

  _createClass(GameFull, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          disabled = _this$props.disabled,
          onWatch = _this$props.onWatch;
      return _react.default.createElement(_Screen.default, {
        title: "Join game",
        message: _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", null, _react.default.createElement("strong", null, "Game full.")), _react.default.createElement("p", null, "You can watch, or", _react.default.createElement("br", null), _react.default.createElement(_link.default, {
          href: "/"
        }, _react.default.createElement("a", null, "join another game")), ".")),
        actions: [_react.default.createElement(_Button.default, {
          disabled: true
        }, "Join"), _react.default.createElement(_Button.default, {
          disabled: disabled,
          bgColor: "#fff",
          color: "#34495f",
          colorDisabled: "rgba(52, 73, 95, 0.6)",
          onClick: onWatch
        }, "Watch")]
      });
    }
  }]);

  return GameFull;
}(_react.Component);

exports.default = GameFull;

/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(0));

var _Button = _interopRequireDefault(__webpack_require__(2));

var _Shake = _interopRequireDefault(__webpack_require__(66));

var _Screen = _interopRequireDefault(__webpack_require__(5));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var GetReady =
/*#__PURE__*/
function (_Component) {
  _inherits(GetReady, _Component);

  function GetReady() {
    _classCallCheck(this, GetReady);

    return _possibleConstructorReturn(this, _getPrototypeOf(GetReady).apply(this, arguments));
  }

  _createClass(GetReady, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          disabled = _this$props.disabled,
          otherPlayer = _this$props.otherPlayer,
          onReady = _this$props.onReady;
      return _react.default.createElement(_Screen.default, {
        title: "Get ready",
        message: _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", null, "Game starts when", _react.default.createElement("br", null), "you're both ready."), _react.default.createElement("p", null, _react.default.createElement("strong", null, "Good luck!"))),
        actions: [_react.default.createElement(_Shake.default, {
          time: otherPlayer.ping
        }, _react.default.createElement(_Button.default, {
          disabled: disabled,
          onClick: onReady
        }, "Ready"))]
      });
    }
  }]);

  return GetReady;
}(_react.Component);

exports.default = GetReady;

/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(0));

var _link = _interopRequireDefault(__webpack_require__(18));

var _Shake = _interopRequireDefault(__webpack_require__(66));

var _FadeIn = _interopRequireDefault(__webpack_require__(35));

var _Button = _interopRequireDefault(__webpack_require__(2));

var _Screen = _interopRequireDefault(__webpack_require__(5));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var WaitingForOther =
/*#__PURE__*/
function (_Component) {
  _inherits(WaitingForOther, _Component);

  function WaitingForOther() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, WaitingForOther);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(WaitingForOther)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      isOtherPlayerIdle: false
    });

    return _this;
  }

  _createClass(WaitingForOther, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      this.timeoutId = setTimeout(function () {
        _this2.setState({
          isOtherPlayerIdle: true
        });
      }, 30000);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.timeoutId) {
        clearTimeout(this.timeoutId);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          disabled = _this$props.disabled,
          curPlayer = _this$props.curPlayer,
          onPing = _this$props.onPing;
      var isOtherPlayerIdle = this.state.isOtherPlayerIdle;
      return _react.default.createElement(_Screen.default, {
        title: "Waiting...",
        message: _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", null, "Your friend is a bit", _react.default.createElement("br", null), "slower. I know..."), _react.default.createElement("p", null, _react.default.createElement("strong", null, "Ping them to hurry!")), isOtherPlayerIdle && _react.default.createElement(_FadeIn.default, null, _react.default.createElement("p", null, "Maybe your friend", _react.default.createElement("br", null), "left you hanging.", _react.default.createElement("br", null), _react.default.createElement(_link.default, {
          href: "/"
        }, _react.default.createElement("a", null, "Join another game?"))))),
        actions: [_react.default.createElement(_Shake.default, {
          time: curPlayer.ping
        }, _react.default.createElement(_Button.default, {
          disabled: disabled,
          onClick: onPing
        }, "Ping"))]
      });
    }
  }]);

  return WaitingForOther;
}(_react.Component);

exports.default = WaitingForOther;

/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(0));

var _game = __webpack_require__(4);

var _Button = _interopRequireDefault(__webpack_require__(2));

var _Shake = _interopRequireDefault(__webpack_require__(66));

var _Invite = _interopRequireDefault(__webpack_require__(73));

var _Screen = _interopRequireDefault(__webpack_require__(5));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var GameOver =
/*#__PURE__*/
function (_Component) {
  _inherits(GameOver, _Component);

  function GameOver() {
    _classCallCheck(this, GameOver);

    return _possibleConstructorReturn(this, _getPrototypeOf(GameOver).apply(this, arguments));
  }

  _createClass(GameOver, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          disabled = _this$props.disabled,
          curUser = _this$props.curUser,
          game = _this$props.game,
          onRestart = _this$props.onRestart;
      var curPlayer = (0, _game.getCurPlayer)(game, curUser);
      var otherPlayer = (0, _game.getOtherPlayer)(game, curPlayer);

      if (!otherPlayer) {
        return _react.default.createElement(_Invite.default, {
          disabled: disabled,
          gameId: game.id,
          onPlay: onRestart
        });
      }

      return _react.default.createElement(_Screen.default, {
        title: getMultiTitle(curPlayer),
        message: getMultiMessage(curPlayer, otherPlayer),
        actions: [_react.default.createElement(_Shake.default, {
          time: otherPlayer.ping
        }, _react.default.createElement(_Button.default, {
          disabled: disabled,
          onClick: onRestart
        }, "Again"))]
      });
    }
  }]);

  return GameOver;
}(_react.Component);

exports.default = GameOver;

function getMultiTitle(curPlayer) {
  return curPlayer.status === 'LOST' ? 'You lost!' : 'You won!';
}

function getMultiMessage(curPlayer, otherPlayer) {
  var maxLosses = Math.max(curPlayer.losses, otherPlayer.losses);
  var numWins = maxLosses + 1;
  var numGames = numWins * 2 - 1;
  var bestOutOfMsg = "Best ".concat(numWins, " out of ").concat(numGames, "?");

  if (curPlayer.status === 'LOST') {
    return _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", null, "Oh well... better luck", _react.default.createElement("br", null), "next time."), _react.default.createElement("p", null, _react.default.createElement("strong", null, bestOutOfMsg)));
  }

  return _react.default.createElement(_react.Fragment, null, _react.default.createElement("p", null, "You kicked ass."), _react.default.createElement("p", null, _react.default.createElement("strong", null, bestOutOfMsg)));
}

/***/ })
/******/ ]);
//# sourceMappingURL=join.js.map